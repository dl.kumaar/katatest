import Image from 'next/image';
import Link from 'next/link';
import blogs from '../../../data/blogs';

const BlogNavigator = ({ current }) => {
	const filteredBlogs = blogs.filter((item) => item.title !== '(Untitled)');

	const currentBlog = filteredBlogs.find((item) => item.slug === current.slug);
	const currentIndex = filteredBlogs.indexOf(currentBlog);

	const prevBlog = filteredBlogs[currentIndex - 1];
	const nextBlog = filteredBlogs[currentIndex + 1];

	return (
		<div className='row y-gap-30 justify-between'>
			<div className='col-auto'>
				{prevBlog && (
					// <a href='#'>
					<Link href={`/blog/article/${prevBlog.slug}`}>
						<div className='d-flex items-center'>
							<div className='icon-arrow-left text-20 mr-20' />
							<div className='text-18 fw-500'>Previous article</div>
						</div>

						<div className='text-15 ml-40 text-blue-1'>{prevBlog?.title}</div>
					</Link>
					// </a>
				)}
			</div>
			<div className='col-auto'>
				<Image
					src='/img/general/menu.svg'
					width={24}
					height={24}
					alt='image'
					className='pt-20'
				/>
			</div>
			<div className='col-auto text-right'>
				<Link href={`/blog/article/${nextBlog?.slug}`}>
					<div className='d-flex items-center justify-end'>
						<div className='text-18 fw-500'>Next article</div>
						<div className='icon-arrow-right text-20 ml-20' />
					</div>
					<div className='text-15 mr-40 text-blue-1'>{nextBlog?.title}</div>
				</Link>
			</div>
		</div>
	);
};

export default BlogNavigator;
