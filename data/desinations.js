export const destinations1 = [
	{ id: 1, city: 'Hawai', properties: '12,683' },
	{ id: 2, city: 'Istanbul', properties: '12,683' },
	{ id: 3, city: 'San Diego', properties: '12,683' },
	{ id: 4, city: 'Phuket', properties: '12,683' },
	{ id: 5, city: 'Reykjavik', properties: '12,683' },
	{ id: 6, city: 'Santorini', properties: '12,683' },
	{ id: 7, city: 'Los Angeles', properties: '12,683' },
	{ id: 8, city: 'Florence', properties: '12,683' },
	{ id: 9, city: 'Mykonos', properties: '12,683' },
	{ id: 10, city: 'Paris', properties: '12,683' },
	{ id: 11, city: 'Dubai', properties: '12,683' },
	{ id: 12, city: 'Krakow', properties: '12,683' },
	{ id: 13, city: 'Jersey', properties: '12,683' },
	{ id: 14, city: 'Prag', properties: '12,683' },
	{ id: 15, city: 'Amsterdam', properties: '12,683' },
	{ id: 16, city: 'İbiza', properties: '12,683' },
	{ id: 17, city: 'Boston', properties: '12,683' },
	{ id: 18, city: 'Florence', properties: '12,683' },
	{ id: 19, city: 'Mykonos', properties: '12,683' },
	{ id: 20, city: 'London', properties: '12,683' },
];

export const destinations2 = [
	{
		id: 1,
		hoverText: '14 Hotel - 22 Cars - 18 Tours - 95 Activity',
		city: 'New York',
		img: '/img/destinations/1/1.png',
	},
	{
		id: 2,
		hoverText: '14 Hotel - 22 Cars - 18 Tours - 95 Activity',
		city: 'London',
		img: '/img/destinations/1/2.png',
	},
	{
		id: 3,
		hoverText: '14 Hotel - 22 Cars - 18 Tours - 95 Activity',
		city: 'Barcelona',
		img: '/img/destinations/1/3.png',
	},
	{
		id: 4,
		hoverText: '14 Hotel - 22 Cars - 18 Tours - 95 Activity',
		city: 'Sydney',
		img: '/img/destinations/1/4.png',
	},
	{
		id: 5,
		hoverText: '14 Hotel - 22 Cars - 18 Tours - 95 Activity',
		city: 'Rome',
		img: '/img/destinations/1/5.png',
	},
	{
		id: 6,
		hoverText: '14 Hotel - 22 Cars - 18 Tours - 95 Activity',
		city: 'New York',
		img: '/img/destinations/1/1.png',
	},
	{
		id: 7,
		hoverText: '14 Hotel - 22 Cars - 18 Tours - 95 Activity',
		city: 'London',
		img: '/img/destinations/1/2.png',
	},
];

export const destinations3 = [
	{
		id: 1,
		img: '/img/destinations/1/1.png',
		title: 'India',
		travellers: '147,681',
		delayAnimation: '0',
	},
	{
		id: 2,
		img: '/img/destinations/1/2.png',
		title: 'Nepal',
		travellers: '147,681',
		delayAnimation: '100',
	},
	{
		id: 3,
		img: '/img/destinations/1/3.png',
		title: 'Maldives',
		travellers: '147,681',
		delayAnimation: '200',
	},
	{
		id: 4,
		img: '/img/destinations/1/4.png',
		title: 'Bhutan',
		travellers: '147,681',
		delayAnimation: '300',
	},
	{
		id: 5,
		img: '/img/destinations/1/5.png',
		title: 'Sri Lanka',
		travellers: '147,681',
		delayAnimation: '400',
	},
	{
		id: 6,
		img: '/img/destinations/3/1.png',
		title: 'Dubai',
		travellers: '147,681',
		delayAnimation: '400',
	},
];

export const destinations4 = [
	{
		id: 1,
		img: '/img/destinations/3/1.png',
		location: 'London, UK',
		properties: '4,090',
		delayAnimation: '0',
	},
	{
		id: 2,
		img: '/img/destinations/3/2.png',
		location: 'Edinburg, UK',
		properties: '4,090',
		delayAnimation: '100',
	},
	{
		id: 3,
		img: '/img/destinations/3/3.png',
		location: 'France',
		properties: '4,090',
		delayAnimation: '200',
	},
	{
		id: 4,
		img: '/img/destinations/3/4.png',
		location: 'Turkey',
		properties: '4,090',
		delayAnimation: '300',
	},
	{
		id: 5,
		img: '/img/destinations/3/5.png',
		location: 'Spain',
		properties: '4,090',
		delayAnimation: '400',
	},
	{
		id: 6,
		img: '/img/destinations/3/1.png',
		location: 'United Kingdom',
		properties: '4,090',
		delayAnimation: '500',
	},
	{
		id: 7,
		img: '/img/destinations/3/2.png',
		location: 'Italy',
		properties: '4,090',
		delayAnimation: '600',
	},
	{
		id: 8,
		img: '/img/destinations/3/3.png',
		location: 'France',
		properties: '4,090',
		delayAnimation: '700',
	},
	{
		id: 9,
		img: '/img/destinations/3/1.png',
		location: 'London, UK',
		properties: '4,090',
		delayAnimation: '800',
	},
	{
		id: 10,
		img: '/img/destinations/3/2.png',
		location: 'Edinburg, UK',
		properties: '4,090',
		delayAnimation: '900',
	},
];

export const destinations5 = [
	{
		id: 1,
		colClass: 'col-xl-3 col-md-4 col-sm-6',
		img: '/img/destinations/2/1.png',
		name: 'los angeles',
		numberOfProperties: '1714',
		delayAnimation: '200',
	},
	{
		id: 2,
		colClass: 'col-xl-6 col-md-4 col-sm-6',
		img: '/img/destinations/2/2.png',
		name: 'london',
		numberOfProperties: '1714',
		delayAnimation: '400',
	},
	{
		id: 3,
		colClass: 'col-xl-3 col-md-4 col-sm-6',
		img: '/img/destinations/2/3.png',
		name: 'reykjavik',
		numberOfProperties: '1714',
		delayAnimation: '600',
	},
	{
		id: 4,
		colClass: 'col-xl-6 col-md-4 col-sm-6',
		img: '/img/destinations/2/4.png',
		name: 'paris',
		numberOfProperties: '1714',
		delayAnimation: '200',
	},
	{
		id: 5,
		colClass: 'col-xl-3 col-md-4 col-sm-6',
		img: '/img/destinations/2/5.png',
		name: 'amsterdam',
		numberOfProperties: '1714',
		delayAnimation: '400',
	},
	{
		id: 6,
		colClass: 'col-xl-3 col-md-4 col-sm-6',
		img: '/img/destinations/2/6.png',
		name: 'istanbul',
		numberOfProperties: '1714',
		delayAnimation: '600',
	},
];

export const destinations6 = [
	{
		id: 1,
		img: '/img/cities/1/1.png',
		location: 'Miami',
		properties: '4,090',
		delayAnim: '0',
	},
	{
		id: 2,
		img: '/img/cities/1/2.png',
		location: 'Roma',
		properties: '4,090',
		delayAnim: '100',
	},
	{
		id: 3,
		img: '/img/cities/1/3.png',
		location: 'New Delhi',
		properties: '4,090',
		delayAnim: '200',
	},
	{
		id: 4,
		img: '/img/cities/1/4.png',
		location: 'London',
		properties: '4,090',
		delayAnim: '300',
	},
	{
		id: 5,
		img: '/img/cities/1/5.png',
		location: 'Amsterdam',
		properties: '4,090',
		delayAnim: '400',
	},
	{
		id: 6,
		img: '/img/cities/1/6.png',
		location: 'Berlin',
		properties: '4,090',
		delayAnim: '500',
	},
	{
		id: 7,
		img: '/img/cities/1/7.png',
		location: 'Paris',
		properties: '4,090',
		delayAnim: '600',
	},
	{
		id: 8,
		img: '/img/cities/1/8.png',
		location: 'New Zealand',
		properties: '4,090',
		delayAnim: '700',
	},
];

export const destinations7 = [
	{
		id: 1,
		colClass: 'col-xl-6 col-md-4 col-sm-6',
		img: '/img/destinations/4/1.png',
		name: 'Paris',
		numberOfProperties: '1714',
		delayAnimation: '100',
	},
	{
		id: 2,
		colClass: 'col-xl-6 col-md-4 col-sm-6',
		img: '/img/destinations/4/2.png',
		name: 'London',
		numberOfProperties: '1714',
		delayAnimation: '200',
	},
	{
		id: 3,
		colClass: 'col-xl-3 col-md-4 col-sm-6',
		img: '/img/destinations/4/3.png',
		name: 'Los Angeles',
		numberOfProperties: '1714',
		delayAnimation: '300',
	},
	{
		id: 4,
		colClass: 'col-xl-3 col-md-4 col-sm-6',
		img: '/img/destinations/4/4.png',
		name: 'Amsterdam',
		numberOfProperties: '1714',
		delayAnimation: '400',
	},
	{
		id: 5,
		colClass: 'col-xl-3 col-md-4 col-sm-6',
		img: '/img/destinations/4/5.png',
		name: 'Istanbul',
		numberOfProperties: '1714',
		delayAnimation: '500',
	},
	{
		id: 6,
		colClass: 'col-xl-3 col-md-4 col-sm-6',
		img: '/img/destinations/4/6.png',
		name: 'Raykjavik',
		numberOfProperties: '1714',
		delayAnimation: '600',
	},
];

export const destinations8 = [
	{
		id: 1,
		img: '/img/destinations/5/1.png',
		location: 'Paris',
		price: '29.52',
		delayAnimation: '100',
	},
	{
		id: 2,
		img: '/img/destinations/5/2.png',
		location: 'London',
		price: '53.52',
		delayAnimation: '200',
	},
	{
		id: 3,
		img: '/img/destinations/5/3.png',
		location: 'Los Angeles',
		price: '35.52',
		delayAnimation: '300',
	},
	{
		id: 4,
		img: '/img/destinations/5/4.png',
		location: 'Amsterdam',
		price: '29.52',
		delayAnimation: '400',
	},
	{
		id: 5,
		img: '/img/destinations/5/5.png',
		location: 'Istanbul',
		price: '27.52',
		delayAnimation: '500',
	},
	{
		id: 6,
		img: '/img/destinations/5/6.png',
		location: 'Raykjavik',
		price: '29.52',
		delayAnimation: '600',
	},
];

export const destinations9 = [
	{
		id: 1,
		location: 'New York',
		numberOfProperties: '4,090',
		delayAnimation: '0',
	},
	{
		id: 2,
		location: 'London',
		numberOfProperties: '4,090',
		delayAnimation: '100',
	},
	{
		id: 3,
		location: 'Los Angeles',
		numberOfProperties: '4,090',
		delayAnimation: '200',
	},
	{
		id: 4,
		location: 'Paris',
		numberOfProperties: '4,090',
		delayAnimation: '300',
	},
	{
		id: 5,
		location: 'Istanbul',
		numberOfProperties: '4,090',
		delayAnimation: '400',
	},
	{
		id: 6,
		location: 'Rome',
		numberOfProperties: '4,090',
		delayAnimation: '500',
	},
	{
		id: 7,
		location: 'Madrid',
		numberOfProperties: '4,090',
		delayAnimation: '600',
	},
];

export const destinations10 = [
	{
		id: 1,
		img: '/img/destinations/4/1.png',
		location: 'paris',
		numberOfProperties: '1714',
		delayAnimation: '200',
	},
	{
		id: 2,
		img: '/img/destinations/4/2.png',
		location: 'london',
		numberOfProperties: '1714',
		delayAnimation: '400',
	},
	{
		id: 3,
		img: '/img/destinations/4/3.png',
		location: 'reykjavik',
		numberOfProperties: '1714',
		delayAnimation: '600',
	},
	{
		id: 4,
		img: '/img/destinations/4/4.png',
		location: 'los angeles',
		numberOfProperties: '1714',
		delayAnimation: '200',
	},
	{
		id: 5,
		img: '/img/destinations/4/5.png',
		location: 'amsterdam',
		numberOfProperties: '1714',
		delayAnimation: '400',
	},
	{
		id: 6,
		img: '/img/destinations/4/6.png',
		location: 'istanbul',
		numberOfProperties: '1714',
		delayAnimation: '600',
	},
	{
		id: 7,
		img: '/img/destinations/4/1.png',
		location: 'paris',
		numberOfProperties: '1714',
		delayAnimation: '200',
	},
	{
		id: 8,
		img: '/img/destinations/4/2.png',
		location: 'london',
		numberOfProperties: '1714',
		delayAnimation: '400',
	},
];

export const india = {
	id: 1,
	name: 'India',
	banner: {
		title: 'Incredible India',
		subtitle: 'Explore the best of India with our travel guide',
		bannerImage:
			'https://images.unsplash.com/photo-1598637749403-82ca6f872457?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1562&q=80',
	},
	categories: [
		{
			id: 1,
			name: 'Popular Destinations',
		},
		{
			id: 2,
			name: 'Tours & Sightseeing',
		},
		{
			id: 3,
			name: 'Activities',
		},
		{
			id: 4,
			name: 'Hotels',
		},
	],
	intro: [
		'India is a country with a rich cultural heritage, stunning natural beauty,\n\t\t\tand a diverse range of experiences to offer. It is a land of contrasts,\n\t\t\twhere ancient traditions coexist with modern technologies, and bustling\n\t\t\tcities blend seamlessly with tranquil villages. If you are looking for a\n\t\t\tunique and memorable travel experience, here are just a few reasons why\n\t\t\tIndia should be at the top of your list:\n\t\t\t',
		"\n\t\t\tFirstly, India's cultural heritage is one of the oldest and most diverse\n\t\t\tin the world. From the grandeur of the Taj Mahal to the intricacy of the\n\t\t\tKhajuraho Temples, India is home to some of the world's most impressive\n\t\t\tarchitect...tion,\n\t\t\tand travelers can experience the benefits of these ancient practices in\n\t\t\tyoga retreats and wellness centers across the country. The country is also\n\t\t\thome to a range of colorful festivals and events, from the Diwali festival\n\t\t\tof lights to the Holi festival of colors, which offer visitors a glimpse\n\t\t\tinto the vibrant culture of India.\n\t\t\t",
		'\n\t\t\tSecondly, India boasts an incredible natural diversity, with a landscape\n\t\t\tthat ranges from snow-capped mountains to golden beaches. The country is\n\t\t\thome to numerous national parks and wildlife reserves, where visitors can\n\t\t\tspot tigers, elephants, rhinoceroses, and other exotic animals in their\n\t\t\tnatural habitats. Trekking through the Himalayas or exploring the deserts\n\t\tof Rajasthan are other popular outdoor activities that allow visitors to\n\t\t\texperience the stunning natural beauty of India.\n\t\t\t',
		'\n\t\t\tFinally, India is renowned for its culinary delights, which are as diverse\n\t\t\tas its culture. From the spicy street food of Mumbai to the vegetarian\ncuisine of South India, the country offers a range of culinary experiences\n\t\t\tthat are sure to tantalize your taste buds. Many food tours and cooking\nclasses are available for travelers to learn about the various regional\ncuisines and to sample the delicious dishes.',
		`Overall, India offers an unparalleled travel experience that is both enriching and exciting. Whether you are looking for adventure, spiritual enlightenment, or simply want to immerse yourself in a new culture, India has something for everyone. Here's a complete travel guide to help you plan your trip to India.`,
		`To make the most of your visit, you could use specialist professional tour guides. Your experienced guide will become your friend, speak your language, and open up the real, hidden India before you.`,
	],
	introImg:
		'https://images.unsplash.com/photo-1532664189809-02133fee698d?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80',
	attractions: {
		title: 'Top India attractions & experiences',
		subtitle: `India is a vast country with a rich cultural heritage, diverse landscapes, and countless attractions to explore. These are just a few of the many things to do during a trip to India. Whether you're interested in history, culture, nature, or food, you're sure to find something to suit your interests.`,
		attractionsList: [
			{
				id: 1,
				name: 'Taj Mahal',
				description:
					'Visit the Taj Mahal in Agra – the iconic monument is one of the Seven Wonders of the World and a symbol of love.',
				img: 'https://images.unsplash.com/photo-1599476160130-3af44b69ec6e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=200&q=80',
			},
			{
				id: 2,
				name: 'Visiting Rajasthan',
				description: `Explore the royal state of Rajasthan – the land of kings and palaces.`,
				img: 'https://images.unsplash.com/photo-1477586957327-847a0f3f4fe3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=200&q=80',
			},
			{
				id: 3,
				name: 'Visiting Kerala',
				description: `Explore the beautiful state of Kerala – the land of backwaters and beaches.`,
				img: 'https://images.unsplash.com/photo-1593693397690-362cb9666fc2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&h=2008&q=80',
			},
			{
				id: 4,
				name: 'Indias Golden Triangle',
				description: `Explore the Golden Triangle – the most popular tourist circuit in India.`,
				img: 'https://images.unsplash.com/photo-1635774462472-83c734381ddd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=200&q=80',
			},
			{
				id: 5,
				name: "Visiting Goa's best beaches",
				description: `Visit Goa's best beaches – the state is home to some of the most beautiful beaches in India.`,
				img: 'https://images.unsplash.com/photo-1581892197913-fd2e407e698a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=200&h=200&q=80',
			},
			{
				id: 6,
				name: 'Festival of Lights, Diwali',
				description: `Experience the Festival of Lights, Diwali – the most popular festival in India.`,
				img: 'https://images.unsplash.com/photo-1504428081227-fa982aa38202?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 7,
				name: 'Holi Celebrations in India',
				description: `Experience the Holi Celebrations in India – the festival of colors.`,
				img: 'https://images.unsplash.com/photo-1617184003170-1f266c325ff3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},

			{
				id: 8,
				name: 'Tiger Safari in India',
				description: `Go on a Tiger Safari in India – the country is home to the largest population of tigers in the world.`,
				img: 'https://images.unsplash.com/photo-1615963244664-5b845b2025ee?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1364&q=80',
			},
			{
				id: 9,
				name: 'Riding a camel in Rajasthan',
				description: `Ride a camel in Rajasthan – the state is home to the largest population of camels in India.`,
				img: 'https://images.unsplash.com/photo-1577089907583-991f1ba4a03c?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 10,
				name: 'Meet Dalai Lama in Dharamsala',
				description: `Meet the Dalai Lama in Dharamsala – the spiritual leader of Tibetans.`,
				img: 'https://images.unsplash.com/photo-1599057164474-3161aa013c31?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 11,
				name: 'Trekking in the Himalayas',
				description: `Trek in the Himalayas – the highest mountain range in the world.`,
				img: 'https://images.unsplash.com/photo-1513614835783-51537729c8ba?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 12,
				name: 'Staying in a Palace in Rajasthan',
				description: `Stay in a Palace in Rajasthan – the state is home to some of the most beautiful palaces in India.`,
				img: 'https://images.unsplash.com/photo-1629210132098-592c636e3d1a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 13,
				name: 'Staying in a Houseboat in Kerala',
				description: `Stay in a Houseboat in Kerala – the state is home to some of the most beautiful backwaters in India.`,
				img: 'https://images.unsplash.com/photo-1602216056096-3b40cc0c9944?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 14,
				name: 'Riding a tuk tuk on the streets of Delhi',
				description: `Ride a tuk tuk on the streets of Delhi – the capital city of India.`,
				img: 'https://images.unsplash.com/photo-1624858020896-4a558c5d7042?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 15,
				name: 'Visit the Harmandir Sahib, the Golden Temple in Amritsar',
				description: `Visit the Harmandir Sahib, the Golden Temple in Amritsar – the holiest shrine of Sikhism.`,
				img: 'https://images.unsplash.com/photo-1514222134-b57cbb8ce073?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 16,
				name: 'Yoga in Rishikesh',
				description: `Practice Yoga in Rishikesh – the yoga capital of the world.`,
				img: 'https://images.unsplash.com/photo-1616824421012-296bb3580556?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 17,
				name: 'Varanasi, the spiritual capital of India',
				description: `Visit Varanasi – the spiritual capital of India.`,
				img: 'https://images.unsplash.com/photo-1627938823193-fd13c1c867dd?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 18,
				name: 'Kamasutra in Khajuraho',
				description: `Visit Khajuraho – the land of Kamasutra.`,
				img: 'https://images.unsplash.com/photo-1638645677644-f01017224e0b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
		],
	},
	faqs: [
		{
			question: 'What is the best time to visit India?',
			answer: `The best time to visit India is between October and March when the weather is pleasant and the tourist season is in full swing. During these months, you can explore various parts of the country without extreme temperatures hindering your plans.\n\n

				India's vast geographical diversity means that different regions experience different climates. In North India, such as Delhi, Agra, and Rajasthan, you can expect cooler temperatures and comfortable days, making it ideal for visiting historical sites, palaces, and forts.\n\n
				
				The monsoon season, from June to September, brings heavy rainfall to many parts of India, making travel challenging and disrupting outdoor activities. However, if you're a nature enthusiast, this can be an excellent time to witness the lush green landscapes and enjoy the unique charm of rain-soaked destinations like Kerala and the Western Ghats.\n\n
				
				The winter season, from December to February, offers cool temperatures, especially in North India, which can be ideal for exploring cultural sites and experiencing festivals like Diwali and Christmas. South India, with its tropical climate, remains pleasant throughout the year, making it a popular destination regardless of the season. Coastal regions like Goa and the Andaman Islands are best visited during the winter months when the weather is perfect for beach activities and water sports.\n\n
				
				Ultimately, the best time to visit India depends on the regions you plan to explore and your personal preferences regarding weather and activities. Consider the different climates and seasons in various parts of the country, and choose the time that aligns with your interests and desired experiences.\n\n
				
				Please note that the weather patterns can vary from year to year, so it's always a good idea to check the specific weather forecasts and plan accordingly for your trip to India.`,
		},
		{
			question: 'What are the popular tourist destinations in India?',
			answer:
				"India is a diverse and enchanting country with a plethora of popular tourist destinations to explore. Whether you're seeking historical landmarks, cultural experiences, natural beauty, or spiritual enlightenment, India has something for everyone. Some of the must-visit destinations include:\n\nThe Taj Mahal in Agra: This UNESCO World Heritage Site is a breathtaking monument of love and an architectural marvel. Its ethereal beauty and intricate details will leave you awe-struck.\n\nJaipur in Rajasthan: Known as the 'Pink City,' Jaipur offers a fascinating blend of history, culture, and vibrant markets. Explore majestic forts like Amber Fort and Nahargarh Fort, visit the City Palace, and immerse yourself in the bustling bazaars.\n\nVaranasi in Uttar Pradesh: Situated on the banks of the sacred Ganges River, Varanasi is one of the oldest inhabited cities in the world. It is a spiritual hub, where pilgrims come to bathe in the holy river and experience mesmerizing rituals and ceremonies.\n\nKerala's Backwaters: Cruise through the tranquil backwaters of Alleppey and Kumarakom, nestled amidst lush green landscapes. Stay in traditional houseboats and soak in the serene beauty of this unique ecosystem.\n\nGoa's Beaches: Known for its sandy shores, vibrant nightlife, and laid-back vibe, Goa offers a perfect blend of relaxation and entertainment. Enjoy sun-kissed beaches, water sports, beach parties, and indulge in delicious seafood.\n\nDelhi: The capital city of India, Delhi, is a vibrant metropolis that seamlessly blends ancient traditions with modernity. Explore historical landmarks like Red Fort and Humayun's Tomb, visit the bustling markets of Chandni Chowk, and savor the diverse culinary delights.\n\nThese are just a few examples, and India has much more to offer. The country boasts numerous other captivating destinations, including the serene beauty of the Himalayas in Himachal Pradesh and Uttarakhand, the wildlife-rich national parks like Ranthambore and Bandhavgarh, the culturally rich cities of Kolkata and Chennai, and the picturesque landscapes of Ladakh and Kashmir. The choice of destinations depends on your preferences, time of travel, and the experiences you seek. It's advisable to plan your itinerary well in advance and consider the diverse aspects of India to make the most of your visit.",
		},
		{
			question: 'What is the best way to get around India?',
			answer: `Getting around India can be an adventure in itself, with various transportation options available to suit different preferences and budgets. Here are some popular ways to travel within the country:\n\n
				1. Domestic Flights: India has an extensive network of domestic flights connecting major cities and tourist destinations. It's a convenient and time-saving option for covering long distances.\n\n
				2. Trains: Indian Railways is one of the largest rail networks in the world, offering an extensive network that reaches almost every corner of the country. Trains are a popular mode of transport, providing comfortable and affordable journeys. Booking tickets in advance is advisable, especially for long-distance and popular routes.\n\n
				3. Buses: State-run and private buses operate throughout India, offering economical transportation options. They are ideal for shorter distances and reaching remote areas where trains may not be available. AC and sleeper buses are available for more comfort during longer journeys.\n\n
				4. Metro and Local Trains: Major cities like Delhi, Mumbai, Kolkata, Chennai, and Bengaluru have efficient metro systems that are convenient for getting around within the city. Local trains also provide a cost-effective means of transportation for short distances.\n\n
				5. Taxis and Auto-rickshaws: Prepaid and app-based taxis, as well as auto-rickshaws, are readily available in cities and towns. They offer flexibility and door-to-door transport, but it's important to negotiate or use fare meters to ensure a fair price.\n\n
				6. Hiring a Private Driver: For more comfort and flexibility, hiring a private driver or car rental service is a popular option, especially for exploring multiple destinations or undertaking a road trip. It allows you to customize your itinerary and travel at your own pace.\n\n
				It's important to consider factors such as distance, time, budget, and personal preferences when choosing the best way to get around India. Planning ahead, researching routes and schedules, and considering the duration of travel will help ensure a smooth and enjoyable journey.`,
		},
		{
			question: 'Eating in India',
			answer: `Indian cuisine is known for its vibrant flavors, aromatic spices, and diverse regional specialties. Exploring the culinary delights of India is an integral part of the travel experience. Here are some tips for enjoying the food in India:\n\n
				1. Try Local Street Food: Street food in India is famous for its delicious flavors. From savory snacks like samosas and chaat to sweet treats like jalebi and kulfi, each region has its own specialties. Be sure to try popular street food items, but prioritize hygiene by choosing busy stalls and vendors.\n\n
				2. Embrace Regional Cuisine: India's culinary landscape varies greatly from one region to another. Each state has its own traditional dishes and cooking techniques. Whether it's the fiery curries of Rajasthan, the seafood delights of Kerala, or the vegetarian fare of Gujarat, exploring regional cuisine is a culinary adventure in itself.\n\n
				3. Be Mindful of Hygiene: While Indian food is delicious, it's important to exercise caution when it comes to hygiene. Choose clean and reputable eateries, opt for freshly cooked food, and avoid consuming raw or uncooked items. Drinking bottled water or sticking to boiled or filtered water is advisable.\n\n
				4. Spice Levels: Indian food can be spicy, but not all dishes are fiery. If you're not accustomed to spicy food, ask for milder versions or request the chef to customize the spice levels according to your preference. Don't hesitate to communicate your spice tolerance to ensure an enjoyable dining experience.\n\n
				5. Vegetarian and Non-vegetarian Options: India is known for its vegetarian cuisine, with a wide variety of vegetarian dishes available. However, non-vegetarian options are also abundant, especially in coastal regions and major cities. You'll find a range of meat, seafood, and poultry dishes to savor.\n\n
				6. Food Allergies and Dietary Restrictions: If you have any food allergies or dietary restrictions, it's essential to communicate them clearly to the restaurant staff or your hosts. They will guide you on suitable options or make necessary accommodations to cater to your needs.\n\n
				Exploring Indian cuisine is a delightful journey, but it's advisable to exercise moderation, maintain good hygiene practices, and listen to your body's limits. Trying local delicacies, embracing new flavors, and enjoying the diverse culinary traditions of India will undoubtedly enhance your travel experience`,
		},
		{
			question: 'Cycling tours in India',
			answer: `Cycling tours in India offer a unique and immersive way to explore the country's diverse landscapes, cultural heritage, and rural beauty. Whether you're an avid cyclist or a casual adventurer, here are some key points to consider for cycling tours in India:\n\n
				1. Choose the Right Route: India offers a wide range of cycling routes, each with its own charm. From the rugged terrains of the Himalayas to the coastal trails of Kerala and the rural villages of Rajasthan, select a route that aligns with your fitness level, interests, and desired experience.\n\n
				2. Guided Tours or Self-Guided: Decide whether you prefer a guided tour or a self-guided adventure. Guided tours provide support, local knowledge, and logistical arrangements, while self-guided tours offer more flexibility and independence. Assess your comfort level and choose accordingly.\n\n
				3. Bike Rental or Bring Your Own: Depending on your travel plans, you can either rent a bike from local tour operators or bring your own. Ensure that the rental bike is in good condition, suited to the terrain, and properly adjusted for your comfort. Carry essential tools and spare parts if bringing your own bike.\n\n
				4. Safety and Equipment: Prioritize safety by wearing a helmet, using reflective gear, and following traffic rules. Carry a basic repair kit, puncture repair tools, and first aid supplies. Stay hydrated, use sunscreen, and wear comfortable cycling attire suitable for the weather conditions.\n\n
				5. Cultural Sensitivity: Respect local customs, traditions, and religious sites along your cycling route. Dress appropriately, interact respectfully with locals, and seek permission before taking photographs. Embrace the opportunity to learn about different cultures and engage with local communities.\n\n
				6. Accommodation and Food: Plan your itinerary in advance to ensure availability of suitable accommodation along the route. Look for cyclist-friendly accommodations or homestays that offer secure bike storage and facilities. Enjoy local cuisine, taste regional specialties, and stay hydrated by carrying enough water and snacks.\n\n
				Cycling tours in India offer a rewarding and immersive experience, allowing you to connect with the country's diverse landscapes, interact with local communities, and challenge yourself physically. With careful planning, respect for local culture, and a spirit of adventure, you'll create unforgettable memories on your cycling journey.`,
		},
		{
			question: 'Travel visa requirements for India',
			answer: `Before traveling to India, it's important to understand the visa requirements and ensure that you have the necessary documentation in place. Here's an overview of the travel visa requirements for India:\n\n
				1. Types of Visas: India offers various visa categories, including tourist visas, business visas, medical visas, and more. For most travelers, a tourist visa (e-visa or regular visa) is appropriate. The e-visa facility allows for online application and is available for citizens of many countries. Check the official Indian government website or consult with the Indian embassy/consulate in your country to determine the specific visa category that suits your travel purpose.\n\n
				2. Validity and Duration: Tourist visas for India typically have a validity of six months to one year, allowing multiple entries. However, the maximum duration of stay per visit is usually limited to 180 days. Make sure your passport is valid for at least six months from the date of arrival in India.\n\n
				3. Application Process: If applying for an e-visa, you can complete the application online, upload the required documents (such as passport copy and recent photograph), and pay the visa fee electronically. The e-visa is then issued electronically and sent to your registered email. For regular visas, you will need to visit the Indian embassy/consulate and follow the application process outlined by the respective authority.\n\n
				4. Supporting Documents: When applying for a tourist visa, you may be required to provide documents such as a copy of your passport, passport-sized photographs, travel itinerary, proof of accommodation, and proof of sufficient financial means to support your stay in India. It's advisable to check the specific requirements for your country of residence.\n\n
				5. Visa Extensions: If you wish to extend your stay in India beyond the permitted duration, you must apply for a visa extension through the Foreigners Regional Registration Office (FRRO) or the Foreigners Registration Office (FRO) in the respective city or district.\n\n
				It's crucial to familiarize yourself with the visa requirements well in advance of your travel dates. Ensure that you have all the necessary documents, follow the application process diligently, and comply with the immigration regulations to enjoy a hassle-free journey to India.`,
		},
		{
			question: 'Can I use credit/debit cards in India?',
			answer: `Credit and debit cards are widely accepted in India, especially in urban areas, popular tourist destinations, and establishments catering to international travelers. Here are some key points to consider regarding the use of credit/debit cards in India:\n\n
				1. Card Acceptance: Most hotels, restaurants, shopping malls, and major retail outlets in cities accept credit and debit cards. It's always advisable to carry sufficient cash as a backup, especially in smaller towns, rural areas, or for smaller establishments where card acceptance may be limited.\n\n
				2. International Transaction Fees: Check with your bank or card issuer regarding any international transaction fees or currency conversion charges that may apply when using your card in India. These fees can vary, so it's important to be aware of the potential additional costs.\n\n
				3. ATM Availability: ATMs are widely available in India, allowing you to withdraw cash using your debit card. However, it's advisable to use ATMs located in secure and well-lit areas, such as inside banks or shopping centers, to minimize the risk of card skimming or other fraudulent activities. Inform your bank about your travel plans to ensure smooth card usage.\n\n
				4. Chip-and-PIN Cards: Chip-and-PIN cards are commonly used in India. Ensure that your card has a chip and set a personal identification number (PIN) for secure transactions. Contactless payments using cards or mobile wallets are also gaining popularity.\n\n
				5. Currency Exchange: If you plan to carry cash, it's recommended to exchange your currency at authorized currency exchange centers or banks. Avoid exchanging money from unauthorized sources, as counterfeit notes are a concern.\n\n
				6. Card Security: Take necessary precautions to safeguard your cards and personal information. Keep your cards in a secure place, shield your PIN when entering it, and be cautious while making transactions to prevent card fraud or identity theft.\n\n
				Using credit and debit cards in India offers convenience and security. However, it's important to carry sufficient cash, inform your bank about your travel plans, and exercise caution while using cards to ensure a smooth and secure financial experience during your trip.`,
		},
		{
			question: 'What to pack for India?',
			answer: `Packing for a trip to India requires careful consideration of the diverse climate, cultural norms, and the activities you plan to undertake. Here's a comprehensive packing list to help you prepare for your journey:\n\n
				1. Clothing: Pack lightweight and breathable clothing suitable for the tropical climate of most parts of India. Carry comfortable, loose-fitting clothes made of natural fabrics like cotton or linen. Include items like t-shirts, shirts, trousers, skirts, dresses, and shorts. Depending on the season and your itinerary, also pack a light jacket, a sweater, or a shawl for cooler evenings or visits to higher altitude regions.\n\n
				2. Footwear: Comfortable walking shoes or sneakers are essential for exploring various attractions and navigating through bustling streets. Sandals or flip-flops are convenient for hot weather or visits to religious sites where you may need to remove your footwear.\n\n
				3. Weather Protection: India experiences diverse weather conditions, so it's important to be prepared. Pack a wide-brimmed hat or a cap to protect yourself from the sun. Carry a lightweight, foldable umbrella or a raincoat for unexpected showers, especially during the monsoon season.\n\n
				4. Personal Care Items: Don't forget to pack essential personal care items like sunscreen, insect repellent, hand sanitizer, wet wipes, and any prescription medications you require. It's also advisable to carry a small first aid kit with basic supplies for minor ailments.\n\n
				5. Electronics and Adapters: If you plan to use electronic devices, bring the necessary adapters or converters to ensure compatibility with Indian electrical outlets. Consider carrying a power bank for charging your devices on the go.\n\n
				6. Travel Documents and Money: Keep your passport, visa, travel insurance, and other important documents in a secure travel organizer. Carry a photocopy of your passport and store digital copies in a secure cloud storage or email them to yourself for easy access. It's advisable to carry a mix of cash and cards for convenience.\n\n
				7. Cultural Considerations: Respect local customs and modesty norms by packing modest clothing, particularly if you plan to visit religious sites or rural areas. Scarves or shawls can be handy for covering your head or shoulders when required.\n\n
				8. Miscellaneous: Other useful items to consider include a travel adapter, a reusable water bottle, a sturdy backpack or daypack, a camera or smartphone for capturing memories, and a travel guidebook or maps.\n\n
				Remember to pack light and pack according to the specific requirements of your trip. Take into account the duration of your stay, planned activities, and the availability of laundry facilities. By packing wisely, you'll be well-prepared to enjoy your time in India and make the most of your travel experiences.`,
		},
		{
			question: 'Is it safe traveling alone in India?',
			answer: `Traveling alone in India can be a rewarding and enriching experience, but it's important to be aware of certain considerations to ensure your safety and well-being. Here are some tips for solo travelers in India:\n\n
				1. Research and Planning: Thoroughly research your intended destinations and understand their safety profiles. Take note of any specific precautions or travel advisories issued for certain regions. Plan your itinerary and accommodation in advance to avoid last-minute hassles.\n\n
				2. Dress Modestly: India is a culturally diverse country with varying norms regarding dress. To avoid unwanted attention, it's advisable to dress modestly, especially in religious sites and rural areas. Respect local customs and norms to blend in with the local culture.\n\n
				3. Accommodation Safety: Choose reputable accommodations that prioritize safety and security. Opt for accommodations with positive reviews, good lighting, secure entrances, and reliable locks. Consider staying in well-established hotels, guesthouses, or hostels that cater to solo travelers.\n\n
				4. Transportation Safety: Use licensed and reputable transportation services. When using taxis or ride-hailing services, share your ride details with a trusted friend or family member. Stick to well-lit and populated areas when traveling at night, and avoid walking alone in secluded or unfamiliar areas.\n\n
				5. Personal Belongings: Keep your belongings secure at all times. Use a money belt or a concealed pouch to carry your valuables, and avoid displaying expensive items openly. Be cautious of pickpockets in crowded places and avoid carrying large sums of cash.\n\n
				6. Stay Connected: Inform your family or friends about your travel plans and share your itinerary with them. Stay connected with loved ones through regular check-ins or by sharing your location using a reliable mobile app. Stay updated with local news and any developments that may affect your travel plans.\n\n
				7. Trust Your Instincts: Trust your instincts and be aware of your surroundings. If something feels uncomfortable or unsafe, remove yourself from the situation. Be cautious of strangers offering unsolicited assistance or overly friendly gestures.\n\n
				8. Solo Female Travelers: Female travelers should take additional precautions, such as avoiding traveling alone at night, using transportation options with female-only compartments, and booking accommodations in safe and well-traveled areas.\n\n
				Solo travel in India can be a transformative experience, allowing you to explore the country's rich culture and heritage. By being prepared, staying vigilant, and following common-sense safety practices, you can have a safe and memorable journey.`,
		},
	],
};

export const nepal = {
	id: 1,
	name: 'Nepal',
	banner: {
		title: 'Explore Nepal',
		subtitle: 'Discover the bewitching beauty of Nepal',
		bannerImage:
			'https://images.unsplash.com/photo-1611516491426-03025e6043c8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1633&q=80',
	},
	categories: [
		{
			id: 1,
			name: 'Popular Destinations',
		},
		{
			id: 2,
			name: 'Tours & Sightseeing',
		},
		{
			id: 3,
			name: 'Activities',
		},
		{
			id: 4,
			name: 'Hotels',
		},
	],
	intro: [
		"Nepal is a beautiful country located in South Asia, between Tibet and India. It is known for its stunning mountain scenery, rich cultural heritage, and diverse landscape, making it a popular destination for tourists and trekkers alike. In this travel guide, we'll cover all the essential information you need to know to plan your trip to Nepal.",
		'The Nepalese culture is just as diverse and colorful as its geography. The Nepalese people live in a variety of places, from big cities to small villages, and from high mountains to flat plains. The Nepalese people are of many different ethnicities, and they live in many different ways. Nepalese practice a variety of religions, including Buddhism, Hinduism, and Shamanism.',
		'The landscape itself has shaped the country and given it a unique identity. The Nepalese people are very proud of their country and of their heritage. They are known for their festivals, their parades, and their friendly nature. The Nepalese are well-known for their bravery and determination.',
		'But, it would be a misrepresentation to regard Nepal as a mythical Shangri-La. It is heavily dependent on its powerful neighbors, and until 1990, it was the last remaining absolute Hindu Monarchy. The Nepalese government was a combination of the repression of China and the bureaucracy of India. It was long economically and politically backward, and it had to develop at an awkward pace. Following a brutal civil war, it turned into the Federal Republic. The sense of excitement and anticipation in Nepal is tangible.',
		"In conclusion, Nepal is a truly unique and amazing travel destination, offering breathtaking scenery, rich cultural heritage, and an abundance of adventure and outdoor activities. Whether you're a trekker, a nature lover, or just looking to relax and explore a new culture, Nepal is a destination you won't want to miss.",
	],
	introImg:
		'https://images.unsplash.com/photo-1529733905113-027ed85d7e33?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=987&q=80',
	attractions: {
		title: 'Top Nepal attractions & experiences',
		subtitle: `Nepal is a country of great diversity, with a rich culture and history. It's also home to some of the world's most breathtaking natural wonders, including Mount Everest, the highest mountain in the world. Here are some of the top attractions and experiences you can enjoy in Nepal:`,
		attractionsList: [
			{
				id: 1,
				name: 'Mount Everest',
				description:
					'Embark on a trek to Mount Everest, the highest peak in the world.',
				img: 'https://images.unsplash.com/photo-1518002054494-3a6f94352e9d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 2,
				name: 'Kathmandu Durbar Square',
				description:
					'Visit Kathmandu Durbar Square, a UNESCO World Heritage Site known for its ancient temples and palaces.',
				img: 'https://images.unsplash.com/photo-1611516491426-03025e6043c8?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 3,
				name: 'Pashupatinath Temple',
				description:
					'Explore the sacred Pashupatinath Temple, one of the most important Hindu temples dedicated to Lord Shiva.',
				img: 'https://images.unsplash.com/photo-1504448252408-b32799ff32f3?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 4,
				name: 'Boudhanath Stupa',
				description:
					'Visit Boudhanath Stupa, one of the largest Buddhist stupas in the world and an important pilgrimage site.',
				img: 'https://images.unsplash.com/photo-1623148185582-b128032f1c08?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 5,
				name: 'Pokhara',
				description:
					'Explore the beautiful city of Pokhara, known for its stunning lakes, breathtaking mountain views, and adventure activities.',
				img: 'https://images.unsplash.com/photo-1610997686651-98492fd08108?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 6,
				name: 'Chitwan National Park',
				description:
					'Experience wildlife in Chitwan National Park, home to diverse flora and fauna, including endangered species like the Bengal tiger and one-horned rhinoceros.',
				img: 'https://images.unsplash.com/photo-1549888668-19281758dfbe?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 7,
				name: 'Lumbini',
				description:
					'Visit Lumbini, the birthplace of Gautama Buddha and a UNESCO World Heritage Site.',
				img: 'https://images.unsplash.com/photo-1572881178197-456f38666aa1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 8,
				name: 'Annapurna Circuit',
				description:
					'Embark on the Annapurna Circuit, a renowned trek that offers stunning mountain views and diverse landscapes.',
				img: 'https://images.unsplash.com/photo-1526772662000-3f88f10405ff?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 9,
				name: 'Nagarkot',
				description:
					'Visit Nagarkot, a hilltop village famous for its panoramic views of the Himalayas and sunrise/sunset vistas.',
				img: 'https://images.unsplash.com/photo-1585898175463-4bb8b8a9dea2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 10,
				name: 'Langtang National Park',
				description:
					'Explore Langtang National Park, known for its diverse wildlife, alpine lakes, and stunning Himalayan landscapes.',
				img: 'https://images.unsplash.com/photo-1678188901030-b1790b35c7b4?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
		],
	},
	faqs: [
		{
			question: 'What is the best time to visit Nepal?',
			answer: `The best time to visit Nepal is during the spring (March to May) and autumn (September to November) seasons. These months offer mild temperatures, clear skies, and favorable weather for trekking, outdoor activities, and exploring cultural sites. Spring brings blooming flowers and rhododendron forests, while autumn offers excellent visibility of the majestic Himalayan peaks. 
			\n\nSummer (June to August) is the monsoon season, which brings heavy rainfall and may affect transportation and trekking routes. However, it can be a good time to visit the lush green landscapes and observe festivals like Teej and Ropai Jatra. Winter (December to February) can be cold, especially in higher altitude regions, but it is still possible to visit for trekking or cultural experiences.\n\n
			Winter (December to February) can be cold, especially in higher altitude regions, but it is still possible to visit for trekking or cultural experiences. Monsoon season (June to August) brings heavy rainfall, which may affect transportation and trekking routes, although it can be a good time to visit the lush green landscapes and observe festivals like Teej and Ropai Jatra.`,
		},
		{
			question: 'What are the popular tourist destinations in Nepal?',
			answer: `
			Nepal is a country renowned for its breathtaking landscapes, rich cultural heritage, and adventure opportunities. Some of the popular tourist destinations in Nepal include:
			\n\n
			Kathmandu Valley: Explore the capital city of Kathmandu and its UNESCO World Heritage Sites, including Pashupatinath Temple, Boudhanath Stupa, Swayambhunath Stupa, and Durbar Squares in Kathmandu, Bhaktapur, and Patan.\n\n
			Pokhara: Known for its stunning lakes and close proximity to the Annapurna mountain range, Pokhara offers a laid-back atmosphere and is a gateway to popular trekking trails like the Annapurna Circuit and Annapurna Base Camp.\n\n
			Chitwan National Park: Experience wildlife encounters in this renowned national park, home to endangered species like the Bengal tiger, one-horned rhinoceros, and various bird species. Enjoy jungle safaris, elephant rides, and cultural experiences with Tharu communities.\n\n
			Lumbini: Visit the birthplace of Lord Buddha and explore the sacred pilgrimage site featuring ancient monasteries, temples, and the Mayadevi Temple marking the exact spot of Buddha's birth.\n\n
			Nagarkot: Enjoy panoramic views of the Himalayas, including Mount Everest, from this hill station located just outside Kathmandu. It's a popular spot for sunrise and sunset views.
			Everest Base Camp: Embark on a challenging trek to the Everest Base Camp and witness stunning vistas of the world's highest peak, Mount Everest, along with other majestic mountains in the Everest region.\n\n
			Nepal offers countless other attractions, trekking routes, and cultural experiences. It is a country of diverse landscapes and rich heritage, making it an ideal destination for adventure seekers, nature lovers, and spiritual enthusiasts.
			`,
		},
		{
			question: 'What trekking options are available in Nepal?',
			answer: `Nepal is renowned for its trekking opportunities, and there are trekking options available for varying levels of fitness and experience. Some popular treks in Nepal include:
\n\n
			Everest Base Camp Trek: A challenging trek that takes you to the base camp of Mount Everest, offering stunning mountain views, Sherpa culture, and a sense of accomplishment.\n\n
			Annapurna Circuit Trek: A classic trek that circumnavigates the entire Annapurna massif, showcasing diverse landscapes, traditional villages, and breathtaking mountain panoramas.\n\n
			Langtang Valley Trek: A shorter trek near Kathmandu that takes you through picturesque valleys, rhododendron forests, and Tamang villages, with views of Langtang Ri and Langtang Lirung peaks.\n\n
			Manaslu Circuit Trek: A remote and less crowded trek that takes you around the eighth highest mountain in the world, Mount Manaslu, offering a glimpse into Tibetan-influenced culture and stunning Himalayan vistas.\n\n
			Upper Mustang Trek: Explore the ancient kingdom of Mustang, known for its unique landscapes resembling the Tibetan plateau, ancient cave dwellings, and Tibetan Buddhist monasteries.\n\n
			These are just a few examples, and there are numerous other trekking options available in Nepal, ranging from short and easy treks to challenging high-altitude adventures.`,
		},
		{
			question: 'Is it safe to trek in Nepal?',
			answer: `Trekking in Nepal is generally safe, but it's important to take necessary precautions and be prepared. It is recommended to trek with a licensed guide or a reputable trekking agency who can ensure your safety and assist in case of any emergencies. Prioritize your physical fitness, acclimatization, and follow the guidelines provided by your guide or agency. It's also essential to have travel insurance that covers trekking and medical evacuation. Always check weather conditions, carry necessary gear, drink plenty of water, and respect local customs and regulations. Stay updated on the latest travel advisories and maintain communication with your embassy or consulate.`,
		},
		{
			question: 'Do I need a visa to visit Nepal?',
			answer: `Yes, most visitors to Nepal require a visa. You can obtain a visa upon arrival at Tribhuvan International Airport in Kathmandu or at various land entry points. Nepal offers both tourist visas and other categories of visas for specific purposes. Tourist visas are generally valid for multiple entries and available for durations of 15, 30, or 90 days. You can also apply for a visa in advance at Nepalese embassies or consulates in your home country. It's advisable to check the official website of the Department of Immigration of Nepal or contact the nearest Nepalese embassy/consulate for the most up-to-date visa information and requirements.
			`,
		},
		{
			question: 'What are the major festivals in Nepal?',
			answer: `Nepal is a country known for its vibrant festivals, celebrated with enthusiasm and religious fervor. Some of the major festivals in Nepal include:
\n\n
			Dashain: The biggest and most important Hindu festival in Nepal, celebrated for 15 days during September or October. It marks the victory of good over evil and includes rituals, feasts, family gatherings, and the worship of the goddess Durga.\n\n
			Tihar (Diwali): The festival of lights celebrated in October or November, involving the worship of different animals and deities, along with the illumination of homes with oil lamps and colorful lights.\n\n
			Holi: The festival of colors celebrated in March, signifying the arrival of spring. People celebrate by throwing colored powders and water at each other, symbolizing the triumph of good over evil.\n\n
			Teej: A women-centric festival observed by Hindu women in August, involving fasting, singing, dancing, and praying for marital bliss, well-being, and the longevity of their husbands.\n\n
			Bisket Jatra: A unique festival celebrated in Bhaktapur, featuring the pulling of chariots, cultural performances, and the display of the Bhairab and Bhadrakali deities.\n\n
			Indra Jatra: A Kathmandu festival honoring the deity Indra, featuring masked dances, chariot processions, and the display of the living goddess Kumari.\n\n
			Nepal has numerous other festivals throughout the year, each with its own significance and rituals. Festivals offer a great opportunity to experience the cultural richness and traditional heritage of the country.
			`,
		},
		{
			question: 'What is the currency used in Nepal?',
			answer: `The currency used in Nepal is the Nepalese Rupee (NPR). It is advisable to carry some Nepalese currency for day-to-day expenses, as not all places accept foreign currencies or credit cards. Currency exchange services are available at banks, exchange counters, and authorized money changers in major cities and tourist areas.`,
		},
	],
};

export const maldives = {
	id: 1,
	name: 'Maldives',
	banner: {
		title: 'Unparalleled beauty of Maldives',
		subtitle: 'Discover the extraordinary beauty of Maldives',
		bannerImage:
			'https://images.unsplash.com/photo-1631100610461-625b78f8a94a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1171&q=80',
	},
	categories: [
		{
			id: 1,
			name: 'Popular Destinations',
		},
		{
			id: 2,
			name: 'Tours & Sightseeing',
		},
		{
			id: 3,
			name: 'Activities',
		},
		{
			id: 4,
			name: 'Hotels',
		},
	],
	intro: [
		'Welcome to the Maldives, a tropical paradise that promises an unforgettable journey of luxury, tranquility, and natural beauty. Nestled in the azure waters of the Indian Ocean, this enchanting archipelago is a dream destination for travelers seeking a slice of paradise.',
		'Imagine pristine white sandy beaches stretching as far as the eye can see, crystal-clear turquoise lagoons teeming with vibrant marine life, and lush palm-fringed islands that exude serenity. The Maldives offers all this and more, making it the perfect escape from the hustle and bustle of everyday life.',
		"Whether you're a sunseeker yearning for the ultimate beach vacation or an adventure enthusiast eager to explore the underwater wonders, the Maldives has something for everyone. Snorkel or dive among stunning coral reefs, where you'll encounter graceful manta rays, colorful tropical fish, and even gentle whale sharks. Unleash your inner explorer as you embark on a thrilling island-hopping adventure, discovering secluded atolls and secluded beaches that feel like your very own slice of paradise.",
		"For those in search of luxury, the Maldives surpasses all expectations. Indulge in world-class resorts that redefine opulence, offering private villas suspended above the crystal-clear waters, where you can enjoy breathtaking sunsets from your own personal infinity pool. Pamper yourself with rejuvenating spa treatments that draw inspiration from the island's natural surroundings, and savor exquisite culinary experiences that blend international flavors with the freshest seafood catches.",
		'Beyond the beaches, the Maldives is a cultural gem with a rich history and warm-hearted locals who are eager to share their traditions with you. Visit local fishing villages to gain insight into the Maldivian way of life, immerse yourself in traditional music and dance performances, or sample authentic Maldivian cuisine bursting with flavors and spices.',
		"Whether you're seeking a romantic honeymoon, a family getaway, or a rejuvenating escape, the Maldives offers a haven of beauty and tranquility that will leave you with cherished memories for a lifetime. Let the gentle waves, swaying palm trees, and warm hospitality of the Maldivian people sweep you away to a world where time stands still and worries melt away.",
		'Embark on a journey to the Maldives, where paradise becomes a reality, and let the allure of this tropical haven capture your heart and soul. Your dream vacation awaits in the jewel of the Indian Ocean.',
	],
	introImg:
		'https://images.unsplash.com/photo-1512100356356-de1b84283e18?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=775&q=80',

	attractions: {
		title: 'Top Maldives attractions & experiences',
		subtitle: `The Maldives is a tropical paradise known for its pristine beaches, crystal-clear waters, and abundant marine life. Here are some of the top attractions and experiences you can enjoy in the Maldives:`,
		attractionsList: [
			{
				id: 1,
				name: 'Underwater Exploration',
				description:
					'Dive or snorkel in the vibrant coral reefs of the Maldives and discover a mesmerizing world of colorful fish, turtles, and rays.',
				img: 'https://images.unsplash.com/photo-1540206351-d6465b3ac5c1?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 2,
				name: 'Luxurious Resorts',
				description:
					"Indulge in the ultimate luxury experience by staying at one of the Maldives's renowned private resorts, offering breathtaking overwater villas and world-class amenities.",
				img: 'https://images.unsplash.com/photo-1573486851655-f248b5cbb324?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 3,
				name: 'Male',
				description:
					'Explore the capital city of Male, a vibrant hub that offers a mix of traditional and modern attractions, including bustling markets, historic mosques, and contemporary art galleries.',
				img: 'https://images.unsplash.com/photo-1561154320-37810ea1b9ac?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 4,
				name: 'Island Hopping',
				description:
					"Embark on a journey to explore the Maldives's picturesque islands, each offering its unique charm, white sandy beaches, and turquoise lagoons.",
				img: 'https://images.unsplash.com/photo-1574223706388-0e0f6f0390b2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 5,
				name: 'Sunset Cruises',
				description:
					'Witness breathtaking sunsets aboard a traditional Maldivian dhoni boat, as you sail across the tranquil waters and soak in the panoramic views.',
				img: 'https://images.unsplash.com/photo-1504434026032-a7e440a30b68?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 6,
				name: 'Water Sports',
				description:
					"Engage in thrilling water sports activities such as jet skiing, parasailing, and windsurfing, taking advantage of the Maldives's idyllic ocean setting.",
				img: 'https://images.unsplash.com/photo-1555149385-c50f336e28b0?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 7,
				name: 'Manta Ray Snorkeling',
				description:
					"Experience the awe-inspiring encounter with majestic manta rays as you snorkel in the Maldives's renowned manta ray hotspots.",
				img: 'https://images.unsplash.com/photo-1544552866-fef1d68c69b5?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 8,
				name: 'Local Island Visits',
				description:
					'Immerse yourself in the local culture and traditions by visiting inhabited local islands, where you can interact with friendly locals, taste Maldivian cuisine, and explore traditional crafts.',
				img: 'https://images.unsplash.com/photo-1540280369237-dea08361593a?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 9,
				name: 'Dolphin Watching',
				description:
					'Embark on a thrilling dolphin-watching excursion and witness the playful dolphins leaping and swimming alongside your boat in their natural habitat.',
				img: 'https://images.unsplash.com/photo-1607153333879-c174d265f1d2?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
			{
				id: 10,
				name: 'Spa and Wellness',
				description:
					"Indulge in rejuvenating spa treatments and wellness retreats offered by the Maldives's luxurious resorts, allowing you to unwind and pamper yourself in a serene tropical setting.",
				img: 'https://images.unsplash.com/photo-1488345979593-09db0f85545f?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=200&q=80',
			},
		],
	},

	faqs: [
		{
			question: 'What is the best time to visit the Maldives?',
			answer:
				"The Maldives enjoys a tropical climate throughout the year, making it a desirable destination for beach lovers and water sports enthusiasts. The best time to visit the Maldives is during the dry season, which runs from November to April. During this period, you can expect sunny days, clear skies, and calm sea conditions, ideal for snorkeling, diving, and other water activities. The temperatures range from 25 to 30 degrees Celsius (77 to 86 degrees Fahrenheit), providing a pleasant and comfortable environment. However, it's important to note that the Maldives experiences high tourist traffic during the peak season, especially around December and January. If you prefer a quieter and more affordable experience, consider visiting during the shoulder seasons of May to July or September to November when the weather is still favorable.",
		},
		{
			question: 'What are the popular tourist activities in the Maldives?',
			answer:
				"The Maldives offers a wide range of exciting activities for tourists to enjoy. Some popular tourist activities in the Maldives include:\n\n1. Snorkeling and Diving: Explore the vibrant coral reefs and diverse marine life by snorkeling or diving in the crystal-clear waters of the Maldives.\n\n2. Relaxing on the Beaches: Indulge in the pristine beauty of the Maldivian beaches, with their white sand, turquoise waters, and swaying palm trees. You can sunbathe, enjoy beach picnics, or simply unwind with a book.\n\n3. Island Hopping: Discover the unique characteristics of different islands by going on island-hopping excursions. Each island has its own charm, ranging from luxurious resorts to uninhabited islands.\n\n4. Water Sports: Engage in thrilling water sports activities such as jet skiing, parasailing, and windsurfing, taking advantage of the Maldives' idyllic ocean setting.\n\n5. Manta Ray Snorkeling: Experience the awe-inspiring encounter with majestic manta rays as you snorkel in the Maldives' renowned manta ray hotspots.\n\nThese are just a few examples, and there are many more activities to enjoy in the Maldives, catering to different interests and preferences.",
		},
		{
			question: 'How do I reach the Maldives?',
			answer:
				"The Maldives is accessible by air, and most international visitors arrive at Velana International Airport (MLE) in the capital city of Male. Numerous airlines operate direct flights to the Maldives from major cities around the world. Once you arrive at the airport, you can reach your resort or desired destination through various means, including domestic flights, speedboats, or seaplanes. Many resorts offer transfer services, and it's advisable to arrange transportation in advance. The scenic transfers themselves can be an exciting part of the Maldives experience, as you get to witness the stunning views of the atolls and turquoise waters from above or during a boat ride.",
		},
		{
			question: 'What is the currency used in the Maldives?',
			answer:
				"The official currency of the Maldives is the Maldivian Rufiyaa (MVR). However, US dollars (USD) are widely accepted in most tourist areas, resorts, and shops. It's advisable to carry some cash in USD for small purchases or when visiting local islands. Credit cards are also commonly accepted in major resorts and tourist establishments. However, it's always a good idea to carry some local currency for transactions in more remote areas or when interacting with local vendors.",
		},
		{
			question: 'Do I need a visa to visit the Maldives?',
			answer:
				"Visitors from most countries, including the United States, the United Kingdom, Canada, and Australia, do not require a pre-arrival visa to enter the Maldives. A free 30-day tourist visa will be issued upon arrival at the airport, provided you have a valid passport with at least six months of validity and a confirmed onward or return ticket. However, it's always recommended to check the visa requirements specific to your country of citizenship and ensure your passport meets the validity requirements. If you plan to stay longer than 30 days or engage in activities other than tourism, it's advisable to contact the Maldivian embassy or consulate in your home country for detailed visa information.",
		},
		{
			question: 'What is the official language spoken in the Maldives?',
			answer:
				"The official language of the Maldives is Dhivehi. However, due to the country's significant tourism industry, English is widely spoken and understood, especially in resorts, hotels, and popular tourist areas. You will have no trouble communicating in English with the locals, staff at resorts, and most service providers.",
		},
		{
			question: 'What is the dress code in the Maldives?',
			answer:
				"The Maldives has a relaxed and casual dress code, particularly in tourist areas. Resort islands usually allow guests to dress informally, including wearing swimwear on the beaches and resort premises. However, when visiting local islands or inhabited areas, it's respectful to dress modestly out of consideration for the predominantly Muslim population. Both men and women should avoid wearing revealing or skimpy clothing in public spaces. It's also advisable to cover shoulders and knees when entering mosques or other religious sites.",
		},
		{
			question: 'Is the Maldives a safe destination?',
			answer:
				"The Maldives is generally considered a safe destination for tourists. The country relies heavily on tourism and takes the safety and security of visitors seriously. However, as with any travel destination, it's always advisable to take certain precautions. Keep an eye on your belongings, especially in crowded areas or when using public transportation. If you plan to engage in water activities, ensure you have the necessary skills and follow safety guidelines provided by the activity operators. It's also essential to be mindful of the sun and stay hydrated due to the Maldives' tropical climate. In case of any emergencies or concerns, contact the local authorities or seek assistance from your resort staff.",
		},
		{
			question:
				'Are there any cultural customs or etiquette I should be aware of in the Maldives?',
			answer:
				"While visiting the Maldives, it's respectful to be aware of and adhere to certain cultural customs and etiquette. Here are a few guidelines:\n\n1. Public Displays of Affection: The Maldives is a predominantly Muslim country, and it's advisable to avoid excessive public displays of affection, particularly in local or inhabited areas.\n\n2. Ramadan: If you visit the Maldives during the Islamic holy month of Ramadan, respect the fasting practices and cultural sensitivities of the locals. It's considerate to refrain from eating, drinking, or smoking in public during daylight hours.\n\n3. Shoes: When entering mosques or religious sites, it's customary to remove your shoes.\n\n4. Local Communities: If you plan to visit local islands or interact with local communities, it's respectful to dress modestly and seek permission before taking photographs of individuals or their property.\n\nBy observing these customs, you'll contribute to a positive and culturally sensitive experience in the Maldives.",
		},
	],
};
