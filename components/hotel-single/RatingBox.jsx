import Image from 'next/image';
import { Swiper, SwiperSlide } from 'swiper/react';

import { testimonial2 } from '../../data/testimonialData';
import { Autoplay, Navigation } from 'swiper';
import React from 'react';
import { nanoid } from 'nanoid';

const INDIA_REVIEW_IMAGES = [
	'https://images.unsplash.com/photo-1524492412937-b28074a5d7da?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1742&q=80',
	'https://images.unsplash.com/photo-1519802772250-a52a9af0eacb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1336&q=80',
	'https://images.unsplash.com/photo-1514222134-b57cbb8ce073?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=774&q=80',
	'https://images.unsplash.com/photo-1549468057-5b7fa1a41d7a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1742&q=80',
	'https://images.unsplash.com/photo-1506461883276-594a12b11cf3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80',
];

const REVIEWS = [
	{
		name: 'John Smith',
		city: 'London',
		country: 'United Kingdom',
		avatar: '/img/avatars/testimonials/6.png',
		text: "My 10-day journey with Kata Travels in India was absolutely incredible. From the bustling streets of Delhi to the serene backwaters of Kerala, each day brought new adventures. The guides were knowledgeable and friendly, offering insights into the country's rich history and vibrant culture. The accommodations were comfortable and the food was delicious. I highly recommend this trip for anyone seeking an immersive experience in India!",
	},
	{
		name: 'Emily Johnson',
		city: 'New York City',
		country: 'United States',
		avatar: '/img/avatars/testimonials/7.png',
		text: "I had the most amazing 12-day trip to India with Kata Travels. From the iconic Taj Mahal in Agra to the spiritual atmosphere of Varanasi, every moment was awe-inspiring. The guides were professional and attentive, making sure we had a deep understanding of the country's traditions and customs. The accommodations were top-notch and the local cuisine was a delightful culinary experience. I wholeheartedly recommend this journey to anyone looking to explore the wonders of India!",
	},
	{
		name: 'Olivia Wilson',
		city: 'Toronto',
		country: 'Canada',
		avatar: '/img/avatars/testimonials/8.png',
		text: "My 15-day trip with Kata Travels through India was an absolute dream come true. From the vibrant markets of Jaipur to the peaceful lakes of Udaipur, each destination had its own unique charm. The guides were friendly and knowledgeable, sharing captivating stories about India's history and culture. The accommodations exceeded my expectations, providing a comfortable and luxurious retreat after each day's adventures. I highly recommend this trip to anyone seeking a magical experience in India!",
	},
	{
		name: 'Daniel Brown',
		city: 'Sydney',
		country: 'Australia',
		avatar: '/img/avatars/testimonials/9.png',
		text: "Kata Travels organized a remarkable 7-day tour of India that exceeded all my expectations. From the chaotic streets of Mumbai to the serene beauty of the Taj Mahal, every moment was filled with wonder. The guides were passionate and well-informed, offering fascinating insights into India's diverse culture. The accommodations were cozy and the local cuisine was a delight to the taste buds. I wholeheartedly recommend this trip to anyone seeking an immersive cultural experience in India!",
	},
	{
		name: 'Sophie Davis',
		city: 'Auckland',
		country: 'New Zealand',
		avatar: '/img/avatars/testimonials/10.png',
		text: "My 12-day adventure in India with Kata Travels was truly unforgettable. From the spiritual city of Varanasi to the historical wonders of Jaipur, each day brought new discoveries. The guides were exceptional, providing valuable knowledge about India's rich heritage. The accommodations were comfortable and the local hospitality was heartwarming. I highly recommend this trip to anyone who wants to immerse themselves in the captivating beauty of India!",
	},
];

function generateRandomRatingAndReviewCount() {
	// generate random rating between 4.5 and 4.9 and review count between 100 and 500
	const rating = Math.random() * (4.9 - 4.5) + 4.5;
	const reviewCount = Math.floor(Math.random() * (500 - 100) + 100);

	return {
		rating: rating.toFixed(1),
		reviewCount,
	};
}

function generatepercentTravellersRecommendation() {
	// generate random percentage between 90 and 100
	return Math.floor(Math.random() * (100 - 90) + 90);
}

const RatingBox = ({ hotel }) => {
	const recommendRatings = [
		{
			id: 1,
			name: 'Breakfast',
			numberOfRatings: '25',
		},
		{
			id: 2,
			name: 'WiFi',
			numberOfRatings: '14',
		},
		{
			id: 3,
			name: 'Food & Dining',
			numberOfRatings: '67',
		},
	];

	return (
		<div className='px-30 py-30 border-light rounded-4 mt-80'>
			<div className='d-flex items-center'>
				<div className='size-40 flex-center bg-blue-1 rounded-4'>
					<div className='text-14 fw-600 text-white'>
						{generateRandomRatingAndReviewCount().rating}
					</div>
				</div>
				<div className='text-14 ml-10'>
					<div className='lh-15 fw-500'>
						{generateRandomRatingAndReviewCount().rating > 4.5
							? 'Exceptional'
							: 'Very Good'}
					</div>
					<div className='lh-15 text-light-1'>
						{generateRandomRatingAndReviewCount().reviewCount} reviews
					</div>
				</div>
			</div>
			{/* End d-flex */}

			<div className='d-flex mt-20'>
				<i className='icon-heart text-16 mr-10 pt-5' />
				<div className='text-15'>
					{generatepercentTravellersRecommendation()}% of travellers recommend
					this trip
				</div>
			</div>
			{/* End d-flex */}

			<div className='border-top-light mt-20 mb-20' />

			<div className='testimonials -type-1 bg-white rounded-4'>
				<div className='relative d-flex justify-center overflow-hidden js-section-slider'>
					<Swiper
						modules={[Navigation, Autoplay]}
						loop={true}
						autoplay={{
							delay: 5000,
							disableOnInteraction: false,
						}}>
						{REVIEWS.map((review, index) => (
							<SwiperSlide key={nanoid(5)}>
								<p className='testimonials__text lh-18 fw-500 text-dark-1'>
									&quot;{review.text}&quot;
								</p>
								<div className='pt-20 mt-28 border-top-light'>
									<div className='row x-gap-20 y-gap-20 items-center'>
										<div className='col-auto'>
											<Image
												width={60}
												height={60}
												src={INDIA_REVIEW_IMAGES[index]}
												alt='testimonial image'
												lang='en'
												loading='lazy'
												className='size-60 rounded-circle'
											/>
										</div>
										<div className='col-auto'>
											<div className='text-15 fw-500 lh-14'>{review.name}</div>
											<div className='text-14 lh-14 text-light-1 mt-5'>
												{review.city}, {review.country}
											</div>
										</div>
									</div>
								</div>
							</SwiperSlide>
						))}
					</Swiper>
				</div>
			</div>

			{/* End .row */}
		</div>
	);
};

export default RatingBox;
