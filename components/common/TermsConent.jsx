import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

const TermsConent = () => {
	return (
		<Tabs>
			<div className='row y-gap-30'>
				<div className='col-lg-3'>
					<div className='px-30 py-30 rounded-4 border-light'>
						<TabList className='tabs__controls row y-gap-10 js-tabs-controls'>
							<Tab className='col-12 tabs__button js-tabs-button'>
								General Terms and Conditions
							</Tab>
							<Tab className='col-12 tabs__button js-tabs-button'>
								Privacy Policy
							</Tab>
							<Tab className='col-12 tabs__button js-tabs-button'>Cookies</Tab>
						</TabList>
					</div>
				</div>
				{/* End .col-lg-3 */}

				<div className='col-lg-9'>
					<TabPanel>
						<div className={`tabs__content js-tabs-content`}>
							<div className={`tabs__pane  -tab-item-1 is-tab-el-active`}>
								<h1 className='text-30 fw-600 mb-15'>Terms And Conditions</h1>

								<p>
									The purchase of any travel services offered by Kata Travels
									Private Limited. (the “Company”, “We” or “Us”) are subject to,
									and conditioned upon, the terms and conditions set out herein
									(the “General Terms and Conditions”). Please read the
									following information carefully.
									<br />
									<br />
									In order for a booking to be completed, You, the customer (the
									customer (“You” or “Your”) must indicate you accept these
									General Terms and Conditions by confirming Your booking as set
									forth below. You are advised to check the Company’s website
									located at www.katatravels.com (the “Website” and collectively
									with any other website owned by the Company, the “Company’s
									Website(s)”) for the latest version of these General Terms and
									Conditions or to request the latest version of these General
									Terms and Conditions from the Company, prior to booking your
									tour.
									<br />
									<br />
									Notwithstanding the foregoing, any provision in these General
									Terms and Conditions that states that it is operative by Your
									use of the Website shall govern Your use of the Website, and
									if you do not agree to such terms, you must not use the
									Website. We at the Company believe in serving our clients with
									sincerity and honesty and therefore You can contact us via
									telephone or write to us with Your queries and we will be
									delighted to answer any query that You may have or provide any
									clarification that You may seek.
									<br />
									<br />
								</p>

								<h2 className='text-16 fw-500'>Price Includes</h2>
								<p className='text-15 mt-5'>
									Hotel accommodations, airport transfers, all land
									transportation, tour assistant when indicated, meals where
									indicated, all interstate taxes &amp; permits, parking, toll
									taxes and driver allowances, all meet and assist services,
									daily breakfasts and service charges. The prices at times also
									include (when applicable).
									<br />
									<br />
									The above is an “overview &amp; general idea” and you should
									always refer to your TOUR ITINERARY INCLUSIONS which is
									specifically for your tour plan.
								</p>

								<h2 className='text-16 fw-500 mt-35'>Price does not Include</h2>
								<p className='text-15 mt-5'>
									International airport taxes, entrance fees to monuments and
									parks (if not mentioned otherwise in your tour itinerary),
									camera charges at monuments and national parks, any items of a
									personal nature, additional expenses resulting from the delay
									or extension of a trip due to weather, flight cancellations,
									political disputes, sickness or other causes beyond our
									reasonable control. Lunches and dinners are usually not
									included in the tours. (At some places like the National parks
									or remote areas the same is included)
								</p>

								<h2 className='text-16 fw-500 mt-35'>Special Requests</h2>
								<p className='text-15 mt-5'>
									Specific requests such as adjacent or connecting rooms,
									bedding request, smoking or non smoking rooms and special
									dietary needs should be advised at time of booking. Please
									note that every effort will be made to secure your special
									request; however, they cannot be guaranteed.
								</p>

								<h2 className='text-16 fw-500 mt-35'>Immediate Occupancy</h2>
								<p className='text-15 mt-5'>
									On program where immediate check-in has not been included in
									the program price, Company does request that hotels do their
									utmost to facilitate this service; however it is not
									guaranteed. Most hotels allow check-in to take place during
									the mid-afternoon. Should you wish to have your room ready for
									immediate check-in on arrival, Company can arrange this for an
									additional charges. Please ask for details and prices.
								</p>

								<h2 className='text-16 fw-500 mt-35'>Air Transportation</h2>
								<p className='text-15 mt-5'>
									International airfares are usually not included in the tour
									price. Please ask your tour consultant for airfares quotes if
									you so require. Local airfare when shown separately is
									separate and not part of the tour cost. All international and
									local airfares are subject to change and can attract penalties
									and surcharges until final payment has been received by the
									company. the company will not be in position to confirm air
									reservations until a passport copy is received for each
									traveller. These programs may use the services of any IATA
									carrier. International carriers are subject to international
									air conventions limiting their liability. The limitations of
									liability are contained on the reverse side of the airlines
									ticket or e-ticket received, or on the carrier’s website and
									form part of the terms and condition of this package. The
									passage contract in use by these companies, when issued, shall
									constitute the sole contract between the companies and the
									purchaser of these travel programs and / or passage.
								</p>

								<h2 className='text-16 fw-500 mt-35'>Costs</h2>
								<p className='text-15 mt-5'>
									All prices are quoted in US Dollars and are subject to change
									without notice. Prices are based on tariffs and exchange rates
									in effect at the time of printing. The company reserves the
									right to increase or decrease tour prices at any time. All
									prices are per person based on double occupancy. Single
									supplements are available upon request. Prices are based on
									tariffs and exchange rates in effect at the time of printing.
									The company reserves the right to increase or decrease tour
									prices at any time.
								</p>

								<h2 className='text-16 fw-500 mt-35'>
									Prices &amp; Arrangements
								</h2>
								<p className='text-15 mt-5'>
									Quoted program prices include planning, handling and
									operational charges, and are based on current rate of
									exchange, tariffs and taxes. Tour quotations are usually in
									USD (US$), GBP (£). One can pay us by their local currency too
									(if the arrangement is payment on arrival by cash) but at all
									times the exchange rate charged will be as per the US$
									conversion since our contracts and prices are based on that
									currency. They include planning, handling and operational
									charges, and are based on the current rate of exchange and
									tariffs. These are subject to change depending on
									international currency fluctuation, fuel surcharges, and/or
									inflation by the time one confirms the tour. However once a
									tour price is finalized and tour confirmed (even if tour
									deposit is not made) there is no change in price whatever be
									the reason. Note that even for NON RESIDENT INDIAN TOURISTS we
									always quote in foreign currencies as our special rates and
									tariffs are in foreign currency. the company is under no
									obligation to give breakdown costs involved in any program.
									For a group, modification of program content to accommodate a
									smaller group size will not affect published program price.
								</p>

								<h2 className='text-16 fw-500 mt-35'>Deposits &amp; Booking</h2>
								<p className='text-15 mt-5'>
									For all tours there is a minimum deposit of 40% of the total
									tour value (it is calculated on the basis of hotels used,
									flights reserved, train tickets blocked, etc. and depends on
									the type of tour booked and at the discretion of the company)
									at the time of booking and the balance is payable 30 days
									prior to the first day of the tour.
								</p>

								<h2 className='text-16 fw-500 mt-35'>Payments</h2>
								<p className='text-15 mt-5'>
									All payments are to be SWIFT TRANSFERS or MASTER / VISA /
									AMERICAN EXPRESS CARD payments (refer payment methods document
									for the formats for each method) and all payments after
									arrival in India (if any) could be also by cash and
									Travellers’ Cheques in addition to the methods mentioned above
									excepting bank transfer. We DO NOT ACCEPT bank drafts and
									personal cheques
								</p>

								<h2 className='text-16 fw-500 mt-35'>Passports &amp; Visas</h2>
								<p className='text-15 mt-5'>
									Valid passports are required for travel to the destinations in
									this brochure. Check with your travel agent or company for the
									latest visa requirements. Please note that non-U.S passport
									holders are responsible for obtaining the required
									documentation applicable for entry. Please note also that
									passports must be valid for six months after the return of
									your journey and should also have sufficient blank pages for
									visas and immigrations stamps. Company cannot be held
									responsible should you be denied entry to a country due to
									non-compliance with these requirements. A copy of your valid
									passport must be submitted to Company in order to confirm air
									reservations.
								</p>

								<h2 className='text-16 fw-500 mt-35'>
									Limitation of Liability
								</h2>
								<p className='text-15 mt-5'>
									The Company, its employees, shareholder, officers, owners,
									directors, successors, agents and assigns, doesn’t own or
									operate any entity which is to or does provide goods or
									services for your trip. It purchases transportation (by
									aircraft, coach, train, vessel or otherwise), hotel and other
									lodging accommodations, restaurant, ground handling and other
									services from various independent suppliers (including from
									time to time other affiliated companies). All such persons and
									entities are independent contractors. As a result the Company
									is not liable for any negligent or willful act of any such
									person or entity or of any third person.
									<br />
									<br />
									The Company and its operators and agents will assume no
									liability for injury, damage, financial or physical loss,
									accident, death, delay or damage to personal property in
									connection with the provision of any goods or services whether
									resulting from but not limited to act of God or force majeure,
									illness, disease, inconvenience, irregularity or additional
									costs resulting directly or indirectly from any of the
									following causes: weather, quarantines, acts of war, civil
									disturbances, theft, government regulations, insurrection or
									revolt, animals, strikes or other labor activities, criminal
									or terrorist activities of anykind, overbooking or downgrading
									of services, food poisoning, mechanical or other failure of
									aircraft or other means of transportation or failure of any
									transportation mechanism to arrive or depart on time. The
									Company reserves the right to change any itinerary at any time
									when it is deemed advisory for thesafety of the guest or the
									group or to comply with government restrictions.
									<br />
									<br />
									There are many inherent risks in adventure travel of the type
									involved here, which can lead to illness, injury or even
									death. These risks are increased by the fact that these trips
									take place in remote locations, far from medical facilities.
									Passengers assume all such risks associated with participating
									in this trip.
								</p>

								<h2 className='text-16 fw-500 mt-35'>
									Assumption of Risk &amp; Release of Liability
								</h2>
								<p className='text-15 mt-5'>
									By booking the tour with the Company, you acknowledge the risk
									and hazards of travel in India. You also accept the
									responsibility for your own welfare and waive future claims
									against the Company for liability to the maximum extent
									permitted by law. You understand that the Company contracts
									with a network of operators in India, Sri Lanka, Maldives,
									Tibet, Bhutan &amp; Nepal through affiliate tour operators.
									These third party service providers are deemed qualified and
									experienced to the best of company’s (the Company) knowledge.
									Therefore, you fully release and hold harmless the Company
									from any and all liability for all their acts or omissions.
									You agree that this Assumption of Risk and Release of
									Liability statement shall be legally binding upon yourself,
									all minors under the age of 18 traveling with you, your heirs,
									successors, and legal representatives. You further agree to
									fully indemnify the Company from all and any liabilities to
									the maximum extent permitted by law and for all its costs,
									expenses, damages, etc (including attorney’s fees) arising
									from or attributable to any claims or actions arising from
									your travel with the Company and which claims or actions are
									specifically released by you in this document.
								</p>

								<h2 className='text-16 fw-500 mt-35'>
									Travel advisories and Warnings
								</h2>
								<p className='text-15 mt-5'>
									It is the responsibility of the traveller to stay informed
									about the most current travel advisories and warnings by
									referring to the relevant State Department’s travel website.
									In the event of an active State Department Travel warnings
									against travel to the specific destination locations of the
									trip, should the traveller still choose to travel,
									notwithstanding any travel advisory or warning, The Traveller
									assumes all risk of personal injury, death or property damage
									that may arise out of the events like those advised or warned
									against.
								</p>

								<h2 className='text-16 fw-500 mt-35'>
									Travellers &amp; Representations
								</h2>
								<p className='text-15 mt-5'>
									The Traveller represents that neither he nor she nor anyone
									travelling with him or her has any physical or other condition
									or disability that could create a hazard to himself or herself
									or other members of the program. Company reserves the right to
									decline to accept anyone on a trip. Company reserves the right
									to remove from the trip, at his or her sole expense, anyone
									whose condition is such that he or she could create a hazard
									to himself or others, or otherwise impact the enjoyment of
									other passengers on the trip. ANY PAYMENT TO COMPANY
									CONSTITUTES YOUR ACCEPTANCE OF THE TERMS AND CONDITIONS SET
									OUT HEREIN AND IN MORE SPECIFIC PREDEPARTURE PASSENGER
									DOCUMENTATION, INCLUDING the Company TOUR BOOKING FORM.
									<br />
									<br />
									<strong> Gratuities: </strong> Gratuities are not included in
									the program price and are entirely at the discretion of the
									traveller and should be based on the level of satisfaction
									with the service received. Recommended guidelines are sent to
									you along with your pre-tour documentation.
								</p>

								<h2 className='text-16 fw-500 mt-35'>
									Photography during Travel
								</h2>
								<p className='text-15 mt-5'>
									The Company reserves the right to take photographs during the
									operation of any program or part thereof and to use them for
									promotional purposes. By booking a program with company,
									program members agree to allow their images to be used in such
									photographs. Program members who prefer that their images not
									used are asked to identify themselves to their Tour Director
									at the beginning of their program.
								</p>

								<h2 className='text-16 fw-500 mt-35'>Footnote</h2>
								<p className='text-15 mt-5'>
									Booking a tour with us suggests that all of us understand and
									accept the above. This document has been well researched and
									most of the inputs have come from our past guests and even the
									language and final editing has been done by some of our good
									friends who travelled with us as clients from all over the
									world. Still if the language is too harsh or the conditions at
									times seem biased and in favor of the company kindly accept
									our apologies for the same as it is unintended. At the same
									time the hotels, flights, trains, etc. have stringent
									cancellation rules that we need to follow to be in business
									with them and maintain a sound track record. We always try to
									be very fair and are known for our “Service First” &amp; “At
									Your Service” attitude.
								</p>
							</div>

							<div className='tabs__pane -tab-item-3'>
								<h1 className='text-30 fw-600 mb-15'>Cookie Policy</h1>

								<h2 className='text-16 fw-500'>1. Your Agreement</h2>
								<p className='text-15 mt-5'>
									We use cookies to collect and store personal information such
									as your name, email address, and other specific information.
									However, this data is anonymous as the cookies do not contain
									any personal identifying information.
									<br />
									<br />
									We use data collected from cookies to:
									<br />
									- personalize your experience and provide you with content
									that is tailored to your interests
									<br />
									- process transactions
									<br />
									- send you periodic emails with information about our products
									services, contests, and promotions.
									<br />
									- administer contests promotions, and surveys
									<br />
									- improve
									<br />
									<br />
									You can delete and disable cookies in your browser settings.
									To learn more about how to manage cookies, please visit
									http://aboutcookies.org
									<br />
									<br />
									We may update this Cookie Policy from time to time. If we make
									any material changes, we will notify you by email (only if you
									have provided us with your email address) or by placing a
									prominent notice on our website.
								</p>
							</div>
						</div>
					</TabPanel>
					{/* End  General Terms of Use */}

					<TabPanel>
						<div className='tabs__content js-tabs-content' data-daos='fade'>
							<div className={`tabs__pane -tab-item-2 is-tab-el-active`}>
								<h1 className='text-30 fw-600 mb-15'>Privacy Policy</h1>

								<h2 className='text-16 fw-500'>
									What personal data we collect and why we collect it?
								</h2>
								<p className='text-15 mt-5'>
									At Kata Travels, we take your privacy seriously. That&amp;s
									why we only collect the personal data that we need in order to
									provide our services. We collect your name, email address, and
									IP address so that we can identify you and provide you with
									the best possible experience. We also collect your payment
									information so that we can process your payments and provide
									you with the services you&amp;ve purchased.
									<br />
									<br />
									We understand that your personal data is important to you, and
									we take steps to protect it. We encrypt all of our data to
									ensure that it remains private and secure. We also never sell
									or share your personal data with any third parties.
									<br />
									<br />
									If you have any questions about our privacy policy, please
									don&amp;t hesitate to contact.
								</p>

								<h2 className='text-16 fw-500 mt-35'>
									How long we retain your data?
								</h2>
								<p className='text-15 mt-5'>
									We retain your data for as long as is necessary for the
									purposes outlined in our Privacy Policy. This may include
									keeping your data for purposes of providing you with our
									services, complying with our legal obligations, and protecting
									our rights and interests. Once your data is no longer needed,
									we will take steps to delete it in a secure manner. If you
									have any questions about how long we retain your data, please
									contact us.
								</p>

								<h2 className='text-16 fw-500 mt-35'>
									What rights you have over your data?
								</h2>
								<p className='text-15 mt-5'>
									When it comes to your data, you have certain rights under
									privacy laws. For example, you have the right to know what
									personal information is being collected about you and why. You
									also have the right to access your personal information and
									request corrections if it is inaccurate. In some cases, you
									may even have the right to have your personal information
									erased.
									<br />
									<br />
									Privacy laws vary from country to country, so it is important
									to check the laws in your specific jurisdiction. However, in
									general, you have the right to know what personal information
									is being collected about you, the right to access that
									information, and the right to have it corrected if it is
									inaccurate. You may also have the right to have your personal
									information erased in certain circumstances.
								</p>
								<h2 className='text-16 fw-500 mt-35'>
									How we protect your data?
								</h2>
								<p className='text-15 mt-5'>
									At Kata Travels we are committed to protecting the privacy of
									our customers&amp; data. We have implemented a number of
									measures to ensure that data is protected from unauthorized
									access, use, or disclosure.
									<br />
									<br />
									Our privacy policy is designed to give customers clear and
									concise information about how we collect, use, and disclose
									their personal data. We have also implemented technical and
									organizational measures to protect data from unauthorized
									access, use, or disclosure.
									<br />
									<br />
									We require all employees and contractors to sign
									confidentiality agreements that prohibit them from disclosing
									customer data to unauthorized individuals or using it for
									unauthorized purposes. We also encrypt all customer data in
									transit and at rest.
									<br />
									<br />
									In addition, we have implemented physical security measures to
									protect our data centers from unauthorized access.
								</p>
								<h2 className='text-16 fw-500 mt-35'>
									Changes to our privacy policy
								</h2>
								<p className='text-15 mt-5'>
									We may update our privacy policy from time to time. If we make
									any material changes, we will notify you by email (only if you
									have provided us with your email address) or by placing a
									prominent notice on our website.
								</p>
								<h2 className='text-16 fw-500 mt-35'>How to contact us?</h2>
								<p className='text-15 mt-5'>
									If you have any questions about our privacy policy, please
									feel free to contact us. You can reach us by email at
									info@katatravels.com, or by phone at +91 97180 81966. We will
									be happy to answer any questions you may have. Thank you for
									your interest in our privacy policy.
								</p>
							</div>

							<div className='tabs__pane -tab-item-3'>
								<h1 className='text-30 fw-600 mb-15'>Cookie Policy</h1>

								<h2 className='text-16 fw-500'>1. Your Agreement</h2>
								<p className='text-15 mt-5'>
									We use cookies to collect and store personal information such
									as your name, email address, and other specific information.
									However, this data is anonymous as the cookies do not contain
									any personal identifying information.
									<br />
									<br />
									We use data collected from cookies to:
									<br />
									- personalize your experience and provide you with content
									that is tailored to your interests
									<br />
									- process transactions
									<br />
									- send you periodic emails with information about our products
									services, contests, and promotions.
									<br />
									- administer contests promotions, and surveys
									<br />
									- improve
									<br />
									<br />
									You can delete and disable cookies in your browser settings.
									To learn more about how to manage cookies, please visit
									http://aboutcookies.org
									<br />
									<br />
									We may update this Cookie Policy from time to time. If we make
									any material changes, we will notify you by email (only if you
									have provided us with your email address) or by placing a
									prominent notice on our website.
								</p>
							</div>
						</div>
					</TabPanel>
					{/* End  Privacy policy */}

					<TabPanel>
						<div className='tabs__content js-tabs-content' data-daos='fade'>
							<div className='tabs__pane -tab-item-3 is-tab-el-active'>
								<h1 className='text-30 fw-600 mb-15'>Cookie Policy</h1>

								<h2 className='text-16 fw-500'>1. Your Agreement</h2>
								<p className='text-15 mt-5'>
									We use cookies to collect and store personal information such
									as your name, email address, and other specific information.
									However, this data is anonymous as the cookies do not contain
									any personal identifying information.
									<br />
									<br />
									We use data collected from cookies to:
									<br />
									- personalize your experience and provide you with content
									that is tailored to your interests
									<br />
									- process transactions
									<br />
									- send you periodic emails with information about our products
									services, contests, and promotions.
									<br />
									- administer contests promotions, and surveys
									<br />
									- improve
									<br />
									<br />
									You can delete and disable cookies in your browser settings.
									To learn more about how to manage cookies, please visit
									http://aboutcookies.org
									<br />
									<br />
									We may update this Cookie Policy from time to time. If we make
									any material changes, we will notify you by email (only if you
									have provided us with your email address) or by placing a
									prominent notice on our website.
								</p>
							</div>
						</div>
					</TabPanel>
					{/* End  Cookie Policy */}
				</div>
				{/* End col-lg-9 */}
			</div>
		</Tabs>
	);
};

export default TermsConent;
