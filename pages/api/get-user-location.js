// get user location from ip address

import axios from 'axios';

export default async function getUserLocation(req, res) {
	const ip =
		process.env.NODE_ENV === 'development'
			? '2803:9800:902e:b6ce:5d9d:c9f7:aaa1:e0cd'
			: req.headers['x-forwarded-for'] || req.connection.remoteAddress;

	// const url = `http://api.ipstack.com/${ip}?access_key=${
	// 	process.env.IP_STACK_KEY || '5b43bca7e46d3c7d7bf715d9f030eb75'
	// }`;

	// const url = `http://apiip.net/api/check?ip=${ip}&accessKey=${'d94c7594-5f31-4782-97f0-782a2fda9f78'}`;
	const url =
		'http://apiip.net/api/check?ip=' +
		ip +
		'&accessKey=' +
		'd94c7594-5f31-4782-97f0-782a2fda9f78';

	try {
		const response = await axios.get(url);
		res.status(200).json(response.data);
	} catch (error) {
		return res.status(400).json({ error: error.message });
	}
}
