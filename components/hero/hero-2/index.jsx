/* eslint-disable @next/next/no-img-element */
import Image from 'next/image';
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { addCurrentTab } from '../../../features/hero/findPlaceSlice';
import MainFilterSearchBox from './MainFilterSearchBox';

const MASTERHEAD_BG = [
	'/img/masthead/2/bg.png',
	'/img/masthead/3/bg.png',
	'/img/masthead/6/bg.png',
	'/img/masthead/egypt-wallpaper.jpg',
	'https://images.unsplash.com/photo-1528181304800-259b08848526?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
	'/img/masthead/caribe.jpg',
	'https://images.unsplash.com/photo-1508050919630-b135583b29ab?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1171&q=80',
];

const SIDE_IMAGES = [
	{
		img: [
			'https://images.unsplash.com/photo-1599476160130-3af44b69ec6e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=411&h=504&q=80',
			'https://images.unsplash.com/photo-1543198926-22fea2a870dd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
			'https://images.unsplash.com/flagged/photo-1577604981316-298e453a19dd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1173&q=80',
		],
	},
];

const Index = () => {
	const { tabs, currentTab } = useSelector((state) => state.hero) || {};

	const dispatch = useDispatch();

	useEffect(() => {
		const interval = setInterval(() => {
			const currentIndex = tabs.findIndex((tab) => tab?.name === currentTab);
			const nextIndex = currentIndex + 1 === tabs.length ? 0 : currentIndex + 1;
			dispatch(addCurrentTab(tabs[nextIndex]?.name));
		}, 5000);
		return () => clearInterval(interval);
	}, [currentTab, dispatch, tabs]);

	return (
		<section className='masthead -type-2 z-2'>
			<div className='masthead__bg bg-dark-3'>
				<Image
					alt='image'
					width={1920}
					height={1080}
					src={
						currentTab === 'India'
							? 'https://katatravels.com/static/img/pages/kata-travels-india.jpeg'
							: currentTab === 'Maldivas'
							? MASTERHEAD_BG[5]
							: currentTab === 'Sudeste Asiático'
							? MASTERHEAD_BG[4]
							: currentTab === 'Medio Oriente'
							? MASTERHEAD_BG[3]
							: MASTERHEAD_BG[0]
					}
					className='js-lazy'
				/>
			</div>
			{/* End bg image */}

			<div className='container'>
				<div className='masthead__tabs'>
					<div className='tabs -bookmark-2 js-tabs'>
						<div className='tabs__controls d-flex items-center js-tabs-controls'>
							{/* {tabs?.map((tab) => (
								<button
									key={tab?.id}
									className={`tabs__button px-30 py-20 sm:px-20 sm:py-15 rounded-4 fw-600 text-white js-tabs-button ${
										tab?.name === currentTab ? 'is-tab-el-active' : ''
									}`}
									onClick={() => dispatch(addCurrentTab(tab?.name))}>
									<i className={`${tab.icon} text-20 mr-10 sm:mr-5`}></i>
									{tab?.name}
								</button>
							))}
							<button
								className={`tabs__button px-30 py-20 sm:px-20 sm:py-15 rounded-4 fw-600 text-white js-tabs-button is-tab-el-active underline`}
								onClick={() => console.log('Salidas Grupales')}>
								<i className={`icon-group text-20 mr-10 sm:mr-5`}></i>
								Salidas Grupales
							</button> */}
						</div>
					</div>
				</div>
				{/* End .masthead__tabs */}

				<div className='masthead__content'>
					<div className='row y-gap-40' style={{ height: 500 }}>
						<div
							className='col-xl-12 text-center'
							data-aos='fade-up'
							data-aos-offset='0'>
							<h1 className='z-2 text-60 lg:text-40 md:text-30 text-white pt-80 xl:pt-0'>
								<span className='text-yellow-1'>Grandes viajes</span>
								<br />a precios increíbles!
							</h1>
							<p className='z-2 text-white mt-20'>
								¡lugares increíbles en ofertas exclusivas!
							</p>

							{/* <MainFilterSearchBox /> */}

							{/* End filter content */}
						</div>
						{/* End .col */}

						{/* End .col */}
					</div>
					{/* End .row */}
				</div>
				{/* End .masthead__content */}
			</div>
			{/* End .container */}
		</section>
	);
};

export default Index;
