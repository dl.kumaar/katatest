import { nanoid } from 'nanoid';
import { useState } from 'react';

const Overview = ({ overview = [], tourType = 'privado' }) => {
	const [showMore, setShowMore] = useState(false);

	const handleShowMore = () => {
		setShowMore(!showMore);
	};

	return (
		<>
			<div className='row x-gap-40 y-gap-40'>
				<div className='col-12'>
					<h3 className='text-22 fw-500'>Overview</h3>

					{overview?.map((item, index) => {
						return (
							<p
								className={
									`text-dark-1 text-15 mt-20` +
									(index > 2 && !showMore ? ' d-none' : '')
								}
								key={nanoid()}>
								{item?.children[0]?.text}
							</p>
						);
					})}

					{overview?.length > 2 && (
						<button
							onClick={handleShowMore}
							className='d-block text-14 text-blue-1 fw-500 underline mt-10'>
							{showMore ? 'Show less' : 'Show more'}
						</button>
					)}
				</div>

				{tourType.toLocaleLowerCase() !== 'privado' ? (
					<div className='col-12'>
						<div className='fw-500 mb-10'>Departure details</div>
						<div className='text-15'>
							Departures from 01st April 2022: Tour departs at 8 am (boarding at
							7.30 am), Victoria Coach Station Gate 1-5
						</div>
					</div>
				) : (
					<div className='row y-gap-30'>
						<div className='col-12'>
							<div className='px-24 py-20 rounded-4 bg-green-1'>
								<div className='row x-gap-20 y-gap-20 items-center'>
									<div className='col-auto'>
										<div className='flex-center size-60 rounded-full bg-white'>
											<i className='icon-star text-yellow-1 text-30'></i>
										</div>
									</div>
									<div className='col-auto'>
										<h4 className='text-18 lh-15 fw-500'>
											All our tours are private and customizable!
										</h4>
										<div className='text-15 lh-15'>
											Our private tours leave 365 days a year and are ideal for
											small groups or families who want a trip to their liking
											to meet their needs and preferences. If you want to add or
											remove something from your itinerary, do not hesitate to
											contact us.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				)}

				<div className='col-md-6'>
					<h5 className='text-16 fw-500'>Available languages</h5>
					<div className='text-15 mt-10'>
						English, French, German, Spanish, Italian, Japanese, Chinese,
						Portuguese
					</div>
				</div>

				<div className='col-md-6'>
					<h5 className='text-16 fw-500'>Rescheduling policy</h5>
					<div className='text-15 mt-10'>
						Free date change, until 15 days prior to the start date of the
						experience.
					</div>
				</div>

				<div className='col-12'>
					<h5 className='text-16 fw-500'>Tour highlights</h5>
					<ul className='list-disc text-15 mt-10'>
						<li>
							Travel between the UNESCO World Heritage sites aboard a
							comfortable coach.
						</li>
						<li>Explore with a guide to delve deeper into the history.</li>
						<li>Great for history buffs and travelers with limited time.</li>
					</ul>
				</div>
			</div>
		</>
	);
};

export default Overview;
