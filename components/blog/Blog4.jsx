import { nanoid } from 'nanoid';
import Image from 'next/image';
import Link from 'next/link';
import blogsData from '../../data/blogs';
import { formatDate } from '../../utils/blogUtils';

const Blog4 = () => {
	let blogs = blogsData
		.map((item) => {
			if (!item) return null;

			// skip if no title
			if (item?.title === '(Untitled)') return null;

			if (!item?.title) return null;

			// skip if no feature image
			if (!item?.feature_image) return null;

			// skip if no slug
			if (!item?.slug) return null;

			// skip if no created_at
			if (!item?.created_at) return null;

			return {
				...item,
				delayAnimation: '100',
			};
		})
		.slice(0, 8);

	blogs = blogs.filter((item) => item);

	return (
		<>
			{blogs.map((item) => (
				<div
					className='col-lg-3 col-sm-6'
					key={nanoid(5)}
					data-aos='fade'
					data-aos-delay={item?.delayAnimation || '100'}>
					<Link
						href={`/blog/article/${item?.slug}`}
						className='blogCard -type-1 d-block '>
						<div className='blogCard__image'>
							<div className='ratio ratio-1:1 rounded-4 rounded-8'>
								{!item?.feature_image ? (
									<Image
										width={400}
										height={400}
										className='img-ratio js-lazy'
										src='/img/blog/1.png'
										alt='image'
									/>
								) : (
									<Image
										width={400}
										height={400}
										className='img-ratio js-lazy'
										src={item?.feature_image}
										alt='image'
									/>
								)}
							</div>
						</div>
						<div className='mt-20'>
							<h4 className='text-dark-1 text-18 fw-500'>{item?.title}</h4>
							<div className='text-light-1 text-15 lh-14 mt-5'>
								{formatDate(item?.created_at?.$date)}
							</div>
						</div>
					</Link>
				</div>
			))}
		</>
	);
};

export default Blog4;
