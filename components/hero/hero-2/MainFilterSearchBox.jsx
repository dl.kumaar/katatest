import Link from 'next/link';
import Router from 'next/router';
import { useState } from 'react';
import DateSearch from '../DateSearch';
import GuestSearch from './GuestSearch';
import LocationSearch from './LocationSearch';

const MainFilterSearchBox = () => {
	const [searchValue, setSearchValue] = useState('');
	const [selectedItem, setSelectedItem] = useState(null);

	const locationSearchContent = [
		{
			id: 1,
			name: 'India',
			icon: '🇮🇳',
		},
		{
			id: 2,
			name: 'Nepal',
			icon: '🇳🇵',
		},
		{
			id: 3,
			name: 'Maldivas',
			icon: '🇲🇻',
		},
		{
			id: 4,
			name: 'Tailandia',
			icon: '🇹🇭',
		},
		{
			id: 5,
			name: 'Vietnam',
			icon: '🇻🇳',
		},
		{
			id: 6,
			name: 'Camboya',
			icon: '🇰🇭',
		},
		{
			id: 7,
			name: 'Indonesia',
			icon: '🇮🇩',
		},
		{
			id: 8,
			name: 'Dubai',
			icon: '🇦🇪',
		},
	];
	return (
		<>
			<div className='mainSearch -w-900 z-2 bg-white pr-10 py-10 lg:px-20 lg:pt-5 lg:pb-20 rounded-4 shadow-1 mt-40'>
				<div className='button-grid items-center'>
					<LocationSearch
						searchValue={searchValue}
						setSearchValue={setSearchValue}
						selectedItem={selectedItem}
						setSelectedItem={setSelectedItem}
						locationSearchContent={locationSearchContent}
					/>
					{/* End Location */}

					<div className='button-item'>
						<Link
							href={`/tours?destino=${searchValue.trim().toLowerCase()}`}
							className='mainSearch__submit button -dark-1 py-15 px-35 h-60 col-12 rounded-4 bg-yellow-1 text-dark-1'>
							<i className='icon-search text-20 mr-10' />
							Buscar
						</Link>
					</div>
				</div>
			</div>
		</>
	);
};

export default MainFilterSearchBox;
