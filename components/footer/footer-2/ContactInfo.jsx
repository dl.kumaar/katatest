const ContactInfo = () => {
	const contactContent = [
		{
			id: 1,
			title: 'Support 24/7',
			action: 'tel:+(91) 783 812 438 6',
			text: '+(91) 783 812 438 6',
		},
		{
			id: 2,
			title: 'Booking Enquiries',
			action: 'mailto:hello@katatravels.com',
			text: 'hello@katatravels.com',
		},
	];
	return (
		<>
			{contactContent.map((item) => (
				<div className='mt-30' key={item.id}>
					<div className={'text-14 mt-30'}>{item.title}</div>
					<a href={item.action} className='text-18 fw-500 text-white mt-5'>
						{item.text}
					</a>
				</div>
			))}
		</>
	);
};

export default ContactInfo;
