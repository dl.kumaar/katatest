import Social from '../../common/social/Social';

const Copyright = () => {
	return (
		<div className='row justify-center items-center y-gap-10'>
			<div className='col-auto'>
				<div className='row x-gap-30 y-gap-10'>
					<div className='col-auto'>
						<div className='d-flex items-center'>
							© {new Date().getFullYear()} by
							<a
								href='https://katatravels.com'
								className='mx-2'
								target='_blank'
								rel='noopener noreferrer'>
								Kata Travels Private Limited.
							</a>
							All rights reserved.
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Copyright;
