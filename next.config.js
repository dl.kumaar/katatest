/** @type {import('next').NextConfig} */

// const NOTFOUND_BLOGS = [
// 	'/blog/paseo-por-los-templos-de-bangkok',
// 	'/blog/que-hacer-chiang-mai',
// 	'/blog/cueva-de-phraya-nakhon',
// 	'/blog/wat-mahathat-templo-de-la-gran-reliquia',
// 	'/blog/18-major-tribes-in-india',
// 	'/blog/understanding-india-everything-you-need-to-know-before-you-travel',
// 	'/blog/living-the-high-life-in-lahaul-and-spiti',
// 	'/blog/top-12-largest-cities-in-india',
// 	'/blog/templo-wat-samphran',
// 	'/blog/bibi-ka-maqbara',
// 	'/blog/wat-tha-ka-rong-tambon-ban-pom-tailandia-un-colorido-templo-kitsch-y-floati',
// ];

// const ENGLISH_BLOGS = [
// 	'/blog/unique-ways-of-celebrating-diwali-in-india',
// 	'/blog/top-places-to-visit-in-india-for-sound-and-light-show',
// 	'/blog/11-most-popular-festivals-of-south-india',
// 	'/blog/20-richest-temples-in-india',
// 	'/blog/a-perfect-weekend-in-kochi',
// 	'/blog/architectureart-and-culture-visiting-the-taj-mahal-plan-the-perfect-trip-to-indias-iconic',
// 	'/blog/rann-utsav-2022-2023-indias-biggest-white-desert-festival',
// 	'/blog/boho-bengaluru-shopping-adventures-in-indias-capital-of-kitsch',
// 	'/blog/wildlife-and-natureactivities-a-first-timers-guide-to-indias-andaman-island',
// 	'/blog/kolkata-in-festivals-a-calendar-of-celebrations',
// 	'/blog/12-best-wildlife-safaris-in-north-india',
// 	'/blog/top-22-places-to-visit-in-gujarat',
// 	'/blog/road-travel-practical-destinations',
// 	'/blog/jaswant-thada-jodhpur-the-taj-mahal-of-mewar',
// 	'/blog/20-tourist-places-to-visit-in-agra',
// 	'/blog/the-22-best-beaches-in-india',
// 	'/blog/10-best-local-markets-in-mumbai-for-shopping',
// 	'/blog/top-12-things-to-experience-at-delhi-airport',
// 	'/blog/untitled-2',
// 	'/blog/mausoleums-mosques-and-markets-uncovering-the-amazing-architecture-of-mughal-india',
// 	'/blog/lake-pichola-udaipur-largest-manmade-freshwater-lake',
// 	'/blog/top-trekking-destinations-in-jammu-kashmir',
// 	'/blog/holi-festival-information-guide-in-india',
// 	'/blog/what-to-do-in-delhi-for-2-days',
// 	'/blog/five-unforgettable-first-time-experiences-in-india',
// 	'/blog/history-art-and-toilets-the-best-museums-in-delhi',
// 	'/blog/20-best-trips-in-india',
// 	'/blog/hazrat-nizamuddin-aulia-dargah-delhi',
// 	'/blog/jodhpur-flamenco-and-gypsy-festival-2023',
// 	'/blog/list-of-famous-buddhist-festivals-in-india',
// 	'/blog/when-is-the-best-time-to-visit-golden-triangle-india-delhi-agra-jaipur',
// 	'/blog/top-20-cleanest-cities-in-india',
// 	'/blog/india-places',
// 	'/blog/10-best-wildlife-safaris-in-south-india',
// 	'/blog/20-best-train-journeys-in-india',
// 	'/blog/most-common-religions-in-india',
// 	'/blog/10-temples-in-rajasthan-you-must-visit',
// 	'/blog/living-the-royal-life-in-hyderabad',
// 	'/blog/untitled',
// ];

// const TOURS = [
// 	'/tours/classical-india-tour',
// 	'/tours/golden-triangle-tour',
// 	'/tours/best-of-north-india-tour',
// 	'/tours/taj-express',
// 	'/tours/golden-triangle-tour-with-udaipur',
// 	'/tours/taj-tigers-and-temples-tour',
// 	'/tours/rajasthan-with-mumbai-and-aurangabad-caves-tour',
// 	'/tours/golden-triangle-with-nepal-tour',
// 	'/tours/best-of-india-and-nepal-tour',
// 	'/tours/amazing-north-india-with-dubai-tour',
// 	'/tours/best-of-nepal-tour',
// 	'/tours/highlights-of-north-india',
// 	'/tours/royal-rajasthan-tour',
// ];

// const blogRedirects = NOTFOUND_BLOGS.map((blog) => ({
// 	source: blog,
// 	destination: '/blog',
// 	permanent: true,
// }));

const nextConfig = {
	reactStrictMode: false,
	eslint: {
		ignoreDuringBuilds: true,
	},
	experimental: {
		appDir: true,
	},
	images: {
		domains: [
			'images.unsplash.com',
			'images.pexels.com',
			'katatravels.com',
			'cdn.sanity.io',
			'res-5.cloudinary.com',
			'res-2.cloudinary.com',
			'res-3.cloudinary.com',
			'res-4.cloudinary.com',
		],
	},

	// redirects: async () => [
	// 	...blogRedirects,
	// 	...ENGLISH_BLOGS.map((blog) => ({
	// 		source: blog,
	// 		destination: '/blog',
	// 		permanent: true,
	// 	})),
	// 	...TOURS.map((tour) => ({
	// 		source: tour,
	// 		destination: '/tours',
	// 		permanent: true,
	// 	})),
	// ],
};

module.exports = nextConfig;
