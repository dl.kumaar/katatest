import Image from 'next/image';

const BlockGuide = () => {
	const blockContent = [
		{
			id: 1,
			icon: '/img/featureIcons/3/1.svg',
			title: 'Viajar sin preocupaciones',
			text: `Viaje con tranquilidad gracias a nuestros expertos locales y a nuestras flexibles opciones de reserva.`,
			delayAnim: '0',
		},
		{
			id: 2,
			icon: '/img/featureIcons/3/2.svg',
			title: 'Viajes Personalizados y Grupales',
			text: `Tenemos una amplia variedad de viajes individuales y grupales para que puedas elegir el que más se adapte a tus necesidades.`,
			delayAnim: '50',
		},
		{
			id: 3,
			icon: '/img/featureIcons/3/4.svg',
			title: 'Soporte 24/7',
			text: `Estamos siempre a tu disposición para cualquier duda o consulta antes, durante o después de tu viaje.`,
			delayAnim: '100',
		},
	];

	return (
		<>
			{blockContent.map((item) => (
				<div
					className='col-lg-4 col-sm-6'
					data-aos='fade'
					data-aos-delay={item.delayAnim}
					key={item.id}>
					<div className='featureIcon -type-1 -hover-shadow px-50 py-50 lg:px-24 lg:py-15'>
						<div className='d-flex justify-center'>
							<Image
								src={item.icon}
								alt='image'
								className='js-lazy'
								width={50}
								height={50}
							/>
						</div>
						<div className='text-center mt-30'>
							<h4 className='text-18 fw-500'>{item.title}</h4>
							<p className='text-15 mt-10'>{item.text}</p>
						</div>
					</div>
				</div>
			))}
		</>
	);
};

export default BlockGuide;
