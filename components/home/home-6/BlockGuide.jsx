import Image from 'next/image';

const BlockGuide = () => {
	const blockContent = [
		{
			id: 1,
			icon: '/img/featureIcons/3/1.svg',
			title: 'Travel Carefree',
			text: `Travel with confidence with our local experts and flexible booking options.`,
			delayAnim: '0',
		},
		{
			id: 2,
			icon: '/img/featureIcons/3/2.svg',
			title: 'Customized and 100% private',
			text: `All our tours are 100% private and can be customized to your needs.`,
			delayAnim: '50',
		},
		{
			id: 3,
			icon: '/img/featureIcons/3/4.svg',
			title: 'Trip Support 24/7',
			text: `We are always here to help you with any questions or concerns.`,
			delayAnim: '100',
		},
	];
	return (
		<>
			{blockContent.map((item) => (
				<div
					className='col-lg-4 col-md-6'
					data-aos='fade'
					data-aos-delay={item.delayAnim}
					key={item.id}>
					<div className='d-flex pr-30'>
						<div className='d-flex justify-center'>
							<Image
								src={item.icon}
								alt='image'
								className='size-50'
								width={50}
								height={50}
							/>
						</div>
						<div className='ml-15'>
							<h4 className='text-18 fw-500'>{item.title}</h4>
							<p className='text-15 mt-10'>{item.text}</p>
						</div>
					</div>
				</div>
			))}
		</>
	);
};

export default BlockGuide;
