import Image from 'next/image';

const Slights = ({ attractions }) => {
	return (
		<>
			{attractions.map((item, index) => (
				<div
					className='col-lg-6'
					key={item.id}
					data-aos='fade'
					data-aos-delay={index * 100 + 100}>
					<div className='rounded-4 border-light'>
						<div className='d-flex flex-wrap y-gap-30'>
							<div className='col-12 col-sm-4'>
								<div className='ratio ratio-1:1 sm:w-200'>
									<Image
										fill
										src={item.img ? item.img : '/img/backgrounds/1.png'}
										alt={item.name ? item.name + ' image' : 'attraction image'}
										className='img-ratio'
									/>
								</div>
							</div>
							<div className='col-12 col-sm-8'>
								<div className='d-flex flex-column justify-center h-full px-30 py-20'>
									<h3 className='text-18 fw-500'>{item.name}</h3>
									<p className='text-15 mt-5'>{item.description}</p>
									<button
										style={{
											alignSelf: 'flex-start',
										}}
										className='d-block text-14  text-blue-1 fw-500 underline mt-5'>
										See More
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			))}
		</>
	);
};

export default Slights;
