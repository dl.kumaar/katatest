import { nanoid } from 'nanoid';
import Image from 'next/image';
import Link from 'next/link';
import Slider from 'react-slick';
import toursData from '../../data/tours';
import isTextMatched from '../../utils/isTextMatched';
import { urlFor } from '../../utils/sanityCalient';
import { riboonColor } from '../hotels/FilterHotels';

function randomIntFromInterval(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

const Tours = ({ tours = [] }) => {
	const settings = {
		dots: true,
		infinite: true,
		speed: 400,
		slidesToShow: tours.length > 2 ? 3 : tours.length,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
				},
			},

			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 520,
				settings: {
					slidesToShow: 1,
				},
			},
		],
	};

	var itemSettings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
	};

	// custom navigation
	function Arrow(props) {
		let className =
			props.type === 'next'
				? 'slick_arrow-between slick_arrow -next arrow-md flex-center button -blue-1 bg-white shadow-1 size-30 rounded-full sm:d-none js-next'
				: 'slick_arrow-between slick_arrow -prev arrow-md flex-center button -blue-1 bg-white shadow-1 size-30 rounded-full sm:d-none js-prev';
		className += ' arrow';
		const char =
			props.type === 'next' ? (
				<>
					<i className='icon icon-chevron-right text-12'></i>
				</>
			) : (
				<>
					<span className='icon icon-chevron-left text-12'></span>
				</>
			);
		return (
			<button className={className} onClick={props.onClick}>
				{char}
			</button>
		);
	}

	return (
		<>
			<Slider {...settings}>
				{[...tours].map((item) => (
					<div
						key={nanoid(5)}
						data-aos='fade'
						data-aos-delay={item?.delayAnimation || '100'}>
						<Link
							href={`/luxury-tours-vacation-packages/${item?.slug}`}
							className='tourCard -type-1 rounded-4 hover-inside-slider'>
							<div className='tourCard__image position-relative'>
								<div className='inside-slider'>
									<Slider
										{...itemSettings}
										arrows={true}
										nextArrow={<Arrow type='next' />}
										prevArrow={<Arrow type='prev' />}>
										{item?.tourImageGallery?.length ? (
											item?.tourImageGallery?.map((slide) => (
												<div
													className='cardImage ratio ratio-1:1'
													key={nanoid(5)}>
													<div className='cardImage__content '>
														<Image
															width={300}
															height={300}
															className='col-12 js-lazy'
															src={urlFor(slide).width(300).height(300).url()}
															alt={`${item?.tourName} image`}
														/>
													</div>
												</div>
											))
										) : (
											<div
												className='cardImage ratio ratio-1:1'
												key={nanoid(5)}>
												<div className='cardImage__content '>
													<Image
														width={300}
														height={300}
														className='col-12 js-lazy'
														src={urlFor(item?.tourImage)
															.width(300)
															.height(300)
															.url()}
														alt={`${item?.tourName} image`}
													/>
												</div>
											</div>
										)}
									</Slider>

									<div className='cardImage__leftBadge'>
										<div
											className={`py-5 px-15 rounded-right-4 text-12 lh-16 fw-500 uppercase ${
												riboonColor(item?.tourPromoText1) ||
												'bg-blue-1 text-white'
											}
												`}>
											{item?.tourPromoText1 || '-25% today'}
										</div>
									</div>

									{item?.tourType !== 'private' ? (
										<div className='cardImage__leftBadge mt-40'>
											<div
												className={`py-5 px-15 rounded-right-4 text-12 lh-16 fw-500 uppercase ${
													isTextMatched(item?.tag, 'breakfast included')
														? 'bg-dark-1 text-white'
														: ''
												} ${
													!isTextMatched(item?.tag, 'best seller')
														? 'bg-blue-1 text-white'
														: ''
												} 
                    }`}>
												Group Tour
											</div>
										</div>
									) : null}
								</div>
							</div>

							<div className='tourCard__content mt-10'>
								<div className='d-flex items-center lh-14 mb-5'>
									<div className='text-14 text-light-1'>
										{item?.tourDuration} días
									</div>
									<div className='size-3 bg-light-1 rounded-full ml-10 mr-10' />
									<div className='text-14 text-light-1'>
										{item?.tourType === 'private'
											? 'Tour privado'
											: 'Tour grupal'}
									</div>
									{item?.tourStartDate ? (
										<>
											<div className='size-3 bg-light-1 rounded-full ml-10 mr-10' />
											<div className='text-15 text-bold-1'>
												{formatDate(item?.tourStartDate)}
											</div>
										</>
									) : null}
								</div>
								<h4 className='tourCard__title text-dark-1 text-18 lh-16 fw-500'>
									<span>{item?.tourName}</span>
								</h4>
								<p className='text-light-1 lh-14 text-14 mt-5'>
									{item?.tourExcerpt[0]?.children[0]?.text || ''}
								</p>

								<div className='row justify-between items-center pt-15'>
									<div className='col-auto'>
										<div className='d-flex items-center'>
											<div className='d-flex items-center x-gap-5'>
												<div className='icon-star text-yellow-1 text-10' />
												<div className='icon-star text-yellow-1 text-10' />
												<div className='icon-star text-yellow-1 text-10' />
												<div className='icon-star text-yellow-1 text-10' />
												<div className='icon-star text-yellow-1 text-10' />
											</div>
											{/* End ratings */}

											<div className='text-14 text-light-1 ml-10'>
												{item?.numberOfReviews ||
													randomIntFromInterval(35, 340)}{' '}
												reviews
											</div>
										</div>
									</div>

									{item?.tourPrice ? (
										<div className='col-auto'>
											<div className='text-14 text-light-1'>
												From
												<span className='text-16 fw-500 text-dark-1'>
													{' '}
													US${item?.tourPrice || 200}
												</span>
											</div>
										</div>
									) : null}
								</div>
							</div>
						</Link>
					</div>
				))}
			</Slider>
		</>
	);
};

export default Tours;
