import Image from 'next/image';
import { useState } from 'react';

const Block1 = () => {
	const [readMore, setReadMore] = useState(false);

	const handleReadMore = () => {
		setReadMore(!readMore);
	};

	return (
		<>
			<div className='col-lg-5'>
				<h2 className='text-30 fw-600'>👋 About Kata Travels</h2>

				<p className='text-dark-1 mt-60 lg:mt-40 md:mt-20'>
					At Kata Travels, we specialize in curating bespoke luxury travel
					experiences to India and the captivating subcontinent. With a deep
					passion for exceptional travel, we go above and beyond to create
					tailor-made journeys that embody the essence of opulence, exclusivity,
					and cultural immersion. With meticulous attention to detail, we
					transform your travel dreams into extraordinary realities, ensuring
					that every moment is filled with unmatched luxury and personalized
					touches.
					<br />
					<br />
					<strong>Luxury Tailor-Made Experiences:</strong>
					<br />
					We believe that true luxury lies in customization. Our team of
					experienced travel experts works closely with you to understand your
					unique preferences, interests, and desires. We craft exquisite
					itineraries that reflect your individuality, incorporating luxurious
					accommodations, exclusive experiences, and insider access to the most
					enchanting destinations in India and the subcontinent. Whether you
					envision a private palace retreat in Rajasthan, a serene wellness
					escape in the Himalayas, or a lavish cultural immersion in Kerala, we
					ensure that every aspect of your journey is flawlessly tailored to
					your discerning taste.
					<br />
					<br />
					{readMore && (
						<>
							<strong>
								Unveiling the Treasures of India and the Subcontinent:
							</strong>
							<br />
							India and the subcontinent are a treasure trove of captivating
							experiences, rich traditions, and awe-inspiring landscapes. With
							Kata Travels, you will uncover the hidden gems and iconic wonders
							of this extraordinary region. From the timeless beauty of the Taj
							Mahal and the vibrant streets of Delhi to the lush tea plantations
							of Darjeeling and the tranquil backwaters of Kerala, each
							destination on your itinerary is carefully handpicked to offer an
							authentic and unforgettable luxury experience. Our deep knowledge
							of the region allows us to guide you to the most exclusive and
							extraordinary destinations, ensuring that your journey is nothing
							short of extraordinary.
							<br />
							<br />
							<strong>Unparalleled Service and Attention to Detail:</strong>
							<br />
							We take pride in providing unparalleled service and attention to
							detail. Our dedicated team of luxury travel specialists is
							committed to exceeding your expectations at every turn. From the
							moment you reach out to us until the time you return home, we are
							with you every step of the way, ensuring seamless coordination,
							personalized assistance, and impeccable service. Our passion for
							luxury travel extends to our handpicked network of partners, who
							share our commitment to excellence, ensuring that you receive
							nothing but the finest experiences and accommodations throughout
							your journey.
							<br />
							<br />
							<strong>Creating Extraordinary Memories:</strong>
							<br />
							At the heart of Kata Travels lies the belief that travel should be
							transformative, leaving you with unforgettable memories that last
							a lifetime. With our luxury tailor-made journeys, we aim to create
							moments of pure enchantment, where you connect with the vibrant
							cultures, ancient traditions, and breathtaking landscapes of India
							and the subcontinent. Whether it&apos;s exploring bustling
							markets, savoring delectable cuisine, participating in spiritual
							rituals, or simply immersing yourself in the serene beauty of
							nature, every experience is thoughtfully designed to awaken your
							senses and ignite your passion for travel.
							<br />
							<br />
							<strong>Begin Your Extraordinary Luxury Journey:</strong>
							<br />
							Are you ready to embark on an extraordinary luxury journey through
							India and the subcontinent? Join us at Kata Travels and let us
							transform your travel dreams into an exquisite reality. Explore
							our collection of handcrafted itineraries, inspired destinations,
							and exclusive experiences, and allow us to curate a personalized
							journey that surpasses all expectations. Your extraordinary luxury
							adventure awaits.
						</>
					)}
					{
						<button
							onClick={handleReadMore}
							className='d-block text-14 text-blue-1 fw-500 underline mt-10'>
							{readMore ? 'Read less' : 'Read more'}
						</button>
					}
				</p>
			</div>
			{/* End .col */}

			<div
				className='col-lg-6'
				style={{
					alignSelf: 'baseline',
				}}>
				<Image
					width={400}
					height={400}
					src='/img/pages/about/2.png'
					alt='image'
					className='rounded-4 w-100'
				/>
			</div>
			{/* End .col */}
		</>
	);
};

export default Block1;
