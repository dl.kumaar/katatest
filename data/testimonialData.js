export const testimonial1 = [
	{
		id: 1,
		avatar: '/img/avatars/testimonials/1.png',
		name: 'Annette Black',
		designation: 'UX / UI Designer',
		text: `The place is in a great location in Gumbet. The area is safe and
        beautiful. The apartment was comfortable and the host was kind and
        responsive to our requests. Really a nice place.`,
		dealyAnimation: '100',
	},
	{
		id: 2,
		avatar: '/img/avatars/testimonials/2.png',
		name: 'Annette Black',
		designation: 'UX / UI Designer',
		text: `The place is in a great location in Gumbet. The area is safe and
        beautiful. The apartment was comfortable and the host was kind and
        responsive to our requests. Really a nice place.`,
		dealyAnimation: '200',
	},
	{
		id: 3,
		avatar: '/img/avatars/testimonials/3.png',
		name: 'Annette Black',
		designation: 'UX / UI Designer',
		text: `The place is in a great location in Gumbet. The area is safe and
        beautiful. The apartment was comfortable and the host was kind and
        responsive to our requests. Really a nice place.`,
		dealyAnimation: '300',
	},
	{
		id: 4,
		avatar: '/img/avatars/testimonials/1.png',
		name: 'Annette Black',
		designation: 'UX / UI Designer',
		text: `The place is in a great location in Gumbet. The area is safe and
        beautiful. The apartment was comfortable and the host was kind and
        responsive to our requests. Really a nice place.`,
		dealyAnimation: '400',
	},
];
export const testimonial2 = [
	{
		id: 1,
		meta: 'Classic India Tour',
		avatar:
			'https://images.unsplash.com/photo-1519802772250-a52a9af0eacb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=70&h=70&q=80',
		name: 'Tony Ramirez',
		designation: 'Spain',
		text: `"I have just returned from a wonderful month in India. What an amazing place! From the bustling and vibrant New Delhi to the beautiful Taj Mahal and the 'otherworldly' Rajasthan, we enjoyed every moment. Kata Travels made the entire trip seamless, and their local knowledge meant they were much more than the standard tour operators I have used before. Thank you to everyone."`,
		dealyAnimation: '100',
	},
	{
		id: 2,
		meta: 'Best of India & Nepal Tour',
		avatar:
			'https://images.unsplash.com/photo-1529733905113-027ed85d7e33?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=70&h=70&q=80',
		name: 'Solange Cambet',
		designation: 'Argentina',
		text: `"I am thrilled to have chosen this travel organization for our trip to India and Nepal. This journey had been a long-standing dream of mine, and it surpassed all expectations. Mr. Rajan and his team were incredibly helpful and organized everything seamlessly. The driver was very friendly and accommodating. The hotels were excellent, and the food was superb. I would highly recommend this company to anyone looking to visit India and Nepal."`,
		dealyAnimation: '200',
	},
	{
		id: 3,
		meta: 'Rajasthan Tour',
		avatar:
			'https://images.unsplash.com/photo-1546722228-7baeca4bd0b3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=70&h=70&q=80',
		name: 'Chris Barber',
		designation: 'Londres, Reino Unido',
		text: `"I am incredibly grateful to Kata Travels, whose representatives were so attentive and knowledgeable. They organized the most incredible journeys for me and opened the doors to the exotic and authentic India. Their representatives understood exactly what I, as a European, was looking for – the places, the people I met, the adventures... It was the most thrilling and immersive travel experience I have ever had. Next time, I will bring my family along.".`,
		dealyAnimation: '300',
	},
	{
		id: 4,
		meta: 'Golden Triangle & Maldives Tour',
		avatar:
			'https://images.unsplash.com/photo-1519802772250-a52a9af0eacb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=70&h=70&q=80',
		name: 'Emily Smith',
		designation: 'New York, USA',
		text: `"Exploring the cultural wonders of India's Golden Triangle followed by the serene beauty of the Maldives was the perfect combination. The itinerary was well-planned, the accommodations were luxurious, and the experiences were simply breathtaking. It was an unforgettable journey that exceeded all expectations. Highly recommended!".`,
		dealyAnimation: '300',
	},
];

export const testimonial3 = [
	{
		id: 1,
		meta: 'Hotel Equatorial Melaka',
		avatar: '/img/testimonials/2/1.png',
		name: 'Annette Black',
		designation: 'UX / UI Designer',
		text: `"Our family was traveling via bullet train between cities in
    Japan with our luggage - the location for this hotel made that so
    easy. Agoda price was fantastic."`,
	},
	{
		id: 2,
		meta: 'Hotel Equatorial Melaka',
		avatar: '/img/testimonials/2/2.png',
		name: 'Annette Black',
		designation: 'UX / UI Designer',
		text: `"Our family was traveling via bullet train between cities in
    Japan with our luggage - the location for this hotel made that so
    easy. Agoda price was fantastic."`,
	},
	{
		id: 3,
		meta: 'Hotel Equatorial Melaka',
		avatar: '/img/testimonials/2/3.png',
		name: 'Annette Black',
		designation: 'UX / UI Designer',
		text: `"Our family was traveling via bullet train between cities in
    Japan with our luggage - the location for this hotel made that so
    easy. Agoda price was fantastic."`,
	},
	{
		id: 4,
		meta: 'Hotel Equatorial Melaka',
		avatar: '/img/testimonials/2/4.png',
		name: 'Annette Black',
		designation: 'UX / UI Designer',
		text: `"Our family was traveling via bullet train between cities in
    Japan with our luggage - the location for this hotel made that so
    easy. Agoda price was fantastic."`,
	},
	{
		id: 5,
		meta: 'Hotel Equatorial Melaka',
		avatar: '/img/testimonials/2/5.png',
		name: 'Annette Black',
		designation: 'UX / UI Designer',
		text: `"Our family was traveling via bullet train between cities in
    Japan with our luggage - the location for this hotel made that so
    easy. Agoda price was fantastic."`,
	},
];
