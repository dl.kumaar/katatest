const CategoryTypes = () => {
	const categories = [
		{
			name: 'Adventure',
			count: 12,
		},
		{
			name: 'Beach',
			count: 12,
		},
		{
			name: 'Cultural',
			count: 12,
		},
		{
			name: 'Family',
			count: 12,
		},
		{
			name: 'Honeymoon',
			count: 12,
		},
		{
			name: 'Wildlife',
			count: 12,
		},
		{
			name: 'Day Tours',
			count: 12,
		},
	];

	return (
		<>
			{categories?.map((category) => (
				<div
					className='row y-gap-10 items-center justify-between'
					key={category.name}>
					<div className='col-auto'>
						<div className='form-checkbox d-flex items-center'>
							<input type='checkbox' />
							<div className='form-checkbox__mark'>
								<div className='form-checkbox__icon icon-check' />
							</div>
							<div className='text-15 ml-10'>{category.name}</div>
						</div>
					</div>
					<div className='col-auto'>
						<div className='text-15 text-light-1'>{category.count}</div>
					</div>
				</div>
			))}
		</>
	);
};

export default CategoryTypes;
