import Image from 'next/image';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation, Pagination } from 'swiper';
import Link from 'next/link';
import { useEffect, useRef, useState } from 'react';
import isTextMatched from '../../../utils/isTextMatched';

const TourProperties = ({ toursList = [] }) => {
	const [limit, setLimit] = useState(5);
	const [tours, setTours] = useState(toursList.slice(0, limit));

	const ref = useRef(null);

	useEffect(() => {
		if (!ref?.current) return;

		const observer = new IntersectionObserver(([entry]) => {
			if (entry.isIntersecting) {
				setLimit((prev) => prev + 5);
				setTours(toursList.slice(0, limit));
				observer.unobserve(entry.target);
			}
		});

		observer.observe(ref.current);
	}, [ref, limit, toursList]);

	return (
		<>
			{tours.slice(0, tours.length - 1).map((item) => {
				return (
					<div
						className='col-12'
						key={item?.id}
						data-aos='fade'
						data-aos-delay={item?.delayAnimation}>
						<div className='border-top-light pt-30'>
							<div className='row x-gap-20 y-gap-20'>
								<div className='col-md-auto'>
									<div className='cardImage ratio ratio-1:1 w-250 md:w-1/1 rounded-4'>
										<div className='cardImage__content custom_inside-slider hover-inside-slider'>
											<Swiper
												className='mySwiper'
												modules={[Pagination, Navigation]}
												pagination={{
													clickable: true,
												}}
												navigation={true}>
												{item?.slideImg?.map((slide, i) => (
													<SwiperSlide key={i}>
														<Image
															width={300}
															height={300}
															className='rounded-4 col-12 js-lazy'
															src={slide}
															alt='image'
														/>
													</SwiperSlide>
												))}
											</Swiper>
										</div>

										<div
											className='cardImage__leftBadge'
											style={{
												position: 'absolute',
												top: '20px',
												left: '0',
												width: 'fit-content',
											}}>
											<div
												className={`py-5 px-15 rounded-right-4 text-12 lh-16 fw-500 uppercase ${
													isTextMatched(item?.tag, 'breakfast included')
														? 'bg-dark-1 text-white'
														: ''
												} ${
													isTextMatched(item?.tag, 'best seller')
														? 'bg-blue-1 text-white'
														: ''
												} 
                    } ${
											isTextMatched(item?.tag, '-25% today')
												? 'bg-brown-1 text-white'
												: ''
										} 
                     ${
												isTextMatched(item?.tag, 'top rated')
													? 'bg-yellow-1 text-dark-1'
													: ''
											}`}>
												{item?.tag}
											</div>
										</div>

										<div className='cardImage__wishlist'>
											<button className='button -blue-1 bg-white size-30 rounded-full shadow-2'>
												<i className='icon-heart text-12' />
											</button>
										</div>
									</div>
								</div>
								{/* End .col-auto */}

								<div className='col-md'>
									<div className='row x-gap-10 items-center'>
										<div className='col-auto'>
											<p className='text-14 lh-14 mb-5'>
												{item?.duration} días
											</p>
										</div>
										{/* End col-auto */}

										<div className='col-auto'>
											<div className='size-3 rounded-full bg-light-1 mb-5'></div>
										</div>
										{/* End col-auto */}

										<div className='col-auto'>
											<p className='text-14 lh-14 mb-5'>{item?.tourType}</p>
										</div>
										{/* End col-auto */}
									</div>
									{/* End .row */}
									<Link href={`/tour/tour-single/${item.id}`}>
										<h3 className='text-18 lh-16 fw-500'>{item?.title}</h3>
									</Link>
									<p className='text-14 lh-14 mt-5'>{item?.location}</p>

									{item?.tourType === 'group' ? (
										<div className='text-14 lh-15 fw-500 mt-20'>
											Salida garantizada
										</div>
									) : (
										<div className='text-14 lh-15 fw-500 mt-20'>
											Salidas diarias, en grupo o privado
										</div>
									)}

									<div className='text-14 text-green-2 fw-500 lh-15 mt-5'>
										Cambio de fecha gratuito hasta 30 días antes del viaje*
									</div>
								</div>
								{/* End col-md */}

								<div className='col-md-auto text-right md:text-left'>
									<div className='d-flex x-gap-5 items-center justify-end md:justify-start'>
										<i className='icon-star text-10 text-yellow-1'></i>
										<i className='icon-star text-10 text-yellow-1'></i>
										<i className='icon-star text-10 text-yellow-1'></i>
										<i className='icon-star text-10 text-yellow-1'></i>
										<i className='icon-star text-10 text-yellow-1'></i>
									</div>

									<div className='text-14 lh-14 text-light-1 mt-10'>
										{item?.numberOfReviews} comentarios
									</div>

									<div className='text-14 text-light-1 mt-50 md:mt-20'>
										From
									</div>
									<div className='text-22 lh-12 fw-600 mt-5'>
										US${item?.price}
									</div>
									<div className='text-14 text-light-1 mt-5'>por persona</div>

									<Link
										href={`/tour/tour-single/${item.id}`}
										className='button -md -dark-1 bg-blue-1 text-white mt-24'>
										Ver Más <div className='icon-arrow-top-right ml-15'></div>
									</Link>
								</div>
								{/* End col-md */}
							</div>
							{/* End .row */}
						</div>
						{/* End border-top */}
					</div>
				);
			})}

			{tours.slice(tours.length - 1).map((item) => {
				return (
					<div
						ref={ref}
						className='col-12'
						key={item?.id}
						data-aos='fade'
						data-aos-delay={item?.delayAnimation}>
						<div className='border-top-light pt-30'>
							<div className='row x-gap-20 y-gap-20'>
								<div className='col-md-auto'>
									<div className='cardImage ratio ratio-1:1 w-250 md:w-1/1 rounded-4'>
										<div className='cardImage__content custom_inside-slider'>
											<Swiper
												className='mySwiper'
												modules={[Pagination, Navigation]}
												pagination={{
													clickable: true,
												}}
												navigation={true}>
												{item?.slideImg?.map((slide, i) => (
													<SwiperSlide key={i}>
														<Image
															width={300}
															height={300}
															className='rounded-4 col-12 js-lazy'
															src={slide}
															alt='image'
														/>
													</SwiperSlide>
												))}
											</Swiper>
										</div>
										{/* End .cardImage__content */}

										<div className='cardImage__wishlist'>
											<button className='button -blue-1 bg-white size-30 rounded-full shadow-2'>
												<i className='icon-heart text-12' />
											</button>
										</div>
										{/* End .cardImage__wishlist */}
									</div>
									{/* End cartImage */}
								</div>
								{/* End .col-auto */}

								<div className='col-md'>
									<div className='row x-gap-10 items-center'>
										<div className='col-auto'>
											<p className='text-14 lh-14 mb-5'>
												{item?.duration} días
											</p>
										</div>
										{/* End col-auto */}

										<div className='col-auto'>
											<div className='size-3 rounded-full bg-light-1 mb-5'></div>
										</div>
										{/* End col-auto */}

										<div className='col-auto'>
											<p className='text-14 lh-14 mb-5'>{item?.tourType}</p>
										</div>
										{/* End col-auto */}
									</div>
									{/* End .row */}
									<Link href={`/tour/tour-single/${item.id}`}>
										<h3 className='text-18 lh-16 fw-500'>{item?.title}</h3>
									</Link>
									<p className='text-14 lh-14 mt-5'>{item?.location}</p>

									{item?.tourType === 'group' ? (
										<div className='text-14 lh-15 fw-500 mt-20'>
											Salida garantizada
										</div>
									) : (
										<div className='text-14 lh-15 fw-500 mt-20'>
											Salidas diarias, en grupo o privado
										</div>
									)}

									<div className='text-14 text-green-2 fw-500 lh-15 mt-5'>
										Cambio de fecha gratuito hasta 30 días antes del viaje*
									</div>
								</div>
								{/* End col-md */}

								<div className='col-md-auto text-right md:text-left'>
									<div className='d-flex x-gap-5 items-center justify-end md:justify-start'>
										<i className='icon-star text-10 text-yellow-1'></i>
										<i className='icon-star text-10 text-yellow-1'></i>
										<i className='icon-star text-10 text-yellow-1'></i>
										<i className='icon-star text-10 text-yellow-1'></i>
										<i className='icon-star text-10 text-yellow-1'></i>
									</div>

									<div className='text-14 lh-14 text-light-1 mt-10'>
										{item?.numberOfReviews} comentarios
									</div>

									<div className='text-14 text-light-1 mt-50 md:mt-20'>
										From
									</div>
									<div className='text-22 lh-12 fw-600 mt-5'>
										US${item?.price}
									</div>
									<div className='text-14 text-light-1 mt-5'>por persona</div>

									<Link
										href={`/tour/tour-single/${item.id}`}
										className='button -md -dark-1 bg-blue-1 text-white mt-24'>
										Ver Más <div className='icon-arrow-top-right ml-15'></div>
									</Link>
								</div>
								{/* End col-md */}
							</div>
							{/* End .row */}
						</div>
						{/* End border-top */}
					</div>
				);
			})}
		</>
	);
};

export default TourProperties;
