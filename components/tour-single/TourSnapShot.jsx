const TourSnapShot = ({ duration, tourType }) => {
	return (
		<div className='row y-gap-30 justify-between pt-40' id='tour-overview'>
			<div className='col-md-auto col-6'>
				<div className='d-flex'>
					<i className='icon-clock text-22 text-blue-1 mr-10'></i>
					<div className='text-15 lh-15'>
						Tour duration:
						<br /> {duration} days
					</div>
				</div>
			</div>
			{/* End .col */}

			<div className='col-md-auto col-6'>
				<div className='d-flex'>
					<i className='icon-customer text-22 text-blue-1 mr-10'></i>
					<div className='text-15 lh-15'>
						English speaking
						<br /> guide
					</div>
				</div>
			</div>
			{/* End .col */}

			<div className='col-md-auto col-6'>
				<div className='d-flex'>
					<i className='icon-route text-22 text-blue-1 mr-10'></i>
					<div className='text-15 lh-15'>
						Tour type:
						<br /> {tourType}
					</div>
				</div>
			</div>
			{/* End .col */}
			<div className='col-md-auto col-6'>
				<div className='d-flex'>
					<i className='icon-star text-22 text-blue-1 mr-10'></i>
					<div className='text-15 lh-15'>
						Free date change:
						<br /> until 15 days before departure
					</div>
				</div>
			</div>
		</div>
	);
};

export default TourSnapShot;
