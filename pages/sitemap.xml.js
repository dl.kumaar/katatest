const BLOGS_URL = 'https://katatravels.com/blog/article';

import blogs from '../data/blogs.js';
import { apIClient } from '../utils/sanityCalient';

function generateSiteMap(posts, tours) {
	return `<?xml version="1.0" encoding="UTF-8"?>
   <urlset 
   xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9">
     <url>
     <loc>https://katatravels.com/</loc>
     <lastmod>2023-02-27T00:31:11+00:00</lastmod>
     <changefreq>daily</changefreq>
     <priority>1.0</priority>
     </url>
     <url>
     <loc>https://katatravels.com/luxury-tours-vacation-packages</loc>
     <lastmod>2023-02-27T00:31:11+00:00</lastmod>
     <changefreq>daily</changefreq>
     <priority>1.0</priority>
     </url>
     <url>
     <loc>https://katatravels.com/blog</loc>
     <lastmod>2023-02-27T00:31:11+00:00</lastmod>
     <changefreq>daily</changefreq>
     <priority>0.80</priority>
     </url>
     <url>
     <loc>https://katatravels.com/about</loc>
     <lastmod>2023-02-27T00:31:11+00:00</lastmod>
     <changefreq>daily</changefreq>
     <priority>0.80</priority>
     </url>
     <url>
     <loc>https://katatravels.com/plan-my-trip</loc>
     <lastmod>2023-02-27T00:31:11+00:00</lastmod>
     <changefreq>daily</changefreq>
     <priority>0.80</priority>
     </url>
     <url>
     <loc>https://katatravels.com/contact</loc>
     <lastmod>2023-02-27T00:31:11+00:00</lastmod>
     <changefreq>daily</changefreq>
     <priority>0.80</priority>
     </url>
     <url>
     <loc>https://katatravels.com/terms</loc>
     <lastmod>2023-02-27T00:31:11+00:00</lastmod>
     <changefreq>daily</changefreq>
     <priority>.80</priority>
     </url>
     ${posts
				.map(({ slug }) => {
					return `
       <url>
           <loc>${`${BLOGS_URL}/${slug}`}</loc>
            <lastmod>2023-02-27T00:31:11+00:00</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.70</priority>
       </url>
     `;
				})
				.join('')}
      ${tours
				.map(({ slug }) => {
					return `
        <url>
            <loc>${`https://katatravels.com/luxury-tours-vacation-packages/${slug}`}</loc>
            <lastmod>2023-02-27T00:31:11+00:00</lastmod>
            <changefreq>daily</changefreq>
            <priority>0.90</priority>
        </url>
      `;
				})
				.join('')}
   </urlset>
 `;
}

function SiteMap() {
	// getServerSideProps will do the heavy lifting
}

export async function getServerSideProps({ res }) {
	// We make an API call to gather the URLs for our site
	const query = `*[_type == 'tours' && tags[0] == 'en'] {
    slug,
}`;
	const request = await apIClient.fetch(query);
	const tours = request.map((tour) => ({
		slug: tour.slug.current,
	}));

	// We generate the XML sitemap with the posts data
	const sitemap = generateSiteMap(blogs, tours);

	res.setHeader('Content-Type', 'text/xml');
	// we send the XML to the browser
	res.write(sitemap);
	res.end();

	return {
		props: {},
	};
}

export default SiteMap;
