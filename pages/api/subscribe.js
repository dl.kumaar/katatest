// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import appendData from '../../utils/appendData';

export default async function handler(req, res) {
	const { email } = req.body;
	const dateTime = new Date().toLocaleString('en-US', {
		timeZone: 'America/New_York',
	});

	const userIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

	try {
		const _res = await appendData('subscribers', [
			dateTime,
			email || '',
			userIp || '',
		]);

		res.status(201).json({
			message: 'Success',
		});
	} catch (error) {
		res.status(500).json({
			message: 'Algo salió mal. Por favor, inténtelo de nuevo.',
		});
	}
}
