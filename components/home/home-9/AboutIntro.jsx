import Image from 'next/image';
import Link from 'next/link';

const AboutIntro = () => {
	return (
		<section className='section-bg layout-pt-lg layout-pb-lg'>
			<div className='section-bg__item -right -w-1165 bg-light-2' />
			<div className='section-bg__item -video-left'>
				<div className='row y-gap-30'>
					<div className='col-sm-6'>
						<Image
							src='/img/video/1.png'
							alt='image'
							width={500}
							height={500}
						/>
					</div>
					{/* End .col */}

					<div className='col-sm-6'>
						<Image
							src='/img/video/2.png'
							alt='image'
							width={500}
							height={500}
						/>
					</div>
				</div>
				{/* End .row */}
			</div>
			{/* End .section-bg__item */}

			<div className='container lg:mt-30'>
				<div className='row'>
					<div className='offset-xl-6 col-xl-5 col-lg-6'>
						<h2 className='text-30 fw-600'>
							Seamless Planning for Effortless Elegance
						</h2>
						<p className='text-dark-1 mt-40 lg:mt-20 sm:mt-15'>
							Planning a luxurious journey should be seamless and enjoyable.
							process. We, at Kata Travels, take care of all the intricate
							details, leaving you free to savor the anticipation of your grand
							adventure.
							<br />
							<br />
							Our team of seasoned professionals handles every aspect of your
							trip, from organizing private flights and selecting the most
							prestigious accommodations to coordinating private guides and
							creating bespoke experiences that are tailored to your
							preferences. Sit back, relax, and allow us to curate an effortless
							and elegant journey for you.
						</p>
						<div className='d-inline-block mt-40 lg:mt-30 sm:mt-20'>
							<Link
								href='/plan-my-trip'
								className='button -md -blue-1 bg-yellow-1 text-dark-1'>
								Plan My Trip <div className='icon-arrow-top-right ml-15' />
							</Link>
						</div>
					</div>
				</div>
				{/* End .row */}
			</div>
			{/* End .col */}
		</section>
	);
};

export default AboutIntro;
