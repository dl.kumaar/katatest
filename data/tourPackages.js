module.exports = [
	{
		id: '1',
		name: 'Triángulo de Oro con Nepal',
		excerpt: 'Delhi, Agra, Jaipur y Katmandú',
		slug: 'triangulo-dorado-con-nepal',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '10',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			'Sumérjase en la India y Nepal con este viaje de 11 días. Empiece explorando las principales atracciones de Delhi, Agra (incluidos el Taj Mahal y el Fuerte de Agra) y Jaipur. A continuación, vuele a Katmandú para pasar un día de turismo antes de dirigirse a Patan y Bhaktapur. Por último, vuele de regreso a Delhi y vuelva a casa',
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1587474260584-136574528ed5?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				city: 'Delhi',
				title: 'Visitas de Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '131',
				image:
					'https://images.unsplash.com/photo-1585483103289-39c79411fda9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				city: 'Agra',
				title: 'Delhi - Agra',
				dayActivity: [
					'Después del desayuno, viaje por la nueva autopista interestatal hasta Agra para visitar el Fuerte Mogol de la ciudad y los mercados cercanos, seguido de una visita al Taj al atardecer',
					"Shah Jahan, el emperador que creó el Taj Mahal, fue encarcelado hacia el final de su vida por su hijo, Aurangzeb, en el esplendor dorado de la gran fortaleza-palacio de los mogoles con vistas al río Yamuna, su primera parada turística del día. A través de sus pilares finamente tallados y sus ventanas en arco de cúspide, podrá saborear las mismas vistas románticas de las que disfrutaba el gobernante enfermo. En la orilla opuesta, la tumba de Itimad-ud Daulah alberga los restos de su primer ministro, o 'Visir'. El famoso e intrincado trabajo de incrustación del mausoleo presagiaba el del Taj, que visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí",
				],
				id: '132',
				image:
					'https://images.unsplash.com/photo-1589884674963-c33aec0347a7?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				city: 'Jaipur',
				title: 'Agra - Jaipur via Abhaneri',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece pisos, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
				],
				id: '133',
				image:
					'https://images.unsplash.com/photo-1561486008-1011a284acfb?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				city: 'Jaipur',
				title: 'Jaipur visitas',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, empezando por una excursión a Amber, seguida del Hawa Mahal, el observatorio Jantar Mantar y el mundialmente famoso museo del Palacio de la Ciudad',
					"Encaramado en el borde de una dramática escarpa, el fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, en particular un resplandeciente 'Salón de los Espejos' o 'Sheesh Mahal', revestido de intrincados mosaicos donde el maharajá y sus consortes disfrutaban de recitales de música y poesía",
					"La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Comenzará en el famoso complejo del Palacio de la Ciudad, que incluye el muy fotografiado 'Hawa Mahal', o 'Palacio de los Vientos', una fachada de cinco pisos con elaboradas ventanas enrejadas desde las que las mujeres de la casa real solían ver las procesiones en las calles de abajo. Los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con columnas que hay debajo, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII",
					'Por la tarde regresará al hotel y podrá relajarse',
					'Alojamiento en un hotel de Jaipur',
				],
				id: '134',
				image:
					'https://images.unsplash.com/photo-1603262110263-fb0112e7cc33?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				city: 'Delhi',
				title: 'Jaipur - Delhi',
				dayActivity: [
					'Tendrás tiempo para un último paseo por el casco antiguo de Jaipur antes de conducir a Delhi por la tarde, donde te alojarás en un hotel cercano al aeropuerto antes de tu partida al día siguiente',
				],
				id: '135',
				image:
					'https://images.unsplash.com/photo-1595928607828-6fdaee9c0942?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '7',
				city: 'Kathmandu',
				title: 'Delhi - Kathmandu',
				dayActivity: [
					'Al llegar al aeropuerto internacional Tribhuvan de Katmandú, será recibido y acogido por nuestro ejecutivo y trasladado a su hotel',
					'Katmandú: una floreciente ciudad moderna de casi un millón de habitantes, abarrotada y caótica. Sin embargo, si usted visita para ver los lugares donde los reyes Shah y Malla establecieron su reino y construyeron palacios y pagodas de madera tallada en la arquitectura newari, estupa y luego sus pensamientos sobre Katmandú cambia mucho ',
					'El resto del día puedes dedicarlo a recuperarte del jetlag. Si la energía lo permite, se puede visitar la cercana zona del mercado de Thamel con tiendas, cafés y restaurantes',
				],
				id: '136',
				image:
					'https://images.unsplash.com/photo-1615753378120-247a44dea109?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Kathmandu visitas',
				city: 'Kathmandu',
				dayActivity: [
					'Después del desayuno, disfrute del día completo de turismo por Katmandú que incluye la visita de Swayambhunath, Boudhanath y Pashupatinath',
					"Swayambhunath: También conocido por el nombre de 'El Templo de los Monos', ya que se pueden ver muchos monos en y alrededor de aquí. La belleza de la estupa es una torre dorada con un par de grandes ojos que todo lo ven y que se ha convertido en sinónimo de Katmandú",
					'Boudhanath: Se dice que es la estupa más grande de Nepal, adornada con cuerdas de banderas de oración que cuelgan de una torre dorada y ojos penetrantes es una experiencia agradable',
					'Pashupatinath: uno de los templos más sagrados de la nación, dedicado al Señor Shiva, se alza a orillas del río Bagmati, situado al este de Katmandú. Por la noche, cientos de personas iluminan los ghats con lámparas parpadeantes',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1617469167379-2e33a480dd6f?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '9',
				title: 'Kathamndu visitas',
				city: 'Kathmandu',
				dayActivity: [
					'Hoy después del desayuno, disfrute de la excursión de un día a Patan, Bhaktapur & Kathmandu Durbar Square',
					'Patan: Patan o Lalitpur ambos son lo mismo, pero localmente es bien conocido como Lalitpur. Tiene el mérito de ser la segunda ciudad más grande del valle de Katmandú y de poseer el mayor número de monumentos religiosos que las otras dos ciudades del valle. La mayoría de sus monumentos se encuentran cerca de la Plaza Durbar, y el más pintoresco es el Palacio Real de los Reyes Malla',
					'Bhaktapur: Un lugar más abajo en el valle es Bhaktapur, donde se puede ver la espectacular colección de plazas, palacios, patios y monasterios, todos ellos de gran arquitectura newari. Algo conveniente de la ubicación de Bhaktapur es la ausencia de tráfico pesado, es fácil ir allí',
					'Plaza Durbar de Katmandú: Los grandes reyes Shah y Malla erigieron en arquitectura newari las estupas doradas, las pagodas talladas en madera y los palacios de la plaza Durbar de Katmandú. La plaza Durbar de Katmandú es el centro de este barrio de increíble ambiente. Sus sinuosas calles adoquinadas están bordeadas por más de 50 templos y ciento seis patios de monasterios y numerosos santuarios. Aquí se puede ver un inusitado alboroto de vendedores de flores, sadhus vestidos de color azafrán, vacas errantes y palomas',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1615753378120-247a44dea109?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '10',
				city: 'Delhi',
				title: 'Kathmandu - Delhi - Salida',
				dayActivity: [
					'Hoy le trasladaremos al aeropuerto internacional de Katmandú para su vuelo a Delhi. Nuestro ejecutivo le recibirá a su llegada al aeropuerto internacional de Delhi y le trasladará a un hotel cercano al aeropuerto.',
				],
				id: '139',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1592635196078-9fdc757f27f4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1624486635462-f562f9f287e2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '4',
				url: 'https://images.unsplash.com/photo-1615753378120-247a44dea109?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '5',
				url: 'https://images.unsplash.com/photo-1526712318848-5f38e2740d44?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Crown Plaza',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Trident Agra',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Crystal Sarovar Premiere',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Shangri-La',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
			{
				id: '5',
				name: 'Almondz Hotel',
				city: 'Delhi',
				country: 'India',
				rating: '3',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
			{
				id: '2',
				name: 'Nepal',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '2',
		name: 'Triángulo Dorado con Ranthambore',
		excerpt: 'Delhi, Agra, Ranthambore & Jaipur',
		slug: 'triangulo-dorado-con-ranthambore',
		price: {
			tourPrice: '450',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '9',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			'Emprende un inolvidable viaje de 9 días por la India, visitando Nueva Delhi, Agra y Jaipur. Visite el mundialmente famoso Taj Mahal en Agra y realice un safari en tigre en Ranthambore. Explore los monumentos históricos de Nueva y Vieja Delhi, y visite el Fuerte Amber en Jaipur',
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si se tiene suerte, se pueden ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
				],
				id: '1',
				image:
					'https://images.unsplash.com/photo-1609258678760-ba05d9b95bb9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Visitas de Delhi',
				city: 'Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1551594278-0162be5d00c3?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Delhi a Agra',
				city: 'Agra',
				dayActivity: [
					'Después del desayuno, viaje por la nueva autopista interestatal hasta Agra para visitar el Fuerte Mogol de la ciudad y los mercados cercanos, seguido de una visita al Taj al atardecer',
					"Shah Jahan, el emperador que creó el Taj Mahal, fue encarcelado hacia el final de su vida por su hijo, Aurangzeb, en el esplendor dorado de la gran fortaleza-palacio de los mogoles con vistas al río Yamuna, su primera parada turística del día. A través de sus pilares finamente tallados y sus ventanas en arco de cúspide, podrá saborear las mismas vistas románticas de las que disfrutaba el gobernante enfermo. En la orilla opuesta, la tumba de Itimad-ud Daulah alberga los restos de su primer ministro, o 'Visir'. La famosa e intrincada marquetería del mausoleo prefigura la del Taj, que se visita al final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí",
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1548013146-72479768bada?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Agra - Ranthambore',
				city: 'Ranthambore',
				dayActivity: [
					'Después del desayuno saldremos de Agra y visitaremos la ciudad fortificada de Fatehpur Sikri. Construida por el gran mogol Akbar y abandonada en 1585 debido a la escasez de agua, sus restos bien conservados constituyen una parada fascinante. Desde aquí continuaremos nuestro viaje a Sawai Madophur, donde se encuentra la vasta reserva de vida salvaje del Parque Nacional de Ranthambore, y nuestro hogar durante las dos próximas noches.',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1592635196078-9fdc757f27f4?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				title: 'Parque Nacional de Ranthambore',
				city: 'Ranthambore',
				dayActivity: [
					'A primera hora de la mañana, cuando la niebla aún se está levantando, nos dirigiremos al parque para nuestro primer safari y la oportunidad de disfrutar de la belleza natural de Ranthambore, donde la posibilidad de ver un tigre, así como muchos otros animales salvajes, es tan alta como en cualquier otro lugar de la India. Después regresaremos a nuestro hotel para desayunar y pasar una mañana relajada. Después de comer, volveremos al parque para realizar otro safari y tener la oportunidad de avistar más animales salvajes. Esta noche cenaremos en nuestro hotel',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1592635196078-9fdc757f27f4?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				title: 'Ranthambore a Jaipur',
				city: 'Jaipur',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece plantas, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1603262110263-fb0112e7cc33?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '7',
				title: 'Jaipur visitas',
				city: 'Jaipur',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, empezando por una excursión a Amber, seguida del Hawa Mahal, el observatorio Jantar Mantar y el mundialmente famoso museo del Palacio de la Ciudad',
					"Encaramado en el borde de una dramática escarpa, el fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, en particular un resplandeciente 'Salón de los Espejos' o 'Sheesh Mahal', revestido de intrincados mosaicos donde el maharajá y sus consortes disfrutaban de recitales de música y poesía",
					"La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Comenzará en el famoso complejo del Palacio de la Ciudad, que incluye el muy fotografiado 'Hawa Mahal', o 'Palacio de los Vientos', una fachada de cinco pisos con elaboradas ventanas enrejadas desde las que las mujeres de la casa real solían ver las procesiones en las calles de abajo. Los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con columnas que hay debajo, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII",
					'Por la tarde regresará al hotel y podrá relajarse',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1603262110263-fb0112e7cc33?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Jaipur a Delhi',
				city: 'Delhi',
				dayActivity: [
					'Tendrás tiempo para un último paseo por el casco antiguo de Jaipur antes de conducir a Delhi por la tarde, donde te alojarás en un hotel cercano al aeropuerto antes de tu partida al día siguiente',
				],
				id: '130',
				image: null,
			},
			{
				day: '9',
				title: 'Delhi - salida',
				city: 'Delhi',
				dayActivity: [
					'Traslado al aeropuerto internacional de Nueva Delhi para su vuelo de regreso o de continuación',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1615963244664-5b845b2025ee?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1615963244664-5b845b2025ee?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1616781752326-98562e412619?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1588673133509-13d815c64b5c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '4',
				url: 'https://images.unsplash.com/photo-1604214797312-d65d6cf91408?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '5',
				url: 'https://images.unsplash.com/photo-1623070861283-17d2c364fc03?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '3',
		name: 'Rajasthan con Cuevas de Ajanata y Ellora',
		excerpt: 'Delhi, Agra, Jaipur, Jodhpur, Udaipur, Mumbai & Aurangabad',
		slug: 'rajasthan-con-cuevas-de-ajanata-y-ellora',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '16',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			'Sumérjase en las maravillas de esta tierra mística mientras explora la cultura, la historia y la energía de Rajastán. Este viaje de dos semanas le llevará tanto a sus lugares más famosos como a sus joyas ocultas, lejos de las multitudes. Disfrute de impresionantes vistas desde palacios restaurados que se alzan sobre acantilados, y deléitese con deliciosas comidas caseras en casas de familia regentadas por sus propietarios. Este viaje por la India le acompañará mucho tiempo después de volver a casa',
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1595928607828-6fdaee9c0942?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				city: 'Delhi',
				title: 'Delhi visitas',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '131',
				image:
					'https://images.unsplash.com/photo-1585483103289-39c79411fda9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				city: 'Agra',
				title: 'Delhi - Agra',
				dayActivity: [
					'Después del desayuno, viaje por la nueva autopista interestatal hasta Agra para visitar el Fuerte Mogol de la ciudad y los mercados cercanos, seguido de una visita al Taj al atardecer',
					"Shah Jahan, el emperador que creó el Taj Mahal, fue encarcelado hacia el final de su vida por su hijo, Aurangzeb, en el esplendor dorado de la gran fortaleza-palacio de los mogoles con vistas al río Yamuna, su primera parada turística del día. A través de sus pilares finamente tallados y sus ventanas en arco de cúspide, podrá saborear las mismas vistas románticas de las que disfrutaba el gobernante enfermo. En la orilla opuesta, la tumba de Itimad-ud Daulah alberga los restos de su primer ministro, o 'Visir'. El famoso e intrincado trabajo de incrustación del mausoleo presagiaba el del Taj, que visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí",
				],
				id: '132',
				image:
					'https://images.unsplash.com/photo-1591018653367-9c01498b3320?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				city: 'Jaipur',
				title: 'Agra - Jaipur via Abhaneri',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece pisos, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
				],
				id: '133',
				image:
					'https://images.unsplash.com/photo-1545126126-ebea588ee202?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				city: 'Jaipur',
				title: 'Jaipur visitas',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, empezando por una excursión a Amber, seguida del Hawa Mahal, el observatorio Jantar Mantar y el mundialmente famoso museo del Palacio de la Ciudad',
					"Encaramado en el borde de una dramática escarpa, el fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, en particular un resplandeciente 'Salón de los Espejos' o 'Sheesh Mahal', revestido de intrincados mosaicos donde el maharajá y sus consortes disfrutaban de recitales de música y poesía",
					"La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Comenzará en el famoso complejo del Palacio de la Ciudad, que incluye el muy fotografiado 'Hawa Mahal', o 'Palacio de los Vientos', una fachada de cinco pisos con elaboradas ventanas enrejadas desde las que las mujeres de la casa real solían ver las procesiones en las calles de abajo. Los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con columnas que hay debajo, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII",
					'Por la tarde regresará al hotel y podrá relajarse',
					'Alojamiento en un hotel de Jaipur',
				],
				id: '134',
				image:
					'https://images.unsplash.com/photo-1477586957327-847a0f3f4fe3?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				city: 'Jodhpur',
				title: 'Jaipur - Jodhpur By Road 335 kms  05½  hours',
				dayActivity: [
					'Tras el desayuno, emprenda viaje a Jodhpur, donde pasará dos noches',
					'Capital del antiguo reino de Marwar, Jodhpur debe su prominencia a la ruta comercial que antaño pasaba por sus puertas, conectando los puertos de Gujarat con las ciudades de las llanuras del norte. La riqueza resultante permitió a los gobernantes marwari construir uno de los fuertes más fabulosos de la India, Mehrangarh, en lo alto de una escarpa casi vertical. Las casas cuboides del casco antiguo que se extienden desde su base están pintadas de cien tonos de azul, una práctica que se dice que denota los hogares de los brahmanes locales (pero que en realidad deriva de los intentos de disuadir a las termitas añadiendo sulfato de cobre a la cal). También es visible, al sur, la enorme mole de Umaid Bhavan, un palacio construido en 1929 por el Maharajá local',
					'Alojamiento en un hotel de Jodhpur',
				],
				id: '92',
				image:
					'https://images.unsplash.com/photo-1517330357046-3ab5a5dd42a1?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '7',
				city: 'Jodhpur',
				title: 'Jodhpur',
				dayActivity: [
					'Hoy, con nuestro guía, explore el Fuerte Mehrangarh, Jaswant Thada y el museo del Palacio Umaid Bhawan',
					'Coronando un gigantesco acantilado de arenisca, el Fuerte Mehrangarh es la más imponente de todas las ciudadelas de Rajastán, con unas vistas a la altura de su colorida historia. Bajo sus elevadas murallas, que encierran un museo especialmente interesante de objetos y apartamentos reales, se extiende un gigantesco mosaico de tejados planos, pintados en tonos azules',
					'Más tarde, visite el hermoso Jaswant Thada y el opulento Museo del Palacio de Umaid Bhawan',
				],
				id: '91',
				image:
					'https://images.unsplash.com/flagged/photo-1571689285715-2103c02539df?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				city: 'Udaipur',
				title: 'Jodhpur - Udaipur via Ranakpur',
				dayActivity: [
					'Después del desayuno, emprenda viaje a Udaipur, donde pasará dos noches. Merece la pena hacer una parada en Ranakpur (03 horas) para visitar un conjunto de templos jainistas de intrincado tallado. A continuación, continúe el viaje hasta Udaipur (03 horas)',
					'Llegará a Udaipur a una hora del día en la que los palacios junto al lago lucen su aspecto más exótico. Disfrute de las sublimes vistas mientras toma una copa al atardecer en la azotea de un haveli, mirando a través del agua hacia las colinas Aravalli en la distancia',
					'Alojamiento en un hotel de Udaipur',
				],
				id: '93',
				image:
					'https://images.unsplash.com/photo-1587300533914-9aaeb48205e9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '9',
				title: 'Udaipur',
				city: 'Udaipur',
				dayActivity: [
					'Acompañado por nuestro guía, explore el complejo del Palacio de la Ciudad de Udaipur, el templo de Jagdish y el casco antiguo',
					'Comience el día visitando el palacio de la ciudad de Udaipur, sede de la dinastía Sisodia y una muestra de la opulenta arquitectura Rajput. Tras visitar sus apartamentos y patios ajardinados, pasee hasta el cercano templo de Jagdish antes de adentrarse en el casco antiguo para explorar los mercados. También se puede visitar el jardín de nombre Sahelion Ki Bari',
					'Pasar la noche en un hotel de Udaipur',
				],
				id: '95',
				image:
					'https://images.unsplash.com/photo-1615836245337-f5b9b2303f10?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '10',
				title: 'Udaipur - Mumbai',
				city: 'Mumbai',
				dayActivity: [
					'Mumbai o Bombay es una ciudad joven y vibrante que ostenta la distinción de ser la de mayor diversidad étnica de la India, y todo el mérito se debe a la convergencia de variadas corrientes culturales y cruzadas. Bombay, antiguo nombre de Mumbai, fue la capital del estado indio de Maharashtra. Geográficamente, se encuentra en el suroeste de la India, en una península formada originalmente por siete islotes en la franja costera de Konkan, al oeste del país. Bombay es el centro financiero y de negocios de la India y su puerto se distingue por ser uno de los más activos del país',
					"Mumbai debe su nombre a la diosa local 'Mumba' de la comunidad de pescadores Koli. También se dice que 'Mumba Devi' es una encarnación de la diosa Parvati, esposa del Señor Shiva. El templo de 'Mumba Devi' se encuentra en la parte sureste de la ciudad de Mumbai",
					'Alojamiento en un hotel en Mumbai.',
				],
				id: '96',
				image:
					'https://images.unsplash.com/photo-1601961405399-801fb1f34581?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '11',
				title: 'Mumbai',
				city: 'Mumbai',
				dayActivity: [
					'Después del desayuno en el hotel, disfrute de la visita a Mumbai',
					'Puerta de la India: Un edificio icónico o digamos un hito importante de Mumbai. Construido en 1911, para celebrar la visita del rey Jorge Ⅴ y la reina María. Su arco de basalto amarillo fue diseñado por el arquitecto escocés Geroge Wittet. Esta estructura arquitectónica indo-sarracena con vistas al mar Arábigo se abrió al público en 1924 y desde entonces es uno de los lugares de picnic favoritos de los habitantes de Bombay, además de figurar entre las visitas obligadas en los itinerarios turísticos de indios y extranjeros.',
				],
				id: '97',
				image:
					'https://images.unsplash.com/photo-1601961405399-801fb1f34581?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '12',
				title: 'Mumbai - Aurangabad',
				city: 'Mumbai',
				dayActivity: [
					'En este día, disfrute de la visita de la ciudad de Mumbai antes de tomar un vuelo a Aurangabad. Los puntos turísticos cubiertos en la visita de hoy son algunos de los lugares emblemáticos de la ciudad',
					'Mani Bhawan: Durante la lucha por la independencia del dominio británico, Mani Bhawan fue la residencia provisional del gran luchador por la libertad de la India, Shri Mahatma Gandhi, desde 1917 hasta 1934. Mani Bhawan ha sido testigo del inicio de muchos movimientos históricos de lucha por la libertad, como la no cooperación, Satyagraha, Swadeshi, Khadi y Khilafat. Ahora, esta casa Bhawan de dos plantas es un museo dedicado al Padre de la nación que alberga una biblioteca junto con una galería fotográfica que muestra las imágenes de Gandhi Ji desde sus años de madurez hasta el año 1948, cuando su alma bondadosa dejó el mundo',
					"Jardín Colgante: Jardín construido en 1881 sobre un embalse que suministra agua a Bombay. Este jardín se conoce popularmente como Jardín Colgante y ofrece una vista impresionante de la 'Ciudad de los Sueños', Bombay",
					"Flora Fountain: En el corazón del sur de Bombay se encuentra una estructura de fuente elegantemente tallada llamada Flora Fountain y también conocida como Hutatma Chowk. Inicialmente se planeó dedicarla al gobernador de Bombay, Sir Bartle Frere, en la época del dominio británico en la India, que dotó a Bombay de muchos grandes edificios públicos durante su mandato.  Pero poco antes de su dedicación, en 1864, se le dio el nombre de 'Flora', el nombre de la diosa romana de las flores",
					"El Museo Príncipe de Gales: Este museo, ahora conocido como 'Chhatrapati Shivaji Maharaj Vastu Sangrahalaya', es uno de los museos más conocidos de la India.  Fue diseñado en arquitectura indo-sarracena por el mismo arquitecto escocés George Wittet que dibujó el plano de la estructura de la Puerta de la India. Este museo expone diferentes artículos de todas las regiones de la India, entre los que se incluyen imágenes budistas, pinturas en miniatura y esculturas extraídas de las cuevas de Elefanta",
					'Chhatrapati Shivaji Terminus (Victoria Terminus): Designado Patrimonio de la Humanidad por la UNESCO en la categoría cultural en el año 2004, es un magnífico edificio de arquitectura de renacimiento gótico victoriano. Esta joya histórica y arquitectónica fue diseñada por el arquitecto británico F. W. Stevens y tardó 10 años en terminarse, de 1878 a 1888, y costó alrededor de 2.60.000 libras esterlinas',
					"Marine Drive: Descrito como el 'Collar de la Reina', es el bulevar costero más opulento de Bombay. Ofrece unas vistas impresionantes del mar Arábigo y también una pista ideal para los que hacen footing por las mañanas",
					'Después de la visita de la ciudad de Mumbai y según el horario del vuelo, le trasladamos al aeropuerto de Mumbai para embarcar en el vuelo a Aurangabad.  Una vez que salga del aeropuerto de Aurangabad después de reclamar sus maletas, será recibido por nuestro ejecutivo y trasladado a un hotel. Regístrese y relájese',
					'Aurangabad: Aurangabad se encuentra en la meseta de Deccan, la mayor meseta de la India. Una muy larga historia cultural y artística asociada a esta región desde la era de la Edad de Piedra y que abarca reglas de varias dinastías que contribuye mucho al arte, la arquitectura, los principios culturales, religiosos, etc.  Además de tener una historia muy rica, esta ciudad es un centro de fabricación de pequeñas y medianas industrias que incluyen productos químicos, componentes de automóviles, textiles, etc, y grandes productores de seda y algodón',
					'Alojamiento en un hotel de Aurangabad',
				],
				id: '98',
				image:
					'https://images.unsplash.com/photo-1629283151312-adcc22231788?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '13',
				title: 'Aurangabad',
				city: 'Aurangabad - Ellora Caves &amp; Aurangabad Caves',
				dayActivity: [
					'Después del desayuno, excursión a las Cuevas de Ellora y a las Cuevas de Aurangabad',
					'Cuevas de Ellora: El complejo de las cuevas de Ellora se extiende a lo largo de 2 kilómetros, y comprende 34 monasterios y templos que fueron construidos uno al lado del otro en la pared de un alto acantilado de basalto. Las cuevas de Ellora dan vida a la cultura de la antigua India con su serie continua de monumentos que datan del 600 d.C. al 1000 d.C..  Aquí hay santuarios dedicados al hinduismo, el budismo y el jainismo, y este complejo no es sólo una creatividad artística única, sino una maravilla tecnológica que muestra el espíritu de tolerancia, característica única de la antigua India',
					"Cuevas de Aurangabad: En las afueras de Aurangabad se encuentran las cuevas olvidadas que fueron excavadas entre los siglos II y VI d.C.. Las cuevas 3 y 7 son fascinantes de visitar, ya que la 3 exhibe esculturas que representan escenas de los cuentos legendarios de 'Jataka' y está sostenida por doce columnas extremadamente elaboradas. Y la cueva 7 muestra intrincadas esculturas de mujeres enjoyadas y una estatua de 'Bodhisattva' rezando por la salvación",
					'Regreso al hotel y descanso',
				],
				id: '98',
				image:
					'https://images.unsplash.com/photo-1629353025246-51ebf45025b9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '14',
				title: 'Aurangabad',
				city: 'Aurangabad - Cuevas de Ellora &amp; Cuevas de Aurangaba',
				dayActivity: [
					'Hoy, después del desayuno, excursión de día completo a las famosas Cuevas de Ajanta',
					'Cuevas de Ajanta: A unos 100 kilómetros de Aurangabad se encuentran las mundialmente famosas cuevas de Ajanta, que son 30 cuevas excavadas en la roca enclavadas en las colinas Sahyadri de Ghat Occidental. En el año 1819, un grupo de oficiales del ejército británico encontró un logro asombroso en la forma de las cuevas de Ajanta, que tardaron aproximadamente 600 años en desarrollarse. Talladas con un cincel y un martillo, las bellas cuevas de Ajanta albergaron en su día a monjes budistas. Los hermosos murales de paredes y techos, paneles y esculturas que representan escenas de la vida de Buda son reconocidos como los primeros y mejores ejemplos de arte pictórico budista en todo el mundo',
					'Regreso al hotel y relax',
				],
				id: '98',
				image:
					'https://images.unsplash.com/photo-1620557812584-adee82f3c16d?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '15',
				title: 'Aurangabad - Mumbai',
				city: 'Mumbai',
				dayActivity: [
					'Hoy, después del desayuno, disfrute de la visita turística local de Aurangabad antes de tomar el vuelo a Mumbai. La visita turística local de Aurangabad incluye la visita de:',
					"Fuerte de Daulatabad: A unos 13 km de Aurangabad se encuentra un magnífico fuerte del siglo XII en una colina que antaño se conocía como 'Devgiri'.  Durante el régimen de Tuglaq en el cinturón del Decán, el sultán (emperador) de Delhi Muhammad Bin Tuglaq le dio el nombre de 'Daulatabad'. El nombre 'Daulatabad' significa ciudad de la fortuna. Este fuerte de la época medieval sólo ha sido conquistado a traición. Una serie de túneles misteriosos en medio del fuerte. Cuando un incauto se encontró con el enemigo, le arrojaron antorchas encendidas y, mientras pensaba en escapar, aceite caliente se derramó por su camino",
					'Bibi Ka Maqbara: El hijo del emperador mogol Aurangzeb, Azam Shah, se inspiró en la belleza del Taj Mahal y construyó una tumba en 1668 d.C. para su madre Rabia - ul - Daurani. Esta arquitectura mogol es impresionante, con sus jardines circundantes y canales que ofrecen oportunidades para la soledad',
					'A continuación, según el horario del vuelo, nuestro ejecutivo le trasladará al aeropuerto de Aurangabad para embarcar en su vuelo a Mumbai. A su llegada a Mumbai, nuestro ejecutivo le llevará a un hotel cercano del aeropuerto.',
				],
				image:
					'https://images.unsplash.com/photo-1562080970-98f06a091a9b?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '16',
				city: 'Mumbai',
				title: 'Mumbai - Departure',
				dayActivity: [
					'Nuestro ejecutivo y chófer le trasladan al aeropuerto internacional de Mumbai para su vuelo de regreso o de continuación con gratos recuerdos de la India',
				],
				id: '98',
				image:
					'https://images.unsplash.com/photo-1601961405399-801fb1f34581?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1598190896090-9dc5c70361d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80exels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1598190896090-9dc5c70361d8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '2',
				url: 'https://images.pexels.com/photos/3587030/pexels-photo-3587030.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '3',
				url: 'https://images.pexels.com/photos/4659128/pexels-photo-4659128.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '4',
				url: 'https://images.pexels.com/photos/7437939/pexels-photo-7437939.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '4',
		name: 'Reflejos de Rajastán Tour',
		excerpt: 'Delhi, Agra, Jaipur, Jodhpur & Udaipur',
		slug: 'reflejos-de-rajasthan-tour',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '11',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			'La India es un país repleto de destinos imprescindibles, desde el encanto del Taj Mahal al amanecer hasta la Plaza de los Vientos en la ciudad rosa de Jaipur. En este viaje de 2 semanas a Rajastán, visitaremos algunos de los lugares imprescindibles, así como otros menos conocidos',
			'Le llevaremos a los lugares más emblemáticos, como el Taj Mahal, el Fuerte Amber y el Fuerte Mehrangarh en Jodhpur. También le llevaremos a algunos lugares menos conocidos, pero igualmente impresionantes, como el Templo de Brahma, el Observatorio Jantar Mantar y el Hawa Mahal',
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '1',
				image:
					'https://images.unsplash.com/photo-1585483103289-39c79411fda9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Delhi visitas',
				city: 'Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1609258678760-ba05d9b95bb9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Delhi - Agra',
				city: 'Agra',
				dayActivity: [
					'Después del desayuno, viaje por la nueva autopista interestatal hasta Agra para visitar el Fuerte Mogol de la ciudad y los mercados cercanos, seguido de una visita al Taj al atardecer',
					"Shah Jahan, el emperador que creó el Taj Mahal, fue encarcelado hacia el final de su vida por su hijo, Aurangzeb, en el esplendor dorado de la gran fortaleza-palacio de los mogoles con vistas al río Yamuna, su primera parada turística del día. A través de sus pilares finamente tallados y sus ventanas en arco de cúspide, podrá saborear las mismas vistas románticas de las que disfrutaba el gobernante enfermo. En la orilla opuesta, la tumba de Itimad-ud Daulah alberga los restos de su primer ministro, o 'Visir'. El famoso e intrincado trabajo de incrustación del mausoleo presagiaba el del Taj, que visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí",
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1564507592333-c60657eea523?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Agra - Jaipur',
				city: 'Jaipur',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece pisos, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1603262110263-fb0112e7cc33?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				title: 'Jaipur visitas',
				city: 'Jaipur',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, empezando por una excursión a Amber, seguida del Hawa Mahal, el observatorio Jantar Mantar y el mundialmente famoso museo del Palacio de la Ciudad',
					"Encaramado en el borde de una dramática escarpa, el fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, en particular un resplandeciente 'Salón de los Espejos' o 'Sheesh Mahal', revestido de intrincados mosaicos donde el maharajá y sus consortes disfrutaban de recitales de música y poesía",
					"La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Comenzará en el famoso complejo del Palacio de la Ciudad, que incluye el muy fotografiado 'Hawa Mahal', o 'Palacio de los Vientos', una fachada de cinco pisos con elaboradas ventanas enrejadas desde las que las mujeres de la casa real solían ver las procesiones en las calles de abajo. Los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con columnas que hay debajo, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII",
					'Por la tarde regresará al hotel y podrá relajarse',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1477587458883-47145ed94245?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				title: 'Jodhpur - la ciudad azul',
				city: 'Jodhpur',
				dayActivity: [
					'Tras el desayuno, emprenda viaje a Jodhpur, donde pasará dos noches',
					'Capital del antiguo reino de Marwar, Jodhpur debe su prominencia a la ruta comercial que antaño pasaba por sus puertas, conectando los puertos de Gujarat con las ciudades de las llanuras del norte. La riqueza resultante permitió a los gobernantes marwari construir uno de los fuertes más fabulosos de la India, Mehrangarh, en lo alto de una escarpa casi vertical. Las casas cuboides del casco antiguo que se extienden desde su base están pintadas de cien tonos de azul, una práctica que se dice que denota los hogares de los brahmanes locales (pero que en realidad deriva de los intentos de disuadir a las termitas añadiendo sulfato de cobre a la cal). También es visible, al sur, la enorme mole de Umaid Bhavan, un palacio construido en 1929 por el Maharajá local',
					'Alojamiento en un hotel de Jodhpur',
				],
				id: '130',
				image:
					'https://images.unsplash.com/flagged/photo-1571689285715-2103c02539df?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '7',
				title: 'Jodhpur visitas',
				city: 'Jodhpur',
				dayActivity: [
					'Hoy, con nuestro guía, explore el Fuerte Mehrangarh, Jaswant Thada y el museo del Palacio Umaid Bhawan',
					'Coronando un gigantesco acantilado de arenisca, el Fuerte Mehrangarh es la más imponente de todas las ciudadelas de Rajastán, con unas vistas a la altura de su colorida historia. Bajo sus elevadas murallas, que encierran un museo especialmente interesante de objetos y apartamentos reales, se extiende un gigantesco mosaico de tejados planos, pintados en tonos azules',
					'Más tarde, visite el hermoso Jaswant Thada y el opulento Museo del Palacio de Umaid Bhawan',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1521182289461-22be748bc522?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Jodhpur - Ranakpur - Udaipur',
				city: 'Udaipur',
				dayActivity: [
					'Después del desayuno, emprenda viaje a Udaipur, donde pasará dos noches. Merece la pena hacer una parada en Ranakpur (03 horas) para visitar un conjunto de templos jainistas de intrincado tallado. A continuación, continúe el viaje hasta Udaipur (03 horas)',
					'Llegará a Udaipur a una hora del día en la que los palacios junto al lago lucen su aspecto más exótico. Disfrute de las sublimes vistas mientras toma una copa al atardecer en la azotea de un haveli, mirando a través del agua hacia las colinas Aravalli en la distancia',
					'Alojamiento en un hotel de Udaipur',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1575994532957-773da2f83eb1?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '9',
				title: 'Udaipur visitas',
				city: 'Udaipur',
				dayActivity: [
					'Acompañado por nuestro guía, explore el complejo del Palacio de la Ciudad de Udaipur, el templo de Jagdish y el casco antiguo',
					'Comience el día visitando el palacio de la ciudad de Udaipur, sede de la dinastía Sisodia y una muestra de la opulenta arquitectura Rajput. Tras visitar sus apartamentos y patios ajardinados, pasee hasta el cercano templo de Jagdish antes de adentrarse en el casco antiguo para explorar los mercados. También se puede visitar el jardín de nombre Sahelion Ki Bari',
					'Pasar la noche en un hotel de Udaipur',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1575994532957-773da2f83eb1?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '10',
				title: 'Vuelo a Delhi',
				city: 'Delhi',
				dayActivity: [
					'Tendrá tiempo para dar un último paseo por el casco antiguo de Udaipur antes de tomar un vuelo a Delhi. A su llegada al aeropuerto de Delhi, nuestro ejecutivo le trasladará a un hotel cercano al aeropuerto antes de su salida al día siguiente.',
				],
				id: '130',
				image: null,
			},
			{
				day: '11',
				title: 'Delhi - salidas',
				city: 'Delhi',
				dayActivity: [
					'Con nuestro ejecutivo, traslado en vehículo al aeropuerto internacional de Nueva Delhi para su vuelo de regreso o de continuación',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/7437939/pexels-photo-7437939.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/7437939/pexels-photo-7437939.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1615836245337-f5b9b2303f10?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1569001852726-323fd51b26f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '5',
		name: 'India clásica Tour',
		excerpt: 'Delhi, Varanasi, Khajuraho, Agra & Jaipur',
		slug: 'india-clasica',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '11',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			'El Tour por la India Clásica es quizá la forma más atractiva de acercarse al rico patrimonio cultural de la India. El tour le acerca al ethos cultural, étnico y tradicional de la civilización india moderna',
			'El recorrido le llevará a las 7 ciudades de la India, cada una de las cuales ofrece algo único y diferente. En Delhi, podrá ver la combinación de la arquitectura mogol, la deliciosa comida y los bulliciosos mercados. En Agra, será testigo de la magnificencia del Taj Mahal. En Jaipur, explorará el esplendor del Rajastán real. En Benarés, visitará una de las ciudades más antiguas del mundo',
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '1',
				image:
					'https://images.unsplash.com/photo-1585483103289-39c79411fda9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Delhi visitas',
				city: 'Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1587474260584-136574528ed5?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Delhi - Varanasi',
				city: 'Varanasi',
				dayActivity: [
					'Hoy tomará un corto vuelo a Benarés, una de las ciudades habitadas más antiguas del mundo. Para los hindúes, Benarés es la ciudad del Señor Shiva y una de las ciudades más sagradas de la India. A su llegada a Varanasi, nuestro ejecutivo le traslada al hotel. Check-in y relajarse y permanecer aquí por dos noches. La tarde es libre o libre por su cuenta. Esta noche, asista al espectacular Ganga Aarti en los ghats del río Ganges (Ganga)',
					'En la orilla del Ganges, corrientes de fieles hindúes acuden todavía hoy a bañarse en las aguas sagradas del río, que se cree que lavan los pecados de vidas pasadas. Tras un paseo por los ghats y templos con su guía, regresará a Dashashwamedh Ghat para contemplar el Ganga Aarti, un espectacular ritual en el que equipos de jóvenes sacerdotes vestidos con espléndidos trajes ceremoniales agitan llameantes lámparas de aceite junto al Ganges. Himnos devocionales, cánticos, tambores, campanas y gongs se combinan para crear una atmósfera intensa',
					'Alojamiento en un hotel de Benarés',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1634920540678-df0e179c5243?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Varanasi visitas',
				city: 'Varanasi',
				dayActivity: [
					'Dé un paseo en barca por el río Ganges a primera hora de la mañana para ver a los fieles ofreciendo plegarias a Surya Dev (el Sol) a la orilla del agua',
					'Al amanecer, es el momento de dar un paseo en barca por el río Ganges, cuando los primeros rayos de luz del día iluminan los ghats de baño de Benarés. Miles de fieles se abren paso en la oscuridad previa al amanecer para bañarse en el río a esta hora propicia, y el espectáculo visto desde el agua, con la cálida luz del sol acentuando los colores de los saris de los bañistas, es sin duda uno de los más coloridos y edificantes de la India',
					'Más tarde, excursión a pie por los ghats ribereños y los templos. Regreso al hotel y desayuno',
					'Alrededor del mediodía o un poco antes, le llevaremos a Sarnath',
					"Sarnath, a las afueras de la ciudad, fue el lugar donde Buda dio su primer sermón en el año 530 a.C., revelando el 'Óctuple Sendero' a sus cinco discípulos. El lugar exacto está marcado por la estupa cilíndrica de Dhamekh, construida en el 500 d.C. sobre los restos de otra mucho más antigua encargada por el emperador mauryano Ashoka",
					'Alojamiento en un hotel de Benarés',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1614164974666-057a7c713ba6?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				title: 'Varanasi - Khajuraho',
				city: 'Khajuraho',
				dayActivity: [
					'Tome un vuelo corto a la pequeña ciudad de Khajuraho. A su llegada, nuestro ejecutivo le trasladará al hotel. Disfrute de una visita por la tarde al renombrado arte erótico del Grupo Occidental de templos de Khajuraho',
					'El extraordinario conjunto de santuarios de Khajuraho debe su supervivencia a la lejanía de su ubicación en el centro de la India. Incluso en el periodo comprendido entre los siglos X y XIII, cuando se construyeron, los templos estaban alejados de los caminos trillados y escaparon milagrosamente a la atención de los merodeadores ejércitos musulmanes que destruyeron gran parte del arte religioso de la India en la época medieval. Famosa sobre todo por su erotismo, la escultura de piedra que adorna hoy los santuarios de Khajuraho parece asombrosamente fresca',
					'Alojamiento en un hotel de Khajuraho',
				],
				id: '130',
				image: null,
			},
			{
				day: '6',
				title: 'Khajuraho - Orchha - Agra',
				city: 'Agra',
				dayActivity: [
					'Después del desayuno, tres horas de viaje hasta el tranquilo asentamiento de Orchha para ver los templos y palacios del siglo XVI. Después del almuerzo, corto trayecto hasta Jhansi y embarque en el tren Gatiman Express con destino a Agra. Llegada a Agra por la tarde. Nuestro ejecutivo le recibirá a su llegada a Agra Cantt. Estación de tren y traslado al hotel y check-in. Aquí se alojará durante dos noches',
					'Orchha es uno de nuestros destinos favoritos del norte de la India, gracias a su ambiente tranquilo y a su pintoresca ubicación en medio de los bosques de dhak a orillas del Betwa, un escenario idílico para un almuerzo tranquilo. Los monumentos están desiertos y, en general, en mal estado de conservación, lo que los hace aún más evocadores. Los más emblemáticos son los 14 hermosos chhatris, o cenotafios, que se elevan desde la orilla norte del río, y que tendrá la oportunidad de visitar antes de continuar viaje hacia Agra',
					'Alojamiento en un hotel de Agra',
				],
				id: '130',
				image: null,
			},
			{
				day: '7',
				title: 'Agra - Taj Mahal tour',
				city: 'Agra',
				dayActivity: [
					'La excursión de hoy por Agra comienza con una visita al amanecer al Taj Mahal, considerado uno de los mejores ejemplos de la arquitectura mogol. Regreso al hotel para desayunar y descansar. Más tarde, visita al impresionante Fuerte de Agra, cuyos edificios, de arenisca roja y mármol, son una mezcla de arquitectura hindú e islámica. Por último, vea la ornamentada tumba de Itimad-ud-Daulah. Después, el resto del día es libre',
					'El gran fuerte mogol de Agra, a orillas del río Yamuna, fue el lugar donde el emperador Shah Jahan, creador del Taj Mahal, fue encarcelado al final de su vida por su hijo Aurangzeb, más bien fanático; se dice que el anciano pasaba los días contemplando la tumba a través de las ventanas de un pabellón dorado en la azotea',
					'En la orilla opuesta, la tumba de Itimad-ud Daulah, exquisitamente decorada, es la siguiente parada de la visita de hoy. El trabajo de taracea del mausoleo prefigura el del Taj, que se visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí',
					'Alojamiento en un hotel de Agra',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1548013146-72479768bada?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Agra - Jaipur',
				city: 'Jaipur',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece plantas, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
					'Alojamiento en un hotel de Jaipur.',
				],
				id: '130',
				image: null,
			},
			{
				day: '9',
				title: 'Jaipur city tour',
				city: 'Jaipur',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, comenzando con una parada fotográfica en Hawa Mahal, una fachada de cinco pisos con elaboradas ventanas enrejadas desde donde las mujeres de la casa real solían ver las procesiones en las calles de abajo. A continuación, diríjase al Fuerte Amber',
					"Encaramado en el borde de una dramática escarpa, el Fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, sobre todo un resplandeciente 'Salón de los Espejos', o 'Sheesh Mahal', revestido de intrincados mosaicos donde el Maharajá y sus consortes disfrutaban de recitales de música y poesía",
					'La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Visitaremos Jantar Mantar, un observatorio, seguido de la visita al famoso Complejo del Palacio de la Ciudad, donde los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con pilares, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII',
					'A última hora de la tarde regresará al hotel y se relajará',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1545126126-ebea588ee202?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '10',
				title: 'Jaipur - Delhi',
				city: 'Delhi',
				dayActivity: [
					'Tendrás tiempo para un último paseo por el casco antiguo de Jaipur antes de conducir a Delhi por la tarde, donde te alojarás en un hotel cercano al aeropuerto antes de tu partida al día siguiente',
				],
				id: '130',
				image: null,
			},
			{
				day: '11',
				title: 'Delhi - salida',
				city: 'Delhi',
				dayActivity: [
					"Nuestro ejecutivo y chófer le trasladan al Aeropuerto Internacional de Nueva Delhi para su vuelo de regreso o de continuación con gratos recuerdos del 'Tour por la India Clásica'",
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/57901/pexels-photo-57901.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/57901/pexels-photo-57901.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '2',
				url: 'https://images.pexels.com/photos/3492589/pexels-photo-3492589.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '3',
				url: 'https://images.pexels.com/photos/5465323/pexels-photo-5465323.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '6',
		name: 'Lo mejor del norte de la India',
		excerpt: 'Delhi, Varanasi, Khajuraho, Agra & Jaipur',
		slug: 'lo-mejor-del-norte-de-la-india',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '13',
		promoText: 'Más vendido',
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			'El norte de la India es una región que alberga algunas de las maravillas arquitectónicas más asombrosas del mundo. Desde el resplandeciente Taj Mahal hasta la intrincada escalinata de Abhanheri, esta región rebosa historia. ',
			"En este viaje de 13 días, podrá explorar algunos de los lugares más impresionantes de la región. Déjese cautivar por la esencia de la India en un recorrido desde la antigua y encantadora Delhi hasta las ciudades patrimonio vivo de Agra y Jaipur, la ciudad santa de Benarés y el majestuoso Taj Mahal. Experimente algunos de los ricos tesoros culturales, históricos y religiosos del país en esta memorable aventura. Empápese del ambiente de 'la ciudad patrimonio viviente'",
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',

				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '1',
				image:
					'https://images.unsplash.com/photo-1585483103289-39c79411fda9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Delhi visitas',
				city: 'Delhi',
				dayActivity: [
					'Hoy tomará un corto vuelo a Benarés, una de las ciudades habitadas más antiguas del mundo. Para los hindúes, Benarés es la ciudad del Señor Shiva y una de las ciudades más sagradas de la India. A su llegada a Varanasi, nuestro ejecutivo le traslada al hotel. Check-in y relajarse y permanecer aquí por dos noches. La tarde es libre o libre por su cuenta. Esta noche, asista al espectacular Ganga Aarti en los ghats del río Ganges (Ganga)',
					'En la orilla del Ganges, corrientes de fieles hindúes acuden todavía hoy a bañarse en las aguas sagradas del río, que se cree que lavan los pecados de vidas pasadas. Tras un paseo por los ghats y templos con su guía, regresará a Dashashwamedh Ghat para contemplar el Ganga Aarti, un espectacular ritual en el que equipos de jóvenes sacerdotes vestidos con espléndidos trajes ceremoniales agitan llameantes lámparas de aceite junto al Ganges. Himnos devocionales, cánticos, tambores, campanas y gongs se combinan para crear una atmósfera intensa',
					'Alojamiento en un hotel de Benarés',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1587474260584-136574528ed5?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Varanasi',
				city: 'Varanasi',
				dayActivity: [
					'Hoy tomará un corto vuelo a Varanasi o Benarés, una de las ciudades habitadas más antiguas del mundo. Para los hindúes, Benarés es la ciudad del Señor Shiva y una de las ciudades más sagradas de la India. A su llegada a Varanasi, nuestro ejecutivo le traslada al hotel. Check-in y relajarse y permanecer aquí por dos noches. La tarde es libre o libre por su cuenta. Esta noche, asista al espectacular Ganga Aarti en los ghats del río Ganges (Ganga).',

					'En la orilla del Ganges, corrientes de fieles hindúes acuden todavía hoy a bañarse en las aguas sagradas del río, que se cree que lavan los pecados de vidas pasadas. Tras un paseo por los ghats y templos con su guía, regresará a Dashashwamedh Ghat para contemplar el Ganga Aarti, un espectacular ritual en el que equipos de jóvenes sacerdotes vestidos con espléndidos trajes ceremoniales agitan llameantes lámparas de aceite junto al Ganges. Himnos devocionales, cánticos, tambores, campanas y gongs se combinan para crear una atmósfera intensa.',
					'Alojamiento en un hotel de Benarés',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1627938823193-fd13c1c867dd?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Varanasi',
				city: 'Varanasi',
				dayActivity: [
					'Dé un paseo en barca por el río Ganges a primera hora de la mañana para ver a los fieles ofreciendo plegarias a Surya Dev (el Sol) a la orilla del agua',
					'Al amanecer, es el momento de dar un paseo en barca por el río Ganges, cuando los primeros rayos de luz del día iluminan los ghats de baño de Benarés. Miles de fieles se abren paso en la oscuridad previa al amanecer para bañarse en el río a esta hora propicia, y el espectáculo visto desde el agua, con la cálida luz del sol acentuando los colores de los saris de los bañistas, es sin duda uno de los más coloridos y edificantes de la India',
					'Más tarde, excursión a pie por los ghats ribereños y los templos. Regreso al hotel y desayuno',
					'Alrededor del mediodía o un poco antes, le llevaremos a Sarnath',
					"Sarnath, a las afueras de la ciudad, fue el lugar donde Buda dio su primer sermón en el año 530 a.C., revelando el 'Óctuple Sendero' a sus cinco discípulos. El lugar exacto está marcado por la estupa cilíndrica de Dhamekh, construida en el 500 d.C. sobre los restos de otra mucho más antigua encargada por el emperador mauryano Ashoka",
					'Alojamiento en un hotel de Benarés',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1599831069477-b2acdc0bcb91?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				title: 'Khajuraho',
				city: 'Khajuraho',
				dayActivity: [
					'Tome un vuelo corto a la pequeña ciudad de Khajuraho. A su llegada, nuestro ejecutivo le trasladará al hotel. Disfrute de una visita por la tarde al renombrado arte erótico del Grupo Occidental de templos de Khajuraho',
					'El extraordinario conjunto de santuarios de Khajuraho debe su supervivencia a la lejanía de su ubicación en el centro de la India. Incluso en el periodo comprendido entre los siglos X y XIII, cuando se construyeron, los templos estaban alejados de los caminos trillados y escaparon milagrosamente a la atención de los merodeadores ejércitos musulmanes que destruyeron gran parte del arte religioso de la India en la época medieval. Famosa sobre todo por su erotismo, la escultura de piedra que adorna hoy los santuarios de Khajuraho parece asombrosamente fresca',
					'Alojamiento en un hotel de Khajuraho',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1638645677644-f01017224e0b?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				title: 'Khajuraho - Orchha - Agra',
				city: 'Agra',
				dayActivity: [
					'Después del desayuno, tres horas de viaje hasta el tranquilo asentamiento de Orchha para ver los templos y palacios del siglo XVI. Después del almuerzo, corto trayecto hasta Jhansi y embarque en el tren Gatiman Express con destino a Agra. Llegada a Agra por la tarde. Nuestro ejecutivo le recibirá a su llegada a Agra Cantt. Estación de tren y traslado al hotel y check-in. Aquí se alojará durante dos noches',
					'Orchha es uno de nuestros destinos favoritos del norte de la India, gracias a su ambiente tranquilo y a su pintoresca ubicación en medio de los bosques de dhak a orillas del Betwa, un escenario idílico para un almuerzo tranquilo. Los monumentos están desiertos y, en general, en mal estado de conservación, lo que los hace aún más evocadores. Los más emblemáticos son los 14 hermosos chhatris, o cenotafios, que se elevan desde la orilla norte del río, y que tendrá la oportunidad de visitar antes de continuar viaje hacia Agra',
					'Alojamiento en un hotel de Agra',
				],
				id: '130',
				image: null,
			},
			{
				day: '7',
				title: 'Agra - Taj Mahal',
				city: 'Agra',
				dayActivity: [
					'La excursión de hoy por Agra comienza con una visita al amanecer al Taj Mahal, considerado uno de los mejores ejemplos de la arquitectura mogol. Regreso al hotel para desayunar y descansar. Más tarde, visita al impresionante Fuerte de Agra, cuyos edificios, de arenisca roja y mármol, son una mezcla de arquitectura hindú e islámica. Por último, vea la ornamentada tumba de Itimad-ud-Daulah. Después, el resto del día es libre',
					'El gran fuerte mogol de Agra, a orillas del río Yamuna, fue el lugar donde el emperador Shah Jahan, creador del Taj Mahal, fue encarcelado al final de su vida por su hijo Aurangzeb, más bien fanático; se dice que el anciano pasaba los días contemplando la tumba a través de las ventanas de un pabellón dorado en la azotea',
					'En la orilla opuesta, la tumba de Itimad-ud Daulah, exquisitamente decorada, es la siguiente parada de la visita de hoy. El trabajo de taracea del mausoleo prefigura el del Taj, que se visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí',
					'Alojamiento en un hotel de Agra',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1589884674963-c33aec0347a7?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Drive to Jaipur via Abhaneri',
				city: 'Jaipur',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece plantas, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
					'Alojamiento en un hotel de Jaipur.',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1477586957327-847a0f3f4fe3?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '9',
				title: 'Jaipur city tour',
				city: 'JAipur',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, comenzando con una parada fotográfica en Hawa Mahal, una fachada de cinco pisos con elaboradas ventanas enrejadas desde donde las mujeres de la casa real solían ver las procesiones en las calles de abajo. A continuación, diríjase al Fuerte Amber',
					"Encaramado en el borde de una dramática escarpa, el Fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, sobre todo un resplandeciente 'Salón de los Espejos', o 'Sheesh Mahal', revestido de intrincados mosaicos donde el Maharajá y sus consortes disfrutaban de recitales de música y poesía",
					'La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Visitaremos Jantar Mantar, un observatorio, seguido de la visita al famoso Complejo del Palacio de la Ciudad, donde los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con pilares, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII',
					'A última hora de la tarde regresará al hotel y se relajará',
					'Pasaremos la noche en un hotel de Jaipur',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1477586957327-847a0f3f4fe3?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '10',
				title: 'Udaipur',
				city: 'Udaipur',
				dayActivity: [
					'Después del desayuno, emprenda viaje a Udaipur, donde pasará dos noches. Merece la pena hacer una parada en Ranakpur (03 horas) para visitar un conjunto de templos jainistas de intrincado tallado. A continuación, continúe el viaje hasta Udaipur (03 horas)',
					'Llegará a Udaipur a una hora del día en la que los palacios junto al lago lucen su aspecto más exótico. Disfrute de las sublimes vistas mientras toma una copa al atardecer en la azotea de un haveli, mirando a través del agua hacia las colinas Aravalli en la distancia',
					'Alojamiento en un hotel de Udaipur',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1572980071199-3d874a56b987?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '11',
				title: 'Udaipur city tour',
				city: 'Udaipur',
				dayActivity: [
					'Acompañado por nuestro guía, explore el complejo del Palacio de la Ciudad de Udaipur, el templo de Jagdish y el casco antiguo',
					'Comience el día visitando el palacio de la ciudad de Udaipur, sede de la dinastía Sisodia y una muestra de la opulenta arquitectura Rajput. Tras visitar sus apartamentos y patios ajardinados, pasee hasta el cercano templo de Jagdish antes de adentrarse en el casco antiguo para explorar los mercados. También se puede visitar el jardín de nombre Sahelion Ki Bari',
					'Pasar la noche en un hotel de Udaipur',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1633702738734-443da2c18f3c?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '12',
				title: 'Udaipur - Delhi by flight',
				city: 'Delhi',
				dayActivity: [
					'Tendrá tiempo para dar un último paseo por el casco antiguo de Udaipur antes de tomar un vuelo a Delhi. A su llegada al aeropuerto de Delhi, nuestro ejecutivo le trasladará a un hotel cercano al aeropuerto antes de su salida al día siguiente.',
				],
				id: '130',
				image: null,
			},
			{
				day: '13',
				title: 'Delhi - salida',
				city: 'Delhi',
				dayActivity: [
					'Con nuestro ejecutivo, traslado en vehículo al aeropuerto internacional de Nueva Delhi para su vuelo de regreso o de continuación',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1477587458883-47145ed94245?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1477587458883-47145ed94245?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1638645677644-f01017224e0b?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1627938823193-fd13c1c867dd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '4',
				url: 'https://images.unsplash.com/photo-1627894485677-105070ad8e2e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
		promoTextBgColor: 'bg-yellow-1',
		promoTextColor: 'text-dark-1',
	},
	{
		id: '7',
		name: 'Lo mejor de la India y nepal',
		excerpt: 'Delhi, Agra, Jaipur, Varanasi & Kathmandu',
		slug: 'lo-mejor-de-la-india-y-nepal',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '12',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			'Desde el sagrado río Ganges hasta las montañas del Himalaya, este viaje le llevará por la India y Nepal. Descubra el espíritu y la cultura de este país diverso y fascinante mientras explora sus bulliciosas ciudades y sus impresionantes paisajes. De Delhi a Katmandú, visitará algunos de los lugares más emocionantes de la India y será testigo de los rituales cotidianos del hinduismo',
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '1',
				image:
					'https://images.unsplash.com/photo-1597040663342-45b6af3d91a5?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Delhi visitas',
				city: 'Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1595928607828-6fdaee9c0942?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Delhi - Agra',
				city: 'Agra',
				dayActivity: [
					'Después del desayuno, viaje por la nueva autopista interestatal hasta Agra para visitar el Fuerte Mogol de la ciudad y los mercados cercanos, seguido de una visita al Taj al atardecer',
					"Shah Jahan, el emperador que creó el Taj Mahal, fue encarcelado hacia el final de su vida por su hijo, Aurangzeb, en el esplendor dorado de la gran fortaleza-palacio de los mogoles con vistas al río Yamuna, su primera parada turística del día. A través de sus pilares finamente tallados y sus ventanas en arco de cúspide, podrá saborear las mismas vistas románticas de las que disfrutaba el gobernante enfermo. En la orilla opuesta, la tumba de Itimad-ud Daulah alberga los restos de su primer ministro, o 'Visir'. El famoso e intrincado trabajo de incrustación del mausoleo presagiaba el del Taj, que visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí",
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1591018653367-9c01498b3320?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Agra - Jaipur',
				city: 'Jaipur',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece pisos, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1603262110263-fb0112e7cc33?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				title: 'Jaipur visitas',
				city: 'Jaipur',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, empezando por una excursión a Amber, seguida del Hawa Mahal, el observatorio Jantar Mantar y el mundialmente famoso museo del Palacio de la Ciudad',
					"Encaramado en el borde de una dramática escarpa, el fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, en particular un resplandeciente 'Salón de los Espejos' o 'Sheesh Mahal', revestido de intrincados mosaicos donde el maharajá y sus consortes disfrutaban de recitales de música y poesía",
					"La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Comenzará en el famoso complejo del Palacio de la Ciudad, que incluye el muy fotografiado 'Hawa Mahal', o 'Palacio de los Vientos', una fachada de cinco pisos con elaboradas ventanas enrejadas desde las que las mujeres de la casa real solían ver las procesiones en las calles de abajo. Los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con columnas que hay debajo, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII",
					'Por la tarde regresará al hotel y podrá relajarse',
				],
				id: '130',
				image: null,
			},
			{
				day: '6',
				title: 'Varanasi',
				city: 'Varanasi',
				dayActivity: [
					' Después del desayuno, traslado al aeropuerto de Jaipur para tomar un vuelo a Varanasi. Llegada a Varanasi por la tarde para una estancia de dos noches. Sea testigo de la espectacular ceremonia aarti del río Ganges por la noche',
					'A orillas del Ganges, Benarés es uno de los lugares habitados de forma continua más antiguos de la Tierra. Los peregrinos hindúes siguen acudiendo aquí para bañarse en las aguas sagradas, que se cree que lavan los pecados acumulados durante toda una vida. Después de observar a los fieles realizar rituales en los ghats que conducen a la orilla del agua, regresará a Dashashwamedh Ghat para experimentar el Ganga Aarthi, en el que equipos de jóvenes sacerdotes vestidos con trajes ceremoniales agitan llameantes lámparas de aceite junto al Ganges',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1627938823193-fd13c1c867dd?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '7',
				title: 'Varanasi city tour',
				city: 'Varanasi',
				dayActivity: [
					'Dé un paseo en barca por el río Ganges a primera hora de la mañana para ver a los fieles ofreciendo plegarias a Surya Dev (el Sol) a la orilla del agua',
					'Al amanecer, es el momento de dar un paseo en barca por el río Ganges, cuando los primeros rayos de luz del día iluminan los ghats de baño de Benarés. Miles de fieles se abren paso en la oscuridad previa al amanecer para bañarse en el río a esta hora propicia, y el espectáculo visto desde el agua, con la cálida luz del sol acentuando los colores de los saris de los bañistas, es sin duda uno de los más coloridos y edificantes de la India',
					'Más tarde, excursión a pie por los ghats ribereños y los templos. Regreso al hotel y desayuno',
					'Alrededor del mediodía o un poco antes, le llevaremos a Sarnath',
					"Sarnath, a las afueras de la ciudad, fue el lugar donde Buda dio su primer sermón en el año 530 a.C., revelando el 'Óctuple Sendero' a sus cinco discípulos. El lugar exacto está marcado por la estupa cilíndrica de Dhamekh, construida en el 500 d.C. sobre los restos de otra mucho más antigua encargada por el emperador mauryano Ashoka",
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1614164974666-057a7c713ba6?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Varanasi - Delhi - Kathmandu',
				city: 'Kathmandu',
				dayActivity: [
					'Tome el vuelo a Katmandú esta mañana para una estancia de tres noches en Katmandú. A su llegada a Katmandú, será recibido por nuestro ejecutivo y conducido y acompañado a su hotel',
					'Katmandú, una ciudad moderna en expansión de casi un millón de habitantes, la capital nepalí puede parecer abrumadoramente abarrotada y caótica a primera vista. Pero si se dirige a su núcleo antiguo, donde los reyes Shah y Malla erigieron sus estupas, palacios y pagodas de madera tallada, todo mejora notablemente',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1617469170169-55626c028519?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '9',
				title: 'Kathmandu city tour',
				city: 'Kathamndu',
				dayActivity: [
					'Hoy, disfrute de un día completo de turismo por los monumentos históricos de la capital, como Swayambhynath, Boudhanath y Pashupatinath',
					"Después del desayuno, nos dirigiremos al templo de Swayambhunath (conocido coloquialmente como 'el Templo de los Monos' por su población residente de macacos), una magnífica estupa cuya torre dorada y el par de enormes ojos que todo lo ven se han convertido en emblemas de Katmandú. No menos sobrecogedora es Boudhanath, la estupa más grande del país, también adornada con ojos penetrantes y ristras de banderas de oración que cuelgan de una espectacular torre dorada. No lejos de Boudhanath, a orillas del río Bagmati, al este de Katmandú, se alza Pashupatinath, el templo de Shiva más sagrado de Nepal. Los fieles acuden aquí en masa por la noche, cuando los ghats se iluminan con cientos de lámparas parpadeantes",
					'Alojamiento en un hotel de Katmandú',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1592285896110-8d88b5b3a5d8?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '10',
				title: 'Kathmandu - Patan - Bhaktapur - Kathmandu Durbar Square',
				city: 'Katmandu',
				dayActivity: [
					'Hoy, después del desayuno, disfrute de la excursión de un día a Patan y Bhaktapur y la Plaza Durbar de Katmandú',
					"Patan (más conocida como 'Lalitpur') es la segunda ciudad más grande del valle de Katmandú y cuenta con una concentración de monumentos religiosos aún mayor que la capital. La mayoría de ellos se agrupan en las calles que rodean la plaza Durbar -una de las plazas más bellas del mundo-, donde el Palacio Real de los reyes Malla alberga un maravilloso museo dedicado a las artes sagradas de Nepal",
					'Bhaktapur, más abajo en el valle, cuenta con otra impresionante concentración de palacios medievales, monasterios con patio y plazas monumentales, todo ello de alto estilo newari. A pesar de ser la tercera ciudad de Nepal, es un destino mucho más tranquilo gracias a la ausencia de tráfico en su núcleo histórico',
					'La plaza Durbar de Katmandú, donde los reyes Shah y Malla erigieron sus estupas, palacios y pagodas de madera tallada de resplandeciente dorado. La plaza Durbar constituye el epicentro de este barrio de magnífico ambiente. Más de cincuenta templos, 106 patios de monasterios e innumerables santuarios se alinean en sus estrechas y empedradas callejuelas. Acomódese en una de las plataformas adosadas de la torre Manju Devi para contemplar el exótico bullicio de vendedores de caléndulas, sadhus vestidos de azafrán, vacas callejeras y palomas',
					'Esta noche le ofrecemos una cena con un espectáculo de danza cultural nepalí',
				],
				image: null,
			},
			{
				day: '11',
				title: 'Kathmandu to Delhi',
				city: 'Delhi',
				dayActivity: [
					'Hoy le trasladaremos al aeropuerto internacional de Katmandú para su vuelo a Delhi. Nuestro ejecutivo le recibirá a su llegada al aeropuerto internacional de Delhi y le trasladará a un hotel cercano al aeropuerto',
					null,
				],
				id: '130',
				image: null,
			},
			{
				day: '12',
				title: 'Delhi - departure',
				city: 'Delhi',
				dayActivity: [
					'Nuestro ejecutivo y chófer le trasladan al Aeropuerto Internacional de Nueva Delhi para su vuelo de regreso o de continuación con gratos recuerdos del Viaje a India y Nepal',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/5465323/pexels-photo-5465323.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/5465323/pexels-photo-5465323.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '2',
				url: 'https://images.pexels.com/photos/11570414/pexels-photo-11570414.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1592639296346-560c37a0f711?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '4',
				url: 'https://images.unsplash.com/flagged/photo-1555475912-a6fb90a6d6ba?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '8',
		name: 'Taj, Tigres & Templos',
		excerpt: 'Delhi, Agra, pueblo de Abhanheri, Jaipur y Benarés',
		slug: 'taj-tigers-y-templos-tour',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '8',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			'Este itinerario es ideal tanto para familias con niños como para parejas en luna de miel. Explore algunos de los mejores lugares de interés cultural de la India y enamórese del Taj Mahal, el Fuerte Amber y todas las demás bellas joyas de este paquete lleno de maravillas. Además del Parque Nacional de Ranthambore, uno de los mejores parques de toda la India, este viaje incluye desde las maravillas de la vieja y la nueva Delhi hasta una visita guiada por la increíble arquitectura y las joyas ocultas de todo el país.',
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos.',
					'Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o cantina, del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales.',
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi.',
				],
				id: '1',
				image:
					'https://images.unsplash.com/photo-1609258678760-ba05d9b95bb9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Visitas de Delhi',
				city: 'Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en las afueras del sur.',
					'Las estrechas callejuelas de la Vieja Delhi constituyeron en su día el centro neurálgico de la capital mogol, antiguamente conocida como Shajahanabad en honor al gran emperador Shah Jahan. Una parada obligatoria debe ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la ciudad vieja.',
					'Tras la visita a Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi.',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en las afueras del sur de Delhi, última parada del día.',
					'Alojamiento en un hotel de Delhi.',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1595928607828-6fdaee9c0942?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Delhi to Agra by road',
				city: 'Agra',
				dayActivity: [
					'Después del desayuno, viaje por la nueva autopista interestatal hasta Agra para visitar el Fuerte Mogol de la ciudad y los mercados cercanos, seguido de una visita al Taj al atardecer.',
					'Shah Jahan, el emperador que creó el Taj Mahal, fue encarcelado hacia el final de su vida por su hijo, Aurangzeb, en el esplendor dorado de la gran fortaleza-palacio de los mogoles con vistas al río Yamuna, su primera parada turística del día. A través de sus pilares finamente tallados y sus ventanas en arco de cúspide, podrá saborear las mismas vistas románticas de las que disfrutaba el gobernante enfermo. En la orilla opuesta, la tumba de Itimad-ud Daulah alberga los restos de su primer ministro, o Visir. El famoso e intrincado trabajo de incrustación del mausoleo prefigura el del Taj, que se visita al final de la tarde, cuando la luz cambiante transforma las superficies de mármol de ocre pálido a naranja y carmesí.',
					'Alojamiento en un hotel de Agra.',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1592635196078-9fdc757f27f4?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Agra to Ranthambore by road',
				city: 'Ranthambore National Park',
				dayActivity: [
					'Después del desayuno saldremos de Agra y visitaremos la ciudad fortificada de Fatehpur Sikri. Construida por el gran mogol Akbar y abandonada en 1585 debido a la escasez de agua, sus restos bien conservados constituyen una parada fascinante. Desde aquí continuaremos nuestro viaje a Sawai Madophur, donde se encuentra la vasta reserva de vida salvaje del Parque Nacional de Ranthambore, y nuestro hogar durante las dos próximas noches.',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1598510343623-7100698a9f69?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				title: 'Ranthambore National Park',
				city: 'Ranthambore National Park',
				dayActivity: [
					'A primera hora de la mañana, cuando la niebla aún se está levantando, nos dirigiremos al parque para nuestro primer safari y la oportunidad de disfrutar de la belleza natural de Ranthambore, donde la posibilidad de ver un tigre, así como muchos otros animales salvajes, es tan alta como en cualquier otro lugar de la India. Después regresaremos a nuestro hotel para desayunar y pasar una mañana relajada. Después de comer, volveremos al parque para realizar otro safari y tener la oportunidad de avistar más animales salvajes. Esta noche cenaremos en nuestro hotel.',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1598510343623-7100698a9f69?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				title: 'Ranthambore to Jaipur by road',
				city: 'Jaipur',
				dayActivity: [
					'Conduzca por la mañana hasta Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. El pozo, que consta de 3.500 escalones tallados repartidos en trece plantas, es el más profundo y complejo de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches.,',
					'Alojamiento en un hotel de Jaipur.',
				],
				id: '4',
				image: null,
			},
			{
				day: '7',
				title: 'Jaipur sightseeing',
				city: 'Jaipur',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, empezando por una excursión a Amber, seguida del Hawa Mahal, el observatorio Jantar Mantar y el mundialmente famoso museo del Palacio de la Ciudad',
					'Encaramado en el borde de una dramática escarpa, el fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India',
					'La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Comenzaremos por el famoso complejo del Palacio de la Ciudad, que incluye el muy fotografiado Hawa Mahal o Palacio de los Vientos, una fachada de cinco pisos con elaboradas ventanas enrejadas desde las que las mujeres de la casa real solían contemplar las procesiones en las calles. Los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con columnas que hay debajo e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII',
					'A última hora de la tarde regresará al hotel y se relajará.,',
					'Pasaremos la noche en un hotel de Jaipur',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1561486008-1011a284acfb?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Jaipur to Varanasi by flight',
				city: 'Varanasi',
				dayActivity: [
					' Después del desayuno, traslado al aeropuerto de Jaipur para tomar un vuelo a Varanasi. Llegada a Varanasi por la tarde para una estancia de dos noches. Por la noche, asista a la espectacular ceremonia del aarti del río Ganges',
					'A orillas del Ganges, Benarés es uno de los lugares habitados más antiguos de la Tierra. Los peregrinos hindúes siguen acudiendo aquí para bañarse en las aguas sagradas, que se cree que lavan los pecados acumulados durante toda una vida. Después de observar a los fieles realizar rituales en los ghats que conducen a la orilla del agua, regresará a Dashashwamedh Ghat para experimentar el Ganga Aarthi, en el que equipos de jóvenes sacerdotes vestidos con trajes ceremoniales agitan lámparas de aceite encendidas junto al Ganges',
					'Alojamiento en un hotel de Benarés',
				],
				id: '130',
				image: null,
			},
			{
				day: '9',
				title: 'Varanasi - the holy city',
				city: 'Varanasi',
				dayActivity: [
					'Tome un crucero por el Ganges a primera hora de la mañana',
					'El amanecer es el momento de dar un paseo en barco por el río Ganges, cuando los primeros rayos de luz del día iluminan los ghats de baño de Benarés. Miles de fieles se abren paso a través de la oscuridad previa al amanecer para bañarse en el río a esta hora propicia, y el espectáculo visto desde el agua, con la cálida luz del sol acentuando los colores de los saris de los bañistas, es sin duda uno de los más coloridos y edificantes de la India',
					'Más tarde, excursión a pie por los ghats ribereños y los templos. Regreso al hotel y desayuno.',
					'Alrededor del mediodía o un poco antes, le llevaremos a Sarnath.',
					"Sarnath, a las afueras de la ciudad, fue donde Buda dio su primer sermón en el año 530 a.C., revelando el 'Óctuple Sendero' a sus cinco discípulos. El lugar exacto está marcado por la estupa cilíndrica de Dhamekh, construida en el 500 d.C. sobre los restos de otra mucho más antigua encargada por el emperador mauryano Ashoka",
					'Alojamiento en un hotel de Benarés',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1627938823193-fd13c1c867dd?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '10',
				title: 'Delhi departure',
				city: 'Delhi',
				dayActivity: [
					'Hoy, le trasladamos al aeropuerto de Varanasi para su vuelo a Delhi. Nuestro ejecutivo le recibirá a su llegada al aeropuerto internacional de Delhi y le trasladará a un hotel cercano al aeropuerto.',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/46251/sumatran-tiger-tiger-big-cat-stripes-46251.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/46251/sumatran-tiger-tiger-big-cat-stripes-46251.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1672215056017-5a34b06ea12a?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1634920540678-df0e179c5243?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '4',
				url: 'https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '5',
				url: 'https://images.pexels.com/photos/14477035/pexels-photo-14477035.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '9',
		name: 'Fascinante India con Dubai',
		excerpt: 'Delhi, Varanasi, Khajuraho, Agra, Jaipur & Dubai',
		slug: 'fascinante-india-con-dubai',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '13',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			[
				'Viaje a los destinos únicos y exóticos de la India y Dubai en este viaje personalizado de 13 días. Este viaje le llevará a algunas de las ciudades más vibrantes de la India y a Dubai para que viva una apasionante aventura llena de asombrosas maravillas creadas por el hombre y hermosos paisajes',
				"Su viaje a la India y Dubai comienza en la capital, Nueva Delhi. Desde allí, viajará a la ciudad santa de Varanasi y Khajuraho antes de llegar a la ciudad de Agra para ver el hermoso Taj Mahal, una de las maravillas del mundo. Después de Agra, visitará Jaipur, la 'Ciudad Rosa', antes de regresar a Delhi. Por último, embarcará en un vuelo a Dubai, donde descubrirá la parte moderna e histórica de la ciudad. Su viaje terminará en Dubai",
			],
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '1',
				image:
					'https://images.unsplash.com/photo-1592639296346-560c37a0f711?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Delhi visitas',
				city: 'Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1597040663342-45b6af3d91a5?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Delhi - Varanasi',
				city: 'Varanasi',
				dayActivity: [
					'Hoy tomará un corto vuelo a Varanasi o Benarés, una de las ciudades habitadas más antiguas del mundo. Para los hindúes, Benarés es la ciudad del Señor Shiva y una de las ciudades más sagradas de la India. A su llegada a Varanasi, nuestro ejecutivo le traslada al hotel. Check-in y relajarse y permanecer aquí por dos noches. La tarde es libre o libre por su cuenta. Esta noche, asista al espectacular Ganga Aarti en los ghats del río Ganges (Ganga)',
					'En la orilla del Ganges, corrientes de fieles hindúes acuden todavía hoy a bañarse en las aguas sagradas del río, que se cree que lavan los pecados de vidas pasadas. Tras un paseo por los ghats y templos con su guía, regresará a Dashashwamedh Ghat para contemplar el Ganga Aarti, un espectacular ritual en el que equipos de jóvenes sacerdotes vestidos con espléndidos trajes ceremoniales agitan llameantes lámparas de aceite junto al Ganges. Himnos devocionales, cánticos, tambores, campanas y gongs se combinan para crear una atmósfera intensa',
					'Alojamiento en un hotel de Benarés',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1614164974666-057a7c713ba6?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Varanasi visitas',
				city: 'Varanasi',
				dayActivity: [
					'Dé un paseo en barca por el río Ganges a primera hora de la mañana para ver a los fieles ofreciendo plegarias a Surya Dev (el Sol) a la orilla del agua',
					'Al amanecer, es el momento de dar un paseo en barca por el río Ganges, cuando los primeros rayos de luz del día iluminan los ghats de baño de Benarés. Miles de fieles se abren paso en la oscuridad previa al amanecer para bañarse en el río a esta hora propicia, y el espectáculo visto desde el agua, con la cálida luz del sol acentuando los colores de los saris de los bañistas, es sin duda uno de los más coloridos y edificantes de la India',
					'Más tarde, excursión a pie por los ghats ribereños y los templos. Regreso al hotel y desayuno',
					'Alrededor del mediodía o un poco antes, le llevaremos a Sarnath',
					"Sarnath, a las afueras de la ciudad, fue el lugar donde Buda dio su primer sermón en el año 530 a.C., revelando el 'Óctuple Sendero' a sus cinco discípulos. El lugar exacto está marcado por la estupa cilíndrica de Dhamekh, construida en el 500 d.C. sobre los restos de otra mucho más antigua encargada por el emperador mauryano Ashoka",
					'Alojamiento en un hotel de Benarés',
				],
				id: '4',
				image: null,
			},
			{
				day: '5',
				title: 'Varanasi - Khajuraho',
				city: 'Khajuraho',
				dayActivity: [
					'Tome un vuelo corto a la pequeña ciudad de Khajuraho. A su llegada, nuestro ejecutivo le trasladará al hotel. Disfrute de una visita por la tarde al renombrado arte erótico del Grupo Occidental de templos de Khajuraho',
					'El extraordinario conjunto de santuarios de Khajuraho debe su supervivencia a la lejanía de su ubicación en el centro de la India. Incluso en el periodo comprendido entre los siglos X y XIII, cuando se construyeron, los templos estaban alejados de los caminos trillados y escaparon milagrosamente a la atención de los merodeadores ejércitos musulmanes que destruyeron gran parte del arte religioso de la India en la época medieval. Famosa sobre todo por su erotismo, la escultura de piedra que adorna hoy los santuarios de Khajuraho parece asombrosamente fresca',
					'Alojamiento en un hotel de Khajuraho',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1594763386213-c207860939a8?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				title: 'Khajuraho - Orchha - Agra',
				city: 'Agra',
				dayActivity: [
					'Después del desayuno, tres horas de viaje hasta el tranquilo asentamiento de Orchha para ver los templos y palacios del siglo XVI. Después del almuerzo, corto trayecto hasta Jhansi y embarque en el tren Gatiman Express con destino a Agra. Llegada a Agra por la tarde. Nuestro ejecutivo le recibirá a su llegada a Agra Cantt. Estación de tren y traslado al hotel y check-in. Aquí se alojará durante dos noches',
					'Orchha es uno de nuestros destinos favoritos del norte de la India, gracias a su ambiente tranquilo y a su pintoresca ubicación en medio de los bosques de dhak a orillas del Betwa, un escenario idílico para un almuerzo tranquilo. Los monumentos están desiertos y, en general, en mal estado de conservación, lo que los hace aún más evocadores. Los más emblemáticos son los 14 hermosos chhatris, o cenotafios, que se elevan desde la orilla norte del río, y que tendrá la oportunidad de visitar antes de continuar viaje hacia Agra',
					'Alojamiento en un hotel de Agra',
				],
				id: '4',
				image: null,
			},
			{
				day: '7',
				title: 'Agra - Taj Mahal tour',
				city: 'Agra',
				dayActivity: [
					'La excursión de hoy por Agra comienza con una visita al amanecer al Taj Mahal, considerado uno de los mejores ejemplos de la arquitectura mogol. Regreso al hotel para desayunar y descansar. Más tarde, visita al impresionante Fuerte de Agra, cuyos edificios, de arenisca roja y mármol, son una mezcla de arquitectura hindú e islámica. Por último, vea la ornamentada tumba de Itimad-ud-Daulah. Después, el resto del día es libre',
					'El gran fuerte mogol de Agra, a orillas del río Yamuna, fue el lugar donde el emperador Shah Jahan, creador del Taj Mahal, fue encarcelado al final de su vida por su hijo Aurangzeb, más bien fanático; se dice que el anciano pasaba los días contemplando la tumba a través de las ventanas de un pabellón dorado en la azotea',
					'En la orilla opuesta, la tumba de Itimad-ud Daulah, exquisitamente decorada, es la siguiente parada de la visita de hoy. El trabajo de taracea del mausoleo prefigura el del Taj, que se visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí',
					'Alojamiento en un hotel de Agra',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1589884674963-c33aec0347a7?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Agra - Jaipur',
				city: 'Jaipur',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece plantas, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
					'Alojamiento en un hotel de Jaipur.',
				],
				id: '130',
				image: null,
			},
			{
				day: '9',
				title: 'Jaipur city tour',
				city: 'Jaipur',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, comenzando con una parada fotográfica en Hawa Mahal, una fachada de cinco pisos con elaboradas ventanas enrejadas desde donde las mujeres de la casa real solían ver las procesiones en las calles de abajo. A continuación, diríjase al Fuerte Amber',
					"Encaramado en el borde de una dramática escarpa, el Fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, sobre todo un resplandeciente 'Salón de los Espejos', o 'Sheesh Mahal', revestido de intrincados mosaicos donde el Maharajá y sus consortes disfrutaban de recitales de música y poesía",
					'La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Visitaremos Jantar Mantar, un observatorio, seguido de la visita al famoso Complejo del Palacio de la Ciudad, donde los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con pilares, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII',
					'A última hora de la tarde regresará al hotel y se relajará',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1545126126-ebea588ee202?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '10',
				title: 'Vuelo a Dubai',
				city: 'Dubai',
				dayActivity: [
					'Hoy tome un vuelo a Dubai para su estancia de cuatro noches aquí. Nuestro ejecutivo le recibirá a su llegada al aeropuerto internacional de Dubai y le trasladará al hotel, donde podrá registrarse y relajarse',
					'Una ciudad cosmopolita, Dubai es un popular destino turístico que le ofrece una visión de las maravillas arquitectónicas modernas y futuristas.., así como instalaciones para conferencias, restaurantes, tiendas y entretenimiento. Vea lo mejor que Dubai puede ofrecerle en el regazo del lujo con nosotros',
					'Alojamiento en un hotel de Dubai',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1512632578888-169bbbc64f33?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '11',
				title: 'Dubai city tour',
				city: 'Dubai',
				dayActivity: [
					"El undécimo día del viaje, disfrutará de la visita a la ciudad de Dubai. Comience el recorrido con una parada fotográfica en el emblemático edificio hotelero Burj Al Arab, cuyo diseño se asemeja a la vela de un barco frente al Golfo Pérsico. La siguiente parada fotográfica es en la playa de Jumeirah para hacer fotos de muchas atracciones famosas de Dubai. Desde aquí, iremos a capturar las vistas desde 'AT THE TOP' la planta 124 del Burj Khalifa y luego le dejaremos en el Dubai Mall",
					'Alojamiento en un hotel en Dubai.',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1628859017536-c2f1d69f3c84?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '12',
				title: 'Abu Dhabi excursion',
				city: 'Dubai',
				dayActivity: [
					"Hoy realizarás una excursión de día completo a Abu Dhabi, la capital de los Emiratos Árabes Unidos. Comience el recorrido con la visita a la Gran Mezquita del Jeque Zayed, símbolo emblemático de Abu Dhabi. Conduzca por la zona de Al Bateen y deténgase en la Casa Zayed para conocer de cerca la vida del difunto jeque Zayed, 'Padre de la Nación'. A continuación, continúe hasta el Palacio de los Emiratos y las Torres Etihad para hacer una parada fotográfica. Conduzca por la hermosa Corniche y deténgase en el rompeolas para hacer fotos. De regreso a Dubai por la isla de Yas, visita al parque temático Ferrari World",
				],
				id: '130',
				image: null,
			},
			{
				day: '13',
				title: 'Dubai - salida',
				city: 'Dubai',
				dayActivity: [
					'Hoy nuestro ejecutivo y chófer le trasladará al aeropuerto internacional de Dubai para embarcar en su vuelo para el viaje de ida / vuelta junto con recuerdos entrañables de INDIA & DUBAI Tour',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1489516408517-0c0a15662682?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1489516408517-0c0a15662682?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1612409578638-b890d0fa9364?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1548013146-72479768bada?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '4',
				url: 'https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '5',
				url: 'https://images.unsplash.com/photo-1618029225782-62568be64623?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '10',
		name: 'El triángulo dorado',
		excerpt: 'Delhi, Agra, Abhanheri, Jaipiur',
		slug: 'el-triangulo-dorado',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '7',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			[
				'El recorrido del Triángulo de Oro es uno de los más famosos del norte de la India. Este viaje de 7 días le llevará por las ciudades más importantes de la historia de la India. El recorrido comienza en Delhi, donde visitará el famoso Fuerte Rojo y la Tumba de Humayun. A continuación, le llevará a Agra, donde contemplará el exquisito Taj Mahal. Después, le llevará a la Ciudad Rosa de Jaipur, donde podrá ver el Hawa Mahal, el Palacio de Amber y el Jantar Mantar',
			],
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '1',
				image:
					'https://images.unsplash.com/photo-1585483103289-39c79411fda9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Delhi visitas',
				city: 'Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1587474260584-136574528ed5?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				city: 'Agra',
				title: 'Delhi - Agra',
				dayActivity: [
					'Después del desayuno, viaje por la nueva autopista interestatal hasta Agra para visitar el Fuerte Mogol de la ciudad y los mercados cercanos, seguido de una visita al Taj al atardecer',
					"Shah Jahan, el emperador que creó el Taj Mahal, fue encarcelado hacia el final de su vida por su hijo, Aurangzeb, en el esplendor dorado de la gran fortaleza-palacio de los mogoles con vistas al río Yamuna, su primera parada turística del día. A través de sus pilares finamente tallados y sus ventanas en arco de cúspide, podrá saborear las mismas vistas románticas de las que disfrutaba el gobernante enfermo. En la orilla opuesta, la tumba de Itimad-ud Daulah alberga los restos de su primer ministro, o 'Visir'. El famoso e intrincado trabajo de incrustación del mausoleo presagiaba el del Taj, que visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí",
				],
				id: '132',
				image:
					'https://images.unsplash.com/photo-1589884674963-c33aec0347a7?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				city: 'Jaipur',
				title: 'Agra - Jaipur via Abhaneri',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece pisos, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
				],
				id: '133',
				image:
					'https://images.unsplash.com/photo-1561486008-1011a284acfb?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				city: 'Jaipur',
				title: 'Jaipur visitas',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, empezando por una excursión a Amber, seguida del Hawa Mahal, el observatorio Jantar Mantar y el mundialmente famoso museo del Palacio de la Ciudad',
					"Encaramado en el borde de una dramática escarpa, el fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, en particular un resplandeciente 'Salón de los Espejos' o 'Sheesh Mahal', revestido de intrincados mosaicos donde el maharajá y sus consortes disfrutaban de recitales de música y poesía",
					"La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Comenzará en el famoso complejo del Palacio de la Ciudad, que incluye el muy fotografiado 'Hawa Mahal', o 'Palacio de los Vientos', una fachada de cinco pisos con elaboradas ventanas enrejadas desde las que las mujeres de la casa real solían ver las procesiones en las calles de abajo. Los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con columnas que hay debajo, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII",
					'Por la tarde regresará al hotel y podrá relajarse',
					'Alojamiento en un hotel de Jaipur',
				],
				id: '134',
				image:
					'https://images.unsplash.com/photo-1603262110263-fb0112e7cc33?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				city: 'Delhi',
				title: 'Jaipur - Delhi',
				dayActivity: [
					'Tendrás tiempo para un último paseo por el casco antiguo de Jaipur antes de conducir a Delhi por la tarde, donde te alojarás en un hotel cercano al aeropuerto antes de tu partida al día siguiente',
				],
				id: '98',
				image: null,
			},
			{
				day: '7',
				title: 'Delhi - salida',
				city: 'Delhi',
				dayActivity: [
					'Hoy nuestro ejecutivo y chófer le trasladará al aeropuerto internacional de Delhi para embarcar en su vuelo para el viaje de ida / vuelta',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1548013146-72479768bada?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1548013146-72479768bada?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1580105142577-98b7a439fbea?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1595243880357-bcf8de1be94f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '4',
				url: 'https://images.unsplash.com/photo-1602339752474-f77aa7bcaecd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '11',
		name: 'Viaje al Triángulo de Oro con Udaipur',
		excerpt: 'Delhi, Agra, Abhanheri, Jaipiur, Chittaurgarh, Udaipur',
		slug: 'viaje-al-triangulo-de-oro-con-udaipur',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '9',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			[
				'El recorrido del Triángulo de Oro es uno de los más famosos del norte de la India. Este viaje de 7 días le llevará por las ciudades más importantes de la historia de la India. El recorrido comienza en Delhi, donde visitará el famoso Fuerte Rojo y la Tumba de Humayun. A continuación, le llevará a Agra, donde contemplará el exquisito Taj Mahal. Después, le llevará a la Ciudad Rosa de Jaipur, donde podrá ver el Hawa Mahal, el Palacio de Amber y el Jantar Mantar',
			],
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1587474260584-136574528ed5?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				city: 'Delhi',
				title: 'Visitas de Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '131',
				image:
					'https://images.unsplash.com/photo-1585483103289-39c79411fda9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				city: 'Agra',
				title: 'Delhi - Agra',
				dayActivity: [
					'Después del desayuno, viaje por la nueva autopista interestatal hasta Agra para visitar el Fuerte Mogol de la ciudad y los mercados cercanos, seguido de una visita al Taj al atardecer',
					"Shah Jahan, el emperador que creó el Taj Mahal, fue encarcelado hacia el final de su vida por su hijo, Aurangzeb, en el esplendor dorado de la gran fortaleza-palacio de los mogoles con vistas al río Yamuna, su primera parada turística del día. A través de sus pilares finamente tallados y sus ventanas en arco de cúspide, podrá saborear las mismas vistas románticas de las que disfrutaba el gobernante enfermo. En la orilla opuesta, la tumba de Itimad-ud Daulah alberga los restos de su primer ministro, o 'Visir'. El famoso e intrincado trabajo de incrustación del mausoleo presagiaba el del Taj, que visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí",
				],
				id: '132',
				image:
					'https://images.unsplash.com/photo-1589884674963-c33aec0347a7?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				city: 'Jaipur',
				title: 'Agra - Jaipur via Abhaneri',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece pisos, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
				],
				id: '133',
				image:
					'https://images.unsplash.com/photo-1561486008-1011a284acfb?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				city: 'Jaipur',
				title: 'Jaipur visitas',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, empezando por una excursión a Amber, seguida del Hawa Mahal, el observatorio Jantar Mantar y el mundialmente famoso museo del Palacio de la Ciudad',
					"Encaramado en el borde de una dramática escarpa, el fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, en particular un resplandeciente 'Salón de los Espejos' o 'Sheesh Mahal', revestido de intrincados mosaicos donde el maharajá y sus consortes disfrutaban de recitales de música y poesía",
					"La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Comenzará en el famoso complejo del Palacio de la Ciudad, que incluye el muy fotografiado 'Hawa Mahal', o 'Palacio de los Vientos', una fachada de cinco pisos con elaboradas ventanas enrejadas desde las que las mujeres de la casa real solían ver las procesiones en las calles de abajo. Los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con columnas que hay debajo, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII",
					'Por la tarde regresará al hotel y podrá relajarse',
					'Alojamiento en un hotel de Jaipur',
				],
				id: '134',
				image:
					'https://images.unsplash.com/photo-1603262110263-fb0112e7cc33?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				city: 'Udaipur',
				title: 'Jaipur - Udaipur via Chittaurgarh',
				dayActivity: [
					'Después del desayuno, emprenda viaje a Udaipur, donde pasará dos noches. Merece la pena considerar una parada en Chittaurgarh durante la ruta. La ciudad alberga uno de los fuertes más espectaculares de la India, escenario de varios asedios sangrientos que acabaron con el suicidio en masa de toda su población',
					'Llegará a Udaipur a una hora del día en la que los palacios junto al lago lucen su aspecto más exótico. Disfrute de las sublimes vistas mientras toma una copa al atardecer en la azotea de una haveli, mirando a través del agua hacia las colinas Aravalli en la distancia',
					'Alojamiento en un hotel de Udaipur',
				],
				id: '134',
				image: null,
			},
			{
				day: '7',
				city: 'Udaipur',
				title: 'Udaipur visitas',
				dayActivity: [
					'Acompañado por un guía turístico, explore el complejo del Palacio de la Ciudad de Udaipur, el templo de Jagdish y el casco antiguo',
					'Comience el día visitando el palacio de la ciudad de Udaipur, sede de la dinastía Sisodia y una muestra de la opulenta arquitectura Rajput. Tras visitar sus apartamentos y jardines, baje hasta el cercano templo de Jagdish antes de adentrarse en el casco antiguo para explorar los mercados. A última hora de la tarde es el momento de estar a orillas del agua, empapándose de los colores del atardecer y del ambiente único de esta romántica ciudad',
					'Alojamiento en un hotel de Udaipur',
				],
				id: '134',
				image:
					'https://images.unsplash.com/photo-1519998994457-43c1f2c8460b?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				city: 'Delhi',
				title: 'Udaipur - Delhi',
				dayActivity: [
					'Tendrá tiempo para dar un último paseo por el casco antiguo de Udaipur antes de tomar un vuelo a Delhi. A su llegada al aeropuerto de Delhi, nuestro ejecutivo le trasladará a un hotel cercano al aeropuerto antes de su salida al día siguiente.',
				],
				id: '98',
				image: null,
			},
			{
				day: '9',
				title: 'Delhi - salida',
				city: 'Delhi',
				dayActivity: [
					'Hoy nuestro ejecutivo y chófer le trasladará al aeropuerto internacional de Delhi para embarcar en su vuelo para el viaje de ida / vuelta',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/14684595/pexels-photo-14684595.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/14684595/pexels-photo-14684595.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '2',
				url: 'https://images.pexels.com/photos/7323438/pexels-photo-7323438.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '3',
				url: 'https://images.pexels.com/photos/6776755/pexels-photo-6776755.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '12',
		name: 'Mágico Norte de la India',
		excerpt: 'Delhi, Varanasi, Khajuraho, Agra, Jaipur, Jodhpur & Udaipur',
		slug: 'magico-norte-de-la-india',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '15',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			[
				'Este viaje de 15 días por Rajastán ofrece un recorrido brillante por esta fascinante parte de la India, permitiéndole conocer las hermosas ciudades de Delhi, Benarés, Agra, Jaipur, Jodhpur y Udaipur. En esencia, este recorrido ofrece una exploración en profundidad de esta zona, haciéndole sentir como un nativo mientras profundiza en la historia y la cultura de la zona, acompañado en todo momento por su conductor y guía privados.',
			],
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1587474260584-136574528ed5?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				city: 'Delhi',
				title: 'Visitas de Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '131',
				image:
					'https://images.unsplash.com/photo-1585483103289-39c79411fda9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Varanasi',
				city: 'Varanasi',
				dayActivity: [
					'Hoy tomará un corto vuelo a Varanasi o Benarés, una de las ciudades habitadas más antiguas del mundo. Para los hindúes, Benarés es la ciudad del Señor Shiva y una de las ciudades más sagradas de la India. A su llegada a Varanasi, nuestro ejecutivo le traslada al hotel. Check-in y relajarse y permanecer aquí por dos noches. La tarde es libre o libre por su cuenta. Esta noche, asista al espectacular Ganga Aarti en los ghats del río Ganges (Ganga)',

					'En la orilla del Ganges, corrientes de fieles hindúes acuden todavía hoy a bañarse en las aguas sagradas del río, que se cree que lavan los pecados de vidas pasadas. Tras un paseo por los ghats y templos con su guía, regresará a Dashashwamedh Ghat para contemplar el Ganga Aarti, un espectacular ritual en el que equipos de jóvenes sacerdotes vestidos con espléndidos trajes ceremoniales agitan llameantes lámparas de aceite junto al Ganges. Himnos devocionales, cánticos, tambores, campanas y gongs se combinan para crear una atmósfera intensa',
					'Alojamiento en un hotel de Benarés',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1627938823193-fd13c1c867dd?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Varanasi',
				city: 'Varanasi',
				dayActivity: [
					'Dé un paseo en barca por el río Ganges a primera hora de la mañana para ver a los fieles ofreciendo plegarias a Surya Dev (el Sol) a la orilla del agua',
					'Al amanecer, es el momento de dar un paseo en barca por el río Ganges, cuando los primeros rayos de luz del día iluminan los ghats de baño de Benarés. Miles de fieles se abren paso en la oscuridad previa al amanecer para bañarse en el río a esta hora propicia, y el espectáculo visto desde el agua, con la cálida luz del sol acentuando los colores de los saris de los bañistas, es sin duda uno de los más coloridos y edificantes de la India',
					'Más tarde, excursión a pie por los ghats ribereños y los templos. Regreso al hotel y desayuno',
					'Alrededor del mediodía o un poco antes, le llevaremos a Sarnath',
					"Sarnath, a las afueras de la ciudad, fue el lugar donde Buda dio su primer sermón en el año 530 a.C., revelando el 'Óctuple Sendero' a sus cinco discípulos. El lugar exacto está marcado por la estupa cilíndrica de Dhamekh, construida en el 500 d.C. sobre los restos de otra mucho más antigua encargada por el emperador mauryano Ashoka",
					'Alojamiento en un hotel de Benarés',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1599831069477-b2acdc0bcb91?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				title: 'Khajuraho',
				city: 'Khajuraho',
				dayActivity: [
					'Tome un vuelo corto a la pequeña ciudad de Khajuraho. A su llegada, nuestro ejecutivo le trasladará al hotel. Disfrute de una visita por la tarde al renombrado arte erótico del Grupo Occidental de templos de Khajuraho',
					'El extraordinario conjunto de santuarios de Khajuraho debe su supervivencia a la lejanía de su ubicación en el centro de la India. Incluso en el periodo comprendido entre los siglos X y XIII, cuando se construyeron, los templos estaban alejados de los caminos trillados y escaparon milagrosamente a la atención de los merodeadores ejércitos musulmanes que destruyeron gran parte del arte religioso de la India en la época medieval. Famosa sobre todo por su erotismo, la escultura de piedra que adorna hoy los santuarios de Khajuraho parece asombrosamente fresca',
					'Alojamiento en un hotel de Khajuraho',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1638645677644-f01017224e0b?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				title: 'Khajuraho - Orchha - Agra',
				city: 'Agra',
				dayActivity: [
					'Después del desayuno, tres horas de viaje hasta el tranquilo asentamiento de Orchha para ver los templos y palacios del siglo XVI. Después del almuerzo, corto trayecto hasta Jhansi y embarque en el tren Gatiman Express con destino a Agra. Llegada a Agra por la tarde. Nuestro ejecutivo le recibirá a su llegada a Agra Cantt. Estación de tren y traslado al hotel y check-in. Aquí se alojará durante dos noches',
					'Orchha es uno de nuestros destinos favoritos del norte de la India, gracias a su ambiente tranquilo y a su pintoresca ubicación en medio de los bosques de dhak a orillas del Betwa, un escenario idílico para un almuerzo tranquilo. Los monumentos están desiertos y, en general, en mal estado de conservación, lo que los hace aún más evocadores. Los más emblemáticos son los 14 hermosos chhatris, o cenotafios, que se elevan desde la orilla norte del río, y que tendrá la oportunidad de visitar antes de continuar viaje hacia Agra',
					'Alojamiento en un hotel de Agra',
				],
				id: '130',
				image: null,
			},
			{
				day: '7',
				title: 'Agra - Taj Mahal',
				city: 'Agra',
				dayActivity: [
					'La excursión de hoy por Agra comienza con una visita al amanecer al Taj Mahal, considerado uno de los mejores ejemplos de la arquitectura mogol. Regreso al hotel para desayunar y descansar. Más tarde, visita al impresionante Fuerte de Agra, cuyos edificios, de arenisca roja y mármol, son una mezcla de arquitectura hindú e islámica. Por último, vea la ornamentada tumba de Itimad-ud-Daulah. Después, el resto del día es libre',
					'El gran fuerte mogol de Agra, a orillas del río Yamuna, fue el lugar donde el emperador Shah Jahan, creador del Taj Mahal, fue encarcelado al final de su vida por su hijo Aurangzeb, más bien fanático; se dice que el anciano pasaba los días contemplando la tumba a través de las ventanas de un pabellón dorado en la azotea',
					'En la orilla opuesta, la tumba de Itimad-ud Daulah, exquisitamente decorada, es la siguiente parada de la visita de hoy. El trabajo de taracea del mausoleo prefigura el del Taj, que se visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí',
					'Alojamiento en un hotel de Agra',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1589884674963-c33aec0347a7?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Drive to Jaipur via Abhaneri',
				city: 'Jaipur',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece plantas, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
					'Alojamiento en un hotel de Jaipur.',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1477586957327-847a0f3f4fe3?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '9',
				title: 'Jaipur city tour',
				city: 'JAipur',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, comenzando con una parada fotográfica en Hawa Mahal, una fachada de cinco pisos con elaboradas ventanas enrejadas desde donde las mujeres de la casa real solían ver las procesiones en las calles de abajo. A continuación, diríjase al Fuerte Amber',
					"Encaramado en el borde de una dramática escarpa, el Fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, sobre todo un resplandeciente 'Salón de los Espejos', o 'Sheesh Mahal', revestido de intrincados mosaicos donde el Maharajá y sus consortes disfrutaban de recitales de música y poesía",
					'La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Visitaremos Jantar Mantar, un observatorio, seguido de la visita al famoso Complejo del Palacio de la Ciudad, donde los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con pilares, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII',
					'A última hora de la tarde regresará al hotel y se relajará',
					'Pasaremos la noche en un hotel de Jaipur',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1477586957327-847a0f3f4fe3?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '10',
				title: 'Jaipur - Jodhpur',
				city: 'Jodhpur',
				dayActivity: [
					'Tras el desayuno, emprenda viaje a Jodhpur, donde pasará dos noches',
					'Capital del antiguo reino de Marwar, Jodhpur debe su prominencia a la ruta comercial que antaño pasaba por sus puertas, conectando los puertos de Gujarat con las ciudades de las llanuras del norte. La riqueza resultante permitió a los gobernantes marwari construir uno de los fuertes más fabulosos de la India, Mehrangarh, en lo alto de una escarpa casi vertical. Las casas cuboides del casco antiguo que se extienden desde su base están pintadas de cien tonos de azul, una práctica que se dice que denota los hogares de los brahmanes locales (pero que en realidad deriva de los intentos de disuadir a las termitas añadiendo sulfato de cobre a la cal). También es visible, al sur, la enorme mole de Umaid Bhavan, un palacio construido en 1929 por el Maharajá local',
					'Alojamiento en un hotel de Jodhpur',
				],
				id: '130',
				image: null,
			},
			{
				day: '11',
				title: 'Jodhpur - la ciudad azul',
				city: 'Jodhpur',
				dayActivity: [
					'Hoy, con nuestro guía, explore el Fuerte Mehrangarh, Jaswant Thada y el museo del Palacio Umaid Bhawan',
					'Coronando un gigantesco acantilado de arenisca, el Fuerte Mehrangarh es la más imponente de todas las ciudadelas de Rajastán, con unas vistas a la altura de su colorida historia. Bajo sus elevadas murallas, que encierran un museo especialmente interesante de objetos y apartamentos reales, se extiende un gigantesco mosaico de tejados planos, pintados en tonos azules',
					'Más tarde, visite el hermoso Jaswant Thada y el opulento Museo del Palacio de Umaid Bhawan',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1504194947363-2f14d9e0e445?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '12',
				title: 'Jodhpur - Ranakpur - Udaipur',
				city: 'Udaipur',
				dayActivity: [
					'Después del desayuno, emprenda viaje a Udaipur, donde pasará dos noches. Merece la pena hacer una parada en Ranakpur (03 horas) para visitar un conjunto de templos jainistas de intrincado tallado. A continuación, continúe el viaje hasta Udaipur (03 horas)',
					'Llegará a Udaipur a una hora del día en la que los palacios junto al lago lucen su aspecto más exótico. Disfrute de las sublimes vistas mientras toma una copa al atardecer en la azotea de un haveli, mirando a través del agua hacia las colinas Aravalli en la distancia',
					'Alojamiento en un hotel de Udaipur',
				],
				id: '130',
				image: null,
			},
			{
				day: '13',
				title: 'Udaipur visitas',
				city: 'Udaipur',
				dayActivity: [
					'Acompañado por nuestro guía, explore el complejo del Palacio de la Ciudad de Udaipur, el templo de Jagdish y el casco antiguo',
					'Comience el día visitando el palacio de la ciudad de Udaipur, sede de la dinastía Sisodia y una muestra de la opulenta arquitectura Rajput. Tras visitar sus apartamentos y patios ajardinados, pasee hasta el cercano templo de Jagdish antes de adentrarse en el casco antiguo para explorar los mercados. También se puede visitar el jardín de nombre Sahelion Ki Bari',
					'Pasar la noche en un hotel de Udaipur',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1633702738734-443da2c18f3c?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '14',
				title: 'Vuelo a Delhi',
				city: 'Delhi',
				dayActivity: [
					'Tendrá tiempo para dar un último paseo por el casco antiguo de Udaipur antes de tomar un vuelo a Delhi. A su llegada al aeropuerto de Delhi, nuestro ejecutivo le trasladará a un hotel cercano al aeropuerto antes de su salida al día siguiente.',
				],
				id: '130',
				image: null,
			},
			{
				day: '15',
				title: 'Delhi - salidas',
				city: 'Delhi',
				dayActivity: [
					'Con nuestro ejecutivo, traslado en vehículo al aeropuerto internacional de Nueva Delhi para su vuelo de regreso o de continuación',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1627938823193-fd13c1c867dd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1627938823193-fd13c1c867dd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1672215051407-6e05138da3a9?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '4',
				url: 'https://images.unsplash.com/photo-1547919965-ee2eb9a1d790?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '13',
		name: 'Lo Mejor de Nepal',
		excerpt: 'Kathmandu, Chitwan & Pokhara',
		slug: 'lo-mejor-de-nepal',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '9',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			[
				'Embárquese en un viaje espiritual por Nepal, donde presenciará el ondear de las banderas de oración y visitará majestuosos monasterios budistas. Desde las serpenteantes calles de Katmandú hasta la tranquilidad que envuelve los templos en lo alto de las colinas, será difícil no sentirse sobrecogido por esta sobrecogedora aventura.',
			],
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Kathmandu',
				city: 'Kathmandu',
				dayActivity: [
					"As soon as you arrive at Kathmandu's Tribhuvan International Airport, you will be met and welcomed by our executive and transferred to your hotel.",

					'Kathmandu: a thriving modern city of nearly a million people, crowded and chaotic. However, if you visit to see the places where the Shah and Malla kings established their kingdom and built palaces and wooden pagodas carved in newari architecture, stupa and then your thoughts about Kathmandu changes a lot.',
					'The rest of the day you can spend recovering from jetlag. If energy permits, you can visit the nearby Thamel market area with stores, cafes and restaurants.',
				],
				id: '1',
				image:
					'https://images.unsplash.com/flagged/photo-1555475912-a6fb90a6d6ba?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Kathmandu visitas',
				city: 'Kathmandu',
				dayActivity: [
					'Después del desayuno, disfrute del día completo de turismo por Katmandú que incluye la visita de Swayambhunath, Boudhanath y Pashupatinath',
					"Swayambhunath: También conocido por el nombre de 'El Templo de los Monos', ya que se pueden ver muchos monos en y alrededor de aquí. La belleza de la estupa es una torre dorada con un par de grandes ojos que todo lo ven y que se ha convertido en sinónimo de Katmandú",
					'Boudhanath: Se dice que es la estupa más grande de Nepal, adornada con cuerdas de banderas de oración que cuelgan de una torre dorada y ojos penetrantes es una experiencia agradable',
					'Pashupatinath: uno de los templos más sagrados de la nación, dedicado al Señor Shiva, se alza a orillas del río Bagmati, situado al este de Katmandú. Por la noche, cientos de personas iluminan los ghats con lámparas parpadeantes',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1617469170169-55626c028519?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Kathamndu visitas',
				city: 'Kathmandu',
				dayActivity: [
					'Hoy después del desayuno, disfrute de la excursión de un día a Patan, Bhaktapur & Kathmandu Durbar Square',
					'Patan: Patan o Lalitpur ambos son lo mismo, pero localmente es bien conocido como Lalitpur. Tiene el mérito de ser la segunda ciudad más grande del valle de Katmandú y de poseer el mayor número de monumentos religiosos que las otras dos ciudades del valle. La mayoría de sus monumentos se encuentran cerca de la Plaza Durbar, y el más pintoresco es el Palacio Real de los Reyes Malla',
					'Bhaktapur: Un lugar más abajo en el valle es Bhaktapur, donde se puede ver la espectacular colección de plazas, palacios, patios y monasterios, todos ellos de gran arquitectura newari. Algo conveniente de la ubicación de Bhaktapur es la ausencia de tráfico pesado, es fácil ir allí',
					'Plaza Durbar de Katmandú: Los grandes reyes Shah y Malla erigieron en arquitectura newari las estupas doradas, las pagodas talladas en madera y los palacios de la plaza Durbar de Katmandú. La plaza Durbar de Katmandú es el centro de este barrio de increíble ambiente. Sus sinuosas calles adoquinadas están bordeadas por más de 50 templos y ciento seis patios de monasterios y numerosos santuarios. Aquí se puede ver un inusitado alboroto de vendedores de flores, sadhus vestidos de color azafrán, vacas errantes y palomas',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1605640840605-14ac1855827b?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Kathmandu a Chitwan',
				city: 'Chitwan',
				dayActivity: [
					'Después del desayuno, salida del hotel y viaje a Chitwan',
					'Chitwan se encuentra a los pies del Himalaya y es uno de los últimos vestigios vírgenes de la región de Terai, que antaño se extendía por las estribaciones de la India y Nepal. El parque alberga una de las escasas poblaciones de rinoceronte de un solo cuerno y grandes felinos escurridizos como el tigre de Bengala y los leopardos, junto con elefantes salvajes asiáticos, cocodrilos, bisontes, hienas sambar, osos perezosos y mucho más',
					'Llegada a Chitwan, check-in en el resort y relax',
					'Por la tarde, podrá ver una presentación de diapositivas sobre la historia y los antecedentes del Parque Nacional de Chitwan',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1549888668-19281758dfbe?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				title: 'Chitwan Parque Nacional',
				city: 'Chitwan',
				dayActivity: [
					'Un día lleno de actividades en la selva que incluyen un paseo por la naturaleza, paseo en canoa, safari en jeep, danza Tharu, paseo por el pueblo, etc',
					'Alojamiento en un complejo turístico en Chitwan',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1498712067384-01239c6b377c?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				title: 'Chitwan a Pokhara',
				city: 'Pokhara',
				dayActivity: [
					'Después del desayuno, viaje a Pokhara que se encuentra en las estribaciones de la cordillera Annaupurna donde se alojará durante dos - noches',
					"Pokhara: La ciudad balneario de Nepal se encuentra a unos 200 km al oeste de Katmandú. La popularidad de esta ciudad entre los turistas de todo el mundo se debe a su mágica vista de Machapuchare 'FishTail', un reflejo que se ve en las aguas del lago Phewa. Es una base perfecta para los senderistas que quieran disfrutar de las excursiones y los trekkings por la cordillera de los Annapurnas",
					'Llegada a Pokhara, y check - in en el resort u hotel. Por la tarde puede pasear por la orilla del lago o puede disfrutar del paseo en barco en el lago Phewa durante una hora',
					'Alojamiento en un hotel o resort en Pokhara.',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1618083840944-31cc42fcf250?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '7',
				title: 'Pokhara visitas',
				city: 'Pokhara',
				dayActivity: [
					'Por la mañana temprano, excursión a Sarangkot para contemplar el amanecer y vistas panorámicas de Pokhara. Un buen lugar para relajarse y apreciar la belleza de las montañas cubiertas de nieve visibles desde aquí y el lago Phewa valle abajo',
					'Regreso al hotel para el desayuno. Después del desayuno procederemos a hacer turismo por el valle de Pokhara que incluye algunos lugares famosos como:',
					'Desfiladero del río Seti: El desfiladero formado por el río Seti es una de las atracciones turísticas más populares de la excursión al valle de Pokhara. Aquí el río Seti corta magníficamente a través de la cordillera Mahabharat y así parece desaparecer en cuevas y túneles en muchos lugares de su curso. Es interesante saber que en algunos lugares, el río Seti tiene unos dos metros de ancho, pero la profundidad aquí es de unos veinte metros. El desfiladero del río Seti puede verse desde varios puntos de la ciudad',
					'Cascada de Davi: Una famosa cascada conocida localmente como Patale Chhango, que significa caída del inframundo. Se dice que debe su nombre a una suiza que fue arrastrada por la cascada en 1961. Esta feroz caída, que corta la roca a su paso, ha creado intrincadas cuevas y túneles',
					'Cueva de Gupteshwar Mahadev: Una hermosa y sagrada cueva dedicada al Señor Shiva, tallada por la caída de Davi, es uno de los lugares más visitados entre la gente que sigue las prácticas religiosas hindúes',
					'Templo Bindhyabasini y Antiguo Bazar: Un templo hindú, de color blanco y construido en estilo pagoda, es uno de los lugares religiosos de Pokhara. Este templo se encuentra en lo alto de una colina y ofrece una vista espectacular de Pokhara y las montañas circundantes. El templo Bindhyabasini está dedicado a una de las encarnaciones de la diosa Durga o Maa Shakti. Cerca del templo hay un antiguo bazar tradicional pero colorido con el estilo de vida newari',
					'Más tarde el día es libre para sus propias actividades.',
				],
				id: '4',
				image:
					'https://images.unsplash.com/photo-1576948187290-457c015b3bff?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8',
				title: 'Pokhara a Kathmandu',
				city: 'Katmandu',
				dayActivity: [
					'Después del desayuno en el hotel o resort, check-out y viaje a Katmandú',
					'Llegada a Katmandú por la tarde. Check - in y relax en el hotel',
					'Esta noche le ofreceremos una cena con un espectáculo de danza cultural nepalí',
					'Alojamiento en un hotel en Katmandú.',
				],
				id: '130',
				image: null,
			},
			{
				day: '9',
				title: 'Kathmandu - salida',
				city: 'Kathmandu',
				dayActivity: [
					'Después del desayuno y según el horario de su vuelo, nuestro ejecutivo le trasladará al aeropuerto internacional Tribhuvan de Katmandú para embarcar en su vuelo de regreso a casa.',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/6370225/pexels-photo-6370225.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/6370225/pexels-photo-6370225.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1549888668-19281758dfbe?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1576948187290-457c015b3bff?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '4',
				url: 'https://images.unsplash.com/photo-1618083840675-f5bdfd4a704e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '14',
		name: 'Excursión al Valle de Kathmandu',
		excerpt:
			'Kathmandu, Pashupatinath, Bodhnath, Swayambhunath, Patan, Bhaktapur',
		slug: 'excursion-al-valle-de-Kathmandu',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '4',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			[
				'Esta excursión de 4 días le llevará de viaje por el corazón de Nepal. Explorará el valle de Katmandú, que es uno de los lugares habitados de forma continuada más antiguos del planeta. El valle está impregnado de historia y salpicado de templos, monasterios y monumentos. El valle también alberga lugares declarados Patrimonio de la Humanidad por la UNESCO, como el templo de Pashupatinath, las estupas budistas de Swayambhu y Boudha, y la plaza Durbar de Katmandú',
			],
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Kathmandu',
				city: 'Kathmandu',
				dayActivity: [
					"As soon as you arrive at Kathmandu's Tribhuvan International Airport, you will be met and welcomed by our executive and transferred to your hotel.",

					'Kathmandu: a thriving modern city of nearly a million people, crowded and chaotic. However, if you visit to see the places where the Shah and Malla kings established their kingdom and built palaces and wooden pagodas carved in newari architecture, stupa and then your thoughts about Kathmandu changes a lot.',
					'The rest of the day you can spend recovering from jetlag. If energy permits, you can visit the nearby Thamel market area with stores, cafes and restaurants.',
				],
				id: '1',
				image:
					'https://images.unsplash.com/flagged/photo-1555475912-a6fb90a6d6ba?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Kathmandu visitas',
				city: 'Kathmandu',
				dayActivity: [
					'Después del desayuno, disfrute del día completo de turismo por Katmandú que incluye la visita de Swayambhunath, Boudhanath y Pashupatinath',
					"Swayambhunath: También conocido por el nombre de 'El Templo de los Monos', ya que se pueden ver muchos monos en y alrededor de aquí. La belleza de la estupa es una torre dorada con un par de grandes ojos que todo lo ven y que se ha convertido en sinónimo de Katmandú",
					'Boudhanath: Se dice que es la estupa más grande de Nepal, adornada con cuerdas de banderas de oración que cuelgan de una torre dorada y ojos penetrantes es una experiencia agradable',
					'Pashupatinath: uno de los templos más sagrados de la nación, dedicado al Señor Shiva, se alza a orillas del río Bagmati, situado al este de Katmandú. Por la noche, cientos de personas iluminan los ghats con lámparas parpadeantes',
				],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1617469170169-55626c028519?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Kathamndu visitas',
				city: 'Kathmandu',
				dayActivity: [
					'Hoy después del desayuno, disfrute de la excursión de un día a Patan, Bhaktapur & Kathmandu Durbar Square',
					'Patan: Patan o Lalitpur ambos son lo mismo, pero localmente es bien conocido como Lalitpur. Tiene el mérito de ser la segunda ciudad más grande del valle de Katmandú y de poseer el mayor número de monumentos religiosos que las otras dos ciudades del valle. La mayoría de sus monumentos se encuentran cerca de la Plaza Durbar, y el más pintoresco es el Palacio Real de los Reyes Malla',
					'Bhaktapur: Un lugar más abajo en el valle es Bhaktapur, donde se puede ver la espectacular colección de plazas, palacios, patios y monasterios, todos ellos de gran arquitectura newari. Algo conveniente de la ubicación de Bhaktapur es la ausencia de tráfico pesado, es fácil ir allí',
					'Plaza Durbar de Katmandú: Los grandes reyes Shah y Malla erigieron en arquitectura newari las estupas doradas, las pagodas talladas en madera y los palacios de la plaza Durbar de Katmandú. La plaza Durbar de Katmandú es el centro de este barrio de increíble ambiente. Sus sinuosas calles adoquinadas están bordeadas por más de 50 templos y ciento seis patios de monasterios y numerosos santuarios. Aquí se puede ver un inusitado alboroto de vendedores de flores, sadhus vestidos de color azafrán, vacas errantes y palomas',
				],
				id: '3',
				image:
					'https://images.unsplash.com/photo-1605640840605-14ac1855827b?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Kathmandu - salida',
				city: 'Kathmandu',
				dayActivity: [
					'Después del desayuno y según el horario de su vuelo, nuestro ejecutivo le trasladará al aeropuerto internacional Tribhuvan de Katmandú para embarcar en su vuelo de regreso a casa.',
				],
				id: '130',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1617469167379-2e33a480dd6f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.unsplash.com/photo-1617469167379-2e33a480dd6f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '2',
				url: 'https://images.unsplash.com/photo-1504448252408-b32799ff32f3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '3',
				url: 'https://images.unsplash.com/photo-1558911534-4a80c06e480e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
			{
				id: '4',
				url: 'https://images.unsplash.com/photo-1512982110293-42d1fc32ecdc?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&q=80',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados privados',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entrance fees',
			},
			{
				id: '2',
				name: 'Visa fees',
			},
			{
				id: '3',
				name: 'Travel insurance',
			},
			{
				id: '4',
				name: 'Flight tickets',
			},
			{
				id: '5',
				name: 'Gratuities (optional)',
			},
			{
				id: '6',
				name: 'Food and drinks, unless specified',
			},
			{
				id: '7',
				name: 'Personal expenses',
			},
			{
				id: '8',
				name: 'Anything not mentioned in the itinerary',
			},
		],
	},
	{
		id: '15',
		name: 'El triángulo dorado de la India con Maldivas',
		excerpt: 'Delhi, Agra, Jaipur & Maldivas',
		slug: 'el-triangulo-dorado-de-la-india-con-maldivas',
		price: {
			tourPrice: '',
			tourPriceCurrency: 'USD',
			salePrice: null,
			singleSupplement: null,
		},
		duration: '10',
		promoText: null,
		startDate: null,
		endDate: null,
		tourType: 'Privado',
		rating: null,
		overview: [
			'Esta maravillosa combinación de exploración, turismo y relax le permitirá conocer los maravillosos lugares de la India y relajarse en la tranquilidad de las Maldivas. Este emocionante viaje comienza en Delhi, antes de viajar a Agra, Jaipur y la Ciudad Rosa de Jaipur. Desde allí partiremos hacia Malé, la bella capital, donde disfrutaremos de las azules aguas del océano Índico. Esto es realmente lo mejor de ambos mundos',
		],
		itinerary: [
			{
				day: '1',
				title: 'Llegada a Delhi',
				city: 'Delhi',
				dayActivity: [
					'A su llegada a Delhi, será recibido por nuestro ejecutivo y chófer y trasladado a su hotel para una estancia de dos noches en la capital de la India. Dedique el resto del día a recuperarse del jetlag con un relajante baño en la piscina del hotel. Si la energía se lo permite, puede visitar el Gurudwara Bangla Sahib, un santuario sij, a última hora de la tarde. Avísenos y se lo organizaremos',
					"Gurudwara Bangla Sahib: De mármol blanco y coronado por una cúpula de cebolla dorada, el Gurudwara Bangla Sahib y el estanque resplandeciente de su interior son lugares de gran santidad para los sijs indios y ofrecen la introducción más atmosférica posible a la capital. El complejo en su forma actual data de finales del siglo XVIII y se construyó en un lugar asociado al octavo gurú sij, Har Krishnan. En el Langar, o 'cantina', del templo, los visitantes y devotos reciben gratuitamente comida nutritiva a base de chapatis y dal negro de manos de voluntarios. Si tiene suerte, podrá ver grupos de Akalis, miembros de una secta guerrera sij, vestidos con trajes ceremoniales tradicionales",
					'Disfrute de la tarde y relájese en el hotel. Pase la noche en un hotel de Delhi',
				],
				id: '130',
				image:
					'https://images.unsplash.com/photo-1595928607828-6fdaee9c0942?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '2',
				title: 'Delhi visitas',
				city: 'Delhi',
				dayActivity: [
					'La jornada de hoy comienza con un paseo en bicirickshaw por la Vieja Delhi, que incluye la mezquita Jama Masjid. Por la tarde, visite la capital imperial de Lutyen, la Tumba de Humayun y el espectacular complejo de Qutb Minar, en la periferia sur',
					"Las estrechas callejuelas de la Vieja Delhi formaban antaño el centro neurálgico de la capital mogol, antiguamente conocida como 'Shajahanabad' en honor al gran emperador Shah Jahan. Una parada obligatoria debería ser la espléndida mezquita Jama Masjid, cuyas gigantescas cúpulas blancas dominan el horizonte de la vieja ciudad",
					'Tras la visita de la mezquita Jama Masjid, disfrute de un paseo en rickshaw por las callejuelas de la Vieja Delhi. Desde aquí nos dirigiremos a Raj Ghat para visitar el lugar de cremación de Mahatma Gandhi',
					'Desde Raj Ghat nos adentraremos en los rincones coloniales de Nueva Delhi, incluidas las grandes obras maestras arquitectónicas de Lutyen diseñadas para abrumar y someter a los lugareños. También visitaremos la Tumba de Humayun, uno de los primeros edificios mogoles más importantes de la India, que se alza en cuidados jardines un poco más al sur y que puede visitarse de camino a la emblemática torre de la victoria Qutb Minar, en la periferia sur de Delhi, última parada del día',
					'Alojamiento en un hotel de Delhi',
				],
				id: '131',
				image:
					'https://images.unsplash.com/photo-1609258678760-ba05d9b95bb9?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '3',
				title: 'Delhi - Agra',
				city: 'Agra',
				dayActivity: [
					'Después del desayuno, viaje por la nueva autopista interestatal hasta Agra para visitar el Fuerte Mogol de la ciudad y los mercados cercanos, seguido de una visita al Taj al atardecer',
					"Shah Jahan, el emperador que creó el Taj Mahal, fue encarcelado hacia el final de su vida por su hijo, Aurangzeb, en el esplendor dorado de la gran fortaleza-palacio de los mogoles con vistas al río Yamuna, su primera parada turística del día. A través de sus pilares finamente tallados y sus ventanas en arco de cúspide, podrá saborear las mismas vistas románticas de las que disfrutaba el gobernante enfermo. En la orilla opuesta, la tumba de Itimad-ud Daulah alberga los restos de su primer ministro, o 'Visir'. El famoso e intrincado trabajo de incrustación del mausoleo presagiaba el del Taj, que visitará hacia el final de la tarde, cuando la luz cambiante transforma las superficies de mármol de un ocre pálido a naranja y carmesí",
				],
				id: '132',
				image:
					'https://images.unsplash.com/photo-1591018653367-9c01498b3320?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '4',
				title: 'Agra - Jaipur',
				city: 'Jaipur',
				dayActivity: [
					'Por la mañana, viaje a Jaipur, haciendo un alto en el camino para visitar uno de los pozos escalonados más impresionantes de la India. Llegará a la capital rajastaní a media tarde',
					'Justo al lado de la autopista principal Agra-Jaipur, el magnífico pozo escalonado de Abhaneri es su parada del día. Con 3.500 escalones tallados repartidos en trece pisos, el pozo es el más profundo e intrincado de su clase en la India',
					'A su llegada a Jaipur, la mayoría de los turistas prefieren pasar el resto del día descansando junto al hotel, pero cuando se hayan recuperado, recomendamos una visita nocturna al templo de Birla. Aquí nos alojaremos durante dos - noches',
				],
				id: '133',
				image:
					'https://images.unsplash.com/photo-1477587458883-47145ed94245?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '5',
				title: 'Jaipur visitas',
				city: 'Jaipur',
				dayActivity: [
					'Disfrute hoy de un día completo de turismo en Jaipur, empezando por una excursión a Amber, seguida del Hawa Mahal, el observatorio Jantar Mantar y el mundialmente famoso museo del Palacio de la Ciudad',
					"Encaramado en el borde de una dramática escarpa, el fuerte de Amber conserva algunos de los mejores interiores que se conservan de los siglos XVI y XVII en la India, en particular un resplandeciente 'Salón de los Espejos' o 'Sheesh Mahal', revestido de intrincados mosaicos donde el maharajá y sus consortes disfrutaban de recitales de música y poesía",
					"La propia Jaipur es un torbellino de vida y color, y sus numerosos monumentos y mercados rajput son el centro de atención del resto del día. Comenzará en el famoso complejo del Palacio de la Ciudad, que incluye el muy fotografiado 'Hawa Mahal', o 'Palacio de los Vientos', una fachada de cinco pisos con elaboradas ventanas enrejadas desde las que las mujeres de la casa real solían ver las procesiones en las calles de abajo. Los tesoros de la casa real se exhiben con orgullo en los antiguos salones de asambleas con columnas que hay debajo, e incluyen joyas, armas y galas del apogeo de Jaipur en el siglo XVIII",
					'Por la tarde regresará al hotel y podrá relajarse',
				],
				id: '134',
				image:
					'https://images.unsplash.com/photo-1599661046827-dacff0c0f09a?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '6',
				city: 'Delhi',
				title: 'Jaipur - Delhi',
				dayActivity: [
					'Tendrás tiempo para un último paseo por el casco antiguo de Jaipur antes de conducir a Delhi por la tarde, donde te alojarás en un hotel cercano al aeropuerto antes de tu partida al día siguiente',
				],
				id: '135',
				image: null,
			},
			{
				day: '7',
				city: 'Male - Maldivas',
				title: 'Delhi - Male',
				dayActivity: [
					' Desayuno en el hotel. Más tarde según el horario del vuelo, nuestro ejecutivo le traslada al aeropuerto internacional de Delhi para embarcar en su vuelo a Male, Maldivas. A su llegada al aeropuerto de Male, embarque en la lancha rápida desde el embarcadero a su hotel / resort. Check - in y relax',
				],
				id: '136',
				image:
					'https://images.unsplash.com/photo-1543731068-7e0f5beff43a?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '8-10',
				title: 'Maldives beach stay',
				city: 'Maldives',
				dayActivity: ['Días de relax o para disfrutar de la playa.'],
				id: '2',
				image:
					'https://images.unsplash.com/photo-1540202404-a2f29016b523?ixlib=rb-4.0?w=350&h=160&fit=crop&crop=faces&auto=format&q=80',
			},
			{
				day: '11',
				city: 'Male',
				title: 'Male - salida',
				dayActivity: [
					'Hoy, le trasladamos al aeropuerto internacional de Male para su vuelo de vuelta a casa',
				],
				id: '139',
				image: null,
			},
		],
		images: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/1287460/pexels-photo-1287460.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		imageGallery: [
			{
				id: '1',
				url: 'https://images.pexels.com/photos/1287460/pexels-photo-1287460.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '2',
				url: 'https://images.pexels.com/photos/5465323/pexels-photo-5465323.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '3',
				url: 'https://images.pexels.com/photos/3163677/pexels-photo-3163677.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
			{
				id: '4',
				url: 'https://images.pexels.com/photos/4204698/pexels-photo-4204698.jpeg?auto=compress&cs=tinysrgb&dpr=1',
			},
		],
		hotels: [
			{
				id: '1',
				name: 'Hotel Taj Mahal',
				city: 'Delhi',
				country: 'India',
				rating: '5',
			},
			{
				id: '2',
				name: 'Hotel Oberoi Amarvilas',
				city: 'Agra',
				country: 'India',
				rating: '5',
			},
			{
				id: '3',
				name: 'Hotel Rambagh Palace',
				city: 'Jaipur',
				country: 'India',
				rating: '5',
			},
			{
				id: '4',
				name: 'Hotel Yak & Yeti',
				city: 'Kathmandu',
				country: 'Nepal',
				rating: '5',
			},
		],
		countries: [
			{
				id: '1',
				name: 'India',
			},
		],
		availableLanguages: [
			{
				id: '1',
				name: 'English',
			},
			{
				id: '2',
				name: 'French',
			},
			{
				id: '3',
				name: 'German',
			},
			{
				id: '4',
				name: 'Spanish',
			},
			{
				id: '5',
				name: 'Italian',
			},
			{
				id: '6',
				name: 'Japanese',
			},
			{
				id: '7',
				name: 'Chinese',
			},
			{
				id: '8',
				name: 'Portuguese',
			},
		],
		camcellationPolicy:
			'For a full refund, cancel at least 24 hours in advance of the start date of the experience.',
		termsAndConditions:
			'This is a privado tour/activity. Only your group will participate.',
		highlights: [
			{
				id: '1',
				name: 'Viaje entre los sitios del Patrimonio Mundial de la UNESCO a bordo de un coche confortable',
			},
			{
				id: '2',
				name: 'Explora con un guía para profundizar en la historia',
			},
			{
				id: '3',
				name: 'Ideal para aficionados a la historia y viajeros con poco tiempo',
			},
		],
		inclusions: [
			{
				id: '1',
				name: 'Traslados desde y hacia el aeropuerto',
			},
			{
				id: '2',
				name: 'Alojamiento en hoteles (3-5) estrellas',
			},
			{
				id: '3',
				name: 'Desayuno diario en el hotel',
			},
			{
				id: '4',
				name: 'Traslados privados',
			},
			{
				id: '5',
				name: 'Visitas guiadas en transporte privado',
			},
			{
				id: '6',
				name: 'Guia de habla hispana',
			},
			{
				id: '7',
				name: 'Comidas como se indica en el itinerario',
			},
			{
				id: '8',
				name: 'Actividades como se indica en el itinerario',
			},
			{
				id: '9',
				name: 'Agua embotellada durante el recorrido (2 botellas por persona por día)',
			},
			{
				id: '10',
				name: 'Todos los impuestos, tasas y gastos de servicio',
			},
		],
		exclusions: [
			{
				id: '1',
				name: 'Entradas a los monumentos',
			},
			{
				id: '2',
				name: 'Visados',
			},
			{
				id: '3',
				name: 'Seguro de viaje',
			},
			{
				id: '4',
				name: 'Pasajes aéreos',
			},
			{
				id: '5',
				name: 'Propinas',
			},
			{
				id: '6',
				name: 'Comida y bebida, salvo que se especifique lo contrario',
			},
			{
				id: '7',
				name: 'Gastos personales',
			},
			{
				id: '8',
				name: 'Todo lo no mencionado en el itinerario',
			},
		],
	},
];
