const TestimonialRating = () => {
	return (
		<>
			<h2 className='text-30 text-white'>
				What Our Travelers
				<br />
				Say About Us
			</h2>
			<p className='text-white mt-20'>
				Our travelers love us! And no wonder, we strive to offer them the best
				possible travel experience!
			</p>

			<div className='row y-gap-30 text-white pt-60 lg:pt-40'>
				<div className='col-sm-5 col-6'>
					<div className='text-30 lh-15 fw-600'>30k+</div>
					<div className='lh-15'>Happy Travelers</div>
				</div>
				{/* End .col */}

				<div className='col-sm-5 col-6'>
					<div className='text-30 lh-15 fw-600'>4.92</div>
					<div className='lh-15'>Overall Rating</div>
					<div className='d-flex x-gap-5 items-center pt-10'>
						<div className='icon-star text-white text-10' />
						<div className='icon-star text-white text-10' />
						<div className='icon-star text-white text-10' />
						<div className='icon-star text-white text-10' />
						<div className='icon-star text-white text-10' />
					</div>
				</div>
				{/* End .col */}
			</div>
		</>
	);
};

export default TestimonialRating;
