FROM node:18.3.0-alpine3.14

RUN apk add --no-cache bash

WORKDIR /app

COPY package.json .

RUN yarn install --frozen-lockfile

# set environment variables
ENV NODE_ENV production

COPY . .

RUN yarn build

CMD ["yarn", "start", "-p", "3000"]