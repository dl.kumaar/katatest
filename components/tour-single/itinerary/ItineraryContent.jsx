import { nanoid } from 'nanoid';
import Image from 'next/image';
import { urlFor } from '../../../utils/sanityCalient';
import { PortableText } from '@portabletext/react';

const ItineraryContent = ({ itinerary = [] }) => {
	const itineraryContent = itinerary?.map((item, index) => {
		return {
			...item,
			id: index + 1,
			targetCollapse: `item_${index + 1}`,
			itemNo: index + 1,
			title: item?.dayTitle,
			day: item?.day,
			daySubtitle: item?.daySubtitle,
			classShowHide: 'show',
			// classShowHide: index === 1 ? 'show' : '',
		};
	});

	return (
		<>
			{itineraryContent.map((item) => (
				<div className='col-12' key={nanoid(4)}>
					<div className='accordion__item '>
						<div className='d-flex'>
							<div className='accordion__icon size-40 flex-center bg-blue-2 text-blue-1 rounded-full'>
								<div className='text-14 fw-500'>{item?.day}</div>
							</div>
							{/* End item number */}
							<div className='ml-20'>
								<div className='text-16 lh-15 fw-500'>{item.title}</div>
								<div className='text-14 lh-15 text-light-1 mt-5'>
									{item?.daySubtitle && (
										<>
											<i className='icon-placeholder text-16 text-light-1'></i>{' '}
										</>
									)}
									{item?.daySubtitle}
								</div>
								<div
									className={`accordion-collapse collapse ${item.classShowHide}`}
									id={item.targetCollapse}
									data-bs-parent='#itineraryContent'>
									<div className='pt-15 pb-15'>
										{item?.dayImage && (
											<Image
												width={350}
												height={160}
												src={urlFor(item?.dayImage)
													.width(350)
													.height(160)
													.url()}
												alt='image'
												className='rounded-4 mt-15'
											/>
										)}

										{item?.dayDescription?.map((item, _) => {
											return (
												<p
													className='text-15 text-dark-1 lh-17 mt-15'
													key={nanoid()}>
													{item?.children[0]?.text}
												</p>
											);
										})}

										{/* <div className='text-14 lh-17 mt-15'>{item.content}</div> */}
									</div>
								</div>
								{/* End accordion conent */}

								{/* <div
									className='accordion__button'
									data-bs-toggle='collapse'
									data-bs-target={`#${item.targetCollapse}`}>
									<button className='d-block lh-15 text-14 text-blue-1 underline fw-500 mt-5'>
										Ver detalles
									</button>
								</div> */}
								{/* End accordion button */}
							</div>
						</div>
					</div>
				</div>
			))}
		</>
	);
};

export default ItineraryContent;
