import dynamic from 'next/dynamic';
import CallToActions from '../components/home/home-2/CallToActions';
import Seo from '../components/common/Seo';
import Footer2 from '../components/footer/footer-3';
import WhyChoose from '../components/block/BlockGuide';
import Block1 from '../components/about/Block1';
import Image from 'next/image';
import Counter from '../components/counter/Counter';
import Team1 from '../components/team/Team1';
import Link from 'next/link';
import Header2 from '../components/header/header-2';
import Subscribe from '../components/home/home-2/Subscribe';
import { apIClient } from '../utils/sanityCalient';
import DefaultMeta from '../components/common/DefaultMeta';
import { NextSeo } from 'next-seo';

const About = ({ agents }) => {
	return (
		<>
			<Seo pageTitle='About Us - Kata Travels' />
			{/* End Page Title */}

			<DefaultMeta />

			<NextSeo
				title='Travel Agency in India - Kata Travels'
				description='Kata Travels is a travel agency in India that offers tours to India and the subcontinent. We are a team of local experts that will help you plan your trip to India.'
				canonical='https://katatravels.com/about'
				openGraph={{
					url: 'https://katatravels.com/about',
					title: 'Travel Agency in India - Kata Travels',
					description:
						'Kata Travels is a travel agency in India that offers tours to India and the subcontinent. We are a team of local experts that will help you plan your trip to India.',
					images: [
						{
							url: 'https://katatravels.com/img/pages/about/2.png',
							width: 1740,
							height: 960,
							alt: 'Kata Travels',
						},
					],
					siteName: 'Kata Travels',
					type: 'website',
					locale: 'en',
				}}
			/>
			<Header2 />
			{/* End Header 1 */}

			<section className='section-bg layout-pt-lg layout-pb-lg'>
				<div className='section-bg__item col-12'>
					<Image
						width={1920}
						height={400}
						src='/img/pages/about/1.png'
						alt='image'
						priority
					/>
				</div>
				{/* End section-bg__item */}

				<div className='container'>
					<div className='row justify-center text-center'>
						<div className='col-xl-6 col-lg-8 col-md-10'>
							<h1 className='text-40 md:text-25 fw-600 text-white'>
								What we do?
							</h1>
							<div className='text-white mt-15'>
								Your trusted travel partner for a perfect holiday in India!
							</div>
							<div className='d-inline-block'>
								<Link
									href='/tours'
									className='button -md -blue-1 w-180 bg-white text-dark-1 mt-30 md:mt-20'>
									Explore Tours
									<i className='ml-5 icon-arrow-right'></i>
								</Link>
							</div>
						</div>
					</div>
				</div>
				{/* End .container */}
			</section>
			{/* End About Banner Section */}

			<section className='layout-pt-lg layout-pb-md'>
				<div className='container'>
					<div className='row justify-center text-center'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>
									Why Choose Kata Travels?
								</h2>
								<p className='sectionTitle__text mt-5 sm:mt-0'>
									We are passionate travelers, and every day we create
									<br className='lg:d-none' />
									unforgettable vacation experiences for discerning travelers
									around the world.
								</p>
								<p className='mt-20'>
									We believe that planning a luxurious journey should be a
									seamless and enjoyable process. At Kata Travels, we take care
									of all the intricate details, leaving you free to savor the
									anticipation of your grand adventure. Our team of seasoned
									professionals handles every aspect of your trip, from
									organizing private flights and selecting the most prestigious
									accommodations to coordinating private guides and creating
									bespoke experiences that are tailored to your preferences. Sit
									back, relax, and allow us to curate an effortless and elegant
									journey for you.
								</p>
							</div>
						</div>
					</div>
					{/* End .row */}

					<div className='row y-gap-40 justify-between pt-50'>
						<WhyChoose />
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>
			{/* End Why Choose Us section */}

			<section className='layout-pt-md'>
				<div className='container'>
					<div className='row y-gap-30 justify-between items-center'>
						<Block1 />
					</div>
				</div>
			</section>
			{/* End about block section */}

			<section className='pt-60'>
				<div className='container'>
					<div className='border-bottom-light pb-40'>
						<div className='row y-gap-30 justify-center text-center'>
							<Counter />
						</div>
					</div>
				</div>
			</section>
			{/* End counter Section */}

			<section className='layout-pt-lg layout-pb-lg'>
				<div className='container'>
					<div className='row y-gap-20 justify-between items-end'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>Our Team</h2>
								<p
									className=' sectionTitle__text mt-5 sm:mt-0'
									style={{
										maxWidth: '700px',
									}}>
									Our team of seasoned professionals handles every aspect of
									your trip, from organizing private flights and selecting the
									most prestigious accommodations to coordinating private guides
									and creating bespoke experiences that are tailored to your
									preferences.
								</p>
							</div>
						</div>
					</div>
					{/* End .row */}

					<div className=' pt-40 js-section-slider'>
						<div className='item_gap-x30'>
							<Team1 agents={agents} />
						</div>
					</div>
					{/* End  js-section-slider */}
				</div>
				{/* End container */}
			</section>
			{/* End team section */}

			{/* <section className='section-bg layout-pt-lg layout-pb-lg'>
				<div className='section-bg__item -mx-20 bg-light-2' />
				<div className='container'>
					<div className='row justify-center text-center'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>
									Overheard from travelers
								</h2>
								<p className=' sectionTitle__text mt-5 sm:mt-0'>
									These popular destinations have a lot to offer
								</p>
							</div>
						</div>
					</div>
			

					<div className='overflow-hidden pt-80 js-section-slider'>
						<div className='item_gap-x30'>
							<Testimonial />
						</div>
					</div>
		

					<div className='row y-gap-30 items-center pt-40 sm:pt-20'>
						<div className='col-xl-4'>
							<Counter2 />
						</div>
					

						<div className='col-xl-8'>
							<div className='row y-gap-30 justify-between items-center'>
								<Brand />
							</div>
						</div>
				
					</div>
					
				</div>
		
			</section> */}
			{/* End testimonial section */}

			<Subscribe />
			{/* End Subscribe Section */}

			{/* <AppBanner /> */}
			{/* End AppBanner Section */}

			{/* <CallToActions /> */}
			{/* End CallToActions Section */}

			<Footer2 />
			{/* End Footer Section */}
		</>
	);
};

export default dynamic(() => Promise.resolve(About), { ssr: false });

export async function getStaticProps() {
	const query = `*[_type == 'travelAgents']{
		travelAgentName,
			travelAgentSurname,
			travelAgentImage
	}`;

	const agents = await apIClient.fetch(query);

	return {
		props: {
			agents,
		},
	};
}
