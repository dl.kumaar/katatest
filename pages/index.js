import Wrapper from './layout/wrapper';
import Home2 from './home/home_2';
import { apIClient } from '../utils/sanityCalient';
import { NextSeo } from 'next-seo';
import DefaultMeta from '../components/common/DefaultMeta';

const MainRoot = ({ tours }) => {
	return (
		<Wrapper>
			<DefaultMeta />
			<NextSeo
				title='Luxury Tours & Trips to India - Private & Group Tours to India - Kata Travels'
				description='Customized tours and trips to India. Private and group tours to India. Travel agency in India with offices in the United States and Argentina.'
				canonical='https://katatravels.com/'
				openGraph={{
					url: 'https://katatravels.com/',
					title:
						'Luxury Tours & Trips to India - Private & Group Tours to India - Kata Travels',
					description:
						'Customized tours and trips to India. Private and group tours to India. Travel agency in India with offices in the United States and Argentina.',
					images: [
						{
							url: 'https://katatravels.com/img/fb-hero.jpg',
							width: 1740,
							height: 960,
							alt: 'Kata Travels - tours to india',
						},
					],
					siteName: 'Kata Travels',
					type: 'website',
					locale: 'en',
				}}
			/>
			<Home2 tours={tours} />
		</Wrapper>
	);
};

export default MainRoot;

// export async function getStaticProps() {
// 	const query = `*[_type == 'tours' && tags[0] == 'en'][0..7] {
// 			tourName,
// 			tourExcerpt,
// 			tourPrice,
// 			tourType,
// 			tourDuration,
// 			slug,
// 			tourImage,
// 			tourImageGallery,
// 			tourPromoText1
// 	}`;

// 	const tours = await apIClient.fetch(query);

// 	return {
// 		props: {
// 			tours,
// 		},
// 	};
// }

export async function getServerSideProps() {
	const query = `*[_type == 'tours' && tags[0] == 'en'][0..7] {
			tourName,
			tourExcerpt,
			tourPrice,
			tourType,
			tourDuration,
			priceOffSeason,
			priceSeason,
			slug,
			tourImage,
			tourImageGallery,
			tourPromoText1
	}`;

	const tours = await apIClient.fetch(query);

	return {
		props: {
			tours,
		},
	};
}
