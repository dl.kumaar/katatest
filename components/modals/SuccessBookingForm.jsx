import Swal from 'sweetalert2';

export default function successBookingForm(reset) {
	return Swal.fire({
		title: 'Gracias por contactarnos!',
		text: 'En breve nos pondremos en contact contigo.',
		icon: 'success',
		showCancelButton: false,
		showCloseButton: true,
		showConfirmButton: false,

		timer: 5000,

		html: `<div className="row x-gap-10 y-gap-10 pl-40 pr-40">
    <p className="text-15 fw-500">Su solicitud ha sido enviada con éxito. Nos pondremos en contact con usted en breve.</p>
    </div>
    <div className='pt-20' style="
    display: flex;
    justify-content: center;
    ">
    <div className="col-auto mr-10">
        <a href='/tours' className="button -blue-1 bg-light-2 px-20 py-15">Ver más tours</a>
      </div>

      <div className="col-auto">
        <a href='/blog' className="button -dark-1 bg-blue-1 text-white px-20 py-15">Nuestro Blog<span class="icon-arrow-top-right ml-15"></span></a>
      </div>
    </div>`,
	}).then(() => {
		if (reset) reset();
		return;
	});
}
