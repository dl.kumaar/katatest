import Image from 'next/image';
import ReviewGallery from './ReviewGallery';

var reviews = [
	{
		id: 1,
		name: 'Tonko',
		date: 'March 2022',
		rating: 9.2,
		text: 'Nice two level apartment in great London location. Located in quiet small street, but just 50 meters from the main street and bus stop. Tube station is a short walk, just like two grocery stores.',
		images: [
			{
				id: 1,
				src: '/img/testimonials/1/1.png',
				thumbnail: '/img/testimonials/1/1.png',
				width: 110,
				height: 110,
			},
			{
				id: 2,
				src: '/img/testimonials/1/2.png',
				thumbnail: '/img/testimonials/1/2.png',
				width: 110,
				height: 110,
			},
			{
				id: 3,
				src: '/img/testimonials/1/3.png',
				thumbnail: '/img/testimonials/1/3.png',
				width: 110,
				height: 110,
			},
			{
				id: 4,
				src: '/img/testimonials/1/4.png',
				thumbnail: '/img/testimonials/1/4.png',
				width: 110,
				height: 110,
			},
		],
		country: 'United Kingdom',
	},
	{
		id: 2,
		name: 'Emma',
		date: 'April 2022',
		rating: 8.5,
		text: 'Had a wonderful stay at this cozy cottage. The surrounding countryside is breathtaking, and the cottage itself was clean and comfortable. Highly recommended for a peaceful getaway!',
		images: [
			{
				id: 1,
				src: '/img/testimonials/2/1.png',
				thumbnail: '/img/testimonials/2/1.png',
				width: 110,
				height: 110,
			},
			{
				id: 2,
				src: '/img/testimonials/2/2.png',
				thumbnail: '/img/testimonials/2/2.png',
				width: 110,
				height: 110,
			},
		],
		country: 'United Kingdom',
	},
	{
		id: 3,
		name: 'Carlos',
		date: 'May 2022',
		rating: 9.8,
		text: 'Exceptional service and luxurious accommodations. The hotel staff went above and beyond to ensure a memorable experience. The room was stunning with breathtaking views of the city skyline.',
		images: [
			{
				id: 1,
				src: '/img/testimonials/3/1.png',
				thumbnail: '/img/testimonials/3/1.png',
				width: 110,
				height: 110,
			},
		],
		country: 'Spain',
	},
	{
		id: 4,
		name: 'Sophia',
		date: 'June 2022',
		rating: 9.0,
		text: 'Lovely beachfront villa with all the amenities you could ask for. The private pool was a highlight, and the sound of the waves was so soothing. A perfect spot for a relaxing vacation.',
		images: [
			{
				id: 1,
				src: '/img/testimonials/4/1.png',
				thumbnail: '/img/testimonials/4/1.png',
				width: 110,
				height: 110,
			},
			{
				id: 2,
				src: '/img/testimonials/4/2.png',
				thumbnail: '/img/testimonials/4/2.png',
				width: 110,
				height: 110,
			},
			{
				id: 3,
				src: '/img/testimonials/4/3.png',
				thumbnail: '/img/testimonials/4/3.png',
				width: 110,
				height: 110,
			},
		],
		country: 'Greece',
	},
	{
		id: 5,
		name: 'David',
		date: 'July 2022',
		rating: 8.2,
		text: 'Enjoyed my stay at this centrally located apartment. The neighborhood had a vibrant atmosphere, and there were plenty of restaurants and shops within walking distance. The apartment was clean and well-equipped.',
		images: [
			{
				id: 1,
				src: '/img/testimonials/5/1.png',
				thumbnail: '/img/testimonials/5/1.png',
				width: 110,
				height: 110,
			},
			{
				id: 2,
				src: '/img/testimonials/5/2.png',
				thumbnail: '/img/testimonials/5/2.png',
				width: 110,
				height: 110,
			},
		],
		country: 'United States',
	},
];

const DetailsReview2 = () => {
	return (
		<>
			{reviews.map((review) => (
				<div key={review.id} className='row y-gap-60 mb-40 border-bottom-light'>
					<div className='col-lg-12'>
						<div className='row x-gap-20 y-gap-20 items-center'>
							<div className='col-auto'>
								<Image
									width={60}
									height={60}
									src='/img/avatars/3.png'
									alt='image'
									className='rounded-circle'
								/>
							</div>
							<div className='col-auto'>
								<div className='fw-500 lh-15'>Tonko</div>
								<div className='text-14 text-light-1 lh-15'>March 2022</div>
							</div>
						</div>
						{/* End .row */}

						<div className='row x-gap-20'>
							<h5 className='fw-500 text-blue-1 mt-20'>9.2 Superb</h5>
						</div>
						<p className='text-15 text-dark-1 mt-10'>
							Nice two level apartment in great London location. Located in
							quiet small street, but just 50 meters from main street and bus
							stop. Tube station is short walk, just like two grocery stores.{' '}
						</p>

						<ReviewGallery />
					</div>
				</div>
			))}

			<div className='col-auto'>
				<a href='#' className='button -md -outline-blue-1 text-blue-1'>
					Show all 116 reviews{' '}
					<div className='icon-arrow-top-right ml-15'></div>
				</a>
			</div>
		</>
	);
};

export default DetailsReview2;
