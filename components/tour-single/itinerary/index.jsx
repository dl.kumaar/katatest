import AccordionContent from './ItineraryContent';

const index = ({ itinerary = [] }) => {
	return (
		<div className='row y-gap-30'>
			<div className='col-lg-12'>
				<div className='relative'>
					<div className='border-test' />
					<div className='accordion -map row y-gap-20' id='itineraryContent'>
						<AccordionContent itinerary={itinerary} />
					</div>
				</div>
			</div>
			{/* End col-lg-4 */}
		</div>
	);
};

export default index;
