import Image from 'next/image';
import Link from 'next/link';
import Header1 from '../components/header/header-2';
import Footer2 from '../components/footer/footer-3';
import { NextSeo } from 'next-seo';

export default function Gracias() {
	return (
		<div>
			<NextSeo
				title='Thank you - Kata Travels'
				description='Thank you for sending your trip request. We will contact you shortly.'
				canonical='https://katatravels.com/thank-you'
			/>

			<Header1 darkNav={true} />
			<section className='masthead -type-6'>
				<div className='masthead__bg bg-dark-3'>
					<Image
						alt='image'
						src={
							'https://images.unsplash.com/photo-1619866640467-86547b9858d1?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1920&q=80'
						}
						width={1920}
						height={1080}
						className='js-lazy'
						loading='eager'
					/>
				</div>

				<div className='container'>
					<div className='row justify-center'>
						<div className='col-xl-9'>
							<div className='text-center'>
								<h1
									className='text-60 lg:text-40 md:text-30 text-white'
									data-aos='fade-up'>
									<span className='text-yellow-1'>
										Thank you for contacting us.
									</span>
								</h1>
								<p className='text-white mt-5 text-18'>
									Soon one of our agents will contact you to help plan your next
									trip.
								</p>
							</div>
							{/* End hero title */}
						</div>

						<div className='col-xl-9 row justify-center'>
							<Link
								href='/'
								className='button -md -yellow-1 w-180 bg-white text-dark-1 mt-30 md:mt-20'>
								Go to homepage
							</Link>

							<Link
								href='/tours'
								className='button -md ml-20 -yellow-1 w-180 bg-dark-1 text-white mt-30 md:mt-20'>
								See our tours
							</Link>
						</div>
					</div>
				</div>
			</section>

			<Footer2 />
		</div>
	);
}
