import Link from 'next/link';
import { useEffect, useState } from 'react';
import MobileMenu from '../MobileMenu';
import Image from 'next/image';

import Logo from '../../../assets/kata-logo.png';
import LogoWhite from '../../../assets/kata-logo-white.png';

import Whatsapp from '../../../assets/whatsapp-png.png';
import MainMenu from '../MainMenu';

import WhatsApp from '../../../assets/WhatsAppButtonGreenLarge.svg';

const Header1 = ({ darkNav }) => {
	const [navbar, setNavbar] = useState(false);

	const changeBackground = () => {
		if (window.scrollY >= 10) {
			setNavbar(true);
		} else {
			setNavbar(false);
		}
	};

	useEffect(() => {
		window.addEventListener('scroll', changeBackground);
	}, []);

	return (
		<>
			{/* <div
				className='d-flex items-center ml-20 is-menu-opened-hide bg-white'
				style={{
					position: 'fixed',
					bottom: '100px',
					right: '20px',
					zIndex: '9999999',
					borderRadius: '50%',
				}}>
				<a aria-label='Chat on WhatsApp' href='https://wa.me/+919868788591'>
					<Image
						src={Whatsapp}
						alt='whatsapp button'
						width={100}
						height={100}
						style={{ width: 'auto', maxHeight: '40px' }}
					/>
				</a>
			</div> */}
			<header
				className={`header ${navbar || darkNav ? 'bg-dark-1 is-sticky' : ''}`}>
				<div className='header__container container'>
					<div className='row justify-between items-center'>
						<div className='col-auto mobile-col'>
							<div className='d-flex items-center'>
								<div className='mr-20 items-center d-none sm:d-flex'>
									{/* <div className='mr-15 d-none md:d-flex'>
										<Link
											href='/others-pages/login'
											className='icon-user text-inherit text-22 text-white'
										/>
									</div> */}

									<button
										className='d-flex items-center icon-menu text-white text-20'
										data-bs-toggle='offcanvas'
										aria-controls='mobile-sidebar_menu'
										data-bs-target='#mobile-sidebar_menu'></button>

									{/* <div
										className='offcanvas offcanvas-start  mobile_menu-contnet'
										tabIndex='-1'
										id='mobile-sidebar_menu'
										aria-labelledby='offcanvasMenuLabel'
										data-bs-scroll='true'>
										<MobileMenu />
									</div> */}
								</div>
								{/* End mobile humberger menu */}

								<Link href='/' className={`header-logo mr-20`}>
									<Image
										src={LogoWhite}
										alt='logo icon'
										width={100}
										height={100}
									/>
									{/* <Image src={Logo} alt='logo icon' width={100} height={100} /> */}
									{/* <img src='/img/general/logo-dark.svg' alt='logo icon' /> */}
									{/* <img src='/img/general/logo-light-2.svg' alt='logo icon' />
									<img src='/img/general/logo-dark.svg' alt='logo icon' /> */}
								</Link>
								{/* End logo */}
								{/* 
								<div className='relative xl:d-none'>
									<LocationSearch />
								</div> */}
								{/* End Search box */}
							</div>
							{/* End d-flex */}
						</div>
						{/* End col */}

						<div className='col-auto'>
							<div className='d-flex items-center'>
								<div className='header-menu'>
									<div className='header-menu__content'>
										<MainMenu style='text-white' />
									</div>
								</div>
								{/* End header-menu */}

								{/* Start btn-group */}
								<div className='d-flex items-center ml-20 is-menu-opened-hide md:d-none'>
									<Link
										href='/plan-my-trip'
										className='button px-30 fw-400 text-16 -blue-1 bg-white h-50 text-dark-1'>
										Plan My Trip
									</Link>
									{/* <Link
										href='/contact'
										className='button px-30 fw-400 text-16 border-white -outline-white h-50 text-white ml-20'>
										Contact
									</Link> */}
									{/* <Link
										href={'https://wa.me/+919868788591'}
										className='px-20 text-16 h-40'>
										<Image
											src={WhatsApp}
											alt='whatsapp button to chat with kata travels team'
											width={200}
											height={300}
										/>
									</Link> */}

									<Link
										href='https://api.whatsapp.com/send?phone=919868788591&text=Hi there, I am interested in your services. Please get in touch with me.'
										className='whatsapp-float ml-20'
										target='_blank'>
										<i className='fa fa-whatsapp my-float'></i>
									</Link>
								</div>
								{/* End btn-group */}

								{/* Start mobile menu icon */}
								<div className='d-none xl:d-flex x-gap-20 items-center pl-30 text-white'>
									{/* <div>
										<Link
											href='/plan-my-trip'
											className='d-flex items-center icon-user text-inherit'
										/>
									</div> */}
									<div>
										<button
											className='d-flex items-center icon-menu text-inherit text-20'
											data-bs-toggle='offcanvas'
											aria-controls='mobile-sidebar_menu'
											data-bs-target='#mobile-sidebar_menu'
										/>

										<div
											className='offcanvas offcanvas-start  mobile_menu-contnet'
											tabIndex='-1'
											id='mobile-sidebar_menu'
											aria-labelledby='offcanvasMenuLabel'
											data-bs-scroll='true'>
											<MobileMenu />
											{/* End MobileMenu */}
										</div>
									</div>
								</div>
								{/* End mobile menu icon */}
							</div>
						</div>

						{/* End col-auto */}
					</div>
					{/* End .row */}
				</div>
				{/* End header_container */}
			</header>
		</>
	);
};

export default Header1;
