// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import appendData from '../../utils/appendData';

export default async function handler(req, res) {
	const {
		name,
		email,
		phone,
		country,
		specialRequest,
		date,
		adults,
		children,
		rooms,
		tour,
		tourName,
	} = req.body;
	const dateTime = new Date().toLocaleString('en-US', {
		timeZone: 'America/New_York',
	});

	const userIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;

	const queryType = 'Tour Package Request';

	try {
		const _res = await appendData('tourInquiry', [
			dateTime,
			queryType,
			name || '',
			email || '',
			phone || '',
			country || '',
			date || '',
			adults || '',
			children || '',
			rooms || '',
			specialRequest || '',
			tourName || '',
			tour || '',
			userIp || '',
		]);

		res.status(201).json({
			message: 'Success',
		});
	} catch (error) {
		res.status(500).json({
			message: 'Algo salió mal. Por favor, inténtelo de nuevo.',
		});
	}
}
