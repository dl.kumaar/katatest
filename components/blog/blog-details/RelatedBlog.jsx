import { nanoid } from 'nanoid';
import Image from 'next/image';
import blogsData from '../../../data/blogs';
import { formatDate } from '../../../utils/blogUtils';

const RelatedBlog = ({ current }) => {
	const currentBlog = blogsData.find((item) => item.slug === current.slug);
	const currentIndex = blogsData.indexOf(currentBlog);

	// 4 blogs different to current blog

	const blogs = blogsData.filter((item) => item.slug !== current.slug);

	return (
		<>
			{blogs.slice(0, 4).map((item) => (
				<div className='col-lg-3 col-sm-6' key={nanoid(5)}>
					<a
						href={`/blog/article/${item?.slug}`}
						className='blogCard -type-2 d-block bg-white rounded-4 shadow-4'>
						<div className='blogCard__image'>
							<div className='rounded-4'>
								<Image
									width={400}
									height={300}
									className='cover w-100 img-fluid'
									src={item?.feature_image}
									alt='image'
									style={{
										maxHeight: '300px',
										height: '200px',
										objectFit: 'cover',
										objectPosition: 'center',
									}}
								/>
							</div>
						</div>
						<div className='px-20 py-20'>
							<h4 className='text-dark-1 text-18 fw-500'>{item?.title}</h4>
							{/* <div className='text-light-1 text-15 lh-14 mt-10'>
								{formatDate(item?.created_at?.$date)}
							</div> */}
						</div>
					</a>
				</div>
			))}
		</>
	);
};

export default RelatedBlog;
