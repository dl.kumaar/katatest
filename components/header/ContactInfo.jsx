const ContactInfo = () => {
	const contactContent = [
		{
			id: 1,
			title: '24/7 Support',
			action: '#',
			text: '+91 783 812 438 6',
		},
		{
			id: 2,
			title: 'Bookings & Enquiries',
			action: 'mailto:hello@katatravels.com',
			text: 'hello@katatravels.com',
		},
	];
	return (
		<>
			{contactContent.map((item) => (
				<div className='mb-20' key={item.id}>
					<div className={'text-14'}>{item.title}</div>
					<a href={item.action} className='text-18 fw-500 text-dark-1 mt-5'>
						{item.text}
					</a>
				</div>
			))}
		</>
	);
};

export default ContactInfo;
