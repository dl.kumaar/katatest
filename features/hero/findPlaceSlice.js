import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	tabs: [
		{ id: 6, name: 'India', icon: 'icon-home' },
		{ id: 1, name: 'Maldivas', icon: 'icon-plans' },
		{ id: 4, name: 'Sudeste Asiático', icon: 'icon-placeholder' },
		{ id: 5, name: 'Medio Oriente', icon: 'icon-compass' },

		// { id: 7, name: 'Salidas Grupales', icon: 'icon-group' },
		// { id: 2, name: 'Americas', icon: 'icon-destination' },
	],
	currentTab: 'India',
};

export const findPlaceSlice = createSlice({
	name: 'find-place',
	initialState,
	reducers: {
		addCurrentTab: (state, { payload }) => {
			state.currentTab = payload;
		},
	},
});

export const { addCurrentTab } = findPlaceSlice.actions;
export default findPlaceSlice.reducer;
