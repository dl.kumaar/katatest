import dynamic from 'next/dynamic';
import Link from 'next/link';

import { india, maldives, nepal } from '../../../data/desinations';
import Seo from '../../../components/common/Seo';
import Header2 from '../../../components/header/header-2';
import Footer2 from '../../../components/footer/footer-3';
import TopDestinations2 from '../../../components/destinations/TopDestinations2';
import Faq from '../../../components/faq/Faq';
import TestimonialLeftCol from '../../../components/home/home-1/TestimonialLeftCol';
import Testimonial from '../../../components/home/home-1/Testimonial';
import Slights from '../../../components/block/Slights';
import Banner from '../../../components/destinations/components/Banner';
import Categories from '../../../components/destinations/components/Categories';
import IntroTown from '../../../components/destinations/components/IntroTown';
import GeneralInfo from '../../../components/destinations/components/GeneralInfo';
import Blog4 from '../../../components/blog/Blog4';
import { useState } from 'react';
import Tours2 from '../../../components/tours/Tours2';
import { useRouter } from 'next/router';
import { apIClient } from '../../../utils/sanityCalient';
import { useEffect } from 'react';

const countries = [
	{
		id: 'india',
		name: 'India',
		data: india,
	},
	{
		id: 'nepal',
		name: 'Nepal',
		data: nepal,
	},
	{
		id: 'maldives',
		name: 'Maldives',
		data: maldives,
	},
];

const Destinations = ({ destination, countryId }) => {
	// const router = useRouter();
	// const destination = router.query.country === 'india' ? india : nepal

	const [attractionOffset, setAttractionOffset] = useState(4);
	const [tours, setTours] = useState([]);

	const query = `*[_type == 'tours' 
  && tags[0] == 'en'
  && $region in tourRegion[]->regionSlug.current
 ]{
   	tourName,
	tourExcerpt,
	tourPrice,
	tourType,
	tourDuration,
	priceOffSeason,
	priceSeason,
	slug,
    tourImage,
    tourImageGallery,
    tourPromoText1,
    tourRegion[]->{
    regionSlug,
    }
 }`;

	function getTours(countryId) {
		return apIClient.fetch(query, { region: countryId });
	}

	useEffect(() => {
		if (!countryId) return;

		getTours(countryId).then((tours) => {
			setTours(tours);
		});

		return () => {};
	}, []);

	return (
		<>
			<Seo pageTitle='Destinations' />
			{/* End Page Title */}

			<div className='header-margin'></div>
			{/* header top margin */}

			<Header2 darkNav={true} />

			<section className='layout-pb-md layout-pt-sm'>
				<div className='container'>
					<div className='row'>
						<Banner
							img={destination.banner.bannerImage}
							title={destination.banner.title}
							subtitle={destination.banner.subtitle}
						/>
					</div>
					{/* End .row */}

					{/* <div className='row x-gap-20 y-gap-20 items-center pt-20 item_gap-x10'>
						<Categories />
					</div> */}
					{/* End .row */}

					<div className='row y-gap-20 pt-40'>
						<div className='col-auto'>
							<h2>What to know before visiting {destination.name}</h2>
						</div>
						{/* End .col-auto */}

						<IntroTown
							text={destination.intro}
							sideImg={destination.introImg}
						/>
					</div>
					{/* End .row */}

					<div className='pt-30 mt-30 border-top-light' />
					<div className='row y-gap-20'>
						<div className='col-12'>
							<h2 className='text-22 fw-500'>General info</h2>
						</div>
						{/* End .col */}
						<GeneralInfo />
					</div>
					{/* End .row */}
					<div className='mt-30 border-top-light' />
					{/* border separation */}
				</div>
				{/* End .container */}
			</section>
			{/* End Top Banner,categorie,intro,weather, generic info section */}

			<section className='layout-pt-md layout-pb-md' id='tours'>
				<div className='container'>
					<div className='row y-gap-20 justify-between items-end'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>
									{destination.name} Tours & Vacation Packages
								</h2>
								<p className=' sectionTitle__text mt-5 sm:mt-0'>
									Discover the best tours to {destination.name} designed by
									local travel experts
								</p>
							</div>
						</div>
						{/* End .col */}
					</div>
					{/* End .row */}

					<div className='row y-gap-30 pt-40 sm:pt-20 item_gap-x30'>
						{/* <Hotels /> */}
						<Tours2 tours={tours} />
					</div>
					<div className='row justify-center pt-60'>
						<div className='col-auto'>
							<Link
								href='/luxury-tours-vacation-packages'
								className='button px-40 h-50 -outline-blue-1 text-blue-1'>
								Explore All Tours
								<div className='icon-arrow-top-right ml-15' />
							</Link>
						</div>
					</div>
				</div>
			</section>
			{/* End  Hotel sections */}

			<section className='layout-pt-md layout-pb-md'>
				<div className='container'>
					<div className='row'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>
									{destination.attractions.title}
								</h2>
								<p className=' sectionTitle__text mt-5 sm:mt-0'>
									{destination.attractions.subtitle}
								</p>
							</div>
						</div>
					</div>
					{/* End .row */}

					<div className='row y-gap-30 pt-40'>
						<Slights
							attractions={destination.attractions.attractionsList.slice(
								0,
								attractionOffset
							)}
						/>
					</div>
					{/* End .row */}

					{attractionOffset <
						destination.attractions.attractionsList.length && (
						<div className='row justify-center mt-40'>
							<div className='col-auto'>
								<button
									onClick={() => setAttractionOffset(attractionOffset + 4)}
									className='button h-50 w-250 -outline-blue-1 text-blue-1'>
									Explore more <div className='icon-arrow-top-right ml-15' />
								</button>
							</div>
						</div>
					)}

					{/* End .row */}
				</div>
				{/* End .container */}
			</section>
			{/* End Top sights in London */}

			{destination.destinations && (
				<section className='layout-pt-md layout-pb-md'>
					<div className='container'>
						<div className='row y-gap-20'>
							<div className='col-auto'>
								<div className='sectionTitle -md'>
									<h2 className='sectionTitle__title'>
										Popular destinations to visit in {destination.name}
									</h2>
									<p className=' sectionTitle__text mt-5 sm:mt-0'>
										Visit the most popular destinations in {destination.name}{' '}
										and discover the best time and places to visit.
									</p>
								</div>
							</div>
						</div>
						{/* End .row */}

						<div className='pt-40 relative'>
							<TopDestinations2 />
						</div>
					</div>
					{/* End .container */}
				</section>
			)}

			{/* End top destinations */}

			<section className='layout-pb-md layout-pt-md'>
				<div className='container'>
					<div className='row justify-center text-center'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>
									🌟✨ Get Inspired for your next <br />
									🌍🧳✈️ trip to {destination.name} 🕌🐘🏞️🌴
								</h2>
							</div>
						</div>
					</div>
					{/* End .row  */}
					<div className='row y-gap-30 pt-40'>
						<Blog4 />

						<div className='row justify-center pt-60'>
							<div className='col-auto'>
								<Link
									href='/blog'
									className='button px-40 h-50 -outline-blue-1 text-blue-1'>
									Read More Articles
									<div className='icon-arrow-top-right ml-15' />
								</Link>
							</div>
						</div>
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>

			<section className='layout-pt-lg layout-pb-lg bg-light-2'>
				<div className='container'>
					<div className='row y-gap-40 justify-between'>
						<div className='col-xl-5 col-lg-6' data-aos='fade-up'>
							<TestimonialLeftCol />
						</div>
						{/* End col */}

						<div className='col-lg-6'>
							<div
								className='overflow-hidden js-testimonials-slider-3'
								data-aos='fade-up'
								data-aos-delay='50'>
								<Testimonial />
							</div>
						</div>
					</div>
					{/* End .row */}
				</div>
				{/* End container */}
			</section>
			{/* End testimonial Section */}

			<section className='layout-pt-lg layout-pb-md'>
				<div className='container'>
					<div className='row y-gap-20'>
						<div className='col-lg-4'>
							<h2 className='text-30 fw-500'>
								FAQs about
								<br />
								{destination.name}
							</h2>
						</div>
						{/* End .col */}

						<div className='col-lg-8'>
							<div className='accordion -simple row y-gap-20 js-accordion'>
								<Faq faqs={destination.faqs} />
							</div>
						</div>
						{/* End .col-lg-8 */}
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>
			{/* End Faq Section */}

			<Footer2 />
			{/* End Call To Actions Section */}
		</>
	);
};

export default dynamic(() => Promise.resolve(Destinations), { ssr: false });

export async function getStaticPaths() {
	const paths = countries.map((country) => {
		return {
			params: { country: country.id },
		};
	});

	return { paths, fallback: true };
}

export async function getStaticProps(ctx) {
	let country;
	country = countries.find((country) => country.id === ctx.params.country);

	if (!country?.data) {
		return {
			notFound: true,
		};
	}

	return {
		props: {
			destination: country?.data ? country.data : null,
			countryId: country?.id ? country.id : null,
		},
	};
}
