import React, { useState } from 'react';
import DatePicker, { DateObject } from 'react-multi-date-picker';

const DateSearch = () => {
	// const [dates, setDates] = useState([
	// 	new DateObject().setDay(15),
	// 	new DateObject().setDay(14).add(1, 'month'),
	// ]);

	const [month, setMonth] = useState(new DateObject().add(0, 'month'));
	const [year, setYear] = useState(new DateObject().add(0, 'year'));

	function handleSetMonth(e) {
		setMonth(e.month);
		setYear(e.year);
	}

	return (
		<div className='text-15 text-light-1 ls-2 lh-16 custom_dual_datepicker'>
			{/* <DatePicker
        inputClass="custom_input-picker"
        containerClassName="custom_container-picker"
        value={dates}
        onChange={setDates}
        numberOfMonths={2}
        offsetY={10}
        range
        rangeHover
        format="MMMM DD"
      /> */}

			<DatePicker
				onlyMonthPicker
				inputClass='custom_input-picker'
				containerClassName='custom_container-picker'
				format='MMMM YYYY'
				months={[
					'Enero',
					'Febrero',
					'Marzo',
					'Abril',
					'Mayo',
					'Junio',
					'Julio',
					'Agosto',
					'Septiembre',
					'Octubre',
					'Noviembre',
					'Diciembre',
				]}
				minDate={new DateObject().add(0, 'month')}
				maxDate={new DateObject().add(18, 'month')}
				onChange={(e) => handleSetMonth(e)}
			/>
		</div>
	);
};

export default DateSearch;
