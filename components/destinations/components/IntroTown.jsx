import Image from 'next/image';
import { useState } from 'react';

const IntroTown = ({ text, sideImg }) => {
	const [showMore, setShowMore] = useState(false);

	const paraGraph = text.map((item, i) => {
		return (
			<p
				key={i}
				className={
					'text-15 text-dark-1 mb-20' + (i > 3 && !showMore ? ' d-none' : '')
				}>
				{item}
			</p>
		);
	});

	const handleShowMore = () => {
		setShowMore(!showMore);
	};

	return (
		<>
			<div className='col-xl-8'>
				{paraGraph}

				{text?.length > 2 && (
					<button
						onClick={handleShowMore}
						className='d-block text-14 text-blue-1 fw-500 underline mt-10'>
						{showMore ? 'Show less' : 'Show more'}
					</button>
				)}
			</div>
			{/* End .col */}

			<div className='col-xl-4'>
				<div className='relative d-flex ml-35 xl:ml-0'>
					<Image
						src={sideImg}
						alt='image'
						width={400}
						height={400}
						className='col-12 rounded-4'
					/>
					<div className='absolute d-flex justify-center items-end col-12 h-full z-1 px-35 py-20'>
						<a
							href='#tours'
							className='button h-50 px-25 -blue-1 bg-white text-dark-1 text-14 fw-500 col-12'>
							<i className='icon-eye text-18 mr-10' />
							See popular Tours
						</a>
					</div>
				</div>
			</div>
		</>
	);
};

export default IntroTown;
