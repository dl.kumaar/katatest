// import DateSearch from '../common/DateSearch';
// import GuestSearch from '../common/GuestSearch';
// import LocationSearch from '../common/LocationSearch';

// const locationSearchContent = [
// 	{
// 		id: 1,
// 		name: 'Subcontinente Indio',
// 	},
// 	{
// 		id: 5,
// 		name: 'Europa',
// 	},
// 	{
// 		id: 7,
// 		name: 'Argentina',
// 	},
// 	{
// 		id: 2,
// 		name: 'Medio Oriente',
// 	},
// 	{
// 		id: 3,
// 		name: 'Sudeste Asiático',
// 	},
// 	{
// 		id: 4,
// 		name: 'Caribe y Centro América',
// 	},
// ];

// const MainFilterSearchBox = () => {
// 	return (
// 		<>
// 			<div className='mainSearch -col-2-big bg-white px-10 py-10 lg:px-20 lg:pt-5 lg:pb-20 rounded-4 mt-30'>
// 				<div className='button-grid items-center'>
// 					<LocationSearch
// 						options={{
// 							placeholder: '¿Qué región deseas visitar?',
// 							list: locationSearchContent,
// 						}}
// 					/>

// 					<div className='button-item h-full'>
// 						<button className='button -dark-1 py-15 px-40 h-full col-12 rounded-0 bg-blue-1 text-white'>
// 							<i className='icon-search text-20 mr-10' />
// 							Buscar Viajes
// 						</button>
// 					</div>
// 					{/* End search button_item */}
// 				</div>
// 			</div>
// 		</>
// 	);
// };

// export default MainFilterSearchBox;
import DateSearch from '../common/DateSearch';
import LocationSearch from './LocationSearch';

const MainFilterSearchBox = () => {
	return (
		<>
			<div className='col-12'>
				<LocationSearch />
				{/* End Location */}
			</div>
			{/* End .col-12 */}

			<div className='col-12'>
				<div className='searchMenu-date px-20 py-10 bg-white rounded-4 -left js-form-dd js-calendar'>
					<div className='d-flex'>
						<i className='icon-calendar-2 text-20 text-light-1 mt-5'></i>
						<div className='ml-10 flex-grow-1'>
							<h4 className='text-15 fw-500 ls-2 lh-16'>
								Mes previsto del viaje
							</h4>
							<DateSearch />
						</div>
					</div>
				</div>
				{/* End check-in-out */}
			</div>
			{/* End .col-12 */}

			<div className='col-12'>
				<div className='button-item h-full'>
					<button className='button -dark-1 py-15 px-40 h-full col-12 rounded-0 bg-blue-1 text-white'>
						<i className='icon-search text-20 mr-10' />
						Buscar
					</button>
				</div>
				{/* End search button_item */}
			</div>
			{/* End .col-12 */}
		</>
	);
};

export default MainFilterSearchBox;
