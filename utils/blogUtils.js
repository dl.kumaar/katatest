// format date in Monthname, day, year
export const formatDate = (date) => {
	const dateObj = randomTimeStamp();

	const month = dateObj.toLocaleString('default', { month: 'long' });
	const day = dateObj.getDate() + 1;
	const year = dateObj.getFullYear();

	return `${month} ${day}, ${year}`;
};

export function extractText(text, length) {
	if (text?.length > length) {
		return text.substring(0, length) + '...';
	} else {
		return text;
	}
}

function randomDate(start, end) {
	return new Date(
		start.getTime() + Math.random() * (end.getTime() - start.getTime())
	);
}

function randomTimeStamp() {
	const date = randomDate(new Date(2022, 0, 1), new Date());
	return date;
}
