import Image from 'next/image';
import { useEffect, useState } from 'react';
import MainFilterSearchBox from './MainFilterSearchBox';

// const MASTERHEAD_BG = [
// 	'https://katatravels.com/static/img/pages/kata-travels-india.jpeg',
// ];

const Index = () => {
	// const [heroImg, setHeroImg] = useState(MASTERHEAD_BG[0]);

	// useEffect(() => {
	// 	const interval = setInterval(() => {
	// 		const currentIndex = MASTERHEAD_BG.indexOf(heroImg);
	// 		const nextIndex =
	// 			currentIndex + 1 === MASTERHEAD_BG.length ? 0 : currentIndex + 1;
	// 		setHeroImg(MASTERHEAD_BG[nextIndex]);
	// 	}, 5000);
	// 	return () => clearInterval(interval);
	// }, [heroImg]);

	return (
		<section className='masthead -type-6'>
			<div className='masthead__bg bg-dark-3'>
				<Image
					alt='image'
					src={'/img/pages/kata-travels-india.jpeg'}
					width={1920}
					height={1080}
					className='js-lazy'
				/>
			</div>

			<div className='container'>
				<div className='row justify-center'>
					<div className='col-xl-9'>
						<div className='text-center'>
							<h1
								className='text-60 lg:text-40 md:text-30 text-white'
								data-aos='fade-up'>
								<span className='text-yellow-1'>Your Gateway to</span>
								<br />
								{/* Exquisite Experiences */}
								Extraordinary Adventures
							</h1>
							<p
								className='text-white mt-5'
								data-aos='fade-up'
								data-aos-delay='100'>
								¡Experience the{' '}
								<strong className='underline'>Indian Subcontinent</strong> Like
								Never Before with Local Travel Experts. Let Your Adventure
								Begin.!
							</p>
						</div>
						{/* End hero title */}
					</div>
				</div>
			</div>
		</section>
	);
};

export default Index;
