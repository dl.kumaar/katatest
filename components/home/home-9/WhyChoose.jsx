import Image from 'next/image';

const WhyChoose = () => {
	const whyChooseContent = [
		{
			id: 1,
			icon: '/img/featureIcons/2/1.svg',
			title: '100% Tailor-Made',
			text: `Tailored Luxury Experiences for the Most Discerning Travelers. Your personal travel advisor works with you to create the perfect tailored itinerary.
			`,
			delayAnimaion: '100',
		},
		{
			id: 2,
			icon: '/img/featureIcons/3/2.svg',
			title: 'Private Tours',
			text: `Unparalleled Service and Unforgettable Experiences. All tours are private unless otherwise indicated. Enjoy individual attention and exceptional service.`,
			delayAnimaion: '200',
		},
		{
			id: 3,
			icon: '/img/featureIcons/3/3.svg',
			title: 'Local Knowledge',
			text: `Our staff and guides are knowledgeable, professional, and passionate about their home country.`,
			delayAnimaion: '300',
		},
		{
			id: 4,
			icon: '/img/featureIcons/3/4.svg',
			title: '24/7 Trip Support',
			text: `All our travelers receive complimentary 24/7 emergency assistance from English-speaking staff.`,
			delayAnimaion: '400',
		},
	];

	return (
		<>
			{whyChooseContent.map((item) => (
				<div
					className='col-sm-6'
					data-aos='fade-up'
					data-aos-delay={item.delayAnimaion}
					key={item.id}>
					<Image
						width={60}
						height='60'
						src={item.icon}
						alt='image'
						className='size-60'
					/>
					<h5 className='text-18 fw-500 mt-10'>{item.title}</h5>
					<p className='mt-10'>{item.text}</p>
				</div>
			))}
		</>
	);
};

export default WhyChoose;
