/* eslint-disable @next/next/no-img-element */
import Social2 from '../../common/social/Social2';

const DetailsContent = ({ content }) => {
	return (
		<>
			{/* <h3 className='text-20 fw-500'>What makes a good brand book?</h3> */}
			<div
				className='blog-content'
				dangerouslySetInnerHTML={{ __html: content }}
			/>

			<div className='row y-gap-20 justify-between pt-30'>
				<div className='col-auto'>
					<div className='d-flex items-center'>
						<div className='fw-500 mr-10'>Share this article</div>
						<div className='d-flex items-center'>
							<Social2 />
						</div>
					</div>
				</div>
				{/* End social share */}

				{/* <div className='col-auto'>
					<div className='row x-gap-10 y-gap-10'>
						{blogsData.slice(0, 3).map((item) => (
							<div key={nanoid(5)} className='col-auto'>
								<Link
									href={`/blog/article/${item.id}`}
									className='button -blue-1 py-5 px-20 bg-blue-1-05 rounded-100 text-15 fw-500 text-blue-1 text-capitalize'>
									{item.tag}
								</Link>
							</div>
						))}
					</div>
				</div> */}
				{/* End tags */}
			</div>
		</>
	);
};

export default DetailsContent;
