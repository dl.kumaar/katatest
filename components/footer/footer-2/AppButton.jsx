import Link from 'next/link';

const AppButton = () => {
	const appContent = [
		{
			id: 1,
			icon: 'icon-facebook',
			link: 'https://www.facebook.com/KataTravelsIndia',
			name: 'Facebook',
			colClass: '',
		},
		{
			id: 2,
			icon: 'icon-instagram',
			link: 'https://www.instagram.com/katatravel/',
			name: 'Instagram',
			colClass: 'mt-20',
		},
	];

	return (
		<>
			{appContent.map((item) => (
				<Link href={item?.link} key={item.id}>
					<div
						className={`d-flex items-center px-20 py-10 rounded-4 border-light  ${item.colClass}`}>
						<i className={`${item.icon} text-24`} />
						<div className='ml-20 d-block'>
							{/* <div className='text-14 text-white'>{item.text}</div> */}
							<div className='text-15 lh-1 fw-500'>{item.name}</div>
						</div>
					</div>
				</Link>
			))}
		</>
	);
};

export default AppButton;
