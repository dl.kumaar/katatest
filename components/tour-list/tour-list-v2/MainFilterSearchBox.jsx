import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useState } from 'react';
import DateSearch from '../common/DateSearch';
import LocationSearch from './LocationSearch';

const MainFilterSearchBox = () => {
	const [query, setQuery] = useState({});

	const router = useRouter();
	console.log('router', query);

	function handleSearch() {
		router.push({
			pathname: '/luxury-tours-vacation-packages',
			query: {
				...query,
			},
		});
	}

	return (
		<>
			<div className='col-12'>
				<LocationSearch setQuery={setQuery} />
				{/* End Location */}
			</div>
			{/* End .col-12 */}

			{/* <div className='col-12'>
				<div className='searchMenu-date px-20 py-10 bg-white rounded-4 -left js-form-dd js-calendar'>
					<div className='d-flex'>
						<i className='icon-calendar-2 text-20 text-light-1 mt-5'></i>
						<div className='ml-10 flex-grow-1'>
							<h4 className='text-15 fw-500 ls-2 lh-16'>
								Mes previsto del viaje{' '}
							</h4>
							<DateSearch />
						</div>
					</div>
				</div>
			</div> */}
			{/* End .col-12 */}

			<div className='col-12'>
				<div className='button-item h-full'>
					<button
						onClick={handleSearch}
						className='button -dark-1 py-15 px-40 h-full col-12 rounded-0 bg-blue-1 text-white'>
						<i className='icon-search text-20 mr-10' />
						Search
					</button>
				</div>
			</div>
			{/* End .col-12 */}
		</>
	);
};

export default MainFilterSearchBox;
