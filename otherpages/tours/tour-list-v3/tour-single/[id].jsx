import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper';
import ModalVideo from 'react-modal-video';
import 'photoswipe/dist/photoswipe.css';
import { Gallery, Item } from 'react-photoswipe-gallery';
import toursData from '../../data/tours';
import tourPackages from '../../data/tourPackages';
import Seo from '../../components/common/Seo';
import Header2 from '../../components/header/header-2';
import Overview from '../../components/tour-single/Overview';
import TourSnapShot from '../../components/tour-single/TourSnapShot';
import TopBreadCrumb from '../../components/tour-single/TopBreadCrumb';
import SidebarRight from '../../components/tour-single/SidebarRight';
import ReviewProgress2 from '../../components/tour-single/guest-reviews/ReviewProgress2';
import DetailsReview2 from '../../components/tour-single/guest-reviews/DetailsReview2';
import ReplyForm from '../../components/tour-single/ReplyForm';
import ReplyFormReview2 from '../../components/tour-single/ReplyFormReview2';
import CallToActions from '../../components/home/home-2/CallToActions';

import Footer2 from '../../components/footer/footer-3';

import Tours from '../../components/tours/Tours';
import Faq from '../../components/faq/Faq';
import Link from 'next/link';
import Itinerary from '../../components/tour-single/itinerary';
import ImportantInfo from '../../components/tour-single/ImportantInfo';
import Image from 'next/image';
import StickyHeader2 from '../../components/hotel-single/StickyHeader2';

const TourSingleV1Dynamic = () => {
	const [isOpen, setOpen] = useState(false);
	const router = useRouter();
	const [tour, setTour] = useState({});
	const id = router.query.id;

	const [tourPackage, setTourPackage] = useState({});

	useEffect(() => {
		if (!id) <h1>Loading...</h1>;
		else setTourPackage(tourPackages.find((item) => item.id == id));

		return () => {};
	}, [id]);

	useEffect(() => {
		if (!id) <h1>Loading...</h1>;
		else setTour(toursData.find((item) => item.id == id));

		return () => {};
	}, [id]);

	return (
		<>
			<ModalVideo
				channel='youtube'
				autoplay
				isOpen={isOpen}
				videoId='oqNZOOWF8qM'
				onClose={() => setOpen(false)}
			/>

			<Seo pageTitle='Tour Single' />
			{/* End Page Title */}

			<div className='header-margin'></div>
			{/* header top margin */}

			<Header2 darkNav={true} />
			{/* End Header 1 */}
			<StickyHeader2 price={tourPackage?.price?.tourPrice} />
			<section className='pt-40'>
				<div className='container'>
					<div className='row y-gap-20 justify-between items-end'>
						<div className='col-auto'>
							<h1 className='text-30 fw-600'>{tourPackage?.name}</h1>
							<div className='row x-gap-20 y-gap-20 items-center pt-10'>
								<div className='col-auto'>
									<div className='d-flex items-center'>
										<div className='d-flex x-gap-5 items-center'>
											<i className='icon-star text-10 text-yellow-1'></i>

											<i className='icon-star text-10 text-yellow-1'></i>

											<i className='icon-star text-10 text-yellow-1'></i>

											<i className='icon-star text-10 text-yellow-1'></i>

											<i className='icon-star text-10 text-yellow-1'></i>
										</div>

										<div className='text-14 text-light-1 ml-10'>
											{tour?.numberOfReviews} reviews
										</div>
									</div>
								</div>

								<div className='col-auto'>
									<div className='row x-gap-10 items-center'>
										<div className='col-auto'>
											<div className='d-flex x-gap-5 items-center'>
												<i className='icon-placeholder text-16 text-light-1'></i>
												<div className='text-15 text-light-1'>
													{tourPackage?.excerpt}
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						{/* End .col */}

						<div className='col-auto'>
							<div className='row x-gap-10 y-gap-10'>
								<div className='col-auto'>
									<button className='button px-15 py-10 -blue-1'>
										<i className='icon-share mr-10'></i>
										Compartir
									</button>
								</div>

								{/* <div className='col-auto'>
									<button className='button px-15 py-10 -blue-1 bg-light-2'>
										<i className='icon-heart mr-10'></i>
										Guardar
									</button>
								</div> */}
							</div>
						</div>
						{/* End .col */}
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>
			{/* End gallery grid wrapper */}

			<section className='pt-40 js-pin-container'>
				<div className='container'>
					<div className='row y-gap-30'>
						<div className='col-xl-8'>
							<div className='relative d-flex justify-center overflow-hidden js-section-slider'>
								<Swiper
									modules={[Navigation]}
									loop={true}
									navigation={{
										nextEl: '.js-img-next',
										prevEl: '.js-img-prev',
									}}>
									{tourPackage?.imageGallery?.map(({ url }) => (
										<SwiperSlide key={url}>
											<Image
												width={451}
												height={450}
												priority
												src={url}
												alt='image'
												style={{ height: '501px' }}
												className='rounded-4 col-12 cover object-cover'
											/>
										</SwiperSlide>
									))}
								</Swiper>

								<Gallery>
									{tourPackage?.imageGallery?.map(({ url }) => (
										<div
											className='absolute px-10 py-10 col-12 h-full d-flex justify-end items-end z-2 bottom-0 end-0'
											key={url}>
											<Item
												original={url}
												thumbnail={url}
												width={780}
												height={450}>
												{({ ref, open }) => (
													<div
														className='button -blue-1 px-24 py-15 bg-white text-dark-1 js-gallery'
														ref={ref}
														onClick={open}
														role='button'>
														Ver todas las fotos
													</div>
												)}
											</Item>
										</div>
									))}
								</Gallery>

								<div className='absolute h-full col-11'>
									<button className='section-slider-nav -prev flex-center button -blue-1 bg-white shadow-1 size-40 rounded-full sm:d-none js-img-prev'>
										<i className='icon icon-chevron-left text-12' />
									</button>
									<button className='section-slider-nav -next flex-center button -blue-1 bg-white shadow-1 size-40 rounded-full sm:d-none js-img-next'>
										<i className='icon icon-chevron-right text-12' />
									</button>
								</div>
								{/* End prev nav button wrapper */}
							</div>
							{/* End relative */}

							{/* slider gallery */}

							<h3 className='text-22 fw-500 mt-40' id='resumen'>
								Tour snapshot
							</h3>
							<TourSnapShot
								duration={tourPackage?.duration}
								tourType={tourPackage?.tourType}
							/>
							{/* End toursnapshot */}
							<div className='border-top-light mt-40 mb-40'></div>

							<Overview overview={tourPackage?.overview} />
							{/* End  Overview */}

							<div className='border-top-light mt-40 mb-40'></div>
							<h3 className='text-22 fw-500 mb-20' id='itinerario'>
								Itinerario
							</h3>
							<Itinerary itinerary={tourPackage?.itinerary} />
							{/* End Itinerary */}

							<div className='mt-40 border-top-light' id='inclusiones'>
								<div className='row x-gap-40 y-gap-40 pt-40'>
									<div className='col-12'>
										<h3 className='text-22 fw-500'>¿Qué incluye el paquete?</h3>

										<div className='row x-gap-40 y-gap-40 pt-20'>
											<div className='col-md-6'>
												<div className='text-dark-1 text-15'>
													<i className='icon-check text-10 mr-10'></i> Entry
													ticket to Harry Potter Warner Bros Studio Tour London
												</div>
												<div className='text-dark-1 text-15'>
													<i className='icon-check text-10 mr-10'></i> Return
													transfers in air-conditioned coach
												</div>
											</div>

											<div className='col-md-6'>
												<div className='text-dark-1 text-15'>
													<i className='icon-close text-green-2 text-10 mr-10'></i>{' '}
													Food and drinks
												</div>
												<div className='text-dark-1 text-15'>
													<i className='icon-close text-green-2 text-10 mr-10'></i>{' '}
													Gratuities
												</div>
												<div className='text-dark-1 text-15'>
													<i className='icon-close text-green-2 text-10 mr-10'></i>{' '}
													Digital guide available in 10 different languages at
													additional cost
												</div>
											</div>

											<div className='col-12'>
												<div className='py-15 px-20 rounded-4 text-15 bg-blue-1-05'>
													Estas son las inclusiones estándar de todos nuestros
													viajes. Sin embargo, algunas inclusiones pueden variar
													en caso de viajes personalizados, grupales y ofertas
													especiales. Si tiene alguna consulta especial, póngase
													en{' '}
													<a href='#' className='text-blue-1 fw-500'>
														contact
													</a>{' '}
													con nosotros.
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						{/* End .col-xl-8 */}

						<div className='col-xl-4'>
							<SidebarRight tour={tour} />
						</div>
						{/* End .col-xl-4 */}
					</div>
					{/* End .row */}
				</div>
				{/* End container */}
			</section>
			{/* End single page content */}

			{/* <section className='mt-40'>
				<div className='container '>
					<div className='pt-40 border-top-light'>
						<div className='row y-gap-20'>
							<div className='col-lg-4'>
								<h2 className='text-22 fw-500'>
									FAQs about
									<br /> The Crown Hotel
								</h2>
							</div>

							<div className='col-lg-8'>
								<div
									className='accordion -simple row y-gap-20 js-accordion'
									id='Faq1'>
									<Faq />
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> */}
			{/* End Faq about sections */}

			{/* <section className='mt-40 border-top-light pt-40'>
				<div className='container'>
					<div className='row y-gap-40 justify-between'>
						<div className='col-xl-3'>
							<h3 className='text-22 fw-500'>Guest reviews</h3>
							<ReviewProgress2 />
						</div>

						<div className='col-xl-8'>
							<DetailsReview2 />
						</div>
					</div>
				</div>
			</section> */}
			{/* End Review section */}
			{/* 
			<section className='mt-40 border-top-light pt-40'>
				<div className='container'>
					<div className='row y-gap-30 justify-between'>
						<div className='col-xl-3'>
							<div className='row'>
								<div className='col-auto'>
									<h3 className='text-22 fw-500'>Leave a Reply</h3>
									<p className='text-15 text-dark-1 mt-5'>
										Your email address will not be published.
									</p>
								</div>
							</div>

							<ReplyFormReview2 />
						</div>

						<div className='col-xl-8'>
							<ReplyForm />
						</div>
					</div>
				</div>
			</section> */}
			{/* End Reply Comment box section */}

			<section className='layout-pt-md layout-pb-lg mt-50 border-top-light'>
				<div className='container'>
					<div className='row y-gap-20 justify-between items-end'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>Viajes similares</h2>
								<p className=' sectionTitle__text mt-5 sm:mt-0'>
									Explore más viajes similares
								</p>
							</div>
						</div>

						<div className='col-auto'>
							<Link
								href='/tours'
								className='button -md -blue-1 bg-blue-1-05 text-blue-1'>
								Ver todos los tours
								<div className='icon-arrow-top-right ml-15' />
							</Link>
						</div>
					</div>

					<div className='row y-gap-30 pt-40 sm:pt-20 item_gap-x30'>
						<Tours />
					</div>
				</div>
			</section>
			{/* End Tours Sections */}

			<CallToActions />
			{/* End Call To Actions Section */}

			<Footer2 />
		</>
	);
};

export default dynamic(() => Promise.resolve(TourSingleV1Dynamic), {
	ssr: false,
});

export async function getStaticProps() {
	return {
		// returns the default 404 page with a status code of 404 in production
		// notFound: process.env.NODE_ENV === 'production',
		props: {
			a: 'a',
		},
	};
}
