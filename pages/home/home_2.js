import dynamic from 'next/dynamic';
import Blog4 from '../../components/blog/Blog4';
import Footer2 from '../../components/footer/footer-3';
import Header2 from '../../components/header/header-2';
import BlockGuide from '../../components/home/home-6/BlockGuide';
import Hero6 from '../../components/hero/hero-6';

import Subscribe from '../../components/home/home-2/Subscribe';
import Testimonial from '../../components/home/home-2/Testimonial';
import TestimonialRating from '../../components/home/home-2/TestimonialRating';
import FilterHotels from '../../components/hotels/FilterHotels';
import AddBanner from '../../components/home/home-3/AddBanner';
import Link from 'next/link';
import AboutIntro from '../../components/home/home-9/AboutIntro';
import WhyChoose from '../../components/home/home-9/WhyChoose';
import Travellers from '../../components/home/home-2/Travellers';

const Home_2 = ({ tours }) => {
	return (
		<>
			{/* <Header2 /> */}
			<Header2 />
			{/* End Header 2 */}

			{/* <Hero2 /> */}
			<Hero6 />
			{/* End Hero 6 */}

			<section className='layout-pt-md layout-pb-md bg-light-2'>
				<div className='container'>
					<div className='row mb-50 justify-center text-center'>
						<div className='sectionTitle -md '>
							<h1 className='sectionTitle__title'>
								Luxury Tailor-Made Travel to India & the Subcontinent
							</h1>
							<p className=' sectionTitle__text mt-5 sm:mt-0'>
								Crafting Extraordinary Journeys through India and the
								Subcontinent.
							</p>
						</div>
					</div>
				</div>
				<div className='container'>
					<div className='row y-gap-30'>
						<BlockGuide />
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>

			<section className='layout-pt-lg layout-pb-md'>
				<div className='container'>
					<div className='row y-gap-10 justify-between items-end'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>💥Special Offers</h2>
								<p className=' sectionTitle__text mt-5 sm:mt-0'>
									Bespoke luxury travel experiences to India and the captivating
									subcontinent
								</p>
							</div>
						</div>
					</div>
					{/* End .row */}
					<div className='row y-gap-20 pt-40'>
						<AddBanner />
					</div>
					{/* End .row */}
				</div>
				{/* End container */}
			</section>
			{/* End AddBanner Section */}

			<section className='layout-pt-md layout-pb-lg'>
				<div className='container'>
					<div className='row y-gap-10 justify-between items-end'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>
									Explore our most popular tours
								</h2>
								<p className=' sectionTitle__text mt-5 sm:mt-0'>
									Discover the best trips designed by local experts
								</p>
							</div>
						</div>
						{/* End .col-auto */}

						{/* <div className='col-auto tabs -pills-2 '>
							<FilterHotelsTabs />
						</div> */}
					</div>
					{/* End .row */}

					<div className='relative overflow-hidden pt-40 sm:pt-20'>
						<div className='row y-gap-30'>
							<FilterHotels tours={tours} />
						</div>
					</div>

					<div className='row justify-center pt-60'>
						<div className='col-auto'>
							<Link
								href='/luxury-tours-vacation-packages'
								className='button px-40 h-50 -outline-blue-1 text-blue-1'>
								Explore All Tours
								<div className='icon-arrow-top-right ml-15' />
							</Link>
						</div>
					</div>
					{/* End relative */}
				</div>
			</section>
			{/* End Best Seller Hotels Sections */}

			{/* <section className='layout-pb-lg'>
				<div className='container'>
					<div className='row y-gap-20 justify-between items-end'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>Popular destinations</h2>
								<p className=' sectionTitle__text mt-5 sm:mt-0'>
									Explore the best of India and the Subcontinent
								</p>
							</div>
						</div>

						<div className='col-auto'>
							<div className='d-flex x-gap-15 items-center justify-center pt-40 sm:pt-20'>
								<div className='col-auto'>
									<button className='d-flex items-center text-24 arrow-left-hover js-places-prev'>
										<i className='icon icon-arrow-left' />
									</button>
								</div>

								<div className='col-auto'>
									<div className='pagination -dots text-border js-places-pag' />
								</div>

								<div className='col-auto'>
									<button className='d-flex items-center text-24 arrow-right-hover js-places-next'>
										<i className='icon icon-arrow-right' />
									</button>
								</div>
							</div>
						</div>
					</div>

					<Travellers />
				</div>
			</section> */}
			{/* End Destinations Sections */}

			<AboutIntro />
			{/* About Intro Cruise  Sections */}

			<section className='layout-pt-lg layout-pb-lg'>
				<div className='container'>
					<div className='row y-gap-30'>
						<div className='col-xl-4 col-lg-5'>
							<h2 className='text-30 fw-600'>Why Choose Us</h2>
							<p className='mt-5'>
								Unforgettable Destinations Awaits Discerning Travelers
							</p>
							<p className='text-dark-1 mt-40 sm:mt-20'>
								We specialize in curating exceptional journeys that redefine the
								meaning of luxury, offering unparalleled comfort, opulence, and
								exclusivity. Immerse yourself in a world where every detail is
								meticulously crafted to exceed your expectations, as we elevate
								your travel experience to new heights of grandeur.
							</p>
							<div className='d-inline-block mt-40 sm:mt-20'>
								<Link
									href='about'
									className='button -md -blue-1 bg-yellow-1 text-dark-1'>
									About Us <div className='icon-arrow-top-right ml-15' />
								</Link>
							</div>
						</div>
						{/* End .col */}

						<div className='col-xl-6 offset-xl-1 col-lg-7'>
							<div className='row y-gap-60'>
								<WhyChoose />
							</div>
						</div>
						{/* End .col */}
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>
			{/* Why Choose  Sections */}
			{/* End whycosse Section */}

			<section className='layout-pt-lg layout-pb-lg bg-dark-3'>
				<div className='container'>
					<div className='row y-gap-60'>
						<div className='col-xl-5 col-lg-6'>
							<TestimonialRating />
						</div>
						{/* End .col */}

						<div className='col-xl-4 offset-xl-2 col-lg-5 offset-lg-1 col-md-10'>
							<Testimonial />
						</div>
						{/* End .col */}
					</div>
					{/* End .row */}
				</div>
			</section>
			{/* End testimonial and brand sections Section */}

			<section className='layout-pb-mg layout-pt-lg'>
				<div className='container'>
					<div className='row justify-center text-center'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>
									🌟✨ Get Inspired for your next trip <br />
									🌍🧳✈️ to India & the Subcontinent 🕌🐘🏞️🌴
								</h2>
							</div>
						</div>
					</div>
					{/* End .row  */}
					<div className='row y-gap-30 pt-40'>
						<Blog4 />

						<div className='row justify-center pt-60'>
							<div className='col-auto'>
								<Link
									href='/blog'
									className='button px-40 h-50 -outline-blue-1 text-blue-1'>
									Read More Articles
									<div className='icon-arrow-top-right ml-15' />
								</Link>
							</div>
						</div>
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>
			{/* End blog Section */}

			<Subscribe />
			{/* End Subscribe Section */}

			{/* <AppBanner /> */}
			{/* End AppBanner Section */}

			{/* <CallToActions /> */}
			{/* End CallToActions Section */}

			<Footer2 />
			{/* End Footer Section */}
		</>
	);
};

export default dynamic(() => Promise.resolve(Home_2), { ssr: false });
// export default Home_2;
