import Image from 'next/image';
import Link from 'next/link';

const AddBanner = () => {
	const addContent = [
		{
			id: 1,
			img: 'https://images.unsplash.com/photo-1635452324346-5c6458ab962e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
			title: (
				<>
					{' '}
					Group Tour - 15 days
					<br /> Diwali Fest 2023
				</>
			),
			meta: '15% OFF - India Group Tour 2023',
			routerPath:
				'/luxury-tours-vacation-packages/diwali-fest-2023-india-group-tour',
			delayAnimation: '100',
			btnText: 'See tour details',
		},
		{
			id: 2,
			img: 'https://images.unsplash.com/photo-1617184003170-1f266c325ff3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80',
			title: (
				<>
					Holi Fest 2024
					<br />
					Best of India - 13 days
				</>
			),
			meta: 'Early Bird - 20% OFF - Group Trip 2024',
			routerPath:
				'/luxury-tours-vacation-packages/india-group-tour-during-holi-fest-2024',
			delayAnimation: '200',
			btnText: 'See tour details',
		},
		{
			id: 3,
			img: '/img/backgrounds/2.png',
			title: 'Upto 40% discount on all tours to India',
			meta: 'Summer deals - Until September 30th',
			routerPath: '/plan-my-trip',
			delayAnimation: '300',
			btnText: 'Plan my trip',
		},
	];

	return (
		<>
			{addContent.map((item, index) => (
				<div
					className='col-lg-4 col-sm-6'
					data-aos='fade'
					data-aos-delay={item.delayAnimation}
					key={item.id}>
					<div className='ctaCard -type-1 rounded-4 '>
						<div className='ctaCard__image ratio ratio-41:45'>
							<Image
								width={410}
								height={455}
								className='js-lazy img-ratio'
								src={item.img}
								alt='image'
							/>
						</div>
						<div className={`ctaCard__content  py-50 px-50 lg:py-30 lg:px-30`}>
							{item?.meta ? (
								<>
									<div className='text-15 fw-500 text-white mb-10'>
										{item?.meta}
									</div>
								</>
							) : (
								''
							)}

							<h4 className='text-30 lg:text-24 text-white'>{item.title}</h4>
							{item.btnText ? (
								<div className='d-inline-block mt-30'>
									<Link
										href={item.routerPath}
										className='button px-48 py-15 -blue-1 -min-180 bg-white text-dark-1'>
										{item.btnText}
									</Link>
								</div>
							) : null}
						</div>
					</div>
				</div>
			))}
		</>
	);
};

export default AddBanner;
