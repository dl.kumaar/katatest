import Head from 'next/head';

const DefaultMeta = () => {
	return (
		<>
			<Head>
				<meta name='viewport' content='width=device-width, initial-scale=1' />
				<meta name='author' content='Kata Travels' />
				<meta name='publisher' content='https://katatravels.com' />
				<meta content='en' httpEquiv='content-language' />
			</Head>
		</>
	);
};

export default DefaultMeta;
