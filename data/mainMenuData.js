export const homeItems = [
	{
		name: 'Home 01',
		routePath: '/',
	},
	{
		name: 'Home 02',
		routePath: '/home/home_2',
	},
	{
		name: 'Home 03',
		routePath: '/home/home_3',
	},
	{
		name: 'Home 04',
		routePath: '/home/home_4',
	},
	{
		name: 'Home 05',
		routePath: '/home/home_5',
	},
	{
		name: 'Home 06',
		routePath: '/home/home_6',
	},
	{
		name: 'Home 07',
		routePath: '/home/home_7',
	},
	{
		name: 'Home 08',
		routePath: '/home/home_8',
	},
	{
		name: 'Home 09',
		routePath: '/home/home_9',
	},
	{
		name: 'Home 10',
		routePath: '/home/home_10',
	},
];
export const blogItems = [
	{
		name: 'Blog',
		routePath: '/blog',
	},
	{
		name: 'Blog List V1',
		routePath: '/blog/blog-list-v1',
	},
	{
		name: 'Blog List V2',
		routePath: '/blog/blog-list-v2',
	},
	{
		name: 'Blog Single',
		routePath: '/blog/article/1',
	},
];
export const pageItems = [
	{
		name: '404',
		routePath: '/404',
	},
	{
		name: 'Tours',
		routePath: '/luxury-tours-vacation-packages',
	},
	{
		name: 'Nosotros',
		routePath: '/nosotros',
	},
	{
		name: 'Become Expert',
		routePath: '/others-pages/become-expert',
	},
	{
		name: 'Help Center',
		routePath: '/others-pages/help-center',
	},
	{
		name: 'Login',
		routePath: '/others-pages/login',
	},
	{
		name: 'Register',
		routePath: '/others-pages/signup',
	},
	{
		name: 'Terms',
		routePath: '/others-pages/terms',
	},
	{
		name: 'Invoice',
		routePath: '/others-pages/invoice',
	},
	{
		name: 'UI Elements',
		routePath: '/others-pages/ui-elements',
	},
];
export const dashboardItems = [
	{
		name: 'Dashboard',
		routePath: '/dashboard/db-dashboard',
	},
	{
		name: 'Booking History',
		routePath: '/dashboard/db-booking',
	},
	{
		name: 'Wishlist',
		routePath: '/dashboard/db-wishlist',
	},
	{
		name: 'Settings',
		routePath: '/dashboard/db-settings',
	},
	{
		name: 'Vendor Dashboard',
		routePath: '/vendor-dashboard/dashboard',
	},
	{
		name: 'Vendor Add Hotel',
		routePath: '/vendor-dashboard/add-hotel',
	},
	{
		name: 'Vendor Booking',
		routePath: '/vendor-dashboard/booking',
	},
	{
		name: 'Vendor Hotels',
		routePath: '/vendor-dashboard/hotels',
	},
	{
		name: 'Vendor Recovery',
		routePath: '/vendor-dashboard/recovery',
	},
	{
		name: 'Logout',
		routePath: '/others-pages/login',
	},
];

export const categorieMegaMenuItems = [
	{
		id: 1,
		menuCol: [
			{
				id: 1,
				megaBanner:
					'https://images.unsplash.com/photo-1635452324346-5c6458ab962e?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
				title: 'Diwali Fest Nov 23 India Group Tour',
				btnText: 'View Trip',
				btnRoute:
					'/luxury-tours-vacation-packages/diwali-fest-2023-india-group-tour',
				menuItems: [
					{
						id: 1,
						menuList: [
							{
								name: 'India Travel Guide',
								routePath: '/travel-guide/india',
								type: 'destination',
							},
							{
								name: 'Best time to visit India',
								routePath:
									'/blog/article/the-best-time-to-visit-india-a-guide-to-planning-your-trip',
							},
							{
								name: 'Visitng India for the first time',
								routePath:
									'/blog/article/visiting-india-for-the-first-time-a-comprehensive-guide',
							},
							{
								name: 'Visa for India',
								routePath:
									'/blog/article/a-comprehensive-guide-to-visa-for-india',
							},
							{
								name: "India's Golden Triangle",
								routePath:
									'/blog/article/exploring-indias-golden-triangle-delhi-agra-fatehpur-sikri-and-jaipur',
							},
							{
								name: 'Visitng Rajasthan',
								routePath:
									'/blog/article/discovering-rajasthan-a-journey-through-vibrant-culture-and-majestic-heritage',
							},
							{
								name: 'Visitng Kerala',
								routePath:
									'/blog/article/exploring-the-serene-beauty-of-kerala-a-blissful-journey-through-gods-own-country',
							},
							{
								name: 'Holi Festival in India',
								routePath:
									'/blog/article/holi-festival-in-india-a-colorful-celebration-of-joy-and-unity',
							},
							{
								name: 'Festival of Lights, Diwali',
								routePath:
									'/blog/article/diwali-the-festival-of-lights-and-joyous-celebrations-in-india',
							},
						],
					},
					{
						id: 2,
						menuList: [
							{
								name: 'Tiger Safari in India',
								routePath:
									'/blog/article/embarking-on-an-unforgettable-tiger-safari-adventure-in-india',
							},
							{
								name: 'Festivals of India',
								routePath:
									'/blog/article/festivals-in-india-a-mosaic-of-culture-tradition-and-celebration',
							},
							{
								name: 'Honeymoon Tours to India',
								routePath:
									'/blog/article/honeymoon-tours-to-india-unforgettable-romantic-experiences',
							},
							{
								name: 'Family Holidays in India',
								routePath:
									'/blog/article/family-holiday-trips-to-india-unforgettable-adventures-with-kata-travels',
							},
							{
								name: 'Luxury Vacation Pacakges',
								routePath:
									'/blog/article/luxury-tours-and-vacation-packages-to-india-unparalleled-experiences-with-kata-travels',
							},
							{
								name: 'Yoga & Wellness Tours',
								routePath:
									'/blog/article/yoga-and-wellness-tours-in-india-embark-on-a-journey-of-inner-transformation',
							},
							{
								name: 'Nepal Tours & Treks',
								routePath: '/travel-guide/nepal',
								type: 'destination',
							},
							{
								name: 'Luxury Vacation in Maldives',
								routePath: '/travel-guide/maldives',
								type: 'destination',
							},
						],
					},
				],
			},
		],
	},
	{
		id: 2,
		menuCol: [
			{
				id: 1,
				megaBanner: '/img/backgrounds/8.png',
				title: 'Things to do on your tour',
				btnText: 'See Tour',
				btnRoute: '/tour/tour-list-v1',
				menuItems: [
					{
						id: 1,
						title: 'Tour List',
						menuList: [
							{
								name: 'Best time to visit India',
								routePath: '/tour/tour-list-v1',
							},
							{
								name: 'Tour List v2',
								routePath: '/tour/tour-list-v2',
							},
						],
					},
					{
						id: 2,
						title: 'Tour Pages',
						menuList: [
							{
								name: 'Tour Map',
								routePath: '/tour/tour-list-v3',
							},
							{
								name: 'Tour Single',
								routePath: '/tour/tour-single/5',
							},
						],
					},
				],
			},
		],
	},
	{
		id: 3,
		menuCol: [
			{
				id: 1,
				megaBanner: '/img/backgrounds/9.png',
				title: 'Things to do on your activity',
				btnText: 'See Activity',
				btnRoute: '/activity/activity-list-v1',
				menuItems: [
					{
						id: 1,
						title: 'Activity List',
						menuList: [
							{
								name: 'Activity List v1',
								routePath: '/activity/activity-list-v1',
							},
							{
								name: 'Activity List v2',
								routePath: '/activity/activity-list-v2',
							},
						],
					},
					{
						id: 2,
						title: 'Activity Pages',
						menuList: [
							{
								name: 'Activity Map',
								routePath: '/activity/activity-list-v3',
							},
							{
								name: 'Activity Single',
								routePath: 'activity-single',
								routePath: '/activity/activity-single/3',
							},
						],
					},
				],
			},
		],
	},
	{
		id: 4,
		menuCol: [
			{
				id: 1,
				megaBanner: '/img/backgrounds/10.png',
				title: 'Things to do on your rentals',
				btnText: 'See Rental',
				btnRoute: '/rental/rental-list-v1',
				menuItems: [
					{
						id: 1,
						title: 'Rental List',
						menuList: [
							{
								name: 'Rental List v1',
								routePath: '/rental/rental-list-v1',
							},
							{
								name: 'Rental List v2',
								routePath: '/rental/rental-list-v2',
							},
						],
					},
					{
						id: 2,
						title: 'Rental Pages',
						menuList: [
							{
								name: 'Rental Map',
								routePath: '/rental/rental-list-v3',
							},
							{
								name: 'Rental Single',
								routePath: '/rental/rental-single/3',
							},
						],
					},
				],
			},
		],
	},
	{
		id: 5,
		menuCol: [
			{
				id: 1,
				megaBanner: '/img/backgrounds/5.png',
				title: 'Things to do on your Next Car',
				btnText: 'See Car',
				btnRoute: '/car/car-list-v1',
				menuItems: [
					{
						id: 1,
						title: 'Car List',
						menuList: [
							{
								name: 'Car List v1',
								routePath: '/car/car-list-v1',
							},
							{
								name: 'Car List v2',
								routePath: '/car/car-list-v2',
							},
						],
					},
					{
						id: 2,
						title: 'Car Pages',
						menuList: [
							{
								name: 'Car Map',
								routePath: '/car/car-list-v3',
							},
							{
								name: 'Car Single',
								routePath: '/car/car-single/1',
								routePath: '/car/car-single/1',
							},
						],
					},
				],
			},
		],
	},
	{
		id: 6,
		menuCol: [
			{
				id: 1,
				megaBanner: '/img/backgrounds/1.png',
				title: 'Things to do on your Cruise',
				btnText: 'See Cruise',
				btnRoute: '/cruise/cruise-list-v1',
				menuItems: [
					{
						id: 1,
						title: 'Cruise List',
						menuList: [
							{
								name: 'Cruise List v1',
								routePath: '/cruise/cruise-list-v1',
							},
							{
								name: 'Cruise List v2',
								routePath: '/cruise/cruise-list-v2',
							},
						],
					},
					{
						id: 2,
						title: 'Cruise Pages',
						menuList: [
							{
								name: 'Cruise Map',
								routePath: '/cruise/cruise-list-v3',
							},
							{
								name: 'Cruise Single',
								routePath: '/cruise/cruise-single/3',
							},
						],
					},
				],
			},
		],
	},
	{
		id: 7,
		menuCol: [
			{
				id: 1,
				megaBanner: '/img/backgrounds/2.png',
				title: 'Things to do on your flights',
				btnText: 'See Flights',
				btnRoute: '/flight/flight-list-v1',
				menuItems: [
					{
						id: 1,
						title: 'Flight List',
						menuList: [
							{
								name: 'Flight List v1',
								routePath: '/flight/flight-list-v1',
							},
						],
					},
				],
			},
		],
	},
];

export const categorieMobileItems = [
	{
		id: 1,
		title: 'Hotel',
		menuItems: [
			{
				id: 1,
				title: 'Hotel List',
				menuList: [
					{
						name: 'Hotel List v1',
						routePath: '/hotel/hotel-list-v1',
					},
					{
						name: 'Hotel List v2',
						routePath: '/hotel/hotel-list-v2',
					},
					{
						name: 'Hotel List v3',
						routePath: '/hotel/hotel-list-v3',
					},
					{
						name: 'Hotel List v4',
						routePath: '/hotel/hotel-list-v4',
					},
					{
						name: 'Hotel List v5',
						routePath: '/hotel/hotel-list-v5',
					},
				],
			},
			{
				id: 2,
				title: 'Hotel Single',
				menuList: [
					{
						name: 'Hotel Single v1',
						routePath: '/hotel/hotel-single-v1/5',
					},
					{
						name: 'Hotel Single v2',
						routePath: '/hotel/hotel-single-v2/5',
					},
				],
			},
			{
				id: 3,
				title: 'Hotel Booking',
				menuList: [
					{
						name: 'Booking Page',
						routePath: '/hotel/booking-page',
					},
				],
			},
		],
	},
	{
		id: 2,
		title: 'Tour',
		menuItems: [
			{
				id: 1,
				title: 'Tour List',
				menuList: [
					{
						name: 'Tour List v1',
						routePath: '/tour/tour-list-v1',
					},
					{
						name: 'Tour List v2',
						routePath: '/tour/tour-list-v2',
					},
				],
			},
			{
				id: 2,
				title: 'Tour Pages',
				menuList: [
					{
						name: 'Tour Map',
						routePath: '/tour/tour-list-v3',
					},
					{
						name: 'Tour Single',
						routePath: '/tour/tour-single/5',
					},
				],
			},
		],
	},
	{
		id: 3,
		title: 'Activity',
		menuItems: [
			{
				id: 1,
				title: 'Activity List',
				menuList: [
					{
						name: 'Activity List v1',
						routePath: '/activity/activity-list-v1',
					},
					{
						name: 'Activity List v2',
						routePath: '/activity/activity-list-v2',
					},
				],
			},
			{
				id: 2,
				title: 'Activity Pages',
				menuList: [
					{
						name: 'Activity Map',
						routePath: '/activity/activity-list-v3',
					},
					{
						name: 'Activity Single',
						routePath: '/activity/activity-single/3',
					},
				],
			},
		],
	},
	{
		id: 4,
		title: 'Hotel Rentals',
		menuItems: [
			{
				id: 1,
				title: 'Rental List',
				menuList: [
					{
						name: 'Rental List v1',
						routePath: '/rental/rental-list-v1',
					},
					{
						name: 'Rental List v2',
						routePath: '/rental/rental-list-v2',
					},
				],
			},
			{
				id: 2,
				title: 'Rental Pages',
				menuList: [
					{
						name: 'Rental Map',
						routePath: '/rental/rental-list-v3',
					},
					{
						name: 'Rental Single',
						routePath: '/rental/rental-single/3',
					},
				],
			},
		],
	},
	{
		id: 5,
		title: 'Car',
		menuItems: [
			{
				id: 1,
				title: 'Car List',
				menuList: [
					{
						name: 'Car List v1',
						routePath: '/car/car-list-v1',
					},
					{
						name: 'Car List v2',
						routePath: '/car/car-list-v2',
					},
				],
			},
			{
				id: 2,
				title: 'Car Pages',
				menuList: [
					{
						name: 'Car Map',
						routePath: '/car/car-list-v3',
					},
					{
						name: 'Car Single',
						routePath: '/car/car-single/1',
					},
				],
			},
		],
	},
	{
		id: 6,
		title: 'Cruise',
		menuItems: [
			{
				id: 1,
				title: 'Cruise List',
				menuList: [
					{
						name: 'Cruise List v1',
						routePath: '/cruise/cruise-list-v1',
					},
					{
						name: 'Cruise List v2',
						routePath: '/cruise/cruise-list-v2',
					},
				],
			},
			{
				id: 2,
				title: 'Cruise Pages',
				menuList: [
					{
						name: 'Cruise Map',
						routePath: '/cruise/cruise-list-v3',
					},
					{
						name: 'Cruise Single',
						routePath: '/cruise/cruise-single/3',
					},
				],
			},
		],
	},
	{
		id: 7,
		title: 'Flights',
		menuItems: [
			{
				id: 1,
				title: 'Flight List',
				menuList: [
					{
						name: 'Flight List v1',
						routePath: '/flight/flight-list-v1',
					},
				],
			},
		],
	},
];

export const destinations = [
	{
		name: 'India',
		routePath: '/travel-guide/india',
	},
	{
		name: 'Nepal',
		routePath: '/travel-guide/nepal',
	},
	{
		name: 'Maldives',
		routePath: '/travel-guide/maldives',
	},
];
