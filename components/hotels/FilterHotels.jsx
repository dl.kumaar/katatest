import Image from 'next/image';
import Link from 'next/link';
import Slider from 'react-slick';
import isTextMatched from '../../utils/isTextMatched';

import { urlFor } from '../../utils/sanityCalient';
import { nanoid } from 'nanoid';
import { useRouter } from 'next/router';

import { formatDate } from '../../utils/blogUtils';

function changeMonthNameinSpanish(date) {
	const monthNames = [
		'Enero',
		'Febrero',
		'Marzo',
		'Abril',
		'Mayo',
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];

	const day = date.getDate();
	const monthIndex = date.getMonth();
	const year = date.getFullYear();

	return `${day} de ${monthNames[monthIndex]} del ${year}`;
}

// get a number between
export function randomIntFromInterval(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

export function riboonColor(tag = '') {
	if (tag?.toLowerCase() === 'hot tour') {
		return 'bg-yellow-1 text-dark-1';
	}
	if (tag === 'Últimos lugares' || tag === 'más vendido') {
		return 'bg-red-2 text-white';
	}

	if (tag?.toLowerCase() === '-25% today') {
		return 'bg-brown-1 text-white';
	}

	if (tag?.toLowerCase() === 'breakfast included') {
		return 'bg-dark-1 text-white';
	}

	return 'bg-dark-1 text-white';
}

const FilterHotels = ({ tours }) => {
	const router = useRouter();

	var itemSettings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
	};

	function ArrowSlick(props) {
		let className =
			props.type === 'next'
				? 'slick_arrow-between slick_arrow -next arrow-md flex-center button -blue-1 bg-white shadow-1 size-30 rounded-full sm:d-none js-next'
				: 'slick_arrow-between slick_arrow -prev arrow-md flex-center button -blue-1 bg-white shadow-1 size-30 rounded-full sm:d-none js-prev';
		className += ' arrow';
		const char =
			props.type === 'next' ? (
				<>
					<i className='icon icon-chevron-right text-12'></i>
				</>
			) : (
				<>
					<span className='icon icon-chevron-left text-12'></span>
				</>
			);
		return (
			<button className={className} onClick={props.onClick}>
				{char}
			</button>
		);
	}

	return (
		<>
			{tours.map((item, index) => (
				<div
					className={`${
						!router.pathname.includes('tours')
							? 'col-xl-3 col-lg-3 col-sm-6'
							: 'col-xl-4 col-lg-4 col-sm-6'
					}  `}
					key={nanoid(8)}
					data-aos='fade'
					data-aos-delay={item?.delayAnimation || '100'}>
					<Link
						href={`/luxury-tours-vacation-packages/${item?.slug?.current}`}
						className='tourCard -type-1 rounded-4 hover-inside-slider'>
						<div className='tourCard__image'>
							<div className='cardImage inside-slider'>
								<Slider
									{...itemSettings}
									arrows={true}
									nextArrow={<ArrowSlick type='next' />}
									prevArrow={<ArrowSlick type='prev' />}>
									{[item.tourImage].map((slide, i) => (
										<div className='cardImage ratio ratio-1:1' key={nanoid(8)}>
											<div className='cardImage__content '>
												<Image
													width={300}
													height={300}
													className='rounded-4 col-12 js-lazy'
													src={urlFor(slide).width(300).height(300).url()}
													alt={`Image for the tour ${item?.tourName}`}
												/>
											</div>
										</div>
									))}
								</Slider>

								{/* <div className='cardImage__wishlist'>
									<button className='button -blue-1 bg-white size-30 rounded-full shadow-2'>
										<i className='icon-heart text-12' />
									</button>
								</div> */}

								{item?.tourPromoText1 && (
									<div className='cardImage__leftBadge'>
										<div
											className={`py-5 px-15 rounded-right-4 text-12 lh-16 fw-500 uppercase ${riboonColor(
												item.tourPromoText1
											)}`}>
											{item.tourPromoText1}
										</div>
									</div>
								)}

								{item?.tourType !== 'private' ? (
									<div className='cardImage__leftBadge mt-40'>
										<div
											className={`py-5 px-15 rounded-right-4 text-12 lh-16 fw-500 uppercase ${
												isTextMatched(item?.tag, 'breakfast included')
													? 'bg-dark-1 text-white'
													: ''
											} ${
												!isTextMatched(item?.tag, 'best seller')
													? 'bg-blue-1 text-white'
													: ''
											} 
                    }`}>
											Group Tour
										</div>
									</div>
								) : null}
							</div>
						</div>

						<div className='tourCard__content mt-10'>
							<div className='d-flex items-center lh-14 mb-5'>
								{item?.tourDuration && (
									<>
										<div className='text-14 text-light-1'>
											{item?.tourDuration} days
										</div>
										<div className='size-3 bg-light-1 rounded-full ml-10 mr-10' />
									</>
								)}

								<div className='text-14 text-light-1'>
									{capitalizeFirstLetter(item?.tourType) +
										(item?.tourType === 'private' ? ' trip' : ' tour')}
								</div>
								{item?.tourStartDate ? (
									<>
										<div className='size-3 bg-light-1 rounded-full ml-10 mr-10' />
										<div className='text-15 text-bold-1'>
											{formatDate(item?.tourStartDate)}
										</div>
									</>
								) : null}
							</div>
							<h4 className='tourCard__title text-dark-1 text-18 lh-16 fw-500'>
								<span>{item?.tourName}</span>
							</h4>
							<p className='text-light-1 lh-14 text-14 mt-5'>
								{item?.tourExcerpt[0]?.children[0]?.text || ''}
							</p>

							<div className='row justify-between items-center pt-15'>
								<div className='col-auto'>
									<div className='d-flex items-center'>
										<div className='d-flex items-center x-gap-5'>
											<div className='icon-star text-yellow-1 text-10' />
											<div className='icon-star text-yellow-1 text-10' />
											<div className='icon-star text-yellow-1 text-10' />
											<div className='icon-star text-yellow-1 text-10' />
											<div className='icon-star text-yellow-1 text-10' />
										</div>
										{/* End ratings */}

										<div className='text-14 text-light-1 ml-10'>
											{item?.numberOfReviews || randomIntFromInterval(35, 340)}{' '}
											reviews
										</div>
									</div>
								</div>

								{item?.priceOffSeason ? (
									<div className='col-auto'>
										<div className='text-14 text-light-1'>
											From
											<span className='text-16 fw-500 text-dark-1'>
												{' '}
												US${item?.priceOffSeason[0]?.tourPrice}
											</span>
										</div>
									</div>
								) : null}
							</div>
						</div>
					</Link>
				</div>
			))}
		</>
	);
};

export default FilterHotels;

export function capitalizeFirstLetter(string) {
	return string?.charAt(0).toUpperCase() + string?.slice(1);
}
