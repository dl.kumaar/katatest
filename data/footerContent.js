module.exports = [
	{
		id: 1,
		title: 'Company',
		menuList: [
			{ name: 'About', routerPath: '/about' },
			{ name: 'Blog', routerPath: '/blog' },
			{ name: 'Careers', routerPath: '#' },
			{ name: 'Payment', routerPath: '#' },
		],
	},
	{
		id: 2,
		title: 'Support',
		menuList: [
			{ name: 'Contact', routerPath: '/contact' },
			{ name: 'Plan My Trip', routerPath: '/plan-my-trip' },
			{ name: 'Privacy Policy', routerPath: '/terms' },
			{ name: 'Terms & Conditions', routerPath: '/terms' },
			{ name: 'Sitemap', routerPath: '/sitemap.xml' },
		],
	},
	// {
	// 	id: 3,
	// 	title: 'Other Services',
	// 	menuList: [
	// 		{ name: 'Car hire', routerPath: '/' },
	// 		{ name: 'Activity Finder', routerPath: '/' },
	// 		{ name: 'Tour List', routerPath: '/' },
	// 		{ name: 'Flight finder', routerPath: '/' },
	// 		{ name: 'Cruise Ticket', routerPath: '/' },
	// 		{ name: 'Holiday Rental', routerPath: '/' },
	// 		{ name: 'Travel Agents', routerPath: '/' },
	// 	],
	// },
];
