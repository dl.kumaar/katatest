import Wrapper from '../layout/wrapper';

import { apIClient } from '../../utils/sanityCalient';
import TourPage from './TourPage';
import { NextSeo } from 'next-seo';

const MainRoot = (props) => {
	const { tours } = props;

	return (
		<Wrapper>
			<NextSeo
				title='Luxury Tours & Vacation Packages - Kata Travels'
				description='Luxry Tours & Vacation Packages to India and the subcontinent - Private and Group Tours to India. Travel Agency in India with offices in the United States and Argentina.'
				canonical='https://katatravels.com/luxury-tours-vacation-packages'
				openGraph={{
					url: 'https://katatravels.com/luxury-tours-vacation-packages',
					title: 'Luxury',
					description:
						'Luxry Tours & Vacation Packages to India and the subcontinent - Private and Group Tours to India. Travel Agency in India with offices in the United States and Argentina.',
					images: [
						{
							url: 'https://katatravels.com/img/fb-hero.jpg',
							width: 1740,
							height: 960,
							alt: 'Kata Travels - Viajes a la India',
						},
					],
					siteName: 'Kata Travels',
					type: 'website',
					locale: 'en',
				}}
			/>

			<TourPage tours={tours} />
		</Wrapper>
	);
};

export default MainRoot;

export async function getStaticProps() {
	const query = `*[_type == 'tours' && tags[0] == "en"] {
			tourName,
			tourExcerpt,
			tourPrice,
			tourType,
			tourDuration,
			price,
			priceOffSeason,
			priceSeason,
			slug,
			tourImage,
			tourStartDate,
			tourImageGallery,
			tourPromoText1
	}`;

	const tours = await apIClient.fetch(query);

	return {
		props: {
			tours,
		},
		revalidate: 10,
	};
}
