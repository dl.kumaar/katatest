/* eslint-disable @next/next/next-script-for-ga */
import { Html, Head, Main, NextScript } from 'next/document';

export default function Document() {
	return (
		<Html lang='en'>
			<Head>
				{/* <!-- Google fonts --> */}
				<link rel='preconnect' href='https://fonts.googleapis.com' />
				<link
					rel='preconnect'
					href='https://fonts.gstatic.com'
					crossOrigin='true'
				/>
				<link
					rel='stylesheet'
					href='https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'
				/>
				<link
					href='https://fonts.googleapis.com/css2?family=Jost:wght@400;500;600&display=swap'
					rel='stylesheet'
				/>

				<link rel='icon' href='/favicon.ico' />

				{/* Google Analytics Tags */}
				{process.env.NODE_ENV !== 'development' && (
					<>
						<script
							type='text/javascript'
							async
							dangerouslySetInnerHTML={{
								__html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
								new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
								j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
								'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
								})(window,document,'script','dataLayer','GTM-M542HVW');`,
							}}
						/>
					</>
				)}
			</Head>
			<body>
				<noscript>
					<iframe
						src='https://www.googletagmanager.com/ns.html?id=GTM-M542HVW'
						height='0'
						width='0'
						style={{
							display: 'none',
							visibility: 'hidden',
						}}></iframe>
				</noscript>

				<Main />
				<NextScript />
			</body>
		</Html>
	);
}
