import React, { useState } from 'react';
import DatePicker, { DateObject } from 'react-multi-date-picker';
import Toolbar from 'react-multi-date-picker/plugins/toolbar';

const DateSearch = ({ dates, setDates }) => {
	// const [dates, setDates] = useState([
	//   new DateObject({ year: 2023, month: 1, day: 22 }),
	//   "December 09 2020",
	//   1597994736000, //unix time in milliseconds (August 21 2020)
	// ]);

	// const [dates2, setDates2] = useState(new Date());

	return (
		<div className='text-15 text-light-1 ls-2 lh-16 custom_dual_datepicker'>
			{/* <DatePicker
				inputClass='custom_input-picker'
				containerClassName='custom_container-picker'
				value={dates}
				onChange={setDates}
				numberOfMonths={2}
				offsetY={10}
				range
				rangeHover
				format='MMMM DD'
				minDate={new DateObject().add(1, 'day')}
				maxDate={new DateObject().add(1, 'year')}
				plugins={[
					<Toolbar
						key={'toolbar'}
						names={{ today: 'Hoy', deselect: 'Limpiar', close: 'Cerrar' }}
						position='bottom'
					/>,
				]}
			/> */}

			{/* Single date and month picker */}
			<DatePicker
				inputClass='custom_input-picker'
				containerClassName='custom_container-picker'
				value={dates}
				onChange={setDates}
				minDate={new DateObject().add(1, 'day')}
				maxDate={new DateObject().add(1, 'year')}
			/>
		</div>
	);
};

export default DateSearch;
