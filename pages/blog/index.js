import React, { useState } from 'react';
import Seo from '../../components/common/Seo';
import Footer2 from '../../components/footer/footer-3';
import Blog2 from '../../components/blog/Blog2';
import BlogSidebar from '../../components/blog/blog-sidebar';
import Image from 'next/image';
import Header1 from '../../components/header/header-2';
import blogsData from '../../data/blogs';

const BlogListV2 = ({ blogs }) => {
	return (
		<>
			<Seo pageTitle='Blog - Kata Travels' />
			{/* End Page Title */}

			<Header1 />
			{/* End Header 1 */}

			<section className='section-bg layout-pt-lg layout-pb-lg'>
				<div className='section-bg__item col-12'>
					<Image
						width={1920}
						height={400}
						src='/img/pages/about/1.png'
						alt='image'
						priority
					/>
				</div>
				{/* End section-bg__item */}

				<div className='container'>
					<div className='row justify-center text-center'>
						<div className='col-xl-6 col-lg-8 col-md-10'>
							<h1 className='text-40 md:text-25 fw-600 text-white'>
								Travel Blog,
								<br />
								Tips & Guides
							</h1>
						</div>
					</div>
				</div>
				{/* End .container */}
			</section>
			{/* End About Banner Section */}

			<section className='layout-pt-md'>
				<div className='container'>
					<div className='row'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>Travel Articles & Guide</h2>
								<p className=' sectionTitle__text mt-5 sm:mt-0'>
									From our team of travel enthusiasts who love to share our
									experiences and knowledge with you.
								</p>
							</div>
						</div>
					</div>
				</div>
			</section>
			{/* End title */}

			<section className='layout-pt-md layout-pb-lg'>
				<div className='container'>
					<div className='row y-gap-30 justify-between'>
						<div className='col-xl-8'>
							<div className='row y-gap-30'>
								<Blog2 blogs={blogs} />
							</div>
							{/* End .row */}
							{/* <BlogPagination /> */}
						</div>
						{/* End .col */}

						{/* <div className='col-xl-3'>
							<BlogSidebar />
						</div> */}
						{/* End .col */}
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>

			<Footer2 />
			{/* End Footer Section */}
		</>
	);
};

export default BlogListV2;

export async function getStaticProps() {
	const blogs = blogsData;
	return {
		props: {
			blogs,
		},
	};
}
