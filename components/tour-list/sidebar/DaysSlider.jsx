import { useState } from 'react';
import InputRange from 'react-input-range';

const DaysSlider = () => {
	const [days, setDays] = useState({
		value: { min: 1, max: 30 },
	});

	const handleOnChange = (value) => {
		setDays({ value });
	};

	return (
		<div className='js-price-rangeSlider'>
			<div className='text-14 fw-500'></div>

			<div className='d-flex justify-between mb-20'>
				<div className='text-15 text-dark-1'>
					<span className='js-lower mx-1'>{days.value.min} días</span>-
					<span className='js-upper mx-1'>
						{days.value.max}
						{days.value.max === 30 ? '+' : ''} días
					</span>
				</div>
			</div>

			<div className='px-5'>
				<InputRange
					formatLabel={(value) => ``}
					minValue={0}
					maxValue={30}
					value={days.value}
					onChange={(value) => handleOnChange(value)}
				/>
			</div>
		</div>
	);
};

export default DaysSlider;
