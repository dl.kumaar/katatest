import React, { useEffect, useState } from 'react';
import CallToActions from '../../../components/home/home-2/CallToActions';
import Seo from '../../../components/common/Seo';
import Footer2 from '../../../components/footer/footer-3';
import RelatedBlog from '../../../components/blog/blog-details/RelatedBlog';
import blogsData from '../../../data/blogs';
import { useRouter } from 'next/router';
import DetailsContent from '../../../components/blog/blog-details/DetailsContent';
import BlogNavigator from '../../../components/blog/blog-details/BlogNavigator';
import Header1 from '../../../components/header/header-2';
import Image from 'next/image';
import Subscribe from '../../../components/home/home-2/Subscribe';
import { formatDate } from '../../../utils/blogUtils';
import Link from 'next/link';
import DefaultMeta from '../../../components/common/DefaultMeta';
import { NextSeo } from 'next-seo';

const bgColors = [
	{
		name: 'black',
		color: '#000000',
	},
	{
		name: 'border',
		color: '#dddddd',
	},
	{
		name: 'dark-1',
		color: '#051036',
	},
	{
		name: 'dark-2',
		color: '#0d2857',
	},
	{
		name: 'dark-3',
		color: '#13357b',
	},
	{
		name: 'dark-4',
		color: '#163c8c',
	},
	{
		name: 'blue-1',
		color: '#3554d1',
	},
	{
		name: 'blue-2',
		color: '#e5f0fd',
	},
	{
		name: 'green-1',
		color: '#ebfcea',
	},
	{
		name: 'green-2',
		color: '#008009',
	},
	{
		name: 'yellow-1',
		color: '#f8d448',
	},
	{
		name: 'yellow-2',
		color: '#e1c03f',
	},
	{
		name: 'yellow-3',
		color: '#ffc700',
	},
	{
		name: 'brown-1',
		color: '#923e01',
	},
	{
		name: 'purple-1',
		color: '#7e53f9',
	},
	{
		name: 'red-1',
		color: '#d93025',
	},
	{
		name: 'red-2',
		color: '#f1416c',
	},
	{
		name: 'red-3',
		color: '#fff5f8',
	},
	{
		name: 'info-1',
		color: '#cde9f6',
	},
	{
		name: 'info-2',
		color: '#4780aa',
	},
	{
		name: 'warning-1',
		color: '#f7f3d7',
	},
	{
		name: 'warning-2',
		color: '#927238',
	},
	{
		name: 'error-1',
		color: '#ecc8c5',
	},
	{
		name: 'error-2',
		color: '#ab3331',
	},
	{
		name: 'success-1',
		color: '#def2d7',
	},
	{
		name: 'success-2',
		color: '#5b7052',
	},
];

const BlogSingleDynamic = ({ blog }) => {
	function getRandomColor() {
		return bgColors[Math.floor(Math.random() * bgColors.length)];
	}

	return (
		<>
			<DefaultMeta />
			<NextSeo
				title={`${blog?.title} - Kata Travels`}
				description={blog?.excerpt}
				canonical={`https://katatravels.com/blog/article/${blog?.slug}`}
				openGraph={{
					url: `https://katatravels.com/blog/article/${blog?.slug}`,
					title: `${blog?.title} - Kata Travels`,
					description: blog?.excerpt,
					images: [
						{
							url:
								blog?.feature_image ||
								'https://images.unsplash.com/photo-1488646953014-85cb44e25828?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1035&q=80',
							width: 1740,
							height: 960,
							alt: blog?.title,
						},
					],
					siteName: 'Kata Travels',
					type: 'website',
					locale: 'en',
				}}
			/>
			{/* End Page Title */}

			<Header1 darkNav={true} />

			<section className='section-bg layout-pt-lg layout-pb-lg'>
				<div className='section-bg__item col-12'>
					<Image
						width={1920}
						height={800}
						src={
							blog?.feature_image ||
							'https://images.unsplash.com/photo-1488646953014-85cb44e25828?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=1035&q=80'
						}
						alt={blog?.title + ' hero image'}
						className='col-12 rounded-8 w-100 img_large_details'
						style={{
							objectFit: 'cover',
							objectPosition: 'center center',
						}}
						priority
					/>
				</div>

				<div className='container pt-80 pb-80'>
					<div className='row justify-center text-center'>
						<div className='col-xl-8 col-lg-8 col-md-10 pt-120'>
							<span className='text-15 fw-500 text-blue-1 mb-8 text-capitalize'>
								{blog?.tag}
							</span>
							<h1
								className={`text-40 md:text-25 fw-600 text-white bg-${
									getRandomColor().name
								}`}>
								{blog?.title}
							</h1>
							{/* <div className='text-white mt-15'>{blog?.created_at?.$date}</div> */}
						</div>
					</div>
				</div>
				{/* End .container */}
			</section>
			{/* End Banner Section */}

			<section className='layout-pt-md layout-pb-md'>
				<div className='container'>
					<div className='row y-gap-30 justify-center'>
						<div className='col-xl-8 col-lg-10 layout-pt-md'>
							<DetailsContent content={blog?.html} />
							{/* Details content */}

							<div className='border-bottom-light py-30' />
							{/* <div className='border-top-light border-bottom-light py-30 mt-30'>
								<TopComment />
							</div> */}
							{/* End  topcommnet  */}
							<div className='border-bottom-light py-30'>
								<BlogNavigator current={blog} />
							</div>
							{/* End BlogNavigator */}
						</div>
						{/* End .col */}
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>
			{/* Details Blog Details Content */}

			<section className='layout-pt-md layout-pb-lg'>
				<div className='container'>
					<div className='row justify-between text-center'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>
									More articles you might like
								</h2>
							</div>
						</div>

						<div className='col-auto'>
							<Link
								href='/blog'
								className='button -md -blue-1 bg-blue-1-05 text-dark-1'>
								See all articles
								<div className='icon-arrow-top-right ml-15'></div>
							</Link>
						</div>
					</div>
					{/* End .row */}

					<div className='row y-gap-30 pt-40'>
						<RelatedBlog current={blog} />
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
			</section>
			{/* End Related Content */}

			<Subscribe />
			{/* End Subscribe Section */}

			{/* <CallToActions /> */}
			{/* End CallToActions Section */}

			<Footer2 />
			{/* End Call To Actions Section */}
		</>
	);
};

export default BlogSingleDynamic;

export async function getStaticPaths() {
	const paths = blogsData.map((blog) => ({
		params: { slug: blog.slug },
	}));

	return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
	const blog = blogsData.find((blog) => blog.slug === params.slug);

	return {
		props: { blog },
	};
}
