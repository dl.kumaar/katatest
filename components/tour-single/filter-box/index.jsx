import GuestSearch from './GuestSearch';
import DateSearch from './DateSearch';
import Link from 'next/link';
import { useEffect, useState } from 'react';
import { DateObject } from 'react-multi-date-picker';
import Swal from 'sweetalert2';
import SingleTourBookingForm from '../../modals/SingleTourBookingForm';
import { getUserLocation } from '../../../utils/getUserCountry';
import { useRouter } from 'next/router';

const Index = ({ tripLength }) => {
	// const [dates, setDates] = useState([
	// 	new DateObject().setDay(5),
	// 	new DateObject().setDay(14).add(1, 'month'),
	// ]);

	const [dates, setDates] = useState(new Date());

	const router = useRouter();

	// function to convert date to YYYY-MM-DD format
	const formatDate = (date) => {
		let d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		// add a zero in front of numbers<10
		if (month.length < 2) month = '0' + month;
		if (day.length < 2) day = '0' + day;

		return [year, month, day].join('-');
	};

	const queryParams = new URLSearchParams(window.location.search);

	const [guestCounts, setGuestCounts] = useState({
		Adultos:
			router.query.number_of_travellers ||
			queryParams.get('number_of_travellers') ||
			2,
		Niños: router.query.children || queryParams.get('children') || 0,
		Habitaciones:
			router.query.rooms_required || queryParams.get('rooms_required') || 1,
	});

	// // change query params on guest count change
	// useEffect(() => {
	// 	router.push({
	// 		pathname: router.pathname,
	// 		query: {
	// 			...router.query,
	// 			number_of_travellers: guestCounts.Adultos,
	// 			children: guestCounts.Niños,
	// 			rooms_required: guestCounts.Habitaciones,
	// 		},
	// 	});

	// 	return () => {};
	// }, [guestCounts]);

	// // change query params on date change
	// useEffect(() => {
	// 	router.push({
	// 		pathname: router.pathname,
	// 		query: {
	// 			...router.query,
	// 			expected_travel_date: formatDate(dates),
	// 			country_: getUserLocation().userCountry || 'Spain',
	// 		},
	// 	});
	// 	return () => {};
	// }, [dates]);

	return (
		<>
			<div className='col-12'>
				<div className='searchMenu-date px-20 py-10 border-light rounded-4 -right js-form-dd js-calendar'>
					<div>
						<h4 className='text-15 fw-500 ls-2 lh-16'>
							Fecha prevista del viaje
						</h4>
						<DateSearch dates={dates} setDates={setDates} />
					</div>
				</div>
				{/* End check-in-out */}
			</div>
			{/* End .col-12 */}

			<div className='col-12'>
				<GuestSearch
					guestCounts={guestCounts}
					setGuestCounts={setGuestCounts}
				/>
				{/* End guest */}
			</div>
			{/* End .col-12 */}

			<div className='col-12'>
				<Link
					href={`#tour-booking-form
					`}
					className='button -dark-1 py-15 px-35 h-60 col-12 rounded-4 bg-blue-1 text-white'>
					Reservar / Cotizar
				</Link>
				{/* <Link
					href={`/plan-my-trip?
						${new URLSearchParams({
							tour: router.query.slug,
							number_of_travellers: guestCounts.Adultos,
							children: guestCounts.Niños,
							rooms_required: guestCounts.Habitaciones,
							trip_length: getDuration(tripLength),
							expected_travel_date: formatDate(dates),
							country_: getUserLocation().userCountry || 'Spain',
						}).toString()}`}
					className='button -dark-1 py-15 px-35 h-60 col-12 rounded-4 bg-blue-1 text-white'>
					Reservar / Cotizar
				</Link> */}
			</div>
			{/* End .col-12 */}
		</>
	);
};

export default Index;

function getDuration(duration) {
	if (duration < 6) {
		return '3 - 5 días';
	}
	if (duration < 9) {
		return '6 - 8 días';
	}
	if (duration < 12) {
		return '9 - 11 días';
	}
	if (duration < 15) {
		return '12 - 15 días';
	}
	if (duration < 18) {
		return '16+ días';
	}

	return 'No definido';
}
