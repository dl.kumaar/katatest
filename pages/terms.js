import dynamic from 'next/dynamic';
import Seo from '../components/common/Seo';
import Header2 from '../components/header/header-2';
import Footer2 from '../components/footer/footer-3';
import TermsConent from '../components/common/TermsConent';
import { NextSeo } from 'next-seo';

const Terms = () => {
	return (
		<>
			<NextSeo
				title='Terms and Conditions - Kata Travels'
				description='Terms and Conditions - Kata Travels'
				canonical='https://katatravels.com/terms'
			/>

			<div className='header-margin'></div>
			{/* header top margin */}

			<Header2 darkNav />
			{/* End Header 1 */}

			<section className='layout-pt-lg layout-pb-lg'>
				<div className='container'>
					<div className='tabs js-tabs'>
						<TermsConent />
					</div>
				</div>
			</section>
			{/* End terms section */}

			<Footer2 />
			{/* End Call To Actions Section */}
		</>
	);
};

// export default dynamic( () => Promise.resolve( Terms ), { ssr: false } );
export default Terms;
