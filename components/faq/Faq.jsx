const Faq = ({ faqs }) => {
	const faqContent = faqs.map((faq, index) => ({
		...faq,
		collapseTarget: `faq-${index + 1}`,
		id: `faq-${index + 1}`,
	}));
	return (
		<>
			{faqContent.map((item) => (
				<div className='col-12' key={item.id}>
					<div className='accordion__item px-20 py-20 border-light rounded-4'>
						<div
							className='accordion__button d-flex items-center'
							data-bs-toggle='collapse'
							data-bs-target={`#${item.collapseTarget}`}>
							<div className='accordion__icon size-40 flex-center bg-light-2 rounded-full mr-20'>
								<i className='icon-plus' />
								<i className='icon-minus' />
							</div>
							<div className='button text-dark-1 text-start'>
								{item.question}
							</div>
						</div>
						{/* End accordion button */}

						<div
							className='accordion-collapse collapse'
							id={item.collapseTarget}
							data-bs-parent='#Faq1'>
							<div className='pt-15 pl-60'>
								{item.answer.split('\n\n').map((answer, index) => (
									<p key={index} className='text-14 lh-24 text-light-1 mb-20'>
										{answer}
									</p>
								))}
							</div>
						</div>
						{/* End accordion conent */}
					</div>
				</div>
			))}
		</>
	);
};

export default Faq;
