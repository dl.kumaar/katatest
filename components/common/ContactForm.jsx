import axios from 'axios';
import React, { useState } from 'react';
import Swal from 'sweetalert2';
import successBookingForm from '../modals/SuccessBookingForm';

const ContactForm = () => {
	const [contact, setContact] = useState({
		name: '',
		email: '',
		phone: '',
		message: '',
		subject: '',
	});

	const handleSubmit = async (event) => {
		event.preventDefault();
		// handle form submission logic here

		if (!Object.entries(contact).every(([key, value]) => value !== '')) {
			return Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'Por favor ingresa todos los campos!',
				timer: 5000,
				confirmButtonColor: '#3554d1',
				confirmButtonText: `Ok`,
				customClass: {
					confirmButton: 'button -dark-1 bg-blue-1 text-white px-20 py-15',
				},
			});
		}

		const res = await axios.post('/api/contact', { ...contact });

		if (res.status === 201) {
			setContact({
				name: '',
				email: '',
				phone: '',
				message: '',
				subject: '',
			});

			return Swal.fire({
				icon: 'success',
				title: 'Thank you!',
				text: 'We have received your message. We will get back to you shortly.',
				timer: 5000,
				confirmButtonColor: '#3554d1',
				confirmButtonText: `Close`,
				customClass: {
					confirmButton: 'button -dark-1 bg-blue-1 text-white px-20 py-15',
				},
			});
		}

		return Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Something went wrong! Please try again.',
			timer: 2000,
			confirmButtonColor: '#3554d1',
			confirmButtonText: `Ok`,
			customClass: {
				confirmButton: 'button -dark-1 bg-blue-1 text-white px-20 py-15',
			},
		});
	};

	const handleFormChange = (event) => {
		const { id, value } = event.target;

		switch (id) {
			case 'name':
				setContact({ ...contact, name: value });
				break;
			case 'email':
				setContact({ ...contact, email: value });
				break;
			case 'phone':
				setContact({ ...contact, phone: value });
				break;
			case 'subject':
				setContact({ ...contact, subject: value });
				break;
			case 'message':
				setContact({ ...contact, message: value });
			default:
				break;
		}
	};

	return (
		<form className='row y-gap-20 pt-20' onSubmit={handleSubmit}>
			<div className='col-12'>
				<div className='form-input'>
					<input
						type='text'
						id='name'
						required
						value={contact.name}
						onChange={handleFormChange}
					/>
					<label htmlFor='name' className='lh-1 text-16 text-light-1'>
						Name
					</label>
				</div>
			</div>
			<div className='col-12'>
				<div className='form-input'>
					<input
						type='email'
						id='email'
						required
						value={contact.email}
						onChange={handleFormChange}
					/>
					<label htmlFor='email' className='lh-1 text-16 text-light-1'>
						Email
					</label>
				</div>
			</div>
			<div className='col-12'>
				<div className='form-input'>
					<input
						type='phone'
						id='phone'
						required
						value={contact.phone}
						onChange={handleFormChange}
					/>
					<label htmlFor='phone' className='lh-1 text-16 text-light-1'>
						Teléfono
					</label>
				</div>
			</div>
			<div className='col-12'>
				<div className='form-input'>
					<input
						type='text'
						id='subject'
						value={contact.subject}
						onChange={handleFormChange}
					/>
					<label htmlFor='subject' className='lh-1 text-16 text-light-1'>
						Subject
					</label>
				</div>
			</div>
			<div className='col-12'>
				<div className='form-input'>
					<textarea
						id='message'
						value={contact.message}
						onChange={handleFormChange}
						required
						rows='4'></textarea>
					<label htmlFor='message' className='lh-1 text-16 text-light-1'>
						Your message
					</label>
				</div>
			</div>
			<div className='col-auto'>
				<button
					type='submit'
					className='button px-24 h-50 -dark-1 bg-blue-1 text-white'>
					Send message <div className='icon-arrow-top-right ml-15'></div>
				</button>
			</div>
		</form>
	);
};

export default ContactForm;
