import Map from './Map';

const SidebarRight2 = ({ tourMap }) => {
	const recommendRatings = [
		{
			id: 1,
			name: 'Desayuno',
			numberOfRatings: '25',
		},
		{
			id: 2,
			name: 'WiFi',
			numberOfRatings: '14',
		},
		{
			id: 3,
			name: 'Vuelos',
			numberOfRatings: '67',
		},
	];
	return (
		<div className='px-30 py-30 border-light rounded-4'>
			<div className='mb-15'>
				<Map map={tourMap} />
			</div>
			{/* End map */}

			<div className='row y-gap-10'>
				<div className='col-12'>
					<div className='d-flex items-center'>
						<i className='icon-award text-20 text-blue-1' />
						<div className='text-14 fw-500 ml-10'>
							Accommodation at first class hotels
						</div>
					</div>
				</div>
				<div className='col-12'>
					<div className='d-flex items-center'>
						<i className='icon-pedestrian text-20 text-blue-1' />
						<div className='text-14 fw-500 ml-10'>
							Private tours with local guides
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default SidebarRight2;
