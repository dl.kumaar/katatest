import Image from 'next/image';
import Link from 'next/link';
import blogsData from '../../../data/blogs';
import { formatDate } from '../../../utils/blogUtils';

const Blog = () => {
	let blogs = blogsData
		.map((item) => {
			if (!item) return null;

			// skip if no title
			if (item?.title === '(Untitled)') return null;

			if (!item?.title) return null;

			// skip if no feature image
			if (!item?.feature_image) return null;

			// skip if no slug
			if (!item?.slug) return null;

			// skip if no created_at
			if (!item?.created_at) return null;

			return {
				...item,
				delayAnimation: '100',
			};
		})
		.slice(0, 9);

	blogs = blogs.filter((item) => item);
	return (
		<>
			{blogs.slice(0, 2).map((item) => (
				<div className='col-lg-4 col-sm-6' key={item.id}>
					<Link
						href={`/blog/article/${item.id}`}
						className='blogCard -type-1 d-block '>
						<div className='blogCard__image'>
							<div className='ratio ratio-4:3 rounded-4 rounded-8'>
								<Image
									width={400}
									height={400}
									className='img-ratio js-lazy'
									src={item?.feature_image}
									alt='image'
								/>
							</div>
						</div>
						<div className='mt-20'>
							<h4 className='text-dark-1 text-18 fw-500'>{item.title}</h4>
							<div className='text-light-1 text-15 lh-14 mt-5'>
								{formatDate(item?.created_at?.$date)}
							</div>
						</div>
					</Link>
				</div>
			))}

			<div className='col-lg-4'>
				<div className='row y-gap-30'>
					{blogs.slice(3, 6).map((item) => (
						<div className='col-lg-12 col-md-6' key={item.id}>
							<Link
								href={`/blog/article/${item.id}`}
								className='blogCard -type-1 d-flex items-center'>
								<div className='blogCard__image size-130 rounded-8'>
									{/* <img
										src={item.feature_image}
										alt='image'
										className='object-cover size-130'
									/> */}

									<Image
										width={130}
										height={130}
										className='img-ratio js-lazy'
										src={item?.feature_image}
										alt='image'
									/>
								</div>
								<div className='ml-24'>
									<h4 className='text-18 lh-14 fw-500 text-dark-1'>
										{item.title}
									</h4>
									{formatDate(item?.created_at?.$date)}
									<p className='text-15'></p>
								</div>
							</Link>
						</div>
					))}
				</div>
			</div>
		</>
	);
};

export default Blog;
