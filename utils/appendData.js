import { google } from 'googleapis';
import { resolve } from 'path';

const auth = new google.auth.GoogleAuth({
	keyFile: 'keys.json', //the key file
	//url to spreadsheets API
	scopes: 'https://www.googleapis.com/auth/spreadsheets',
});

const tourInquirySpreadsheetId = '1HaRFwLdbA7zjLSEvheuk6AFA6OFL6j_43UONn-jnE1o';
const subscribersSpreadsheetId = '1ncs65fAbCo3UwzgK7BlY-xEFXEbBzzvhrvihAVGO_7c';
const contactSpreadsheetId = '1ReX_Nx3NxjV-dwIfwoYhSncr9xMjGiH_mTI5_ZrddbM';

/**
 * @param {string} type 'tourInquiry' or 'subscribers'
 * @param {array} data
 * */
const appendData = async (type, data) => {
	try {
		const authClientObject = await auth.getClient();

		const googleSheetsInstance = google.sheets({
			version: 'v4',
			auth: authClientObject,
		});

		// Add name, email, phone, country, specialRequest, date, adults, children, rooms, tour, tourName

		function spreadsheetId(type = 'tourInquiry') {
			if (type === 'tourInquiry') {
				return tourInquirySpreadsheetId;
			} else if (type === 'subscribers') {
				return subscribersSpreadsheetId;
			} else if (type === 'contact') {
				return contactSpreadsheetId;
			}

			return tourInquirySpreadsheetId;
		}

		const request = {
			spreadsheetId: spreadsheetId(type),
			range: 'Sheet1!A1',
			valueInputOption: 'USER_ENTERED',
			insertDataOption: 'INSERT_ROWS',
			resource: {
				values: [data],
			},
			auth: authClientObject,
		};

		const response = await googleSheetsInstance.spreadsheets.values.append(
			request
		);
		return response;
	} catch (error) {
		console.log(error);
		return error;
	}
};

export default appendData;
