import { nanoid } from 'nanoid';
import Image from 'next/image';
import Slider from 'react-slick';
import { urlFor } from '../../utils/sanityCalient';

const agentNames = [
	'Carol',
	'Laura',
	'Rahul',
	'Vikram',
	'Paola',
	'Florencia',
	'Romina',
	'Priyanka',
];

const Team1 = ({ agents = [] }) => {
	var settings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1600,
				settings: {
					slidesToShow: 5,
				},
			},
			{
				breakpoint: 1400,
				settings: {
					slidesToShow: 4,
				},
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
				},
			},

			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 520,
				settings: {
					slidesToShow: 1,
					dots: true,
				},
			},
		],
	};

	// custom navigation
	function Arrow(props) {
		let name =
			props.type === 'next'
				? 'arrow-top-right d-flex items-center text-24 arrow-right-hover arrow-next'
				: 'arrow-top-right d-flex items-center text-24 arrow-left-hover arro-prev';

		const char =
			props.type === 'next' ? (
				<>
					<i className='icon icon-arrow-right'></i>
				</>
			) : (
				<>
					<span className='icon icon-arrow-left'></span>
				</>
			);
		return (
			<button className={name} onClick={props.onClick}>
				{char}
			</button>
		);
	}

	return (
		<>
			<Slider
				{...settings}
				nextArrow={<Arrow type='next' />}
				prevArrow={<Arrow type='prev' />}>
				{agents.map((item) => (
					<div className='swiper-slide' key={nanoid(5)}>
						<Image
							width={234}
							height={300}
							src={urlFor(item?.travelAgentImage).width(234).height(300).url()}
							alt='image'
							className='rounded-4 col-12'
						/>
						<div className='mt-10'>
							<div className='text-18 lh-15 fw-500'>
								{item?.travelAgentName} {item?.travelAgentSurname}
							</div>
							<div className='text-14 lh-15'>
								{agentDesgination(item?.travelAgentName)}
							</div>
						</div>
					</div>
				))}
			</Slider>
		</>
	);
};

export default Team1;

function agentDesgination(name) {
	switch (name) {
		case 'Carol':
			return 'Admin. Manager';
		case 'Laura':
			return 'Travel Advisor';
		case 'Rahul':
			return 'Travel Advisor';
		case 'Vikram':
			return 'Manager Operations';
		case 'Paola':
			return 'Travel Advisor';
		case 'Florencia':
			return 'Travel Advisor';
		case 'Romina':
			return 'Travel Advisor';
		case 'Priyanka':
			return 'Operations Executive';
		case 'Shubham':
			return 'Travel Advisor';
		default:
			return 'Travel Advisor';
	}
}
