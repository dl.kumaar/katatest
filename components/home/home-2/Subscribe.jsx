import axios from 'axios';
import Image from 'next/image';
import { useState } from 'react';
import Swal from 'sweetalert2';

const Subscribe = () => {
	const [email, setEmail] = useState('');

	const handleClick = async (e) => {
		e.preventDefault();

		// validate email
		const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
		if (!emailRegex.test(email))
			return Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'Please enter a valid email address!',
				timer: 5000,
				confirmButtonColor: '#3554d1',
				confirmButtonText: `Ok`,
				customClass: {
					confirmButton: 'button -dark-1 bg-blue-1 text-white px-20 py-15',
				},
			});

		if (!email)
			return Swal.fire({
				icon: 'error',
				title: 'Oops...',
				text: 'Please enter your email!',
				timer: 5000,
				confirmButtonColor: '#3554d1',
				confirmButtonText: `Ok`,
				customClass: {
					confirmButton: 'button -dark-1 bg-blue-1 text-white px-20 py-15',
				},
			});

		const res = await axios.post('/api/subscribe', {
			email,
		});

		if (res.status === 201) {
			setEmail('');
			return Swal.fire({
				icon: 'success',
				title: 'Thank you!',
				text: 'You have successfully subscribed to our newsletter.',
				timer: 5000,
				confirmButtonColor: '#3554d1',
				confirmButtonText: `Cerrar`,
				customClass: {
					confirmButton: 'button -dark-1 bg-blue-1 text-white px-20 py-15',
				},
			});
		}
		return Swal.fire({
			icon: 'error',
			title: 'Oops...',
			text: 'Something went wrong! Please try again.',
			timer: 2000,
			confirmButtonColor: '#3554d1',
			confirmButtonText: `Ok`,
			customClass: {
				confirmButton: 'button -dark-1 bg-blue-1 text-white px-20 py-15',
			},
		});
	};

	return (
		<section className='layout-pt-md layout-pb-md' data-aos='fade-up'>
			<div className='container'>
				<div className='row ml-0 mr-0 items-center justify-between'>
					<div className='col-xl-5 px-0'>
						<Image
							className='col-12 h-400'
							src='/img/newsletter/1.png'
							alt='image'
							width={761}
							height={601}
						/>
					</div>
					{/* End .col */}

					<div className='col px-0'>
						<div className='d-flex justify-center flex-column h-400 px-80 py-40 md:px-30 bg-light-2'>
							<div className='icon-newsletter text-60 sm:text-40 text-dark-1' />
							<h2 className='text-30 sm:text-24 lh-15 mt-20'>
								Your Trip to India and Beyond Starts Here
							</h2>
							<p className='text-dark-1 mt-5'>
								Subscribe to our newsletter to get the latest news, updates, and
								amazing offers delivered directly to your inbox.
							</p>

							<div className='single-field -w-410 d-flex x-gap-10 flex-wrap y-gap-20 pt-30'>
								<div className='col-auto'>
									<input
										className='col-12 bg-white h-60'
										type='text'
										placeholder='Your Email'
										required
										value={email}
										onChange={(e) => setEmail(e.target.value)}
									/>
								</div>

								<div className='col-auto'>
									<button
										onClick={handleClick}
										className='button -md h-60 -blue-1 bg-yellow-1 text-dark-1'>
										Subscribe Now
									</button>
								</div>
							</div>
							{/* End single-field */}
						</div>
					</div>
					{/* End .col */}
				</div>
			</div>
		</section>
	);
};

export default Subscribe;
