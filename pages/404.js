import dynamic from 'next/dynamic';
import CallToActions from '../components/common/CallToActions';
import Seo from '../components/common/Seo';
import NotFound from '../components/common/NotFound';
import Header2 from '../components/header/header-2';
import Footer2 from '../components/footer/footer-3';
import { NextSeo } from 'next-seo';

const index = () => {
	return (
		<>
			<NextSeo title='404 - Kata Travels' description='404 - Kata Travels' />

			<div className='header-margin'></div>
			{/* header top margin */}

			<Header2 darkNav={true} />
			{/* End Header 1 */}

			<NotFound />
			{/* End 404 section */}

			<Footer2 />
			{/* End Call To Actions Section */}
		</>
	);
};

export default dynamic(() => Promise.resolve(index), { ssr: false });
