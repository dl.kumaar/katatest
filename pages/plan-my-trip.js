import dynamic from 'next/dynamic';
import Seo from '../components/common/Seo';
import Header2 from '../components/header/header-2';

import Footer2 from '../components/footer/footer-3';

import Image from 'next/image';
import HubspotForm from 'react-hubspot-form';
import { useEffect, useState } from 'react';
import Link from 'next/link';
import successBookingForm from '../components/modals/SuccessBookingForm';
import { nanoid } from 'nanoid';
import { NextSeo } from 'next-seo';

const PlanMyTrip = (props) => {
	const [formSeed, setFormSeed] = useState(nanoid(5));

	const reset = () => {
		setFormSeed(nanoid(5));
	};

	return (
		<>
			<NextSeo
				title='Custom Trip to India and the Subcontinent - Kata Travels'
				description='Create your own custom trip to India and the subcontinent with the help of our local experts. We will help you plan your trip to India and the subcontinent.'
				canonical='https://katatravels.com/plan-my-trip'
				openGraph={{
					url: 'https://katatravels.com/plan-my-trip',
					title: 'Tailor Made Trip - Kata Travels',
					description:
						'Create your own custom trip to India and the subcontinent with the help of our local experts. We will help you plan your trip to India and the subcontinent. ',
					images: [
						{
							url: 'https://katatravels.com/img/pages/about/2.png',
							width: 1740,
							height: 960,
							alt: 'Kata Travels',
						},
					],
					siteName: 'Kata Travels',
					type: 'website',
					locale: 'en',
				}}
			/>

			<div className='header-margin'></div>
			{/* header top margin */}

			<Header2 darkNav={true} />
			{/* End Header 1 */}

			<section className='pt-40 js-pin-container layout-pb-md'>
				<div className='container'>
					<div className='row y-gap-30'>
						<div className='col-xl-4'>
							{/* Sidebar */}

							<div className='col y-gap-40 justify-center pt-50 border-light rounded-4 px-20'>
								<div className='col-sm-12'>
									<div className='featureIcon -type-1'>
										<div className='d-flex justify-center'>
											<Image
												src='/img/featureIcons/3/1.svg'
												data-src='/img/featureIcons/3/1.svg'
												alt='image'
												className='js-lazy'
												width={70}
												height={70}
											/>
										</div>

										<div className='text-center mt-30'>
											<h4 className='text-18 fw-500'>Travel carefree</h4>
											<p className='text-15 mt-10'>
												{/* Viaje con tranquilidad gracias a nuestros expertos
												locales y a nuestras flexibles opciones de reserva. */}
												Travel with confidence with our local experts and
												flexible booking options.
											</p>
										</div>
									</div>
								</div>

								<div className='col-sm-12'>
									<div className='featureIcon -type-1'>
										<div className='d-flex justify-center'>
											<Image
												src='/img/featureIcons/3/2.svg'
												data-src='/img/featureIcons/3/2.svg'
												alt='image'
												className='js-lazy'
												width={70}
												height={70}
											/>
										</div>

										<div className='text-center mt-30'>
											<h4 className='text-18 fw-500'>Authentic experiences</h4>
											<p className='text-15 mt-10'>
												Tailored Luxury Experiences for the Most Discerning
												Travelers.
											</p>
										</div>
									</div>
								</div>

								<div className='col-sm-12'>
									<div className='featureIcon -type-1'>
										<div className='d-flex justify-center'>
											<Image
												src='/img/featureIcons/3/4.svg'
												data-src='/img/featureIcons/3/4.svg'
												alt='image'
												className='js-lazy'
												width={70}
												height={70}
											/>
										</div>

										<div className='text-center mt-30'>
											<h4 className='text-18 fw-500'>24/7 Trip Support</h4>

											<p className='text-15 mt-10'>
												Our team of experts is available 24/7 to help you with
												any questions or concerns you may have.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						{/* End Sidebar */}

						<div className='col-xl-8 col-lg-9' id='custom-tour'>
							<h2 className='text-22 fw-500 md:mt-24'>
								Tell us your travel plans
							</h2>
							<div className='text-16 mt-10'>
								We will create a custom itinerary for you based on your
								preferences and budget. Simply fill out the form below and one
								of our travel experts will be in touch shortly.
							</div>
							<div className='py-15 px-20 rounded-4 text-15 bg-blue-1-05'>
								We are flexible. Postpone your trip at{' '}
								<span className='text-blue-1 fw-500'>no cost</span> up to 15
								days prior to departure.
							</div>

							<div className='row x-gap-20 y-gap-20 pt-20'>
								<HubspotForm
									key={formSeed}
									portalId='20635927'
									region='na1'
									formId='1ece552b-1e78-4626-b517-7a2bd35363c3'
									onReady={(form) => console.log('Form ready!')}
									loading={<div>Loading...</div>}
									onFormSubmitted={(form, data) => {
										if (!data.redirectUrl) {
											successBookingForm(reset);
										}
									}}
								/>

								<div
									className='py-15 px-20 rounded-4 text-15 bg-white'
									style={{
										marginTop: '-90px',
									}}></div>
							</div>
						</div>
						{/* End Form */}
					</div>
				</div>
			</section>

			<Footer2 />
		</>
	);
};

export default dynamic(() => Promise.resolve(PlanMyTrip), {
	ssr: false,
});
