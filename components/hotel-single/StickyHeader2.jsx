import Link from 'next/link';

import { useEffect, useState } from 'react';
const StickyHeader2 = ({ price }) => {
	const [header, setHeader] = useState(false);

	const changeBackground = () => {
		if (window.scrollY >= 200) {
			setHeader(true);
		} else {
			setHeader(false);
		}
	};

	useEffect(() => {
		window.addEventListener('scroll', changeBackground);
	}, []);

	return (
		<div
			className={`singleMenu js-singleMenu ${header ? '-is-active' : ''}`}
			id='singleMenu'>
			<div className='col-12'>
				<div className='singleMenu__content'>
					<div className='container'>
						<div className='row y-gap-20 justify-between md:justify-center items-center'>
							<div className='col-auto md:d-none'>
								<div className='singleMenu__links row x-gap-30 y-gap-10'>
									<div className='col-auto'>
										<a href='#tour-overview'>Overview</a>
									</div>
									<div className='col-auto'>
										<a href='#tour-itinerary'>Itinerary</a>
									</div>
									<div className='col-auto'>
										<a href='#inclusions'>Inclusions</a>
									</div>
									{price ? (
										<div className='col-auto'>
											<a href='#tour-price'>Price</a>
										</div>
									) : null}
								</div>
							</div>

							<div className='col-auto'>
								<div className='row x-gap-15  items-center'>
									{price ? (
										<div className='col-auto'>
											<div className='text-14'>
												From{' '}
												<span className='text-22 text-dark-1 fw-500'>
													US${price}
												</span>
											</div>
										</div>
									) : null}
									<div className='col-auto d-flex items-center'>
										<Link
											id='tour-booking-form-btn'
											href={`#tour-booking-form`}
											className='button h-50 px-24 -dark-1 bg-blue-1 text-white'>
											Make an inquiry
											<div className='icon-arrow-top-right ml-15' />
										</Link>
										<div className='d-flex ml-10'>
											<Link
												href='https://api.whatsapp.com/send?phone=919868788591&text=Hi there, I am interested in your services. Please get in touch with me.'
												className='sticky-whatsapp-float'
												target='_blank'>
												<i className='fa fa-whatsapp sticky-my-float'></i>
											</Link>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default StickyHeader2;
