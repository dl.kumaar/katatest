import Image from 'next/image';
import Link from 'next/link';

const NotFound = () => {
	const data = {
		imageSrc: '/img/general/404.svg',
		title: 'Oops! Seems like you are lost',
		description:
			'The page you are looking for is not available. Try to search again or use the Go Home button below.',
		buttonLabel: 'Go Home',
		buttonUrl: '/',
	};

	return (
		<section className='layout-pt-lg layout-pb-lg'>
			<div className='container'>
				<div className='row y-gap-30 justify-between items-center'>
					<div className='col-lg-6'>
						<Image src={data.imageSrc} width={629} height={481} alt='image' />
					</div>
					<div className='col-lg-5'>
						<div className='no-page'>
							<div className='no-page__title'>
								40<span className='text-blue-1'>4</span>
							</div>
							<h2 className='text-30 fw-600'>{data.title}</h2>
							<div className='pr-30 mt-5'>{data.description}</div>
							<div className='d-inline-block mt-40 md:mt-20'>
								<Link
									href={data.buttonUrl}
									className='
                  button -md -dark-1 rounded-4 border-light'>
									{data.buttonLabel}
								</Link>
							</div>

							<div className='d-inline-block mt-40 ml-20 md:mt-20'>
								<Link
									href={'/tours'}
									className='button -md -dark-1 bg-blue-1 text-white'>
									{'View Tours'}
								</Link>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	);
};

export default NotFound;
