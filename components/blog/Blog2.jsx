import { nanoid } from 'nanoid';
import Image from 'next/image';
import Link from 'next/link';
import { useEffect, useRef, useState } from 'react';
// import blogsData from '../../data/blogs';
import { extractText, formatDate } from '../../utils/blogUtils';

const Blog2 = ({ blogs: blogsData }) => {
	// Create infinte scroll for blogs

	const [blogs, setBlogs] = useState([...blogsData.slice(0, 4)]);

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const handleObserver = (entities) => {
		const target = entities[0];
		if (target.isIntersecting) {
			setBlogs([...blogs, ...blogsData.slice(blogs.length, blogs.length + 4)]);
		}
	};

	// Create a ref to the element we want to observe
	const ref = useRef(null);
	const formatDate = (date) => {
		const dateObj = randomTimeStamp();

		const month = dateObj.toLocaleString('default', { month: 'long' });
		const day = dateObj.getDate() + 1;
		const year = dateObj.getFullYear();

		return `${month} ${day}, ${year}`;
	};
	// Observe the element
	useEffect(() => {
		if (!ref.current) return;

		// Create the observer
		const observer = new IntersectionObserver(handleObserver, {
			root: null,
			rootMargin: '20px',
			threshold: 1.0,
		});

		if (ref.current) {
			observer.observe(ref.current);
		}
	}, [handleObserver]);

	return (
		<>
			{blogs.map((item, index) =>
				item?.title === '(Untitled)' ? null : (
					<>
						<Link
							href={`/blog/article/${item?.slug}`}
							className='blogCard -type-1 col-12'
							key={nanoid(5)}>
							<div className='row y-gap-15 items-center md:justify-center md:text-center'>
								<div className='col-lg-4'>
									<div className='blogCard__image rounded-4'>
										{!item?.feature_image ? (
											<Image
												width={250}
												height={250}
												className='cover w-100 img-fluid'
												src='/img/blog/1.png'
												alt='image'
											/>
										) : (
											<Image
												width={250}
												height={250}
												className='cover w-100 img-fluid'
												src={item?.feature_image}
												alt='image'
											/>
										)}
									</div>
								</div>
								<div className='col-lg-8'>
									<div className='text-15 text-light-1'>
										{/* {formatDate(item?.created_at?.$date)} */}
										{/* {item?.created_at?.$date} */}
									</div>

									<h3 className='text-22 text-dark-1 mt-10 md:mt-5'>
										{item?.title}
									</h3>
									<div className='text-15 lh-16 text-light-1 mt-10 md:mt-5'>
										{extractText(item?.excerpt, 200)}
									</div>
								</div>
							</div>
						</Link>

						<div
							ref={blogs.length === blogsData.length ? null : ref}
							className='col-12'
						/>
					</>
				)
			)}
		</>
	);
};

export default Blog2;
