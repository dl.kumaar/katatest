'use client';

import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import {
	ProSidebarProvider,
	Sidebar,
	Menu,
	MenuItem,
	SubMenu,
} from 'react-pro-sidebar';
import { destinations } from '../../data/mainMenuData';
import { isActiveLink } from '../../utils/linkActiveChecker';
import Social from '../common/social/Social';
import ContactInfo from './ContactInfo';

import Logo from '../../assets/kata-logo.png';

const MobileMenu = () => {
	const router = useRouter();

	return (
		<>
			<div className='pro-header d-flex align-items-center justify-between border-bottom-light'>
				<Link href='/'>
					<Image src={Logo} alt='brand' height={100} width={100} />
				</Link>
				{/* End logo */}

				<div
					className='fix-icon'
					data-bs-dismiss='offcanvas'
					aria-label='Close'>
					<i className='icon icon-close'></i>
				</div>
				{/* icon close */}
			</div>
			{/* End pro-header */}

			<ProSidebarProvider>
				<Sidebar width='400' backgroundColor='#fff'>
					<Menu>
						<MenuItem
							component={
								<Link
									href='/'
									className={router.pathname === '/' ? 'menu-active-link' : ''}
								/>
							}>
							Home
						</MenuItem>

						<SubMenu label='Destinations'>
							{destinations.map((item, i) => (
								<MenuItem
									key={i}
									component={
										<Link
											href={item.routePath}
											className={
												isActiveLink(item.routePath, router.asPath)
													? 'menu-active-link'
													: ''
											}
										/>
									}>
									{item.name}
								</MenuItem>
							))}
						</SubMenu>
						{/* End  Destinations */}

						<MenuItem
							component={
								<Link
									href='/luxury-tours-vacation-packages'
									className={
										router.pathname === '/luxury-tours-vacation-packages'
											? 'menu-active-link'
											: ''
									}
								/>
							}>
							Tour Packages
						</MenuItem>

						{/* <SubMenu label='Destinos'>
							{categorieMobileItems.map((item) => (
								<SubMenu label={item.title} key={item.id}>
									{item.menuItems.map((single) => (
										<SubMenu label={single.title} key={single.id}>
											{single.menuList.map((menu, i) => (
												<MenuItem
													key={i}
													component={
														<Link
															href={menu.routePath}
															className={
																isActiveLink(menu.routePath, router.asPath)
																	? 'menu-active-link'
																	: ''
															}
														/>
													}>
													{menu.name}
												</MenuItem>
											))}
										</SubMenu>
									))}
								</SubMenu>
							))}
						</SubMenu> */}
						{/* End  All Categories Menu */}

						<MenuItem
							component={
								<Link
									href='/blog'
									className={
										router.pathname === '/blog' ? 'menu-active-link' : ''
									}
								/>
							}>
							Blog
						</MenuItem>

						<MenuItem
							component={
								<Link
									href='/about'
									className={
										router.pathname === '/about' ? 'menu-active-link' : ''
									}
								/>
							}>
							About
						</MenuItem>

						<MenuItem
							component={
								<Link
									href='/plan-my-trip'
									className={
										router.pathname === '/plan-my-trip'
											? 'menu-active-link'
											: ''
									}
								/>
							}>
							Plan My Trip{' '}
						</MenuItem>

						<MenuItem
							component={
								<Link
									href='/contact'
									className={
										router.pathname === '/contact' ? 'menu-active-link' : ''
									}
								/>
							}>
							Contact
						</MenuItem>
						{/* End Contact  Menu */}
					</Menu>
				</Sidebar>
			</ProSidebarProvider>

			<div className='mobile-footer px-20 py-5 border-top-light'></div>

			<div className='pro-footer'>
				<ContactInfo />
				<div className='mt-10'>
					<h5 className='text-16 fw-500 mb-10'>Follow us on social media</h5>
					<div className='d-flex x-gap-20 items-center'>
						<Social />
					</div>
				</div>
				{/* <div className='mt-20'>
					<Link
						className=' button -dark-1 px-30 fw-400 text-14 bg-blue-1 h-50 text-white'
						href='/others-pages/login'>
						Planear mi viaje
					</Link>
				</div> */}
			</div>
			{/* End pro-footer */}
		</>
	);
};

export default MobileMenu;
