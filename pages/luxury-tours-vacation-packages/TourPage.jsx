import Seo from '../../components/common/Seo';
import Header2 from '../../components/header/header-2';
import Footer2 from '../../components/footer/footer-3';
import HowWorks from '../../components/block/HowWorks';
import Link from 'next/link';

import Image from 'next/image';
import FilterHotels from '../../components/hotels/FilterHotels';
import dynamic from 'next/dynamic';
import Sidebar from '../../components/tour-list/tour-list-v2/Sidebar';
import TopHeaderFilter from '../../components/tour-list/tour-list-v2/TopHeaderFilter';
import TourProperties from '../../components/tour-list/tour-list-v2/TourProperties';
import { useRouter } from 'next/router';

const Index = ({ tours }) => {
	const router = useRouter();
	const query = router.query;

	// console.log('query', query);

	return (
		<>
			<div className='header-margin'></div>
			{/* header top margin */}

			<Header2 darkNav />
			{/* End Header 1 */}

			<section className='section-bg pt-200 pb-40 relative z-5'>
				<div className='section-bg__item col-12'>
					<Image
						src='/img/misc/bg-1.png'
						alt='image'
						className='w-full h-full object-cover'
						width={1920}
						height={100}
					/>
				</div>

				<div className='container pt-80'>
					<div className='row'>
						<div className='col-12'>
							<div className='text-center'>
								<h1 className='text-30 fw-600 text-white'>
									Find your dream trip
								</h1>

								<div className='text-18 pt-20 text-white'>
									<span className='fw-500'>
										✈️ All our trips are fully customizable and leave 365 days a
										year.
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section className='layout-pt-md layout-pb-lg'>
				<div className='container'>
					<div className='row y-gap-30'>
						{/* <div className='col-xl-3'>
							<aside className='sidebar y-gap-40 xl:d-none'>
								<Sidebar />
							</aside>

							<div
								className='offcanvas offcanvas-start'
								tabIndex='-1'
								id='listingSidebar'>
								<div className='offcanvas-header'>
									<h5 className='offcanvas-title' id='offcanvasLabel'>
										Filter Tours
									</h5>
									<button
										type='button'
										className='btn-close'
										data-bs-dismiss='offcanvas'
										aria-label='Close'></button>
								</div>

								<div className='offcanvas-body'>
									<aside className='sidebar y-gap-40  xl:d-block'>
										<Sidebar />
									</aside>
								</div>
							</div>
						</div> */}

						<div className='col-xl-12'>
							{/* <TopHeaderFilter /> */}
							<div className='mt-30'></div>
							{/* End mt--30 */}
							<div className='row y-gap-30'>
								<FilterHotels tours={tours} />
							</div>
						</div>
					</div>
				</div>
			</section>

			<section className='layout-pt-lg layout-pb-lg bg-light'>
				<div className='container'>
					<div className='row y-gap-20 justify-center text-center'>
						<div className='col-auto'>
							<div className='sectionTitle -md'>
								<h2 className='sectionTitle__title'>
									Don&apos;t like any trip?
								</h2>
								<p className=' sectionTitle__text mt-5 sm:mt-0'>
									No problem. We can customize any tour for you. Just tell us
									what you want and we will make it happen.
								</p>
							</div>
						</div>
					</div>
					{/* End .row */}

					<div className='row y-gap-30 justify-between pt-40'>
						<HowWorks />
					</div>

					{/* End .row */}

					<div className='row justify-center pt-60'>
						<div className='col-auto'>
							<Link
								href='/plan-my-trip'
								className='button px-40 h-50 -outline-blue-1 text-blue-1'>
								Plan my trip
								<div className='icon-arrow-top-right ml-15' />
							</Link>
						</div>
					</div>
				</div>
				{/* End .container */}
			</section>
			{/* End how works Section */}

			<Footer2 />
		</>
	);
};

export default dynamic(() => Promise.resolve(Index), { ssr: false });
