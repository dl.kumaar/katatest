import DetailsReview2 from '../../components/hotel-single/guest-reviews/DetailsReview2';
import ReviewProgress2 from '../../components/hotel-single/guest-reviews/ReviewProgress2';
import ReplyForm from '../../components/hotel-single/ReplyForm';
import ReplyFormReview2 from '../../components/hotel-single/ReplyFormReview2';

export default function Reviews() {
	return (
		<>
			<section className='pt-80' id='reviews'>
				<div className='container'>
					<div className='row y-gap-40 justify-between'>
						<div className='col-xl-3'>
							<h3 className='text-22 fw-500'>Guest reviews</h3>
							<ReviewProgress2 />
							{/* End review with progress */}
						</div>
						{/* End col-xl-3 */}

						<div className='col-xl-8'>
							<DetailsReview2 />
						</div>
						{/* End col-xl-8 */}
					</div>
					{/* End .row */}
				</div>
				{/* End .container */}
				{/* End container */}
			</section>
			{/* End Review section */}

			<div className='container mt-60 mb-60' />

			{/* <section className='layout-pb-lg'>
				<div className='container'>
					<div className='row y-gap-30 justify-between'>
						<div className='col-xl-3'>
							<div className='row'>
								<div className='col-auto'>
									<h3 className='text-22 fw-500'>Leave a Reply</h3>
									<p className='text-15 text-dark-1 mt-5'>
										Your email address will not be published.
									</p>
								</div>
							</div>

							<ReplyFormReview2 />
						</div>

						<div className='col-xl-8'>
							<ReplyForm />
						</div>
					</div>
				</div>
			</section> */}
		</>
	);
}
