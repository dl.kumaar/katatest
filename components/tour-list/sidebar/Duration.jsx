import { useRouter } from 'next/router';

const durationOptions = [
	{
		label: '1-3 Days',
		count: 12,
	},
	{
		label: '4-6 Days',
		count: 12,
	},
	{
		label: '7-9 Days',
		count: 12,
	},
	{
		label: '10-12 Days',
		count: 12,
	},
	{
		label: '13-15 Days',
		count: 12,
	},
	{
		label: '16+ Days',
		count: 12,
	},
];

const Duration = () => {
	const router = useRouter();
	const { query } = router;

	function handleDurationChange(e) {
		if (e.target.checked) {
			console.log('b');
			router.push({
				pathname: '/luxury-tours-vacation-packages',
				query: {
					...query,
					duration: query?.duration
						? `${query.duration},${e.target.name}`
						: e.target.name,
				},
			});
		} else {
			const durationArray = query?.duration?.split(',');
			const index = durationArray?.indexOf(e.target.name);
			durationArray.splice(index, 1);

			if (durationArray.length === 0) {
				delete query.duration;
				console.log('a');
				router.push({
					pathname: '/luxury-tours-vacation-packages',
					query: {
						...query,
					},
				});
			}

			router.push({
				pathname: '/luxury-tours-vacation-packages',
				query: {
					...query,
					duration: durationArray.join(','),
				},
			});
		}
	}

	return (
		<>
			{durationOptions.map((option, index) => (
				<div className='row y-gap-10 items-center justify-between' key={index}>
					<div className='col-auto'>
						<div className='form-checkbox d-flex items-center'>
							<input
								type='checkbox'
								name={option.label}
								value={option.label}
								checked={query?.duration?.includes(option.label)}
								onChange={(e) => handleDurationChange(e)}
							/>

							{query.duration?.includes(option.label) ? (
								<div className='form-checkbox__mark'>
									<div className='form-checkbox__icon icon-check' />
								</div>
							) : (
								<div className='form-checkbox__mark'>
									<div className='form-checkbox__icon icon-check' />
								</div>
							)}

							<div className='text-15 ml-10'>{option.label}</div>
						</div>
					</div>
					<div className='col-auto'>
						<div className='text-15 text-light-1'>{option.count}</div>
					</div>
				</div>
			))}
		</>
	);
};

export default Duration;
