import { nanoid } from 'nanoid';
import Image from 'next/image';
import Link from 'next/link';
import blogsData from '../../../../data/blogs';
import { formatDate } from '../../../../utils/blogUtils';

const RecentPost = () => {
	const blogs = blogsData
		.map((item) => {
			// skip if no title
			if (item?.title === '(Untitled)') return null;

			// skip if no feature image
			if (!item?.feature_image) return null;

			// skip if no slug
			if (!item?.slug) return null;

			// skip if no created_at
			if (!item?.created_at) return null;

			return {
				...item,
				delayAnimation: '100',
			};
		})
		.slice(0, 5);

	return (
		<>
			{blogs.map((item) => (
				<div className='col-12' key={nanoid(5)}>
					<div className='d-flex items-center'>
						{!item?.feature_image ? (
							<Image
								width={65}
								height={65}
								className='size-65 rounded-8'
								src='/img/blog/1.png'
								alt='image'
							/>
						) : (
							<Image
								width={65}
								height={65}
								className='size-65 rounded-8'
								src={item?.feature_image}
								alt='image'
							/>
						)}
						<div className='ml-15'>
							<h5 className='text-15 lh-15 fw-500'>
								<Link href={`/blog/article/${item?.slug}`}>{item?.title}</Link>
							</h5>
							{/* <div className='text-13 lh-1 mt-5'>
								{formatDate(item?.created_at?.$date)}
							</div> */}
						</div>
					</div>
				</div>
			))}
		</>
	);
};

export default RecentPost;
