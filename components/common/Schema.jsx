import Head from 'next/head';

function strip(html) {
	var one = html.replace(/<\/?[^>]+(>|$)/gm, '');
	var two = one.replace(/[\r\n]\s*[\r\n]/gm, '');
	return two;
}

// Faq Schema
// https://metamanager.io/schema-markup/how-to-add-faq-schema-markup-to-your-website/

const samplePost = {
	title: 'Sample Post Title',
	blurb: 'Sample post blurb.',
	featuredImage: {
		sourceUrl: 'https://www.example.com/images/sample.jpg',
	},
	date: '2021-01-01T00:00:00',
	modified: '2021-01-01T00:00:00',
	slug: 'sample-post-title',
	commentCount: 10,
	author: {
		slug: 'sample-author',
		name: 'Sample Author',
	},
	ratingCount: 10,
	ratingAverage: 9,
	citations: [
		{
			name: 'Sample Citation',
			url: 'https://www.example.com/sample-citation',
		},
	],
};

const Schema = ({ post = samplePost }) => {
	const site = 'https://www.example.com/';
	const siteTitle = 'Example.com';

	const {
		title,
		blurb,
		featuredImage,
		date,
		modified,
		slug,
		commentCount,
		author,
		ratingCount,
		ratingAverage,
		citations,
	} = post;
	const published = new Date(date);
	const copyrightYear = published.getFullYear();

	let mediaDetails, sourceUrl;

	if (featuredImage) {
		sourceUrl = featuredImage.sourceUrl;
	}

	const citationsList = citations.map((citation, i) => {
		return `{ "@type": "CreativeWork", "citation": ${JSON.stringify(
			citation
		)} }${i === citations.length - 1 ? '' : ','}\n`;
	});
	const citationsText = citationsList.join('');

	const org = `{ "@id": "${site}#organization", "type": "Organization", "name":"${siteTitle}", "logo": {
    "@type": "ImageObject",
    "name": "${siteTitle} Logo",
    "width": "230",
    "height": "67",
    "url": "${site}images/logo.png"
} }`;

	return (
		<Head>
			<script type='application/ld+json'>{`
    {
      "@context":"https://schema.org/",
      "@type":"Article",
      "name":"${title}",
      ${
				ratingAverage > 4
					? `"aggregateRating": {
        "@type":"AggregateRating",
        "ratingValue":${ratingAverage},
        "reviewCount":${ratingCount}
      },`
					: ''
			}
      "about": "${blurb}",
      "author": { "@type": "Person", "@id": "${site}author/${
				author.slug
			}", "name": "${author.name}" },
      ${
				citationsText.length
					? `"citation": [
        ${citationsText}
      ],`
					: ''
			}
      "commentCount": ${commentCount},
      "copyrightHolder": { "@id": "${site}#organization" },
      "copyrightYear": ${copyrightYear},
      "datePublished": "${date}",
      "dateModified": "${modified}",
      "description": "${blurb}",
      "discussionUrl": "${site}articles/${slug}#comments",
      "editor": { "@id": "${site}author/${author.slug}#author" },
      "headline": "${title}",
      ${sourceUrl ? `"image": "${sourceUrl}",` : ''}
      "inLanguage": "English",
      "mainEntityOfPage": "${site}articles/${slug}",
      "publisher": { "@id": "${site}#organization" },
      "sourceOrganization": ${org},
      "url": "${site}articles/${slug}"
    }
    `}</script>
		</Head>
	);
};

export default Schema;
