import { useRouter } from 'next/router';

function getCurrentTIme(timezone) {
	const date = new Date();
	const utc = date.getTime() + date.getTimezoneOffset() * 60000;
	const newDate = new Date(utc + 3600000 * timezone);
	return newDate.toLocaleString();
}

const countryInfo = [
	{
		country: 'india',
		timezone: 'GMT +05:30',
		currentTime: getCurrentTIme(5.5),
		currency: 'Indian Rupee',
		currencyValue: '1USD ~ 81.00INR',
		language: 'Hindi, English',
		bestTime: {
			monthFrom: 'October',
			monthTo: 'March',
		},
	},
	{
		country: 'nepal',
		timezone: 'GMT +05:45',
		currentTime: getCurrentTIme(5.75),
		currency: 'Nepalese Rupee',
		currencyValue: '1USD ~ 120.00NPR',
		language: 'Nepali',
		bestTime: {
			monthFrom: 'October',
			monthTo: 'March',
		},
	},
	{
		country: 'maldives',
		timezone: 'GMT +05:00',
		currentTime: getCurrentTIme(5),
		currency: 'Maldivian Rufiyaa',
		currencyValue: '1USD ~ 15.42MVR',
		language: 'Dhivehi',
		bestTime: {
			monthFrom: 'November',
			monthTo: 'April',
		},
	},
];

const GeneralInfo = () => {
	const router = useRouter();
	const country = router.query.country;

	const countryData = countryInfo.find((item) => item.country === country);

	return (
		<>
			<div className='col-xl-3 col-6'>
				<div className='text-15'>Time Zone</div>
				<div className='fw-500'>{countryData?.timezone}</div>
				<div className='text-15 text-light-1'>{countryData?.currentTime}</div>
			</div>
			{/* End .col */}

			<div className='col-xl-3 col-6'>
				<div className='text-15'>Currency</div>
				<div className='fw-500'>{countryData?.currency}</div>
				<div className='text-15 text-light-1'>{countryData?.currencyValue}</div>
			</div>
			{/* End .col */}

			<div className='col-xl-3 col-6'>
				<div className='text-15'>Language</div>
				<div className='fw-500'>{countryData?.language}</div>
			</div>
			{/* End .col */}

			<div className='col-xl-3 col-6'>
				<div className='text-15'>Best time to visit</div>
				<div className='row y-gap-20'>
					<div className='col-auto'>
						<div className='fw-500'>From</div>
						<div className='text-15 text-light-1'>
							{countryData?.bestTime?.monthFrom}
						</div>
					</div>
					{/* End .col */}

					<div className='col-auto'>
						<div className='fw-500'>To</div>
						<div className='text-15 text-light-1'>
							{countryData?.bestTime?.monthTo}
						</div>
					</div>
				</div>
				{/* End .row */}
			</div>
			{/* End .col */}
		</>
	);
};

export default GeneralInfo;
