import Link from 'next/link';

import CategoriesMegaMenu from './CategoriesMegaMenu';
import {
	isActiveParent,
	isActiveLink,
	isActiveParentChaild,
} from '../../utils/linkActiveChecker';
import { useRouter } from 'next/router';

const MainMenu = ({ style = '' }) => {
	const router = useRouter();

	return (
		<nav className='menu js-navList'>
			<ul className={`menu__nav ${style} -is-active`}>
				<li className={`${isActiveLink('/', router.asPath) ? 'current' : ''}`}>
					<Link href='/'>
						<span className='mr-10 text-16'>🏡</span>
					</Link>
				</li>
				{/* End home page menu */}
				<li className='menu-item-has-children -has-mega-menu'>
					<a href='#'>
						<span className='mr-10 text-16'>Destination</span>
						<i className='icon icon-chevron-sm-down' />
					</a>
					<div
						className='mega'
						style={{
							boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.20)',
						}}>
						<CategoriesMegaMenu />
					</div>
				</li>

				{/* <li
					className={`menu-item-has-children ${
						router.query?.country === 'india'
							? 'current'
							: router.query?.country === 'nepal'
							? 'current'
							: router.query?.country === 'maldives'
							? 'current'
							: ''
					}`}>
					<a href='#'>
						<span className='mr-10 text-16'>Destinations</span>
						<i className='icon icon-chevron-sm-down' />
					</a>
					<ul className='subnav'>
						<li>
							<Link href='/travel-guide/india'>India</Link>
						</li>
						<li>
							<Link href='/travel-guide/nepal'>Nepal</Link>
						</li>
						<li>
							<Link href='/travel-guide/maldives'>Maldives</Link>
						</li>
					</ul>
				</li> */}
				{/* End categories menu items */}

				<li
					className={`${
						isActiveLink('/luxury-tours-vacation-packages', router.asPath)
							? 'current'
							: ''
					}`}>
					<Link href='/luxury-tours-vacation-packages'>
						<span className='mr-10 text-16'>Tour Packages</span>
					</Link>
				</li>
				{/* End tours page menu */}

				<li
					className={`${
						isActiveLink('/blog', router.asPath) ? 'current' : ''
					}`}>
					<Link href='/blog'>
						<span className='mr-10 text-16'>Blog</span>
						{/* <i className='icon icon-chevron-sm-down' /> */}
					</Link>
				</li>
				{/* End blogIems */}

				<li
					className={`${
						isActiveLink('/about', router.asPath) ? 'current' : ''
					}`}>
					<Link href='/about'>
						<span className='mr-10 text-16'>About</span>
					</Link>
				</li>
				{/* End pages items */}
			</ul>
		</nav>
	);
};

export default MainMenu;
{
	/* <li className='menu-item-has-children'>
<a href='#'>
	<span className='mr-10 text-16'>Destinos</span>
	<i className='icon icon-chevron-sm-down' />
</a>
<ul className='subnav'>
	<li>
		<Link href='/destination/india'>India</Link>
	</li>
	<li>
		<Link href='/destination/nepal'>Nepal</Link>
	</li>
	<li>
		<Link href='/destination/maldives'>Maldives</Link>
	</li>
</ul>
</li> */
}
