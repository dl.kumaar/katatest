import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { categorieMegaMenuItems } from '../../data/mainMenuData';
import {
	isActiveParent,
	isActiveLink,
	isActiveParentChaild,
} from '../../utils/linkActiveChecker';

const CategoriesMegaMenu = () => {
	const router = useRouter();

	const itemList = [
		'India',
		'Nepal',
		'Maldives',
		'Bhutan',
		// 'Dubai',
		// 'Sri Lanka',
		// 'Thailand',
		// 'Vietnam',
	];

	return (
		<>
			<Tabs className='tabs row justify-between -underline-2 js-tabs'>
				{/* <div className='col-1'>
					<TabList className='tabs__controls flex-col x-gap-40 y-gap-10 lg:x-gap-20 pb-30 js-tabs-controls'>
						{itemList.map((item, i) => (
							<Tab className='col-auto' key={i}>
								<button className='tabs__button text-light-1 fw-500 js-tabs-button'>
									{item}
								</button>
							</Tab>
						))}
					</TabList>
				</div> */}
				{/* End tab-controls */}

				<div
					className='col-12 js-tabs-content'
					style={{
						width: 'content-fit',
					}}>
					{categorieMegaMenuItems.map((megaMenu) => (
						<TabPanel key={megaMenu.id}>
							{megaMenu?.menuCol?.map((megaCol, i) => (
								<ul className='mega__content' key={i}>
									<li className='mega__grid'>
										{megaCol?.menuItems?.map((item) => (
											<div className='mega__item' key={item.id}>
												{/* <Link className='text-15 fw-500' href={item?.to || '/'}>
													{item.title}
												</Link> */}
												<div className='y-gap-15 text-15 pt-5'>
													{item?.menuList?.map((list, i) => (
														<div
															key={i}
															className={
																isActiveLink(list.routePath, router.asPath)
																	? 'current'
																	: ''
															}>
															<Link href={list.routePath}>
																{list?.type === 'destination' ? (
																	<strong>{list.name}</strong>
																) : (
																	list.name
																)}
															</Link>
														</div>
													))}
												</div>
											</div>
										))}
									</li>
									{/* End mega menu list left */}

									<li className='mega__image d-flex relative'>
										<Image
											width={270}
											height={300}
											src={megaCol?.megaBanner}
											alt='image'
											className='rounded-4 js-lazy'
										/>

										<div className='absolute w-full h-full px-30 py-24'>
											<div className='text-22 fw-500 lh-15 text-white'>
												{megaCol?.title}
											</div>
											<Link
												href={megaCol?.btnRoute}
												className='button text-uppercase h-50 px-30 -blue-1 text-dark-1 bg-white mt-20 d-inline-flex'>
												{megaCol?.btnText}
											</Link>
										</div>
									</li>
									{/* End mega menu right images */}
								</ul>
							))}
						</TabPanel>
					))}
				</div>
				{/* End tab_content */}
			</Tabs>
		</>
	);
};

export default CategoriesMegaMenu;
