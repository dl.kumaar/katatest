// is active parent check
export const isActiveParent = (data = [], path) => {
	// console.log(data, path);
	if (data?.length !== 0) {
		const a = data?.some(({ items }) =>
			items?.some(
				(menu) =>
					menu.routePath.replace(/\/\d+/, '') === path.replace(/\/\d+/, '')
			)
		);

		// console.log(a);
		return a;
	}
};

// is active parent childe check
export const isActiveParentChaild = (data = [], path) => {
	// console.log(data, path);
	if (data?.length !== 0) {
		return data?.some(
			(menu) =>
				menu.routePath.replace(/\/\d+/, '') === path.replace(/\/\d+/, '')
		);
	}
};

// is active link check
export const isActiveLink = (menuPath, routePath) => {
	if (menuPath && routePath) {
		return menuPath.replace(/\/\d+/, '') === routePath.replace(/\/\d+/, '');
	}
};
