import CategoryTypes from '../sidebar/CategoryTypes';
import OthersFilter from '../sidebar/OthersFilter';
import Duration from '../sidebar/Duration';
import Languages from '../sidebar/Languages';
import PirceSlider from '../sidebar/PirceSlider';
import MainFilterSearchBox from './MainFilterSearchBox';
import DaysSlider from '../sidebar/DaysSlider';

const Sidebar = () => {
	return (
		<>
			<div className='sidebar__item -no-border'>
				<div className='px-20 py-20 bg-light-2 rounded-4'>
					<h5 className='text-18 fw-500 mb-10'>Search Tours</h5>

					<div className='row y-gap-20 pt-20'>
						<MainFilterSearchBox />
					</div>
				</div>
			</div>

			<div className='sidebar__item -no-border'>
				<h5 className='text-18 fw-500 mb-10'>Ofertas</h5>
				<div className='sidebar-checkbox'>
					<CategoryTypes
						categories={[
							{ name: 'Rebajas de última hora', count: 92 },
							{ name: 'Pago parcial en el destino', count: 45 },
						]}
					/>
				</div>
				{/* End Sidebar-checkbox */}
			</div>

			<div className='sidebar__item'>
				<h5 className='text-18 fw-500 mb-10'>Tipo de viaje</h5>
				<div className='sidebar-checkbox'>
					<CategoryTypes
						categories={[
							{ name: 'Viajes Individuales', count: 92 },
							{ name: 'Viajes Grupales', count: 45 },
							{ name: 'Tours Diarios', count: 21 },
						]}
					/>
				</div>
				{/* End Sidebar-checkbox */}
			</div>
			{/* End popular filter */}

			<div className='sidebar__item'>
				<div className='sidebar-checkbox'>
					<OthersFilter />
				</div>
				{/* End Sidebar-checkbox */}
			</div>
			{/* End Aminities filter */}

			<div className='sidebar__item'>
				<h5 className='text-18 fw-500 mb-10'>Duración del viaje</h5>
				<div className='sidebar-checkbox'>
					<DaysSlider />
				</div>
			</div>
			{/* End style filter */}

			<div className='sidebar__item pb-30'>
				<h5 className='text-18 fw-500 mb-10'>Precio</h5>
				<div className='row x-gap-10 y-gap-30'>
					<div className='col-12'>
						<PirceSlider />
					</div>
				</div>
			</div>
			{/* End Nightly priceslider */}
		</>
	);
};

export default Sidebar;
