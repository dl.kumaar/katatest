import { createClient } from 'next-sanity';
import imageUrlBuilder from '@sanity/image-url';

import keys from '../keys.json';

//...

export const apIClient = createClient({
	projectId: keys.sanity_project_id,
	dataset: keys.sanity_dataset,

	useCdn: false, // set to `true` to fetch from edge cache
	apiVersion: '2023-07-10', // use current date (YYYY-MM-DD) to target the latest API version
	token: keys.sanity_token, // or leave blank for unauthenticated usage
});

export const builder = imageUrlBuilder(apIClient);

export function urlFor(source) {
	return builder.image(source);
}

{
	/* <img src={urlFor(img).width(400).url()} alt='' /> */
}
