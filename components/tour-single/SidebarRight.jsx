import FilterBox from '../../components/tour-single/filter-box';
import HubSpotForm from 'react-hubspot-form';

const SidebarRight = ({ tour }) => {
	return (
		<div className='d-flex justify-start js-pin-content'>
			<div className='w-360 lg:w-full d-flex flex-column items-center'>
				<div className='px-30 py-30 rounded-4 border-light bg-white shadow-4'>
					{tour?.tourPrice ? (
						<div className='text-14 text-light-1'>
							From{' '}
							<span className='text-20 fw-500 text-dark-1 ml-5'>
								US${tour?.tourPrice}
							</span>
						</div>
					) : null}

					{/* End div */}

					<div className='row y-gap-20 pt-30'>
						<FilterBox tripLength={tour?.tourDuration} />
						{/* <HubSpotForm
							portalId='20635927'
							region='na1'
							formId='0041463b-49d6-4466-946f-670663a264d2'
							onSubmit={() => console.log('Submit!')}
							onReady={(form) => console.log('Form ready!')}
							loading={<div>Loading...</div>}
						/> */}

						{/* <div
							className='bg-white'
							style={{
								width: '100%',
								marginTop: '-100px',
							}}
						/>} */}
					</div>
					{/* End div */}

					<div className='d-flex items-center pt-20'>
						<div className='size-40 flex-center bg-light-2 rounded-full'>
							<i className='icon-heart text-16 text-green-2' />
						</div>
						<div className='text-14 lh-16 ml-10'>
							El 94% de los viajeros recomienda esta experiencia
						</div>
					</div>
				</div>
				{/* End px-30 */}

				<div className='px-30'>
					<div className='text-14 text-light-1 mt-30'>
						¿No estás seguro? Habla con nuestros expertos en viajes para obtener
						un programa personalizado.
					</div>
				</div>
				{/* End div */}
			</div>
		</div>
	);
};

export default SidebarRight;
{
	/* <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/embed/v2.js"></script>
<script>
  hbspt.forms.create({
    region: "na1",
    portalId: "20635927",
    formId: "0041463b-49d6-4466-946f-670663a264d2"
  });
</script> */
}
