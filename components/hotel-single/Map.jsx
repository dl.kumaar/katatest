import Image from 'next/image';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

const Map = ({ map }) => {
	const MapSwal = withReactContent(Swal);

	const router = useRouter();
	const slug = router.query.slug || window.location.pathname.split('/')[2];

	return (
		<div
			className='flex-center'
			style={{
				backgroundImage: map
					? `url(${map})`
					: `url(/img/mapa-india-viajes-fallback.png)`,
				height: '180px',
				backgroundSize: 'cover',
				backgroundPosition: 'center',
			}}>
			<button
				className='button py-15 px-24 -blue-1 bg-white text-dark-1 absolute'
				onClick={() =>
					MapSwal.fire({
						showCloseButton: true,
						showCancelButton: false,
						showConfirmButton: false,
						width: '40vw',
						html: (
							<Image
								alt={`${slug}-mapa`}
								src={map || '/img/mapa-india-viajes-fallback.png'}
								className='rounded-10'
								width={'1920'}
								height={'1080'}
							/>
						),
					})
				}>
				<i className='icon-destination text-22 mr-10' />
				Tour Map
			</button>
		</div>
	);
};

export default Map;
