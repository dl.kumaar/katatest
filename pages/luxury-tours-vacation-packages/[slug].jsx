import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper';
import 'photoswipe/dist/photoswipe.css';
import { Gallery, Item } from 'react-photoswipe-gallery';
import Header2 from '../../components/header/header-2';
import Overview from '../../components/tour-single/Overview';
import TourSnapShot from '../../components/tour-single/TourSnapShot';
import HubspotForm from 'react-hubspot-form';

import Footer2 from '../../components/footer/footer-3';

import Logo from '../../assets/kata-logo.png';

import Tours from '../../components/tours/Tours';
import Link from 'next/link';
import Itinerary from '../../components/tour-single/itinerary';
import Image from 'next/image';
import StickyHeader2 from '../../components/hotel-single/StickyHeader2';
import { apIClient, urlFor } from '../../utils/sanityCalient';
import { nanoid } from 'nanoid';
import SidebarRight2 from '../../components/hotel-single/SidebarRight2';
import RatingBox from '../../components/hotel-single/RatingBox';
import { NextSeo } from 'next-seo';
import DefaultMeta from '../../components/common/DefaultMeta';
import Reviews from './Reviews';
import SlideGallery from '../../components/activity-single/SlideGallery';
import TopBreadCrumb from '../../components/activity-single/TopBreadCrumb';

// get a number between
function randomIntFromInterval(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

const TourSingleV1Dynamic = (props) => {
	const router = useRouter();
	const [tour, _] = useState(props.tour || {});
	const id = router.query.id;

	console.log(tour);

	const [formSeed, setFormSeed] = useState(1);

	const reset = () => {
		setFormSeed(Math.random());
	};

	const checkOffSeasonPrice = tour?.priceOffSeason.every(
		(e) => e?.tourPrice !== 0
	);

	const checkSeasonPrice = tour?.priceSeason.every((e) => e?.tourPrice !== 0);

	return (
		<>
			<DefaultMeta />

			<NextSeo
				title={
					tour?.tourName + ' - Kata Travels' ||
					'Luxury Tour Package - Kata Travels'
				}
				description={`Customized tour pacakge: ${
					tour?.tourExcerpt[0]?.children[0]?.text
				}. ${tour?.tourOverview?.join(' . ')} `}
				canonical={`https://katatravels.com/luxury-tours-vacation-packages/${tour?.slug.current}`}
				openGraph={{
					url: `https://katatravels.com/luxury-tours-vacation-packages/${tour?.slug.current}`,
					title: tour?.tourName || 'Kata Travels',
					description: `Customized tour pacakge: ${tour?.tourExcerpt[0]?.children[0]?.text}. `,
					images: [
						{
							url: urlFor(tour?.tourImage).width().height().url(),
							width: 800,
							height: 600,
							alt: 'Viaje' + tour?.tourName || 'Kata Travels',
						},
					],
					siteName: 'Kata Travels',
					type: 'website',
					locale: 'en',
				}}
			/>

			{/* <Schema /> */}

			<div className='header-margin'></div>
			{/* header top margin */}

			<Header2 darkNav={true} />
			{/* End Header 1 */}
			<StickyHeader2 price={props?.tourPrice} />
			{/* 
			<section className='pt-40'>
				<div className='container'>
					<div className='row y-gap-20 justify-between items-end'>
						<div className='col-auto'>
							<div className='row x-gap-20  items-center'>
								<div className='col-auto'>
									<div className='row x-gap-20 y-gap-20 items-center pt-10'>
										<div className='col-auto'>
											<div className='d-flex items-center'>
												<h1 className='text-30 fw-600'>{tour?.tourName}</h1>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div className='row x-gap-20 y-gap-20 items-center'>
								<div className='col-auto'>
									<div className='row x-gap-20 y-gap-20 items-center pt-10'>
										<div className='col-auto'>
											<div className='d-flex items-center'>
												<div className='d-flex x-gap-5 items-center'>
													<i className='icon-star text-10 text-yellow-1'></i>

													<i className='icon-star text-10 text-yellow-1'></i>

													<i className='icon-star text-10 text-yellow-1'></i>

													<i className='icon-star text-10 text-yellow-1'></i>

													<i className='icon-star text-10 text-yellow-1'></i>
												</div>

												<div className='text-14 text-light-1 ml-10'>
													{tour?.numberOfReviews ||
														randomIntFromInterval(35, 430)}{' '}
													reviews
												</div>
											</div>
										</div>

										<div className='col-auto'>
											<div className='row x-gap-10 items-center'>
												<div className='col-auto'>
													<div className='d-flex x-gap-5 items-center'>
														<i className='icon-placeholder text-16 text-light-1'></i>
														<div className='text-15 text-light-1'>
															{tour?.tourExcerpt[0]?.children[0]?.text}
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className='col-auto'>
							<div className='row x-gap-15 y-gap-15 items-center'>
								{tour?.tourPrice ? (
									<div className='col-auto'>
										<div className='text-14'>
											From{' '}
											<span className='text-22 text-dark-1 fw-500'>
												US${tour?.tourPrice}
											</span>
										</div>
									</div>
								) : null}

								<div className='col-auto'>
									<Link
										href='#tour-booking-form'
										className='button h-50 px-24 -dark-1 bg-blue-1 text-white'>
										Make an inquiry
									</Link>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> */}
			{/* <TopBreadCrumb /> */}

			<section className='pt-10'>
				<div className='container'>
					<SlideGallery />
				</div>
			</section>
			{/* End gallery grid wrapper */}
			{/* 
			<section className='pt-40 js-pin-container'>
				<div className='container'>
					<div className='row y-gap-30'>
						<div className='col-xl-8'>
							<div className='row y-gap-20 justify-between items-end'>
								<div className='col-auto'>
									<h1 className='text-26 fw-600'>{tour?.tourName}</h1>
									<div className='row x-gap-10 y-gap-20 items-center pt-10'>
										<div className='col-auto'>
											<div className='d-flex items-center'>
												<i className='icon-star text-10 text-yellow-1'></i>

												<div className='text-14 text-light-1 ml-10'>
													{tour?.numberOfReviews ||
														randomIntFromInterval(35, 430)}{' '}
													reviews
												</div>
											</div>
										</div>

										<div className='col-auto'>
											<div className='row x-gap-10 items-center'>
												<div className='col-auto'>
													<div className='d-flex x-gap-5 items-center'>
														<i className='icon-location-2 text-16 text-light-1'></i>
														<div className='text-15 text-light-1'>
															{tour?.tourExcerpt[0]?.children[0]?.text}
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div className='col-auto'>
									<div className='row x-gap-10 y-gap-10'>
										<div className='col-auto'>
											<button className='button px-15 py-10 -blue-1 bg-light-2'>
												<i className='icon-share mr-10'></i>
												Download Itinerary
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> */}

			<section className='js-pin-container'>
				<div className='container'>
					<div className='row y-gap-30'>
						<div className='col-xl-8'>
							{/* <div className='relative d-flex justify-center overflow-hidden js-section-slider'>
								<Swiper
									modules={[Navigation]}
									loop={true}
									navigation={{
										nextEl: '.js-img-next',
										prevEl: '.js-img-prev',
									}}>
									{tour?.tourImageGallery ? (
										tour?.tourImageGallery?.map((img) => (
											<SwiperSlide key={nanoid(5)}>
												<Image
													width={850}
													height={450}
													quality={80}
													priority
													src={urlFor(img).width(850).height(450).url()}
													alt='image'
													style={{ height: '501px' }}
													className='rounded-4 col-12 cover object-cover'
												/>
											</SwiperSlide>
										))
									) : (
										<SwiperSlide>
											<Image
												width={850}
												height={450}
												quality={80}
												priority
												src={urlFor(tour?.tourImage)
													.width(451)
													.height(450)
													.url()}
												alt='image'
												style={{ height: '501px' }}
												className='rounded-4 col-12 cover object-cover'
											/>
										</SwiperSlide>
									)}
								</Swiper>

								{tour?.tourImageGallery && (
									<Gallery>
										{tour?.tourImageGallery?.map((img) => (
											<div
												className='absolute px-10 py-10 col-12 h-full d-flex justify-end items-end z-2 bottom-0 end-0'
												key={nanoid(5)}>
												<Item
													original={urlFor(img).width(780).height(450).url()}
													thumbnail={urlFor(img).width(300).height(200).url()}
													width={780}
													height={450}>
													{({ ref, open }) => (
														<div
															className='button -blue-1 px-24 py-15 bg-white text-dark-1 js-gallery'
															ref={ref}
															onClick={open}
															role='button'>
															See all photos
														</div>
													)}
												</Item>
											</div>
										))}
									</Gallery>
								)}

								<div className='absolute h-full col-11'>
									<button className='section-slider-nav -prev flex-center button -blue-1 bg-white shadow-1 size-40 rounded-full sm:d-none js-img-prev'>
										<i className='icon icon-chevron-left text-12' />
									</button>
									<button className='section-slider-nav -next flex-center button -blue-1 bg-white shadow-1 size-40 rounded-full sm:d-none js-img-next'>
										<i className='icon icon-chevron-right text-12' />
									</button>
								</div>
							</div> */}

							<div className='row y-gap-20 justify-between items-end pt-40'>
								<div className='col-auto'>
									<h1 className='text-26 fw-600'>{tour?.tourName}</h1>
									<div className='row x-gap-10 y-gap-20 items-center pt-10'>
										<div className='col-auto'>
											<div className='d-flex items-center'>
												<i className='icon-star text-10 text-yellow-1'></i>

												<div className='text-14 text-light-1 ml-10'>
													{tour?.numberOfReviews ||
														randomIntFromInterval(35, 430)}{' '}
													reviews
												</div>
											</div>
										</div>
										{/* End .col */}

										<div className='col-auto'>
											<div className='row x-gap-10 items-center'>
												<div className='col-auto'>
													<div className='d-flex x-gap-5 items-center'>
														<i className='icon-location-2 text-16 text-light-1'></i>
														<div className='text-15 text-light-1'>
															{tour?.tourExcerpt[0]?.children[0]?.text}
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								{/* <div className='col-auto'>
									<div className='row x-gap-10 y-gap-10'>
										<div className='col-auto'>
											<button className='button px-15 py-10 -blue-1 bg-light-2'>
												<i className='icon-share mr-10'></i>
												Download Itinerary
											</button>
										</div>
									</div>
								</div> */}
							</div>

							<TourSnapShot
								duration={tour?.tourDuration}
								tourType={tour?.tourType === 'private' ? 'Private' : 'Group'}
							/>
							{/* End toursnapshot */}
							<div className='border-top-light mt-40 mb-40'></div>

							<Overview overview={tour?.tourOverview} />
							{/* End  Overview */}

							<div className='border-top-light mt-40 mb-40'></div>
							<h3 className='text-22 fw-500 mb-20' id='tour-itinerary'>
								Itinerary
							</h3>
							<Itinerary itinerary={tour?.tourItinerary} />
							{/* End Itinerary */}

							<div className='mt-40 border-top-light' id='inclusions'>
								<div className='row x-gap-40 y-gap-40 pt-40'>
									<div className='col-12'>
										<h3 className='text-22 fw-500'>Tour Includes</h3>

										<div className='row x-gap-40 y-gap-40 pt-20'>
											<div className='col-md-6'>
												{tour?.tourIncludes?.map((item) => (
													<div className='text-dark-1 text-15' key={nanoid(5)}>
														<i className='icon-check text-10 mr-10'></i>{' '}
														{item?.children[0]?.text}
													</div>
												))}
											</div>

											<div className='col-md-6'>
												{tour?.tourExcludes?.map((item) => (
													<div className='text-dark-1 text-15' key={nanoid(4)}>
														<i className='icon-close text-green-2 text-10 mr-10'></i>{' '}
														{item?.children[0]?.text}
													</div>
												))}
											</div>

											<div className='col-12'>
												<div className='py-15 px-20 rounded-4 text-15 bg-blue-1-05'>
													These are standard inclusions for all our tours.
													However, some inclusions may vary in case of custom
													tours and special offers. If you have any special
													requests, please{' '}
													<Link
														href='#tour-booking-form'
														className='text-blue-1 fw-500'>
														Contact Us
													</Link>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							{checkOffSeasonPrice || checkSeasonPrice ? (
								<div className='mt-40 border-top-light' id='tour-price'>
									<div className='row x-gap-40 y-gap-40 pt-40'>
										<div className='col-12'>
											<h3 className='text-22 fw-500'>Tour Price</h3>

											<div className='row x-gap-40 y-gap-40 pt-20'>
												<div className='container tour-price-container'>
													{checkOffSeasonPrice ? (
														<>
															<strong>
																Prices valid for travel between April 1st and
																September 30th, 2023:
															</strong>
															<table className='table table-striped table-bordered'>
																<thead className='thead-dark'>
																	<tr>
																		<th>Hotel Type</th>
																		{tour.priceOffSeason.map((e) => (
																			<th key={nanoid(5)}>
																				{e?.hotelCategory}-Star
																			</th>
																		))}
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>Price</td>
																		{tour.priceOffSeason.map((e) => (
																			<td key={nanoid(5)}>${e?.tourPrice}</td>
																		))}
																	</tr>
																</tbody>
															</table>
														</>
													) : null}

													{checkSeasonPrice ? (
														<>
															<strong>
																Prices valid for travel between October 1st and
																March 31st, 2024:
															</strong>
															<table className='table table-striped table-bordered'>
																<thead className='thead-dark'>
																	<tr>
																		<th>Hotel Type</th>
																		{tour.priceSeason.map((e) => (
																			<th key={nanoid(5)}>
																				{e?.hotelCategory}-Star
																			</th>
																		))}
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>Price</td>
																		{tour.priceSeason.map((e) => (
																			<td key={nanoid(5)}>${e?.tourPrice}</td>
																		))}
																	</tr>
																</tbody>
															</table>
														</>
													) : null}

													<div>
														<p>
															*Prices are in US Dollars per person and based on
															2 travelers sharing a double/twin room.
														</p>
														<p>
															*Single supplement is available for travelers who
															would like their own room.
														</p>
														<p>
															*The price may vary depending on the season, the
															number of travelers, and the hotel category
															chosen.
														</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							) : null}
						</div>
						{/* End .col-xl-8 */}

						<div className='col-xl-4'>
							{/* <SidebarRight2
								tourMap={
									tour?.tourMap
										? urlFor(tour?.tourMap).width(1920).height(1080).url()
										: null
								}
							/> */}
							<RatingBox />
						</div>
					</div>
				</div>
			</section>

			{tour?.relatedTours?.length > 0 ? (
				<section className='layout-pt-md layout-pb-lg mt-50 border-top-light'>
					<div className='container'>
						<div className='row y-gap-20 justify-between items-end'>
							<div className='col-auto'>
								<div className='sectionTitle -md'>
									<h2 className='sectionTitle__title'>Simliar tour packages</h2>
									<p className=' sectionTitle__text mt-5 sm:mt-0'>
										These are some of our most popular tour packages. We can
										also customize any tour package to suit your needs.
									</p>
								</div>
							</div>

							<div className='col-auto'>
								<Link
									href='/tours'
									className='button -md -blue-1 bg-blue-1-05 text-blue-1'>
									See all tours
									<div className='icon-arrow-top-right ml-15' />
								</Link>
							</div>
						</div>

						<div className='row y-gap-30 pt-40 sm:pt-20 item_gap-x30'>
							<Tours tours={tour?.relatedTours} />
						</div>
					</div>
				</section>
			) : (
				<div className='layout-pb-lg' />
			)}

			<section
				id='tour-booking-form'
				className='bg-light layout-pt-lg layout-pb-md'>
				<div className='container'>
					<div className='row y-gap-30'>
						<div className='col-xl-8 col-lg-9 bg-white' id='custom-tour'>
							<Image
								src={Logo}
								alt={tour?.tourName + 'Booking Form Logo'}
								width={150}
								height={150}
								className='mb-10'
							/>

							<h2 className='text-22 fw-500 md:mt-24'>
								Customize your trip with us
							</h2>
							<div className='text-16 mt-10'>
								We will get back to you within 24 hours.
							</div>
							<div className='py-15 px-20 rounded-4 text-15 bg-blue-1-05'>
								We are flexible. *Postpone your trip{' '}
								<span className='text-blue-1 fw-500'>without any cost</span>{' '}
								until 15 days before departure.
							</div>

							<div className='row x-gap-20 y-gap-20 pt-20'>
								<HubspotForm
									key={formSeed}
									portalId='20635927'
									region='na1'
									formId='1ece552b-1e78-4626-b517-7a2bd35363c3'
									loading={<div>Loading...</div>}
								/>

								<div
									className='py-15 px-20 rounded-4 text-15 bg-white hidden'
									style={{
										marginTop: '-90px',
									}}></div>
							</div>
						</div>
						{/* End Form */}

						<div className='col-xl-4'>
							{/* Sidebar */}
							<div className='col y-gap-40 bg-white justify-center pt-50 border-light rounded-4 px-20'>
								<div className='col-sm-12'>
									<div className='featureIcon -type-1'>
										<div className='d-flex justify-center'>
											<Image
												src='/img/featureIcons/3/1.svg'
												data-src='/img/featureIcons/3/1.svg'
												alt='image'
												className='js-lazy'
												width={70}
												height={70}
											/>
										</div>

										<div className='text-center mt-30'>
											<h4 className='text-18 fw-500'>Travel carefree</h4>
											<p className='text-15 mt-10'>
												Travel carefree thanks to our local experts and our
												flexible booking options.
											</p>
										</div>
									</div>
								</div>

								<div className='col-sm-12'>
									<div className='featureIcon -type-1'>
										<div className='d-flex justify-center'>
											<Image
												src='/img/featureIcons/3/2.svg'
												data-src='/img/featureIcons/3/2.svg'
												alt='image'
												className='js-lazy'
												width={70}
												height={70}
											/>
										</div>

										<div className='text-center mt-30'>
											<h4 className='text-18 fw-500'>
												100% Private Tours &amp; Customizable
											</h4>
											<p className='text-15 mt-10'>
												Your tour will be organized according to your
												requirements. You will not be put together with other
												travelers unless you have requested to be in a group.
											</p>
										</div>
									</div>
								</div>

								<div className='col-sm-12'>
									<div className='featureIcon -type-1'>
										<div className='d-flex justify-center'>
											<Image
												src='/img/featureIcons/3/4.svg'
												data-src='/img/featureIcons/3/4.svg'
												alt='image'
												className='js-lazy'
												width={70}
												height={70}
											/>
										</div>

										<div className='text-center mt-30'>
											<h4 className='text-18 fw-500'>24/7 Customer Support</h4>

											<p className='text-15 mt-10'>
												Our team of local travel experts is available 24 hours a
												day, 7 days a week, to help you with any questions or
												concerns you may have.
											</p>
										</div>
									</div>
								</div>

								<div className='col-sm-12 text-center text-18 '>
									<span>Email:</span>{' '}
									<span className='underline'>hello@katatravels.com</span>
									<br />
									<span>Whatsapp:</span>{' '}
									<span className='underline fw-500'>+91 986 878 859 1</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			{/* <Reviews /> */}

			<Footer2 />
		</>
	);
};

export default dynamic(() => Promise.resolve(TourSingleV1Dynamic), {
	ssr: false,
});

export async function getStaticPaths() {
	const query = `*[_type == 'tours'] {
    slug,
}`;

	const paths = await apIClient.fetch(query).then((res) => {
		return res.map((tour) => {
			return {
				params: {
					slug: tour.slug.current,
				},
			};
		});
	});

	return {
		paths,
		fallback: 'blocking',
	};
}

export async function getStaticProps({ params }) {
	const query = `*[_type == 'tours' && slug.current == '${params.slug}']{
		...,
		"relatedTours": relatedTours[]->{
			tourName,
			tourExcerpt,
			tourPrice,
			tourType,
			tourDuration,
			slug,
			priceOffSeason,
			priceSeason,
			tourImage,
			tourImageGallery,
			tourPromoText1
		},
	}`;

	let tour = await apIClient.fetch(query).then((res) => res[0]);

	if (!tour) {
		return {
			notFound: true,
		};
	}

	tour = {
		...tour,
		priceOffSeason: [
			{
				hotelCategory: '3',
				tourPrice: 0,
			},
		],
		priceSeason: tour?.priceSeason || [{ tourPrice: 0 }],
	};
	const tourPrice = tour?.priceOffSeason
		? tour?.priceOffSeason[0]?.tourPrice
		: tour?.priceSeason[0]?.tourPrice || 0;

	return {
		props: {
			tour,
			tourPrice,
		},
		revalidate: 1,
	};
}
