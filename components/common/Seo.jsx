import Head from 'next/head';
import { useRouter } from 'next/router';

const Seo = ({ pageTitle, facebookMetaTags, twitterMetaTags }) => {
	const router = useRouter();

	// Get the current page URL
	const homePath = router?.pathname === '/' ? true : false;

	return (
		<>
			<Head>
				<title>{pageTitle && `${pageTitle}`}</title>
				<meta name='viewport' content='width=device-width, initial-scale=1' />

				{
					// If the current page is home page then add the following meta tags
					homePath ? (
						<>
							<meta name='author' content='Kata Travels' />
							<meta name='publisher' content='https://katatravels.com' />

							<link
								rel='image_src'
								href='https://katatravels.com/img/fb-hero.jpg'
							/>

							<meta property='og:locale' content='en' />
							<meta property='og:type' content='website' />
							<meta
								property='og:title'
								content='Viajes a la India - Paquetes de Viajes Privados y Grupales'
							/>
							<meta
								property='og:description'
								content='Viajes a la India - Paquetes de viaje privados y viajes grupales a la India. Agencia de viajes en la India con oficias en estados unidos y Argentina.'
							/>
							<meta property='og:url' content='https://katatravels.com/' />
							<meta property='og:site_name' content='Kata Travels' />
							<meta
								property='og:image'
								content='https://katatravels.com/img/fb-hero.jpg'
							/>
							<meta property='og:image:secure_url' content='/img/fb-hero.jpg' />
							<meta property='og:image:width' content='1740' />
							<meta property='og:image:height' content='960' />
							<meta
								property='og:image:alt'
								content='Kata Travels - Viajes a la India'
							/>
							<meta property='og:image:type' content='image/jpeg' />

							<meta name='robots' content='INDEX, FOLLOW' />
						</>
					) : null
				}

				{/* Facebook Meta Tags */}
				{facebookMetaTags &&
					facebookMetaTags.map((item, _) => {
						return (
							<meta
								key={item?.property}
								property={item?.property}
								content={item?.content}
							/>
						);
					})}

				{/* Twitter Meta Tags */}
				{twitterMetaTags &&
					twitterMetaTags.map((item, _) => {
						return (
							<meta
								key={item?.name}
								name={item?.name}
								content={item?.content}
							/>
						);
					})}
			</Head>
		</>
	);
};

export default Seo;
